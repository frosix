/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file util/frosix-util.c
 * @brief helper functions for various tasks
 * @author Joel Urech
 */

#include "frosix_util_lib.h"
#include "frost_low.h"
#include "frost_high.h"






void
FROSIX_debug_print_point (const struct FROST_Point *pt)
{
  for (int i = 0; i < sizeof (pt); i++)
    fprintf (stderr, "%02X ", pt->xcoord[i]);
  fprintf (stderr, "\n");
}

void
FROSIX_debug_print_scalar (const struct FROST_Scalar *scal)
{
  for (int i = 0; i < sizeof (scal); i++)
    fprintf (stderr, "%02X ", scal->scalarbytes[i]);
  fprintf (stderr, "\n");
}

void
FROSIX_debug_print_crockford32 (const void *data,
                                size_t length,
                                const char *title)
{
  char *crock;
  crock = GNUNET_STRINGS_data_to_string_alloc (data,
                                               length);
  fprintf (stderr,
           "%s: %s\n",
           title,
           crock);
  free (crock);
}
