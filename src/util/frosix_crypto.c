/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file util/frosix_crypto.c
 * @brief helper functions for various tasks
 * @author Joel Urech
 */

#include "frosix_crypto.h"
#include "frost_low.h"
#include "frost_high.h"

enum GNUNET_GenericReturnValue
FROSIX_secretbox_keydata (struct FROSIX_KeyDataEncrypted *ciphertext,
                          const struct FROSIX_KeyDataRaw *plaintext,
                          const struct FROSIX_EncryptionNonceP *nonce,
                          const struct FROSIX_EncryptionKey *key)
{
  if (0 == crypto_secretbox_easy (ciphertext->bytes,
                                  plaintext->bytes,
                                  sizeof (plaintext->bytes),
                                  nonce->bytes,
                                  key->bytes))
    return GNUNET_OK;
  return GNUNET_NO;
}


enum GNUNET_GenericReturnValue
FROSIX_secretbox_keydata_open (struct FROSIX_KeyDataRaw *plaintext,
                               const struct FROSIX_KeyDataEncrypted *ciphertext,
                               const struct FROSIX_EncryptionNonceP *nonce,
                               const struct FROSIX_EncryptionKey *key)
{
  if (0 == crypto_secretbox_open_easy (plaintext->bytes,
                                       ciphertext->bytes,
                                       sizeof (ciphertext->bytes),
                                       nonce->bytes,
                                       key->bytes))
    return GNUNET_OK;
  return GNUNET_NO;
}


void
FROSIX_secretbox_nonce_randombytes (struct FROSIX_EncryptionNonceP *nonce)
{
  randombytes_buf (nonce,
                   sizeof (nonce->bytes));
}


void
FROSIX_hash_encryption_key (struct FROST_HashCode *enc_key_hash,
                            const struct FROSIX_EncryptionKey *enc_key)
{
  struct FROST_HashState enc_key_state;
  FROST_hash_init (&enc_key_state);
  FROST_hash_fixed_update (&enc_key_state,
                           enc_key,
                           sizeof (*enc_key));
  FROST_hash_final (&enc_key_state,
                    enc_key_hash);
}

void
FROSIX_compute_auth_hash (struct FROSIX_ChallengeHashP *auth_hash,
                          const char *auth_data,
                          const struct GNUNET_HashCode *auth_nonce)
{
  struct GNUNET_HashContext *h_ctx = GNUNET_CRYPTO_hash_context_start ();
  GNUNET_CRYPTO_hash_context_read (h_ctx,
                                   auth_data,
                                   strlen (auth_data));
  GNUNET_CRYPTO_hash_context_read (h_ctx,
                                   auth_nonce,
                                   sizeof (*auth_nonce));
  GNUNET_CRYPTO_hash_context_read (h_ctx,
                                   "FROSIX",
                                   strlen ("FROSIX"));
  GNUNET_CRYPTO_hash_context_finish (h_ctx,
                                     &auth_hash->hash);
}


void
FROSIX_compute_challenge_request_id (
  struct FROSIX_ChallengeRequestIdP *request_id,
  const struct FROSIX_EncryptionKey *enc_key,
  const struct FROST_MessageHash *message_hash)
{
  /* id = H (encryption_key ||
           message_hash ||
           "FROSIX-SIG") */
  struct FROST_HashState hs;

  FROST_hash_init (&hs);
  FROST_hash_fixed_update (&hs,
                           enc_key,
                           sizeof (*enc_key));
  FROST_hash_fixed_update (&hs,
                           message_hash,
                           sizeof (*message_hash));
  FROST_hash_fixed_update (&hs,
                           "FROSIX-AUTH",
                           strlen ("FROSIX-AUTH"));
  FROST_hash_final (&hs,
                    &request_id->id);
}


void
FROSIX_compute_signature_request_id (
  struct FROSIX_SigRequestIdP *request_id,
  const struct FROST_HashCode *enc_key_hash,
  const struct FROST_MessageHash *message_hash)
{
  /* id = H (H(encryption_key) ||
           message_hash ||
           "FROSIX-SIG") */
  struct FROST_HashState hs;

  FROST_hash_init (&hs);
  FROST_hash_fixed_update (&hs,
                           enc_key_hash,
                           sizeof (*enc_key_hash));
  FROST_hash_fixed_update (&hs,
                           message_hash,
                           sizeof (*message_hash));
  FROST_hash_fixed_update (&hs,
                           "FROSIX-SIG",
                           strlen ("FROSIX-SIG"));
  FROST_hash_final (&hs,
                    &request_id->id);
}

void
FROSIX_compute_dkg_request_id (
  struct FROSIX_DkgRequestIdP *request_id,
  const struct FROSIX_DkgContextStringP *context_string,
  const struct FROSIX_ChallengeHashP *challenge_hash,
  const struct FROSIX_ProviderSaltP *provider_salt,
  uint8_t provider_index,
  uint8_t num_of_participants,
  uint8_t threshold)
{
  struct FROST_HashState check_id_state;

  FROST_hash_init (&check_id_state);
  FROST_hash_hash_update (&check_id_state,
                          &context_string->hash);
  FROST_hash_fixed_update (&check_id_state,
                           &challenge_hash->hash,
                           sizeof (*challenge_hash));
  FROST_hash_uint8_update (&check_id_state,
                           provider_index);
  FROST_hash_uint8_update (&check_id_state,
                           num_of_participants);
  FROST_hash_uint8_update (&check_id_state,
                           threshold);
  FROST_hash_fixed_update (&check_id_state,
                           provider_salt,
                           sizeof (*provider_salt));
  FROST_hash_fixed_update (&check_id_state,
                           "FROSIX-DKG-ID",
                           strlen ("FROSIX-DKG-ID"));
  FROST_hash_final (&check_id_state,
                    &request_id->id);
}


enum GNUNET_GenericReturnValue
FROSIX_gnunet_hash_cmp (const struct GNUNET_HashCode *hc1,
                        const struct GNUNET_HashCode *hc2)
{
  if (0 == GNUNET_CRYPTO_hash_cmp (hc1,
                                   hc2))
    return GNUNET_OK;
  return GNUNET_NO;
}

void
FROSIX_compute_challenge_id (struct FROSIX_ChallengeIdP *challenge_id,
                             const struct FROST_HashCode *enc_key_hash,
                             const struct FROST_MessageHash *mh)
{
  struct GNUNET_HashContext *hc = GNUNET_CRYPTO_hash_context_start ();
  GNUNET_CRYPTO_hash_context_read (hc,
                                   enc_key_hash,
                                   sizeof (*enc_key_hash));
  GNUNET_CRYPTO_hash_context_read (hc,
                                   mh,
                                   sizeof (*mh));
  GNUNET_CRYPTO_hash_context_read (hc,
                                   "FROSIX",
                                   strlen ("FROSIX"));
  GNUNET_CRYPTO_hash_context_finish (hc,
                                     &challenge_id->hash);
}

void
FROSIX_hash_answer (uint64_t code,
                    struct GNUNET_HashCode *hashed_code)
{
  char cbuf[40];

  GNUNET_snprintf (cbuf,
                   sizeof (cbuf),
                   "%llu",
                   (unsigned long long) code);
  GNUNET_CRYPTO_hash (cbuf,
                      strlen (cbuf),
                      hashed_code);
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Hashed answer %llu to %s\n",
              (unsigned long long) code,
              GNUNET_h2s (hashed_code));
}


void
FROSIX_nonce_to_pow_salt (struct FROST_PowSalt *salt,
                          const struct GNUNET_HashCode *nonce)
{
  GNUNET_CRYPTO_kdf (salt,
                     sizeof (*salt),
                     "FROSIX",
                     strlen ("FROSIX"),
                     nonce,
                     sizeof (*nonce),
                     NULL,
                     0);
}

void
FROSIX_hash_pow (struct GNUNET_HashCode *hash,
                 struct GNUNET_CRYPTO_Edx25519PrivateKey *priv,
                 struct GNUNET_CRYPTO_Edx25519PublicKey *pub,
                 const struct GNUNET_HashCode *nonce,
                 const char *auth_answer,
                 unsigned int amplifier)
{
  /* get salt */
  struct FROST_PowSalt salt;
  FROSIX_nonce_to_pow_salt (&salt,
                            nonce);

  /* make big calculation*/
  struct FROST_HashCode hashed_answer;
  FROST_pow_hash (&hashed_answer,
                  auth_answer,
                  strlen (auth_answer),
                  &salt,
                  amplifier);

  /* with the seed, get the private key */
  GNUNET_CRYPTO_edx25519_key_create_from_seed (&hashed_answer,
                                               sizeof (hashed_answer),
                                               priv);

  /* and the public key */
  GNUNET_CRYPTO_edx25519_key_get_public (priv,
                                         pub);

  /* hash public key */
  struct GNUNET_HashContext *hc = GNUNET_CRYPTO_hash_context_start ();
  GNUNET_CRYPTO_hash_context_read (hc,
                                   pub,
                                   sizeof (*pub));
  GNUNET_CRYPTO_hash_context_read (hc,
                                   "FROSIX",
                                   strlen ("FROSIX"));
  GNUNET_CRYPTO_hash_context_finish (hc,
                                     hash);
}