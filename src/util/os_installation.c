/*
     This file is part of GNU Frosix.
     Copyright (C) 2019, 2021 Anastasis SARL

     Frosix is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     Frosix is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Frosix; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file os_installation.c
 * @brief initialize libgnunet OS subsystem for Frosix.
 * @author Christian Grothoff
 */
#include "platform.h"
#include <gnunet/gnunet_util_lib.h>
#include "frost_high.h"


/**
 * Default project data used for installation path detection
 * for Frosix.
 */
static const struct GNUNET_OS_ProjectData frosix_pd = {
  .libname = "libfrosixutil",
  .project_dirname = "frosix",
  .binary_name = "frosix-httpd",
  .env_varname = "FROSIX_PREFIX",
  .base_config_varname = "FROSIX_BASE_CONFIG",
  .bug_email = "",
  .homepage = "",
  .config_file = "frosix.conf",
  .user_config_file = "~/.config/frosix.conf",
  .version = "0.0.0",
  .is_gnu = 0,
  .gettext_domain = "frosix",
  .gettext_path = NULL,
};


/**
 * Return default project data used by Frosix.
 */
const struct GNUNET_OS_ProjectData *
FROSIX_project_data_default (void)
{
  return &frosix_pd;
}


/**
 * Initialize libfrosixutil.
 */
void __attribute__ ((constructor))
FROSIX_OS_init ()
{
  GNUNET_OS_init (&frosix_pd);
  GNUNET_assert (-1 != FROST_init ());
}


/* end of os_installation.c */
