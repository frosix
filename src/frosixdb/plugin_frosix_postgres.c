/*
  This file is part of Frosix
  Copyright (C) 2020, 2021, 2022 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file frosixdb/plugin_frosix_postgres.c
 * @brief database helper functions for postgres used by Frosix
 * @author Christian Grothoff
 * @author Marcello Stanisci
 */
#include "platform.h"
#include "frosix_database_plugin.h"
#include "frosix_database_lib.h"
#include "keygen.h"
#include "frosix-httpd_dkg.h"
#include <taler/taler_pq_lib.h>
#include <gnunet/gnunet_pq_lib.h>

/**
 * How long do we keep transient accounts open (those that have
 * not been paid at all, but are awaiting payment). This puts
 * a cap on how long users have to make a payment after a payment
 * request was generated.
 */
#define TRANSIENT_LIFETIME GNUNET_TIME_UNIT_WEEKS

/**
 * How often do we re-try if we run into a DB serialization error?
 */
#define MAX_RETRIES 3


/**
 * Type of the "cls" argument given to each of the functions in
 * our API.
 */
struct PostgresClosure
{

  /**
   * Postgres connection handle.
   */
  struct GNUNET_PQ_Context *conn;

  /**
   * Underlying configuration.
   */
  const struct GNUNET_CONFIGURATION_Handle *cfg;

  /**
   * Name of the currently active transaction, NULL if none is active.
   */
  const char *transaction_name;

  /**
   * Currency we accept payments in.
   */
  char *currency;

  /**
   * Prepared statements have been initialized.
   */
  bool init;
};


/**
 * Drop anastasis tables
 *
 * @param cls closure our `struct Plugin`
 * @return #GNUNET_OK upon success; #GNUNET_SYSERR upon failure
 */
static enum GNUNET_GenericReturnValue
postgres_drop_tables (void *cls)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_Context *conn;
  enum GNUNET_GenericReturnValue ret;

  conn = GNUNET_PQ_connect_with_cfg (pg->cfg,
                                     "frosixdb-postgres",
                                     NULL,
                                     NULL,
                                     NULL);
  if (NULL == conn)
    return GNUNET_SYSERR;
  ret = GNUNET_PQ_exec_sql (conn,
                            "drop");
  GNUNET_PQ_disconnect (conn);
  return ret;
}


/**
 * Initialize tables.
 *
 * @param cls the `struct PostgresClosure` with the plugin-specific state
 * @return #GNUNET_OK upon success; #GNUNET_SYSERR upon failure
 */
static enum GNUNET_GenericReturnValue
postgres_create_tables (void *cls)
{
  struct PostgresClosure *pc = cls;
  struct GNUNET_PQ_Context *conn;
  struct GNUNET_PQ_ExecuteStatement es[] = {
    GNUNET_PQ_make_execute ("SET search_path TO frosix;"),
    GNUNET_PQ_EXECUTE_STATEMENT_END
  };

  conn = GNUNET_PQ_connect_with_cfg (pc->cfg,
                                     "frosixdb-postgres",
                                     "frosix-",
                                     es,
                                     NULL);
  if (NULL == conn)
    return GNUNET_SYSERR;
  GNUNET_PQ_disconnect (conn);
  return GNUNET_OK;
}


/**
 * Establish connection to the database.
 *
 * @param cls plugin context
 * @return #GNUNET_OK upon success; #GNUNET_SYSERR upon failure
 */
static enum GNUNET_GenericReturnValue
prepare_statements (void *cls)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_PreparedStatement ps[] = {
    GNUNET_PQ_make_prepare ("do_commit",
                            "COMMIT"),
    GNUNET_PQ_make_prepare ("dkg_commitment_insert",
                            "INSERT INTO frosix_public_commitments "
                            "(dkg_id"
                            ",time_stamp"
                            ",commitments"
                            ") VALUES "
                            "($1, $2, $3);"),
    GNUNET_PQ_make_prepare ("dkg_commitments_select",
                            "SELECT"
                            " commitments"
                            " FROM frosix_public_commitments"
                            " WHERE dkg_id=$1;"),
    GNUNET_PQ_make_prepare ("dkg_commitments_lookup",
                            "SELECT"
                            " dkg_id"
                            " FROM frosix_public_commitments"
                            " WHERE dkg_id=$1;"),
    GNUNET_PQ_make_prepare ("dkg_key_insert",
                            "INSERT INTO frosix_key "
                            "(key_id"
                            ",identifier"
                            ",expiration"
                            ",encryption_nonce"
                            ",enc_key_data"
                            ",challenge_data"
                            ") VALUES "
                            "($1, $2, $3, $4, $5, $6);"),
    GNUNET_PQ_make_prepare ("key_lookup",
                            "SELECT"
                            " key_id"
                            " FROM frosix_key"
                            " WHERE key_id=$1;"),
    GNUNET_PQ_make_prepare ("key_data_select",
                            "SELECT"
                            " identifier"
                            ",encryption_nonce"
                            ",enc_key_data"
                            " FROM frosix_key"
                            " WHERE key_id=$1;"),
    GNUNET_PQ_make_prepare ("key_delete",
                            "DELETE"
                            " FROM frosix_key"
                            " WHERE key_id=$1;"),
    GNUNET_PQ_make_prepare ("auth_hash_select",
                            "SELECT"
                            " challenge_data"
                            " FROM frosix_key"
                            " WHERE key_id=$1;"),
    GNUNET_PQ_make_prepare ("seed_insert",
                            "INSERT INTO frosix_seed "
                            "(seed_id"
                            ",seed"
                            ",time_stamp"
                            ") VALUES "
                            "($1, $2, $3);"),
    GNUNET_PQ_make_prepare ("seed_select",
                            "SELECT"
                            " seed"
                            " FROM frosix_seed"
                            " WHERE seed_id=$1;"),
    GNUNET_PQ_make_prepare ("seed_delete",
                            "DELETE"
                            " FROM frosix_seed "
                            " WHERE seed_id=$1;"),
    GNUNET_PQ_make_prepare ("challengecode_insert",
                            "INSERT INTO frosix_challengecode "
                            "(challenge_id"
                            ",code"
                            ",creation_date"
                            ",expiration_date"
                            ",retry_counter"
                            ") VALUES "
                            "($1, $2, $3, $4, $5);"),
    GNUNET_PQ_make_prepare ("challengecode_select",
                            "SELECT "
                            " code"
                            ",satisfied"
                            " FROM frosix_challengecode"
                            " WHERE challenge_id=$1"
                            "   AND retry_counter != 0;"),
    GNUNET_PQ_make_prepare ("challengecode_select_meta",
                            "SELECT "
                            " code"
                            ",retry_counter"
                            ",retransmission_date"
                            " FROM frosix_challengecode"
                            " WHERE challenge_id=$1"
                            "   AND expiration_date > $2"
                            "   AND creation_date > $3"
                            " ORDER BY creation_date DESC"
                            " LIMIT 1;"),
    GNUNET_PQ_make_prepare ("challengecode_update_retry",
                            "UPDATE frosix_challengecode"
                            " SET retry_counter=retry_counter - 1"
                            " WHERE challenge_id=$1"
                            "   AND code=$2"
                            "   AND retry_counter != 0;"),
    GNUNET_PQ_make_prepare ("challengecode_mark_sent",
                            "UPDATE frosix_challengecode"
                            " SET retransmission_date=$3"
                            " WHERE challenge_id=$1"
                            "   AND code=$2"
                            "   AND creation_date IN"
                            " (SELECT creation_date"
                            "    FROM frosix_challengecode"
                            "   WHERE challenge_id=$1"
                            "     AND code=$2"
                            "    ORDER BY creation_date DESC"
                            "     LIMIT 1);"),
    GNUNET_PQ_make_prepare ("gc_challengecodes",
                            "DELETE FROM frosix_challengecode "
                            "WHERE "
                            "expiration_date < $1;"),
    GNUNET_PQ_PREPARED_STATEMENT_END
  };

  {
    enum GNUNET_GenericReturnValue ret;

    ret = GNUNET_PQ_prepare_statements (pg->conn,
                                        ps);
    if (GNUNET_OK != ret)
      return ret;
    pg->init = true;
    return GNUNET_OK;
  }
}


/**
 * Check that the database connection is still up.
 *
 * @param cls a `struct PostgresClosure` with connection to check
 */
static void
check_connection (void *cls)
{
  struct PostgresClosure *pg = cls;

  GNUNET_PQ_reconnect_if_down (pg->conn);
}


/**
 * Connect to the database if the connection does not exist yet.
 *
 * @param pg the plugin-specific state
 * @param skip_prepare true if we should skip prepared statement setup
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
internal_setup (struct PostgresClosure *pg,
                bool skip_prepare)
{
  if (NULL == pg->conn)
  {
#if AUTO_EXPLAIN
    /* Enable verbose logging to see where queries do not
       properly use indices */
    struct GNUNET_PQ_ExecuteStatement es[] = {
      GNUNET_PQ_make_try_execute ("LOAD 'auto_explain';"),
      GNUNET_PQ_make_try_execute ("SET auto_explain.log_min_duration=50;"),
      GNUNET_PQ_make_try_execute ("SET auto_explain.log_timing=TRUE;"),
      GNUNET_PQ_make_try_execute ("SET auto_explain.log_analyze=TRUE;"),
      /* https://wiki.postgresql.org/wiki/Serializable suggests to really
         force the default to 'serializable' if SSI is to be used. */
      GNUNET_PQ_make_try_execute (
        "SET SESSION CHARACTERISTICS AS TRANSACTION ISOLATION LEVEL SERIALIZABLE;"),
      GNUNET_PQ_make_try_execute ("SET enable_sort=OFF;"),
      GNUNET_PQ_make_try_execute ("SET enable_seqscan=OFF;"),
      GNUNET_PQ_make_execute ("SET search_path TO anastasis;"),
      GNUNET_PQ_EXECUTE_STATEMENT_END
    };
#else
    struct GNUNET_PQ_ExecuteStatement es[] = {
      GNUNET_PQ_make_execute ("SET search_path TO frosix;"),
      GNUNET_PQ_EXECUTE_STATEMENT_END
    };
#endif
    struct GNUNET_PQ_Context *db_conn;

    db_conn = GNUNET_PQ_connect_with_cfg (pg->cfg,
                                          "frosixdb-postgres",
                                          NULL,
                                          es,
                                          NULL);
    if (NULL == db_conn)
      return GNUNET_SYSERR;
    pg->conn = db_conn;
  }
  if (NULL == pg->transaction_name)
    GNUNET_PQ_reconnect_if_down (pg->conn);
  if (pg->init)
    return GNUNET_OK;
  if (skip_prepare)
    return GNUNET_OK;
  return prepare_statements (pg);
}


/**
 * Do a pre-flight check that we are not in an uncommitted transaction.
 * If we are, try to commit the previous transaction and output a warning.
 * Does not return anything, as we will continue regardless of the outcome.
 *
 * @param cls the `struct PostgresClosure` with the plugin-specific state
 * @return #GNUNET_OK if everything is fine
 *         #GNUNET_NO if a transaction was rolled back
 *         #GNUNET_SYSERR on hard errors
 */
static enum GNUNET_GenericReturnValue
postgres_preflight (void *cls)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_ExecuteStatement es[] = {
    GNUNET_PQ_make_execute ("ROLLBACK"),
    GNUNET_PQ_EXECUTE_STATEMENT_END
  };

  if (! pg->init)
  {
    if (GNUNET_OK !=
        internal_setup (pg,
                        false))
      return GNUNET_SYSERR;
  }
  if (NULL == pg->transaction_name)
    return GNUNET_OK;  /* all good */
  if (GNUNET_OK ==
      GNUNET_PQ_exec_statements (pg->conn,
                                 es))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "BUG: Preflight check rolled back transaction `%s'!\n",
                pg->transaction_name);
  }
  else
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "BUG: Preflight check failed to rollback transaction `%s'!\n",
                pg->transaction_name);
  }
  pg->transaction_name = NULL;
  return GNUNET_NO;
}


/**
 * Start a transaction.
 *
 * @param cls the `struct PostgresClosure` with the plugin-specific state
 * @param name unique name identifying the transaction (for debugging),
 *             must point to a constant
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
begin_transaction (void *cls,
                   const char *name)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_ExecuteStatement es[] = {
    GNUNET_PQ_make_execute ("START TRANSACTION ISOLATION LEVEL SERIALIZABLE"),
    GNUNET_PQ_EXECUTE_STATEMENT_END
  };

  check_connection (pg);
  GNUNET_break (GNUNET_OK ==
                postgres_preflight (pg));
  pg->transaction_name = name;
  if (GNUNET_OK !=
      GNUNET_PQ_exec_statements (pg->conn,
                                 es))
  {
    TALER_LOG_ERROR ("Failed to start transaction\n");
    GNUNET_break (0);
    return GNUNET_SYSERR;
  }
  return GNUNET_OK;
}


/**
* Roll back the current transaction of a database connection.
*
* @param cls the `struct PostgresClosure` with the plugin-specific state
* @return #GNUNET_OK on success
*/
static void
rollback (void *cls)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_ExecuteStatement es[] = {
    GNUNET_PQ_make_execute ("ROLLBACK"),
    GNUNET_PQ_EXECUTE_STATEMENT_END
  };

  if (GNUNET_OK !=
      GNUNET_PQ_exec_statements (pg->conn,
                                 es))
  {
    TALER_LOG_ERROR ("Failed to rollback transaction\n");
    GNUNET_break (0);
  }
  pg->transaction_name = NULL;
}


/**
 * Commit the current transaction of a database connection.
 *
 * @param cls the `struct PostgresClosure` with the plugin-specific state
 * @return transaction status code
 */
static enum GNUNET_DB_QueryStatus
commit_transaction (void *cls)
{
  struct PostgresClosure *pg = cls;
  enum GNUNET_DB_QueryStatus qs;
  struct GNUNET_PQ_QueryParam no_params[] = {
    GNUNET_PQ_query_param_end
  };

  qs = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                           "do_commit",
                                           no_params);
  pg->transaction_name = NULL;
  return qs;
}


/**
 * Register callback to be invoked on events of type @a es.
 *
 * @param cls database context to use
 * @param es specification of the event to listen for
 * @param timeout how long to wait for the event
 * @param cb function to call when the event happens, possibly
 *         multiple times (until cancel is invoked)
 * @param cb_cls closure for @a cb
 * @return handle useful to cancel the listener
 */
static struct GNUNET_DB_EventHandler *
postgres_event_listen (void *cls,
                       const struct GNUNET_DB_EventHeaderP *es,
                       struct GNUNET_TIME_Relative timeout,
                       GNUNET_DB_EventCallback cb,
                       void *cb_cls)
{
  struct PostgresClosure *pg = cls;

  return GNUNET_PQ_event_listen (pg->conn,
                                 es,
                                 timeout,
                                 cb,
                                 cb_cls);
}


/**
 * Stop notifications.
 *
 * @param eh handle to unregister.
 */
static void
postgres_event_listen_cancel (struct GNUNET_DB_EventHandler *eh)
{
  GNUNET_PQ_event_listen_cancel (eh);
}


/**
 * Notify all that listen on @a es of an event.
 *
 * @param cls database context to use
 * @param es specification of the event to generate
 * @param extra additional event data provided
 * @param extra_size number of bytes in @a extra
 */
static void
postgres_event_notify (void *cls,
                       const struct GNUNET_DB_EventHeaderP *es,
                       const void *extra,
                       size_t extra_size)
{
  struct PostgresClosure *pg = cls;

  return GNUNET_PQ_event_notify (pg->conn,
                                 es,
                                 extra,
                                 extra_size);
}

/**
 * FIXME
*/
static enum FROSIX_DB_StoreStatus
postgres_store_dkg_commitment (
  void *cls,
  const struct FROSIX_DkgRequestIdP *dkg_id,
  const struct FROSIX_DkgCommitmentsRaw *dkg_commits)
{
  struct PostgresClosure *pg = cls;
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  GNUNET_break (GNUNET_OK ==
                postgres_preflight (pg));
  for (unsigned int retry = 0; retry < MAX_RETRIES; retry++)
  {
    if (GNUNET_OK !=
        begin_transaction (pg,
                           "dkg_commitment_insert"))
    {
      GNUNET_break (0);
      return FROSIX_DB_STORE_STATUS_HARD_ERROR;
    }

    {

      // FIXME: do some checks
      struct GNUNET_TIME_Timestamp now = GNUNET_TIME_timestamp_get ();
      struct GNUNET_PQ_QueryParam params[] = {
        GNUNET_PQ_query_param_auto_from_type (dkg_id),
        GNUNET_PQ_query_param_timestamp (&now),
        GNUNET_PQ_query_param_fixed_size (dkg_commits->ptr_commits,
                                          dkg_commits->length),
        GNUNET_PQ_query_param_end
      };

      qs = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                               "dkg_commitment_insert",
                                               params);

      switch (qs)
      {
      case GNUNET_DB_STATUS_HARD_ERROR:
        rollback (pg);
        return FROSIX_DB_STORE_STATUS_HARD_ERROR;
      case GNUNET_DB_STATUS_SOFT_ERROR:
        goto retry;
      case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
        GNUNET_break (0);
        rollback (pg);
        return FROSIX_DB_STORE_STATUS_HARD_ERROR;
      case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
        qs = commit_transaction (pg);
        if (GNUNET_DB_STATUS_SOFT_ERROR == qs)
          goto retry;
        if (qs < 0)
          return FROSIX_DB_STORE_STATUS_HARD_ERROR;
        return FROSIX_DB_STORE_STATUS_SUCCESS;
      }
    }
    retry:
    rollback (pg);
  }
  return FROSIX_DB_STORE_STATUS_SOFT_ERROR;
}



/**
 * FIXME
*/
static enum GNUNET_DB_QueryStatus
postgres_get_dkg_commitment (
  void *cls,
  const struct FROSIX_DkgRequestIdP *dkg_id,
  struct FROSIX_DkgCommitmentsRaw *dkg_commits)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (dkg_id),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_variable_size ("commitments",
                                         &dkg_commits->ptr_commits,
                                         &dkg_commits->length),
    GNUNET_PQ_result_spec_end
  };

  check_connection (pg);
  GNUNET_break (GNUNET_OK == postgres_preflight (pg));

  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "dkg_commitments_select",
                                                   params,
                                                   rs);
}


/**
 * FIXME
*/
static enum FROSIX_DB_CommitmentStatus
postgres_lookup_dkg_commitment (
  void *cls,
  const struct FROSIX_DkgRequestIdP *dkg_id)
{
  enum GNUNET_DB_QueryStatus qs;

  struct PostgresClosure *pg = cls;
  check_connection (pg);
  GNUNET_break (GNUNET_OK == postgres_preflight (pg));

  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (dkg_id),
    GNUNET_PQ_query_param_end
  };

  struct FROSIX_DkgRequestIdP id;
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_auto_from_type ("dkg_id",
                                          &id),
    GNUNET_PQ_result_spec_end
  };

  qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                 "dkg_commitments_lookup",
                                                 params,
                                                 rs);

  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
    return FROSIX_DB_COMMITMENT_STATUS_HARD_ERROR;
  case GNUNET_DB_STATUS_SOFT_ERROR:
    GNUNET_break (0);
    return FROSIX_DB_COMMITMENT_STATUS_SOFT_ERROR;
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    return FROSIX_DB_COMMITMENT_STATUS_NO_RESULTS;
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    return FROSIX_DB_COMMITMENT_STATUS_ONE_RESULT;
  default:
    GNUNET_break (0);
    return FROSIX_DB_COMMITMENT_STATUS_HARD_ERROR;
  }
}


/**
 * FIXME
*/
static enum FROSIX_DB_StoreStatus
postgres_store_key (
  void *cls,
  const struct FROST_HashCode *id,
  const struct FROSIX_EncryptionNonceP *nonce,
  const struct FROSIX_KeyDataEncrypted *key_data,
  const struct FROSIX_ChallengeHashP *challenge_hash,
  uint32_t expiration,
  uint8_t identifier)
{
  struct PostgresClosure *pg = cls;
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  GNUNET_break (GNUNET_OK ==
                postgres_preflight (pg));
  for (unsigned int retry = 0; retry < MAX_RETRIES; retry++)
  {
    if (GNUNET_OK !=
        begin_transaction (pg,
                           "dkg_key_insert"))
    {
      GNUNET_break (0);
      return FROSIX_DB_STORE_STATUS_HARD_ERROR;
    }

    {
      const uint32_t id_32 = identifier;
      const uint32_t expiration_32 = expiration;
      // FIXME: do some checks?
      struct GNUNET_PQ_QueryParam params[] = {
        GNUNET_PQ_query_param_auto_from_type (id),
        GNUNET_PQ_query_param_uint32 (&id_32),
        GNUNET_PQ_query_param_uint32 (&expiration_32),
        GNUNET_PQ_query_param_auto_from_type (nonce),
        GNUNET_PQ_query_param_auto_from_type (key_data),
        GNUNET_PQ_query_param_auto_from_type (challenge_hash),
        GNUNET_PQ_query_param_end
      };
      qs = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                               "dkg_key_insert",
                                               params);

      switch (qs)
      {
      case GNUNET_DB_STATUS_HARD_ERROR:
        rollback (pg);
        return FROSIX_DB_STORE_STATUS_HARD_ERROR;
      case GNUNET_DB_STATUS_SOFT_ERROR:
        goto retry;
      case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
        GNUNET_break (0);
        rollback (pg);
        return FROSIX_DB_STORE_STATUS_HARD_ERROR;
      case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
        qs = commit_transaction (pg);
        if (GNUNET_DB_STATUS_SOFT_ERROR == qs)
          goto retry;
        if (qs < 0)
          return FROSIX_DB_STORE_STATUS_HARD_ERROR;
        return FROSIX_DB_STORE_STATUS_SUCCESS;
      }
    }
    retry:
    rollback (pg);
  }
  return FROSIX_DB_STORE_STATUS_SOFT_ERROR;
}


/**
 * FIXME
*/
static enum FROSIX_DB_KeyStatus
postgres_lookup_key (
  void *cls,
  const struct FROST_HashCode *id)
{
  enum GNUNET_DB_QueryStatus qs;

  struct PostgresClosure *pg = cls;
  check_connection (pg);
  GNUNET_break (GNUNET_OK == postgres_preflight (pg));

  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (id),
    GNUNET_PQ_query_param_end
  };

  struct FROST_HashCode id_db;
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_auto_from_type ("key_id",
                                          &id_db),
    GNUNET_PQ_result_spec_end
  };

  qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                 "key_lookup",
                                                 params,
                                                 rs);

  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
    return FROSIX_DB_KEY_STATUS_HARD_ERROR;
  case GNUNET_DB_STATUS_SOFT_ERROR:
    GNUNET_break (0);
    return FROSIX_DB_KEY_STATUS_SOFT_ERROR;
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    return FROSIX_DB_KEY_STATUS_NO_RESULTS;
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    return FROSIX_DB_KEY_STATUS_ONE_RESULT;
  default:
    GNUNET_break (0);
    return FROSIX_DB_KEY_STATUS_HARD_ERROR;
  }
}


/**
 * FIXME
*/
static enum GNUNET_DB_QueryStatus
postgres_get_auth_hash (
  void *cls,
  const struct FROST_HashCode *db_id,
  struct FROSIX_ChallengeHashP *challenge_hash)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (db_id),
    GNUNET_PQ_query_param_end
  };

  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_fixed_size ("challenge_data",
                                      challenge_hash,
                                      sizeof (*challenge_hash)),
    GNUNET_PQ_result_spec_end
  };

  check_connection (pg);
  GNUNET_break (GNUNET_OK == postgres_preflight (pg));

  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "auth_hash_select",
                                                   params,
                                                   rs);
}


/**
 * FIXME
*/
static enum GNUNET_DB_QueryStatus
postgres_get_key_data (
  void *cls,
  const struct FROST_HashCode *db_id,
  uint32_t *identifier,
  struct FROSIX_EncryptionNonceP *nonce,
  struct FROSIX_KeyDataEncrypted *enc_key_data)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (db_id),
    GNUNET_PQ_query_param_end
  };

  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_uint32 ("identifier",
                                  identifier),
    GNUNET_PQ_result_spec_fixed_size ("encryption_nonce",
                                      nonce,
                                      sizeof (*nonce)),
    GNUNET_PQ_result_spec_fixed_size ("enc_key_data",
                                      enc_key_data,
                                      sizeof (*enc_key_data)),
    GNUNET_PQ_result_spec_end
  };

  check_connection (pg);
  GNUNET_break (GNUNET_OK == postgres_preflight (pg));

  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "key_data_select",
                                                   params,
                                                   rs);
}


/**
 * FIXME
*/
static enum GNUNET_DB_QueryStatus
postgres_delete_key_data (
  void *cls,
  const struct FROST_HashCode *db_id)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (db_id),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  GNUNET_break (GNUNET_OK == postgres_preflight (pg));

  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "key_delete",
                                             params);
}


/**
 * FIXME
*/
static enum GNUNET_DB_QueryStatus
postgres_store_commitment_seed (
  void *cls,
  const struct GNUNET_HashCode *db_id,
  const struct FROST_CommitmentSeed *seed)
{
  struct PostgresClosure *pg = cls;
  enum GNUNET_DB_QueryStatus qs;
  struct GNUNET_TIME_Timestamp now = GNUNET_TIME_timestamp_get ();

  check_connection (pg);
  GNUNET_break (GNUNET_OK ==
                postgres_preflight (pg));
  for (unsigned int retry = 0; retry < MAX_RETRIES; retry++)
  {
    if (GNUNET_OK !=
        begin_transaction (pg,
                           "seed_insert"))
    {
      GNUNET_break (0);
      return FROSIX_DB_STORE_STATUS_HARD_ERROR;
    }

    {
      struct GNUNET_PQ_QueryParam params[] = {
        GNUNET_PQ_query_param_auto_from_type (db_id),
        GNUNET_PQ_query_param_auto_from_type (seed),
        GNUNET_PQ_query_param_timestamp (&now),
        GNUNET_PQ_query_param_end
      };
      qs = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                               "seed_insert",
                                               params);

      switch (qs)
      {
      case GNUNET_DB_STATUS_HARD_ERROR:
        rollback (pg);
        return FROSIX_DB_STORE_STATUS_HARD_ERROR;
      case GNUNET_DB_STATUS_SOFT_ERROR:
        goto retry;
      case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
        GNUNET_break (0);
        rollback (pg);
        return FROSIX_DB_STORE_STATUS_HARD_ERROR;
      case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
        qs = commit_transaction (pg);
        if (GNUNET_DB_STATUS_SOFT_ERROR == qs)
          goto retry;
        if (qs < 0)
          return FROSIX_DB_STORE_STATUS_HARD_ERROR;
        return FROSIX_DB_STORE_STATUS_SUCCESS;
      }
    }
    retry:
    rollback (pg);
  }
  return FROSIX_DB_STORE_STATUS_SOFT_ERROR;
}


/**
 * FIXME
*/
static enum GNUNET_DB_QueryStatus
postgres_get_and_delete_commitment_seed (
  void *cls,
  const struct GNUNET_HashCode *db_id,
  struct FROST_CommitmentSeed *seed)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (db_id),
    GNUNET_PQ_query_param_end
  };

  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_auto_from_type ("seed",
                                          seed),
    GNUNET_PQ_result_spec_end
  };

  check_connection (pg);
  GNUNET_break (GNUNET_OK == postgres_preflight (pg));

  enum GNUNET_DB_QueryStatus qs;

  qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                 "seed_select",
                                                 params,
                                                 rs);

  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
    return FROSIX_DB_STORE_STATUS_HARD_ERROR;
  case GNUNET_DB_STATUS_SOFT_ERROR:
    return FROSIX_DB_STORE_STATUS_SOFT_ERROR;
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    return FROSIX_DB_STORE_STATUS_NO_RESULTS;
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    break;
  }

  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "seed_delete",
                                             params);
}


/**
 * Create a new challenge code for a given challenge identified by the challenge
 * public key. The function will first check if there is already a valid code
 * for this challenge present and won't insert a new one in this case.
 *
 * @param cls closure
 * @param truth_uuid the identifier for the challenge
 * @param rotation_period for how long is the code available
 * @param validity_period for how long is the code available
 * @param retry_counter amount of retries allowed
 * @param[out] retransmission_date when to next retransmit
 * @param[out] code set to the code which will be checked for later
 * @return transaction status,
 *        #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if we are out of valid tries,
 *        #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT if @a code is now in the DB
 */
enum GNUNET_DB_QueryStatus
postgres_create_challenge_code (
  void *cls,
  const struct FROSIX_ChallengeIdP *truth_uuid,
  struct GNUNET_TIME_Relative rotation_period,
  struct GNUNET_TIME_Relative validity_period,
  uint32_t retry_counter,
  struct GNUNET_TIME_Timestamp *retransmission_date,
  uint64_t *code)
{
  struct PostgresClosure *pg = cls;
  enum GNUNET_DB_QueryStatus qs;
  struct GNUNET_TIME_Timestamp now = GNUNET_TIME_timestamp_get ();
  struct GNUNET_TIME_Timestamp expiration_date;
  struct GNUNET_TIME_Absolute ex_rot;

  check_connection (pg);
  expiration_date = GNUNET_TIME_relative_to_timestamp (validity_period);
  ex_rot = GNUNET_TIME_absolute_subtract (now.abs_time,
                                          rotation_period);
  for (unsigned int retries = 0; retries<MAX_RETRIES; retries++)
  {
    if (GNUNET_OK !=
        begin_transaction (pg,
                           "create_challenge_code"))
    {
      GNUNET_break (0);
      return GNUNET_DB_STATUS_HARD_ERROR;
    }

    {
      uint32_t old_retry_counter;
      struct GNUNET_PQ_QueryParam params[] = {
        GNUNET_PQ_query_param_auto_from_type (truth_uuid),
        GNUNET_PQ_query_param_timestamp (&now),
        GNUNET_PQ_query_param_absolute_time (&ex_rot),
        GNUNET_PQ_query_param_end
      };
      struct GNUNET_PQ_ResultSpec rs[] = {
        GNUNET_PQ_result_spec_uint64 ("code",
                                      code),
        GNUNET_PQ_result_spec_uint32 ("retry_counter",
                                      &old_retry_counter),
        GNUNET_PQ_result_spec_timestamp ("retransmission_date",
                                         retransmission_date),
        GNUNET_PQ_result_spec_end
      };
      enum GNUNET_DB_QueryStatus qs;

      qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                     "challengecode_select_meta",
                                                     params,
                                                     rs);
      switch (qs)
      {
      case GNUNET_DB_STATUS_HARD_ERROR:
        GNUNET_break (0);
        rollback (pg);
        return qs;
      case GNUNET_DB_STATUS_SOFT_ERROR:
        goto retry;
      case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
        GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                    "No active challenge found, creating a fresh one\n");
        break;
      case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
        if (0 == old_retry_counter)
        {
          rollback (pg);
          GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                      "Active challenge %llu has zero tries left, refusing to create another one\n",
                      (unsigned long long) *code);
          return GNUNET_DB_STATUS_SUCCESS_NO_RESULTS;
        }
        rollback (pg);
        GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                    "Active challenge has %u tries left, returning old challenge %llu\n",
                    (unsigned int) old_retry_counter,
                    (unsigned long long) *code);
        return qs;
      }
    }

    *code = GNUNET_CRYPTO_random_u64 (GNUNET_CRYPTO_QUALITY_NONCE,
                                      FROSIX_PIN_MAX_VALUE);
    *retransmission_date = GNUNET_TIME_UNIT_ZERO_TS;
    {
      struct GNUNET_PQ_QueryParam params[] = {
        GNUNET_PQ_query_param_auto_from_type (truth_uuid),
        GNUNET_PQ_query_param_uint64 (code),
        GNUNET_PQ_query_param_timestamp (&now),
        GNUNET_PQ_query_param_timestamp (&expiration_date),
        GNUNET_PQ_query_param_uint32 (&retry_counter),
        GNUNET_PQ_query_param_end
      };

      qs = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                               "challengecode_insert",
                                               params);
      switch (qs)
      {
      case GNUNET_DB_STATUS_HARD_ERROR:
        rollback (pg);
        return GNUNET_DB_STATUS_HARD_ERROR;
      case GNUNET_DB_STATUS_SOFT_ERROR:
        goto retry;
      case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
        GNUNET_break (0);
        rollback (pg);
        return GNUNET_DB_STATUS_HARD_ERROR;
      case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
        GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                    "Created fresh challenge with %u tries left\n",
                    (unsigned int) retry_counter);
        break;
      }
    }
    qs = commit_transaction (pg);
    if (GNUNET_DB_STATUS_SOFT_ERROR == qs)
      goto retry;
    if (qs < 0)
      return qs;
    return GNUNET_DB_STATUS_SUCCESS_ONE_RESULT;
    retry:
    rollback (pg);
  }
  return GNUNET_DB_STATUS_SOFT_ERROR;
}


/**
 * Closure for check_valid_code().
 */
struct CheckValidityContext
{
  /**
   * Code to check for.
   */
  const struct GNUNET_HashCode *hashed_code;

  /**
   * Truth we are processing.
   */
  const struct FROSIX_ChallengeIdP *truth_uuid;

  /**
   * Database context.
   */
  struct PostgresClosure *pg;

  /**
   * Set to the matching challenge code (if @e valid).
   */
  uint64_t code;

  /**
   * Set to true if a code matching @e hashed_code was found.
   */
  bool valid;

  /**
   * Set to true if a code matching @e hashed_code was set to 'satisfied' by the plugin.
   */
  bool satisfied;

  /**
   * Set to true if we had a database failure.
   */
  bool db_failure;

};


/**
 * Helper function for #postgres_verify_challenge_code().
 * To be called with the results of a SELECT statement
 * that has returned @a num_results results.
 *
 * @param cls closure of type `struct CheckValidityContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
check_valid_code (void *cls,
                  PGresult *result,
                  unsigned int num_results)
{
  struct CheckValidityContext *cvc = cls;
  struct PostgresClosure *pg = cvc->pg;

  for (unsigned int i = 0; i < num_results; i++)
  {
    uint64_t server_code;
    uint8_t sat;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_uint64 ("code",
                                    &server_code),
      GNUNET_PQ_result_spec_auto_from_type ("satisfied",
                                            &sat),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      cvc->db_failure = true;
      return;
    }
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Found issued challenge %llu (client: %s)\n",
                (unsigned long long) server_code,
                GNUNET_h2s (cvc->hashed_code));
    {
      struct GNUNET_HashCode shashed_code;

      FROSIX_hash_answer (server_code,
                          &shashed_code);
      if (0 ==
          GNUNET_memcmp (&shashed_code,
                         cvc->hashed_code))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                    "Challenge is valid challenge (%s)\n",
                    (0 != sat) ? "satisfied" : "not satisfied");
        cvc->valid = true;
        cvc->code = server_code;
        cvc->satisfied = (0 != sat);
      }
      else
      {
        /* count failures to prevent brute-force attacks */
        struct GNUNET_PQ_QueryParam params[] = {
          GNUNET_PQ_query_param_auto_from_type (cvc->truth_uuid),
          GNUNET_PQ_query_param_uint64 (&server_code),
          GNUNET_PQ_query_param_end
        };
        enum GNUNET_DB_QueryStatus qs;

        qs = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                                 "challengecode_update_retry",
                                                 params);
        if (qs <= 0)
        {
          GNUNET_break (0);
          cvc->db_failure = true;
        }
      }
    }
  }
}


/**
 * Verify the provided code with the code on the server.
 * If the code matches the function will return with success, if the code
 * does not match, the retry counter will be decreased by one.
 *
 * @param cls closure
 * @param truth_uuid identification of the challenge which the code corresponds to
 * @param hashed_code code which the user provided and wants to verify
 * @param[out] code set to the original numeric code
 * @param[out] satisfied set to true if the challenge is set to satisfied
 * @return code validity status
 */
enum FROSIX_DB_CodeStatus
postgres_verify_challenge_code (
  void *cls,
  const struct FROSIX_ChallengeIdP *truth_uuid,
  const struct GNUNET_HashCode *hashed_code,
  uint64_t *code,
  bool *satisfied)
{
  struct PostgresClosure *pg = cls;
  struct CheckValidityContext cvc = {
    .truth_uuid = truth_uuid,
    .hashed_code = hashed_code,
    .pg = pg
  };
  struct GNUNET_TIME_Timestamp now = GNUNET_TIME_timestamp_get ();
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (truth_uuid),
    // GNUNET_PQ_query_param_timestamp (&now),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  *satisfied = false;
  check_connection (pg);
  qs = GNUNET_PQ_eval_prepared_multi_select (pg->conn,
                                             "challengecode_select",
                                             params,
                                             &check_valid_code,
                                             &cvc);
  if ( (qs < 0) ||
       (cvc.db_failure) )
    return FROSIX_DB_CODE_STATUS_HARD_ERROR;
  *code = cvc.code;
  if (cvc.valid)
  {
    *satisfied = cvc.satisfied;
    return FROSIX_DB_CODE_STATUS_VALID_CODE_STORED;
  }
  if (0 == qs)
    return FROSIX_DB_CODE_STATUS_NO_RESULTS;
  return FROSIX_DB_CODE_STATUS_CHALLENGE_CODE_MISMATCH;
}


/**
 * Remember in the database that we successfully sent a challenge.
 *
 * @param cls closure
 * @param truth_uuid the identifier for the challenge
 * @param code the challenge that was sent
 */
static enum GNUNET_DB_QueryStatus
postgres_mark_challenge_sent (
  void *cls,
  const struct FROSIX_ChallengeIdP *truth_uuid,
  uint64_t code)
{
  struct PostgresClosure *pg = cls;
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  {
    struct GNUNET_TIME_Timestamp now;
    struct GNUNET_PQ_QueryParam params[] = {
      GNUNET_PQ_query_param_auto_from_type (truth_uuid),
      GNUNET_PQ_query_param_uint64 (&code),
      GNUNET_PQ_query_param_timestamp (&now),
      GNUNET_PQ_query_param_end
    };

    now = GNUNET_TIME_timestamp_get ();
    qs = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "challengecode_mark_sent",
                                             params);
    if (qs <= 0)
      return qs;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Marking challenge %llu as issued\n",
              (unsigned long long) code);
  return qs;
}


/**
 * Function called to remove all expired codes from the database.
 *
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
postgres_challenge_gc (void *cls)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_TIME_Timestamp time_now = GNUNET_TIME_timestamp_get ();
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_timestamp (&time_now),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  GNUNET_break (GNUNET_OK ==
                postgres_preflight (pg));
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "gc_challengecodes",
                                             params);
}


/**
 * Initialize Postgres database subsystem.
 *
 * @param cls a configuration instance
 * @return NULL on error, otherwise a `struct FROSIX_DatabasePlugin`
 */
void *
libfrosix_plugin_db_postgres_init (void *cls)
{
  struct GNUNET_CONFIGURATION_Handle *cfg = cls;
  struct PostgresClosure *pg;
  struct FROSIX_DatabasePlugin *plugin;

  pg = GNUNET_new (struct PostgresClosure);
  pg->cfg = cfg;

  if (GNUNET_OK !=
      GNUNET_CONFIGURATION_get_value_string (cfg,
                                             "taler",
                                             "CURRENCY",
                                             &pg->currency))
  {
    GNUNET_log_config_missing (GNUNET_ERROR_TYPE_ERROR,
                               "taler",
                               "CURRENCY");
    GNUNET_PQ_disconnect (pg->conn);
    GNUNET_free (pg);
    return NULL;
  }
  plugin = GNUNET_new (struct FROSIX_DatabasePlugin);
  plugin->cls = pg;
  /* FIXME: Should this be the same? */
  plugin->connect = &postgres_preflight;
  plugin->create_tables = &postgres_create_tables;
  plugin->drop_tables = &postgres_drop_tables;
  // plugin->gc = &postgres_gc;
  plugin->preflight = &postgres_preflight;
  plugin->rollback = &rollback;
  plugin->start = &begin_transaction;
  plugin->commit = &commit_transaction;
  plugin->event_listen = &postgres_event_listen;
  plugin->event_listen_cancel = &postgres_event_listen_cancel;
  plugin->event_notify = &postgres_event_notify;
  plugin->store_dkg_commitment = &postgres_store_dkg_commitment;
  plugin->get_dkg_commitment = &postgres_get_dkg_commitment;
  plugin->lookup_dkg_commitment = &postgres_lookup_dkg_commitment;
  plugin->store_key = &postgres_store_key;
  plugin->lookup_key = &postgres_lookup_key;
  plugin->get_key_data = &postgres_get_key_data;
  plugin->delete_key_data = &postgres_delete_key_data;
  plugin->get_auth_hash = &postgres_get_auth_hash;
  plugin->store_commitment_seed = &postgres_store_commitment_seed;
  plugin->get_and_delete_commitment_seed =
    &postgres_get_and_delete_commitment_seed;
  plugin->create_challenge_code = &postgres_create_challenge_code;
  plugin->verify_challenge_code = &postgres_verify_challenge_code;
  plugin->mark_challenge_sent = &postgres_mark_challenge_sent;
  plugin->challenge_gc = &postgres_challenge_gc;
  return plugin;
}


/**
 * Shutdown Postgres database subsystem.
 *
 * @param cls a `struct FROSIX_DB_STATUS_Plugin`
 * @return NULL (always)
 */
void *
libfrosix_plugin_db_postgres_done (void *cls)
{
  struct FROSIX_DatabasePlugin *plugin = cls;
  struct PostgresClosure *pg = plugin->cls;

  GNUNET_PQ_disconnect (pg->conn);
  GNUNET_free (pg->currency);
  GNUNET_free (pg);
  GNUNET_free (plugin);
  return NULL;
}


/* end of plugin_frosix_postgres.c */
