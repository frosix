--
-- This file is part of Frosix
-- Copyright (C) 2014--2022 Anastasis Systems SA
--
-- Frosix is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
--

-- Everything in one big transaction
BEGIN;

-- Unregister patch (0001.sql)
SELECT _v.unregister_patch('frosix-0001');
DROP SCHEMA frosix CASCADE;

-- And we're out of here...
COMMIT;
