--
-- This file is part of Frosix
-- Copyright (C) 2022, 2023 Joel Urech
--
-- Frosix is free software; you can redistribute it and/or modify it under the
-- terms of the GNU Affero General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License along with
-- Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
--

-- Everything in one big transaction
BEGIN;

-- Check patch versioning is in place.
SELECT _v.register_patch('frosix-0001', NULL, NULL);

CREATE SCHEMA frosix;
COMMENT ON SCHEMA frosix IS 'frosix backend data';

SET search_path TO frosix;


CREATE TABLE IF NOT EXISTS frosix_key
  (key_id BYTEA PRIMARY KEY CHECK(LENGTH(key_id)=64), -- hash of encryption key
   identifier INT NOT NULL,
   expiration INT NOT NULL,
   encryption_nonce BYTEA CHECK(LENGTH(encryption_nonce)=24), 
   enc_key_data BYTEA CHECK(LENGTH(enc_key_data)=80), -- (secret share, public_key + MAC)
   challenge_data BYTEA CHECK(LENGTH(challenge_data)=64)); -- salted_hash H(auth_data, auth_nonce) auth_data=answer, phone number, etc.

COMMENT ON TABLE frosix_key
  IS 'Saves all needed data to participate in a signing process';
COMMENT ON COLUMN frosix_key.key_id
  IS 'The key id identifies this key record. It is the hash of the key which was used to encrypt the data in this record';
COMMENT ON COLUMN frosix_key.identifier
  IS 'Identifier in the signing group';
COMMENT ON COLUMN frosix_key.expiration
  IS 'At which date the key record expire';
COMMENT ON COLUMN frosix_key.encryption_nonce
  IS 'Nonce used in encryption and decryption of the key data';
COMMENT ON COLUMN frosix_key.enc_key_data
  IS 'Stores the encrypted secret key share and the corresponding public key which is used in the signing process';
COMMENT ON COLUMN frosix_key.challenge_data
  IS 'Stores the salted hash of the challenge data which are used to authenticate a signing request.';


CREATE TABLE IF NOT EXISTS frosix_public_commitments
  (dkg_id BYTEA PRIMARY KEY CHECK(LENGTH(dkg_id)=64), -- h(context_string || public provider salt)
   time_stamp TIMESTAMP NOT NULL,
   commitments BYTEA NOT NULL);

COMMENT ON TABLE frosix_public_commitments
  IS 'Saves commitments used in distributed key generation';
COMMENT ON COLUMN frosix_public_commitments.dkg_id
  IS 'ID of the corresponding dkg';
COMMENT ON COLUMN frosix_public_commitments.time_stamp
  IS 'Timestamp from the creation of this record. Used for garbage collection';
COMMENT ON COLUMN frosix_public_commitments.commitments
  IS 'The stored commitments from all other participants';


CREATE INDEX IF NOT EXISTS frosix_public_commitments_expiration_index
  ON frosix_public_commitments
  (dkg_id,time_stamp);

COMMENT ON INDEX frosix_public_commitments_expiration_index
  IS 'for dkg commitments garbage collection';


CREATE TABLE IF NOT EXISTS frosix_challengecode
  (challenge_id BYTEA CHECK(LENGTH(challenge_id)=64) NOT NULL,
   code INT8 NOT NULL,
   creation_date INT8 NOT NULL,
   expiration_date INT8 NOT NULL,
   retransmission_date INT8 NOT NULL DEFAULT 0,
   retry_counter INT4 NOT NULL,
   satisfied BOOLEAN NOT NULL DEFAULT FALSE);

COMMENT ON TABLE frosix_challengecode
  IS 'Stores a code which is checked for the authentication by SMS, E-Mail..';
COMMENT ON COLUMN frosix_challengecode.challenge_id
  IS 'ID of the challenge, is unique for each message and key pair';
COMMENT ON COLUMN frosix_challengecode.code
  IS 'The pin code which is sent to the user and verified';
COMMENT ON COLUMN frosix_challengecode.creation_date
  IS 'Creation date of the code';
COMMENT ON COLUMN frosix_challengecode.retransmission_date
  IS 'When did we last transmit the challenge to the user';
COMMENT ON COLUMN frosix_challengecode.expiration_date
  IS 'When will the code expire';
COMMENT ON COLUMN frosix_challengecode.retry_counter
  IS 'How many tries are left for this code must be > 0';
COMMENT ON COLUMN frosix_challengecode.satisfied
  IS 'Has this challenge been satisfied by the user, used if it is not enough for the user to know the code (like for video identification or SEPA authentication). For SMS/E-mail/Post verification, this field being FALSE does not imply that the user did not meet the challenge.';


CREATE INDEX IF NOT EXISTS frosix_challengecode_expiration_index
  ON frosix_challengecode
  (challenge_id,expiration_date);
  
COMMENT ON INDEX frosix_challengecode_expiration_index
  IS 'for challenge garbage collection';


CREATE TABLE IF NOT EXISTS frosix_seed
  (seed_id BYTEA PRIMARY KEY CHECK(LENGTH(seed_id)=64) NOT NULL, --H(H(enc_key), i, D, E)
   seed BYTEA CHECK(LENGTH(seed)=32) NOT NULL,
   time_stamp TIMESTAMP NOT NULL);

COMMENT ON TABLE frosix_seed
  IS 'Stores the random seed to generate the commitment in Frosix Sign between round 1 and 2';
COMMENT ON COLUMN frosix_seed.seed_id
  IS 'Identifier of the stored seed, instantiated as hash over the resulting commitment and the encryption key';
COMMENT ON COLUMN frosix_seed.seed
  IS 'This seed must never be used for two signatures! Delete immediately after use!';
COMMENT ON COLUMN frosix_seed.time_stamp
  IS 'Timestamp of creation, used for garbage collection';

CREATE INDEX IF NOT EXISTS frosix_seed_expiration_index
  ON frosix_seed
  (seed_id,time_stamp);

COMMENT ON INDEX frosix_seed_expiration_index
  IS 'For seed garbage collection';

-- Complete transaction
COMMIT;
