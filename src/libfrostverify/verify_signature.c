/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Frosix

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file verify_signature.c
 * @brief Implementation of the signature verification function
 * @author Joel Urech
*/
#include "high_common.h"

/**
 * Verifies the validity of a given signature and hash of a message against a
 * public key.
 */
enum GNUNET_GenericReturnValue
FROST_verify_signature (
  const struct FROST_PublicKey *public_key,
  const struct FROST_Signature *signature,
  const struct FROST_MessageHash *message_hash)
{
  struct FROST_GroupCommitment sig_r;
  FROST_point_copy_to (&sig_r.commitment, &signature->r);

  // compute challenge H(r || pk || m)
  struct FROST_Challenge challenge;
  FROST_compute_challenge_ (&challenge, &sig_r, public_key, message_hash);

  // pk^challenge
  struct FROST_Point pk_challenge;
  FROST_point_mul_scalar (&pk_challenge, &public_key->pk,
                          &challenge.challenge);

  // g^sig
  struct FROST_Point g_sig;
  FROST_base_mul_scalar (&g_sig, &signature->z);

  // g^sig - pk^challenge
  struct FROST_Point ver_r;
  FROST_point_sub_point (&ver_r, &g_sig, &pk_challenge);

  return FROST_point_cmp (&signature->r,
                          &ver_r);
}
