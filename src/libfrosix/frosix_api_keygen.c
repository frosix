/*
  This file is part of Frosix
  Copyright (C) 2020, 2021 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Frosix; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file reducer/frosix_api_keygen.c
 * @brief frosix reducer keygen api
 * @author Christian Grothoff
 * @author Dominik Meister
 * @author Dennis Neufeld
 * @author Joel Urech
 */

#include "platform.h"
#include "frosix.h"
#include "frosix_api.h"
#include <taler/taler_merchant_service.h>
#include <time.h>

#define FROSIX_KDF_CONTEXT "FROSIX-KEY"

/**
 * FIXME
*/
struct FROSIX_DkgProvider
{
  /**
   * Name of the provider
  */
  char *provider_name;

  /**
   * URL of the provider.
  */
  char *backend_url;

  /**
   * Public provider salt
  */
  struct FROSIX_ProviderSaltP provider_salt;

  /**
   * Long term public key of the provider
  */
  struct GNUNET_CRYPTO_EddsaPublicKey provider_public_key;

  /**
   * Index of the provider
  */
  uint8_t provider_index;

  /**
   * Authentication method.
  */
  char *auth_method;

  /**
   * Authentication data.
  */
  char *auth_data;

  /**
   * Answer to the question in @e auth_data.
   * This field is optional and only used if authentication method is of type
   * security question.
  */
  char *auth_answer;

  /**
   * Context String for this provider.
  */
  struct FROSIX_DkgContextStringP context_string;

  /**
   * Salt for the hash of the authentication data
  */
  struct GNUNET_HashCode hash_salt;

  /**
   * Salted hash of the challenge data
  */
  struct FROSIX_ChallengeHashP auth_hash;

  /**
   * Commitments which we get from round 1
  */
  struct FROSIX_DkgCommitment commitment;

  /**
   * Pre encryption key
  */
  struct FROSIX_EncryptionKey pre_enc_key;

  /**
   * All the secret shares received from this provider.
   * Number or length of this array is always `number_of_participants - 1`
  */
  struct FROSIX_DkgSecretShare *secret_shares;

  /**
   * Public key, should be the same at all providers
  */
  struct FROST_PublicKey public_key;

  /**
   * Provider's signature over public key and authentication data.
  */
  struct GNUNET_CRYPTO_EddsaSignature providers_signature;

  /**
   * Seed of provider
  */
  uint8_t seed[64];
};


/**
 * Struct to store all parsed data from the cli and /config and /seed
*/
struct FROSIX_DkgData
{
  /**
   * What is the threshold value?
  */
  uint8_t threshold;

  /**
   * Number of participating providers. Is the length of the @e providers array
  */
  uint8_t num_of_participants;

  /**
   * Our master key we have to export at the end of the DKG.
  */
  struct GNUNET_HashCode master_key;

  /**
   * Pointer to a list of providers
  */
  struct FROSIX_DkgProvider *providers;

  /**
   * Array of the public keys from all providers.
   * Length is @e num_of_participants
  */
  struct GNUNET_CRYPTO_EddsaPublicKey *providers_public_keys;

  /**
   * Number of years the key data should be stored at the providers.
  */
  uint64_t expiration;
};


/**
 * State of a keygen procedure
*/
struct FROSIX_DkgState
{
  /**
   * State we are updating.
   */
  struct FROSIX_DkgData *dkg_data;

  /**
   * Function to call when we are done.
   */
  FROSIX_ActionCallback cb;

  /**
   * Closure for @e cb.
   */
  void *cb_cls;

  /**
   * Redux action we returned to our controller.
   */
  struct FROSIX_ReduxAction ra;

  /**
   * Number of provider operations that are still awaiting completion.
   */
  uint8_t counter;

  /**
   * Last error code
  */
  enum TALER_ErrorCode error_code;

  /**
    * Array of config requests, with length `num_of_participants`
  */
  struct FROSIX_ConfigOperation **co;

  /**
   * Array of seed requests, with length `num_of_participants`
  */
  struct FROSIX_SeedGetOperation **sgo;

  /**
   * DKG-Commitment Requests
  */
  struct FROSIX_DkgCommitmentRequestOperation **dco;

  /**
   * DKG-Share Requests
  */
  struct FROSIX_DkgShareRequestOperation **dso;

  /**
   * DKG-Key Requests
  */
  struct FROSIX_DkgKeyStoreOperation **dko;
};


/**
 * Helper function to free all initialized memory of a
 * `struct FROSIX_DkgProvider`
 *
 * @param dp Pointer to the provider we want to free the memory.
*/
static void
free_dkg_provider_struct (struct FROSIX_DkgProvider *dp)
{
  if (NULL != dp->provider_name)
    GNUNET_free (dp->provider_name);

  if (NULL != dp->backend_url)
    GNUNET_free (dp->backend_url);

  if (NULL != dp->auth_method)
    GNUNET_free (dp->auth_method);

  if (NULL != dp->auth_data)
    GNUNET_free (dp->auth_data);

  if (NULL != dp->auth_answer)
    GNUNET_free (dp->auth_answer);

  FROST_free_dkg_commitment (&dp->commitment.commitment,
                             1);

  if (NULL != dp->secret_shares)
    GNUNET_free (dp->secret_shares);
}


/**
 * Function to free all initialized memory of a provider from a
 * `struct FROSIX_DkgData`
 *
 * @param dd the struct to clean up
*/
static void
free_dkg_data_struct (struct FROSIX_DkgData *dd)
{
  /* go through all providers - if any */
  if (NULL != dd->providers)
  {
    for (unsigned int i = 0; i < dd->num_of_participants; i++)
    {
      if (NULL != &dd->providers[i])
        free_dkg_provider_struct (&dd->providers[i]);
    }
    GNUNET_free (dd->providers);
  }

  if (NULL != dd->providers_public_keys)
    GNUNET_free (dd->providers_public_keys);
}


/**
 * Function called when keygen is being aborted.
 * Frees all initialized memory!
 *
 * @param cls a `struct FROSIX_DkgState`
*/
static void
keygen_cancel_cb (void *cls)
{
  struct FROSIX_DkgState *ds = cls;

  /* free dkg data */
  if (NULL != ds->dkg_data)
  {
    free_dkg_data_struct (ds->dkg_data);
    GNUNET_free (ds->dkg_data);
  }

  /* free operations */
  if (NULL != ds->co)
    GNUNET_free (ds->co);

  if (NULL != ds->sgo)
    GNUNET_free (ds->sgo);

  if (NULL != ds->dco)
    GNUNET_free (ds->dco);

  if (NULL != ds->dso)
    GNUNET_free (ds->dso);

  if (NULL != ds->dko)
    GNUNET_free (ds->dko);

  /* free dkg state */
  GNUNET_free (ds);
}


/**
 * FIXME: move to frosix common crypto functions library!
*/
static void
derive_encryption_key (
  struct FROSIX_EncryptionKey *enc_key,
  const struct FROSIX_EncryptionKey *pre_enc_key,
  const struct FROST_PublicKey *pk,
  const struct FROSIX_ProviderSaltP *provider_salt,
  uint8_t provider_index)
{
  GNUNET_CRYPTO_kdf (enc_key,
                     sizeof (*enc_key),
                     FROSIX_KDF_CONTEXT,
                     sizeof (FROSIX_KDF_CONTEXT),
                     pre_enc_key,
                     sizeof (*pre_enc_key),
                     pk,
                     sizeof (*pk),
                     provider_salt,
                     sizeof (*provider_salt),
                     &provider_index,
                     sizeof (provider_index),
                     NULL,
                     0);
}

/**
 * Helper function to build an array out of all provider public keys.
 * The assembled array is then stored in our @a dd struct.
 *
 * @param dd Our main struct to store all relevant data in.
*/
static void
keygen_build_public_key_array (struct FROSIX_DkgData *dd)
{
  /* prepare array of providers public keys */
  dd->providers_public_keys = GNUNET_malloc (
    dd->num_of_participants
    * sizeof (struct GNUNET_CRYPTO_EddsaPublicKey));

  GNUNET_assert (NULL != dd->providers_public_keys);

  for (unsigned int i = 0; i < dd->num_of_participants; i++)
  {
    memcpy (&dd->providers_public_keys[i],
            &dd->providers[i].provider_public_key,
            sizeof (dd->providers->provider_public_key));
  }
}


/**
 * This function gets called after we have received a positive result from all
 * providers. It prepares a json struct, which we then return back to the cli.
 *
 * @param ds The state of our keygen process.
*/
static void
keygen_prepare_result (struct FROSIX_DkgState *ds)
{
  /* first check if signature from providers can be verified. */
  // FIXME: define constant for size of ps struct
  for (unsigned int i = 0; i < ds->dkg_data->num_of_participants; i++)
  {
    struct FROSIX_DkgProvider *dp = &ds->dkg_data->providers[i];

    struct FROSIX_DkgKeySignaturePS ks = {
      .purpose.purpose = htonl (104),
      .purpose.size = htonl (sizeof (ks)),
      .public_key = dp->public_key,
      .auth_hash = dp->auth_hash,
    };

    GNUNET_assert (GNUNET_OK == GNUNET_CRYPTO_eddsa_verify (
                     104,
                     &ks,
                     &dp->providers_signature,
                     &dp->provider_public_key));
  }

  /* check if every reported public key is the same */
  for (unsigned int i = 0; i < ds->dkg_data->num_of_participants; i++)
  {
    if (GNUNET_OK != FROST_point_cmp (
          &ds->dkg_data->providers[i].public_key.pk,
          &ds->dkg_data->providers[0].public_key.pk))
    {
      json_t *error_message;
      error_message = GNUNET_JSON_PACK (
        GNUNET_JSON_pack_string ("Frosix_client_error",
                                 "Public keys do not match"));

      ds->cb (ds->cb_cls,
              0,
              error_message);

      return;
    }
  }

  /* calculate encryption keys */
  struct FROSIX_EncryptionKey enc_keys[ds->dkg_data->num_of_participants];
  for (unsigned int i = 0; i < ds->dkg_data->num_of_participants; i++)
  {
    struct FROSIX_DkgProvider *dp = &ds->dkg_data->providers[i];
    derive_encryption_key (&enc_keys[i],
                           &dp->pre_enc_key,
                           &dp->public_key,
                           &dp->provider_salt,
                           dp->provider_index);
  }

  /* build json */
  json_t *result;
  json_t *providers = json_array ();

  for (unsigned int i = 0; i < ds->dkg_data->num_of_participants; i++)
  {
    /* build single provider */
    struct FROSIX_DkgProvider *dp = &ds->dkg_data->providers[i];
    json_t *provider;
    provider = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_uint64 ("provider_index",
                               dp->provider_index),
      GNUNET_JSON_pack_string ("provider_name",
                               dp->provider_name),
      GNUNET_JSON_pack_string ("backend_url",
                               dp->backend_url),
      GNUNET_JSON_pack_data_auto ("encryption_key",
                                  &enc_keys[i]),
      GNUNET_JSON_pack_string ("auth_method",
                               dp->auth_method),
      GNUNET_JSON_pack_string ("auth_data",
                               dp->auth_data),
      GNUNET_JSON_pack_data_auto ("auth_nonce",
                                  &dp->hash_salt),
      GNUNET_JSON_pack_data_auto ("auth_hash",
                                  &dp->auth_hash),
      GNUNET_JSON_pack_data_auto ("provider_signature",
                                  &dp->providers_signature),
      GNUNET_JSON_pack_data_auto ("provider_public_key",
                                  &dp->provider_public_key));

    /* add to provider array */
    GNUNET_assert (0 ==
                   json_array_append_new (
                     providers,
                     provider));
  }

  result = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_data_auto ("public_key",
                                &ds->dkg_data->providers[0].public_key),
    GNUNET_JSON_pack_uint64 ("number_of_participants",
                             ds->dkg_data->num_of_participants),
    GNUNET_JSON_pack_uint64 ("threshold",
                             ds->dkg_data->threshold),
    GNUNET_JSON_pack_array_steal ("providers",
                                  providers));

  /* free all initialized data */
  free_dkg_data_struct (ds->dkg_data);

  ds->cb (ds->cb_cls,
          ds->error_code,
          result);
}


/**
 * Callback to process a POST /dkg-key request
 *
 * @param cls closure
 * @param dcd the decoded response body
*/
static void
dkg_key_request_cb (void *cls,
                    const struct FROSIX_DkgKeyStoreDetails *dkd)
{
  GNUNET_assert (NULL != cls);
  struct FROSIX_DkgState *ds = cls;

  if (0 >= ds->counter)
  {
    /* We haven't expected this callback, thus just ignore it */
    return;
  }

  /* check if the call was successful */
  if (0 == dkd->http_status)
  {
    ds->counter = 0;

    // FIXME: cancel all other running and not yet returned requests
    json_t *error_message;
    error_message = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_string ("Frosix_client_error",
                               "Error requesting dkg key from provider"));

    ds->cb (ds->cb_cls,
            0,
            error_message);

    return;
  }

  /* set error code */
  ds->error_code = dkd->ec;

  /* check if provider index is valid */
  GNUNET_assert (0 < dkd->provider_index);
  GNUNET_assert (ds->dkg_data->num_of_participants >= dkd->provider_index);

  /* copy commitment in our struct - just assign pointer */
  struct FROSIX_DkgProvider *dp = &ds->dkg_data->providers[dkd->provider_index
                                                           - 1];

  memcpy (&dp->public_key,
          &dkd->details.public_key,
          sizeof (dkd->details.public_key));

  memcpy (&dp->providers_signature,
          &dkd->details.signature,
          sizeof (dkd->details.signature));

  /* call next dkg-commitment request */
  ds->counter--;
  if (0 == ds->counter)
  {
    /* we are the last, go ahead with checking our result */
    GNUNET_free (ds->dko);
    ds->dko = NULL;
    keygen_prepare_result (ds);
  }
}


/**
 * FIXME
*/
static void
start_dkg_key (struct FROSIX_DkgState *ds)
{
  ds->dko = GNUNET_malloc (ds->dkg_data->num_of_participants * sizeof (char *));
  ds->counter = 0;

  for (unsigned int i = 0; i < ds->dkg_data->num_of_participants; i++)
  {
    struct FROSIX_DkgData *dd = ds->dkg_data;
    struct FROSIX_DkgProvider *dp = &ds->dkg_data->providers[i];

    /* set up array of secret shares */
    struct FROSIX_DkgKeyStoreShare secret_shares[dd->num_of_participants - 1];

    for (unsigned int j = 0; j < dd->num_of_participants; j++)
    {
      if (j < i)
      {
        /* check if target of the secret share matches */
        if (dp->provider_index != dd->providers[j].secret_shares[i - 1].target)
        {
          json_t *error_message;
          error_message = GNUNET_JSON_PACK (
            GNUNET_JSON_pack_string ("Frosix_client_error",
                                     "Error while assigning the secret shares"));

          ds->cb (ds->cb_cls,
                  0,
                  error_message);

          return;
        }

        secret_shares[j].identifier =
          dd->providers[j].secret_shares[i - 1].issuer;

        memcpy (&secret_shares[j].secret_share,
                &dd->providers[j].secret_shares[i - 1].encrypted_share,
                sizeof (dd->providers[j].secret_shares[i - 1].encrypted_share));
        memcpy (&secret_shares[j].ephemeral_key,
                &dd->providers[j].secret_shares[i - 1].ephemeral_key,
                sizeof (dd->providers[j].secret_shares[i - 1].ephemeral_key));
      }
      else if (j > i)
      {
        /* check if target of the secret share matches */
        if (dp->provider_index != dd->providers[j].secret_shares[i].target)
        {
          json_t *error_message;
          error_message = GNUNET_JSON_PACK (
            GNUNET_JSON_pack_string ("Frosix_client_error",
                                     "Error while assigning the secret shares"));

          ds->cb (ds->cb_cls,
                  0,
                  error_message);

          return;
        }

        secret_shares[j - 1].identifier =
          dd->providers[j].secret_shares[i].issuer;
        memcpy (&secret_shares[j - 1].secret_share,
                &dd->providers[j].secret_shares[i].encrypted_share,
                sizeof (dd->providers[j].secret_shares[i].encrypted_share));
        memcpy (&secret_shares[j - 1].ephemeral_key,
                &dd->providers[j].secret_shares[i].ephemeral_key,
                sizeof (dd->providers[j].secret_shares[i].ephemeral_key));
      }
    }

    /* compute request id */
    struct FROSIX_DkgRequestIdP request_id;
    FROSIX_compute_dkg_request_id (
      &request_id,
      &dp->context_string,
      &dp->auth_hash,
      &dp->provider_salt,
      dp->provider_index,
      ds->dkg_data->num_of_participants,
      ds->dkg_data->threshold);

    ds->dko[i] = FROSIX_dkg_key_store (
      FROSIX_REDUX_ctx_,
      dp->backend_url,
      &request_id,
      dp->provider_index,
      dd->threshold,
      dd->num_of_participants,
      &dp->context_string,
      &dp->auth_hash,
      dd->providers_public_keys,
      &dp->pre_enc_key,
      dd->expiration,
      secret_shares,
      dd->num_of_participants - 1,
      &dkg_key_request_cb,
      ds);

    ds->counter++;
  }
}


/**
 * Callback to process a POST /dkg-shares request
 *
 * @param cls closure
 * @param dcd the decoded response body
*/
static void
dkg_share_request_cb (void *cls,
                      const struct FROSIX_DkgShareRequestDetails *dsd)
{
  GNUNET_assert (NULL != cls);
  struct FROSIX_DkgState *ds = cls;

  if (0 >= ds->counter)
  {
    /* We haven't expected this callback, thus just ignore it */
    return;
  }

  /* check if the call was successful */
  if (0 == dsd->http_status)
  {
    ds->counter = 0;

    // FIXME: cancel all other running and not yet returned requests
    json_t *error_message;
    error_message = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_string ("Frosix_client_error",
                               "Error requesting dkg shares from provider"));

    ds->cb (ds->cb_cls,
            0,
            error_message);

    return;
  }

  /* set error code */
  ds->error_code = dsd->ec;

  /* check if provider index is valid */
  GNUNET_assert (0 < dsd->provider_index);
  GNUNET_assert (ds->dkg_data->num_of_participants >= dsd->provider_index);

  /* check number of secret shares */
  if (ds->dkg_data->num_of_participants - 1 != dsd->shares_len)
  {
    ds->counter = 0;

    // FIXME: cancel all other running and not yet returned requests
    json_t *error_message;
    error_message = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_string ("Frosix_client_error",
                               "Error in dkg shares response"));

    ds->cb (ds->cb_cls,
            0,
            error_message);

    return;
  }

  /* check if our provider index is same as in the response */
  for (unsigned int i = 0; i < dsd->shares_len; i++)
  {
    if (dsd->provider_index != dsd->shares[i].issuer)
    {
      ds->counter = 0;

      // FIXME: cancel all other running and not yet returned requests
      json_t *error_message;
      error_message = GNUNET_JSON_PACK (
        GNUNET_JSON_pack_string ("Frosix_client_error",
                                 "Error in dkg shares response"));

      ds->cb (ds->cb_cls,
              0,
              error_message);

      return;
    }
  }

  /* copy commitment in our struct - just assign pointer */
  ds->dkg_data->providers[dsd->provider_index - 1].secret_shares = dsd->shares;

  /* call next dkg-commitment request */
  ds->counter--;
  if (0 == ds->counter)
  {
    /* we are the last, go ahead with dkg-key */
    GNUNET_free (ds->dso);
    ds->dso = NULL;
    start_dkg_key (ds);
  }
}


/**
 * FIXME
*/
static void
start_dkg_shares (struct FROSIX_DkgState *ds)
{
  ds->dso = GNUNET_malloc (ds->dkg_data->num_of_participants * sizeof (char *));
  ds->counter = 0;

  for (unsigned int i = 0; i < ds->dkg_data->num_of_participants; i++)
  {
    struct FROSIX_DkgData *dd = ds->dkg_data;
    struct FROSIX_DkgProvider *dp = &ds->dkg_data->providers[i];

    /* set up array of dkg commitments */
    struct FROSIX_DkgCommitment dkg_commitments[dd->num_of_participants - 1];
    for (unsigned int j = 0; j < dd->num_of_participants; j++)
    {
      if (j < i)
      {
        dkg_commitments[j] = dd->providers[j].commitment;
      }
      else if (j > i)
      {
        dkg_commitments[j - 1] = dd->providers[j].commitment;
      }
    }

    /* compute request id */
    struct FROSIX_DkgRequestIdP request_id;
    FROSIX_compute_dkg_request_id (
      &request_id,
      &dp->context_string,
      &dp->auth_hash,
      &dp->provider_salt,
      dp->provider_index,
      ds->dkg_data->num_of_participants,
      ds->dkg_data->threshold);

    ds->dso[i] = FROSIX_dkg_share_request (
      FROSIX_REDUX_ctx_,
      dp->backend_url,
      &request_id,
      dp->provider_index,
      dd->threshold,
      dd->num_of_participants,
      &dp->context_string,
      &dp->auth_hash,
      dkg_commitments,
      dd->num_of_participants - 1,
      dd->providers_public_keys,
      &dkg_share_request_cb,
      ds);

    ds->counter++;
  }

  /* free dkg commitments */
  for (unsigned int i = 0; i < ds->dkg_data->num_of_participants; i++)
  {
    FROST_free_dkg_commitment (
      &ds->dkg_data->providers[i].commitment.commitment,
      1);
  }
}


/**
 * Callback to process a POST /dkg-commitment request
 *
 * @param cls closure
 * @param dcd the decoded response body
*/
static void
dkg_commitment_request_cb (void *cls,
                           const struct FROSIX_DkgCommitmentRequestDetails *dcd)
{
  GNUNET_assert (NULL != cls);
  struct FROSIX_DkgState *ds = cls;

  if (0 >= ds->counter)
  {
    /* We haven't expected this callback, thus just ignore it */
    return;
  }

  /* check if the call was successful */
  if (0 == dcd->http_status)
  {
    ds->counter = 0;

    // FIXME: cancel all other running and not yet returned requests
    json_t *error_message;
    error_message = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_string ("Frosix_client_error",
                               "Error requesting dkg commitment from provider"));

    ds->cb (ds->cb_cls,
            0,
            error_message);

    return;
  }

  /* set error code */
  ds->error_code = dcd->ec;

  /* check if provider index is valid */
  GNUNET_assert (0 < dcd->provider_index);
  GNUNET_assert (ds->dkg_data->num_of_participants >= dcd->provider_index);

  /* check if our provider index is same as in the response */
  if (dcd->provider_index != dcd->dkg_commitment.identifier)
  {
    ds->counter--;

    // FIXME: cancel all other running and not yet returned requests
    json_t *error_message;
    error_message = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_string ("Frosix_client_error",
                               "Error in dkg commitment response"));

    ds->cb (ds->cb_cls,
            0,
            error_message);

    return;
  }

  /* check if length of commitments equals our threshold value */
  if (dcd->dkg_commitment.shares_commitments_length != ds->dkg_data->threshold)
  {
    ds->counter = 0;

    // FIXME: cancel all other running and not yet returned requests
    json_t *error_message;
    error_message = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_string ("Frosix_client_error",
                               "Error in dkg commitment response"));

    ds->cb (ds->cb_cls,
            0,
            error_message);

    return;
  }

  /* copy commitment in our struct */
  struct FROSIX_DkgProvider *dp = &ds->dkg_data->providers[dcd->provider_index
                                                           - 1];

  /* Fill return values in our central struct */
  memcpy (&dp->commitment.encryption_public_key,
          &dcd->public_key,
          sizeof (dcd->public_key));

  FROST_initialize_dkg_commitment (
    &dp->commitment.commitment,
    dcd->provider_index,
    dcd->dkg_commitment.shares_commitments_length);

  for (unsigned int i = 0; i < dcd->dkg_commitment.shares_commitments_length;
       i++)
  {
    memcpy (&dp->commitment.commitment.share_comm[i],
            &dcd->dkg_commitment.share_comm[i],
            sizeof (dcd->dkg_commitment.share_comm[i]));
  }

  memcpy (&dp->commitment.commitment.zkp.r,
          &dcd->dkg_commitment.zkp.r,
          sizeof (dcd->dkg_commitment.zkp.r));

  memcpy (&dp->commitment.commitment.zkp.z,
          &dcd->dkg_commitment.zkp.z,
          sizeof (dcd->dkg_commitment.zkp.z));

  /* call next dkg-commitment request */
  ds->counter--;
  if (0 == ds->counter)
  {
    /* we are the last, go ahead with dkg-share */
    GNUNET_free (ds->dco);
    ds->dco = NULL;
    start_dkg_shares (ds);
  }
}


/**
 * Function to derive the master key and all context strings.
 *
 * @param[in,out] dd the struct to fill in the entropy
 * @param[in] client_entropy high entropy bytes to derive more high entropy bytes
*/
static void
keygen_derive_entropy (struct FROSIX_DkgData *dd,
                       const struct GNUNET_HashCode *client_entropy,
                       const struct GNUNET_HashCode *provider_entropy)
{
  {
    /* derive high entropy bytes */
    size_t length = sizeof (dd->master_key)
                    + (dd->num_of_participants
                       * sizeof (dd->providers->context_string));

    char *derived_entropy = GNUNET_malloc (length);

    /* kill process if allocation failed */
    GNUNET_assert (NULL != derived_entropy);

    GNUNET_assert (GNUNET_CRYPTO_kdf (derived_entropy,
                                      length,
                                      "FROSIX",
                                      strlen ("FROSIX"),
                                      client_entropy,
                                      sizeof (*client_entropy),
                                      provider_entropy,
                                      sizeof (*provider_entropy),
                                      NULL,
                                      0));

    /* copy the derived bytes to the right place */
    unsigned int byte_counter = 0;
    memcpy (&dd->master_key,
            &derived_entropy[byte_counter],
            sizeof (dd->master_key));
    byte_counter += sizeof (dd->master_key);

    for (unsigned int i = 0; i < dd->num_of_participants; i++)
    {
      memcpy (&dd->providers[i].context_string,
              &derived_entropy[byte_counter],
              sizeof (dd->providers->context_string));
      byte_counter += sizeof (dd->providers->context_string);
    }

    /* free allocated memory */
    GNUNET_free (derived_entropy);
  }

  {
    /* derive authentication salts and pre encryption keys */
    size_t length = dd->num_of_participants
                    * (sizeof (dd->providers->hash_salt)
                       + sizeof (dd->providers->pre_enc_key));

    char *client_entropy = GNUNET_malloc (length);

    /* kill process if allocation failed */
    GNUNET_assert (NULL != client_entropy);

    GNUNET_CRYPTO_kdf (client_entropy,
                       length,
                       "FROSIX",
                       strlen ("FROSIX"),
                       &dd->master_key,
                       sizeof (dd->master_key),
                       NULL,
                       0);

    /* copy bytes to the right place */
    unsigned int byte_counter = 0;
    for (unsigned int i = 0; i < dd->num_of_participants; i++)
    {
      memcpy (&dd->providers[i].hash_salt,
              &client_entropy[byte_counter],
              sizeof (dd->providers->hash_salt));
      byte_counter += sizeof (dd->providers->hash_salt);
    }

    for (unsigned int i = 0; i < dd->num_of_participants; i++)
    {
      memcpy (&dd->providers[i].pre_enc_key,
              &client_entropy[byte_counter],
              sizeof (dd->providers->pre_enc_key));
      byte_counter += sizeof (dd->providers->pre_enc_key);
    }

    /* free allocated memory */
    GNUNET_free (client_entropy);
  }
}


/**
 * FIXME
*/
static void
merge_provider_entropy (struct GNUNET_HashCode *provider_entropy,
                        const struct FROSIX_DkgState *ds)
{
  struct GNUNET_HashContext *hc = GNUNET_CRYPTO_hash_context_start ();
  for (unsigned int i = 0; i < ds->dkg_data->num_of_participants; i++)
  {
    GNUNET_CRYPTO_hash_context_read (hc,
                                     ds->dkg_data->providers[i].seed,
                                     sizeof (ds->dkg_data->providers[i].seed));
  }

  GNUNET_CRYPTO_hash_context_finish (hc,
                                     provider_entropy);
}


/**
 * FIXME
*/
static void
start_dkg_commitments (struct FROSIX_DkgState *ds)
{
  /* measure cpu speed */
  // unsigned int target_difficulty = 5000; // FIXME
  unsigned int amplifier = 1;
  {
    struct FROST_PowSalt salt;
    GNUNET_CRYPTO_random_block (GNUNET_CRYPTO_QUALITY_NONCE,
                                &salt,
                                sizeof (salt));

    /* start */
    // clock_t start = clock ();
    struct FROST_HashCode hash;
    FROST_pow_hash (&hash,
                    "Frosix",
                    strlen ("Frosix"),
                    &salt,
                    1);
    /* end */
    // clock_t end = clock ();
    // float seconds = (float) (end - start) / 1000;
    // amplifier = target_difficulty / seconds;

  }

  /* merge provider seeds to a single hash code */
  struct GNUNET_HashCode provider_entropy;
  merge_provider_entropy (&provider_entropy,
                          ds);

  /* derive master key, context strings, salts and pre encryption keys */
  struct GNUNET_HashCode client_entropy;
  GNUNET_CRYPTO_random_block (GNUNET_CRYPTO_QUALITY_NONCE,
                              &client_entropy,
                              sizeof (client_entropy));

  keygen_derive_entropy (ds->dkg_data,
                         &client_entropy,
                         &provider_entropy);

  /* compute salted hash of authentication data */
  for (unsigned int i = 0; i < ds->dkg_data->num_of_participants; i++)
  {
    struct FROSIX_DkgProvider *dp = &ds->dkg_data->providers[i];
    if (NULL != dp->auth_answer)
    {
      /* we have a security question here */
      struct GNUNET_CRYPTO_Edx25519PrivateKey priv;
      struct GNUNET_CRYPTO_Edx25519PublicKey pub;
      FROSIX_hash_pow (&dp->auth_hash.hash,
                       &priv,
                       &pub,
                       &dp->hash_salt,
                       dp->auth_answer,
                       amplifier);
    }
    else
    {
      FROSIX_compute_auth_hash (&dp->auth_hash,
                                dp->auth_data,
                                &dp->hash_salt);
    }
  }

  ds->dco = GNUNET_malloc (ds->dkg_data->num_of_participants * sizeof (char *));
  ds->counter = 0;

  for (unsigned int i = 0; i < ds->dkg_data->num_of_participants; i++)
  {
    struct FROSIX_DkgProvider *dp = &ds->dkg_data->providers[i];

    /* compute request id */
    struct FROSIX_DkgRequestIdP request_id;
    FROSIX_compute_dkg_request_id (
      &request_id,
      &dp->context_string,
      &dp->auth_hash,
      &dp->provider_salt,
      dp->provider_index,
      ds->dkg_data->num_of_participants,
      ds->dkg_data->threshold);

    ds->dco[i] = FROSIX_dkg_commitment_request (
      FROSIX_REDUX_ctx_,
      dp->backend_url,
      &request_id,
      dp->provider_index,
      ds->dkg_data->threshold,
      ds->dkg_data->num_of_participants,
      &dp->context_string,
      &dp->auth_hash,
      ds->dkg_data->providers_public_keys,
      &dkg_commitment_request_cb,
      ds);

    ds->counter++;
  }
}


/**
 Callback to process a GET /seed request
 *
 * @param cls closure
 * @param ps the decoded response body
*/
static void
seed_request_cb (void *cls,
                 unsigned int http_status,
                 const struct FROSIX_ProviderSeed *ps)
{
  GNUNET_assert (NULL != cls);
  struct FROSIX_DkgState *ds = cls;

  if (0 >= ds->counter)
  {
    /* We haven't expected this callback, thus just ignore it */
    return;
  }

  /* check if the call was successful */
  if (0 == http_status)
  {
    ds->counter = 0;

    // FIXME: cancel all other running and not yet returned requests
    json_t *error_message;
    error_message = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_string ("Frosix_client_error",
                               "Error requesting seed from provider"));

    ds->cb (ds->cb_cls,
            0,
            error_message);

    return;
  }

  /* check if provider index is valid */
  GNUNET_assert (0 < ps->provider_index);
  GNUNET_assert (ds->dkg_data->num_of_participants >= ps->provider_index);

  /* copy seed to our main struct */
  struct FROSIX_DkgProvider *dp = &ds->dkg_data->providers[ps->provider_index
                                                           - 1];

  memcpy (&dp->seed,
          ps->seed,
          FROSIX_SEED_SIZE);

  ds->counter--;
  if (0 == ds->counter)
  {
    GNUNET_free (ds->sgo);
    ds->sgo = NULL;
    /* we are the last, go ahead with dkg-commitment */
    start_dkg_commitments (ds);
  }
}


/**
 * FIXME
*/
static void
start_seed_requests (struct FROSIX_DkgState *ds)
{
  ds->counter = 0;
  ds->sgo = GNUNET_malloc (ds->dkg_data->num_of_participants * sizeof (char *));

  for (unsigned int i = 0; i < ds->dkg_data->num_of_participants; i++)
  {
    ds->sgo[i] = FROSIX_seed_get (FROSIX_REDUX_ctx_,
                                  ds->dkg_data->providers[i].backend_url,
                                  i + 1,
                                  &seed_request_cb,
                                  ds);
    ds->counter++;
  }
}


/**
 * Callback to process a GET /config request
 *
 * @param cls closure
 * @param fcfg the decoded response body
*/
static void
config_request_cb (void *cls,
                   unsigned int http_status,
                   const struct FROSIX_Config *fcfg)
{
  GNUNET_assert (NULL != cls);
  struct FROSIX_DkgState *ds = cls;

  if (0 >= ds->counter)
  {
    /* We haven't expected this callback, thus just ignore it */
    return;
  }

  /* check if the call was successful */
  if (0 == http_status)
  {
    ds->counter = 0;

    // FIXME: cancel all other running and not yet returned requests
    json_t *error_message;
    error_message = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_string ("Frosix_client_error",
                               "Error requesting config from provider"));

    ds->cb (ds->cb_cls,
            0,
            error_message);

    return;
  }

  /* check if provider index is valid */
  GNUNET_assert (0 < fcfg->provider_index);
  GNUNET_assert (ds->dkg_data->num_of_participants >= fcfg->provider_index);

  {
    /* copy config data to our main struct */
    struct FROSIX_DkgProvider *dp =
      &ds->dkg_data->providers[fcfg->provider_index - 1];

    size_t len = strlen (fcfg->business_name);
    dp->provider_name = strndup (fcfg->business_name,
                                 len);

    memcpy (&dp->provider_salt,
            &fcfg->provider_salt,
            sizeof (fcfg->provider_salt));

    memcpy (&dp->provider_public_key,
            &fcfg->public_key,
            sizeof (fcfg->public_key));
  }

  ds->counter--;
  if (0 == ds->counter)
  {
    GNUNET_free (ds->co);
    ds->co = NULL;

    /* we are the last, build array of public keys
    and go ahead with the seeds */
    keygen_build_public_key_array (ds->dkg_data);

    start_seed_requests (ds);
  }
};


/**
 * FIXME
*/
static void
start_config_requests (struct FROSIX_DkgState *ds)
{
  ds->counter = 0;
  ds->co = GNUNET_malloc (ds->dkg_data->num_of_participants * sizeof (char *));

  for (unsigned int i = 0; i < ds->dkg_data->num_of_participants; i++)
  {
    ds->dkg_data->providers[i].provider_index = i + 1;
    ds->co[i] = FROSIX_get_config (FROSIX_REDUX_ctx_,
                                   ds->dkg_data->providers[i].backend_url,
                                   i + 1,
                                   &config_request_cb,
                                   ds);
    ds->counter++;
  }
}


/**
 * Helper function to parse a keygen input
 *
 * @param[in,out] dkg_data A initialized struct
 * @param[in] arguments Input from the cli
*/
enum GNUNET_GenericReturnValue
parse_keygen_arguments (struct FROSIX_DkgData *dkg_data,
                        const json_t *arguments)
{
  json_t *providers = NULL;

  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_uint8 ("threshold",
                            &dkg_data->threshold),
    GNUNET_JSON_spec_uint64 ("expiration",
                             &dkg_data->expiration),
    GNUNET_JSON_spec_json ("providers",
                           &providers),
    GNUNET_JSON_spec_end ()
  };

  if (GNUNET_OK != GNUNET_JSON_parse (arguments,
                                      spec,
                                      NULL,
                                      NULL))
  {
    GNUNET_break (0);
    GNUNET_JSON_parse_free (spec);
    return GNUNET_NO;
  }

  /* get number of providers */
  size_t num_of_providers = json_array_size (providers);

  /* validate number of providers */
  if (num_of_providers > 254
      || num_of_providers < 2
      || dkg_data->threshold < 1
      || num_of_providers <= dkg_data->threshold)
    return GNUNET_NO;

  dkg_data->num_of_participants = num_of_providers;

  /* initialize providers */
  dkg_data->providers =  GNUNET_new_array (dkg_data->num_of_participants,
                                           struct FROSIX_DkgProvider);

  /* parse provider data */
  for (unsigned int i = 0; i < dkg_data->num_of_participants; i++)
  {
    struct GNUNET_JSON_Specification prov_spec[] = {
      GNUNET_JSON_spec_string ("url",
                               (const
                                char**) &dkg_data->providers[i].backend_url),
      GNUNET_JSON_spec_fixed_auto ("public_key",
                                   &dkg_data->providers[i].provider_public_key),
      GNUNET_JSON_spec_string ("auth_method",
                               (const
                                char**) &dkg_data->providers[i].auth_method),
      GNUNET_JSON_spec_string ("auth_data",
                               (const
                                char**) &dkg_data->providers[i].auth_data),
      GNUNET_JSON_spec_mark_optional (
        GNUNET_JSON_spec_string ("auth_answer",
                                 (const
                                  char**) &dkg_data->providers[i].auth_answer),
        NULL),
      GNUNET_JSON_spec_end ()
    };

    if (GNUNET_OK != GNUNET_JSON_parse (json_array_get (providers,
                                                        i),
                                        prov_spec,
                                        NULL,
                                        NULL))
    {
      GNUNET_break (0);
      GNUNET_JSON_parse_free (prov_spec);
      GNUNET_JSON_parse_free (spec);
      return GNUNET_NO;
    }

    GNUNET_JSON_parse_free (prov_spec);
  }

  GNUNET_JSON_parse_free (spec);
  return GNUNET_OK;
}


struct FROSIX_ReduxAction *
FROSIX_redux_keygen_start (const json_t *arguments,
                           FROSIX_ActionCallback cb,
                           void *cb_cls)
{
  /* initialize dkg data struct */
  struct FROSIX_DkgData *dkg_data = GNUNET_new (struct FROSIX_DkgData);

  /* parse arguments from cli */
  if (GNUNET_OK != parse_keygen_arguments (dkg_data,
                                           arguments))
  {
    free_dkg_data_struct (dkg_data);
    GNUNET_free (dkg_data);
    json_t *error_message;
    error_message = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_string ("Frosix_client_error",
                               "Error while parsing input"));

    cb (cb_cls,
        0,
        error_message);

    return NULL;
  }

  /* lets create a state */
  struct FROSIX_DkgState *ds = GNUNET_new (struct FROSIX_DkgState);
  ds->cb = cb;
  ds->cb_cls = cb_cls;
  ds->dkg_data = dkg_data;
  ds->ra.cleanup = &keygen_cancel_cb;
  ds->ra.cleanup_cls = ds;

  /* get configs from all parsed providers */
  start_config_requests (ds);

  return &ds->ra;
}