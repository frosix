/*
  This file is part of Frosix
  Copyright (C) 2020, 2021, 2022 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Frosix; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file libfrosix/frosix_api.c
 * @brief frosix client api
 * @author Christian Grothoff
 * @author Dominik Meister
 * @author Dennis Neufeld
 */
#include <platform.h>
#include <jansson.h>
#include "frosix.h"
// #include "anastasis_error_codes.h"
#include <taler/taler_json_lib.h>
#include "frosix_api.h"
#include "frost_verify.h"
#include "frost_high.h"
#include "frost_low.h"
#include "frost_high.h"


/**
 * Reducer API's CURL context handle.
 */
struct GNUNET_CURL_Context *FROSIX_REDUX_ctx_;

void
FROSIX_redux_fail_ (FROSIX_ActionCallback cb,
                    void *cb_cls,
                    enum TALER_ErrorCode ec,
                    const char *detail)
{
  json_t *estate;

  estate = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_allow_null (
      GNUNET_JSON_pack_string ("detail",
                               detail)),
    GNUNET_JSON_pack_string ("reducer_type",
                             "error"),
    GNUNET_JSON_pack_uint64 ("code",
                             ec),
    GNUNET_JSON_pack_string ("hint",
                             TALER_ErrorCode_get_hint (ec)));
  cb (cb_cls,
      ec,
      estate);
  json_decref (estate);
}


void
FROSIX_redux_init (struct GNUNET_CURL_Context *ctx)
{
  FROSIX_REDUX_ctx_ = ctx;
}


void
FROSIX_redux_action_cancel (struct FROSIX_ReduxAction *ra)
{
  ra->cleanup (ra->cleanup_cls);
}


enum GNUNET_GenericReturnValue
FROSIX_verify_signature (const char *message,
                         const json_t *arguments)
{
  /* parse json */
  struct FROST_MessageHash mh;
  struct FROST_PublicKey pk;
  struct FROST_Signature sig;

  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_fixed_auto ("message_hash",
                                 &mh),
    GNUNET_JSON_spec_fixed_auto ("public_key",
                                 &pk),
    GNUNET_JSON_spec_fixed_auto ("signature_r",
                                 &sig.r),
    GNUNET_JSON_spec_fixed_auto ("signature_z",
                                 &sig.z),
    GNUNET_JSON_spec_end ()
  };

  if (GNUNET_OK != GNUNET_JSON_parse (arguments,
                                      spec,
                                      NULL,
                                      NULL))
  {
    GNUNET_break (0);
    GNUNET_JSON_parse_free (spec);
    return GNUNET_NO;
  }

  /* check if hash of message matches hash in signature */
  struct FROST_MessageHash input_mh;
  FROST_message_to_hash (&input_mh,
                         message,
                         strlen (message));

  if (GNUNET_OK != FROST_hash_cmp (&mh.hash,
                                   &input_mh.hash))
    return GNUNET_NO;

  return FROST_verify_signature (&pk,
                                 &sig,
                                 &mh);
}


void
FROSIX_export_public_key (const json_t *arguments,
                          FROSIX_ActionCallback cb,
                          void *cb_cls)
{
  json_t *result = json_object ();

  /* public key*/
  json_object_set (result,
                   "public_key",
                   json_object_get (arguments,
                                    "public_key"));

  /* provider array */
  json_t *providers = json_object_get (arguments,
                                       "providers");
  size_t num_of_providers = json_array_size (providers);

  /* go through each provider */
  json_t *temp_providers = json_array ();
  for (size_t i = 0; i < num_of_providers; i++)
  {
    json_t *provider_i = json_array_get (providers,
                                         i);

    json_t *temp_provider = json_object ();

    json_object_set (temp_provider,
                     "provider_name",
                     json_object_get (provider_i,
                                      "provider_name"));

    json_object_set (temp_provider,
                     "backend_url",
                     json_object_get (provider_i,
                                      "backend_url"));

    /* parse authentication data and compute salted hash */
    {
      struct FROSIX_ChallengeHashP auth_hash;
      /*const char *auth_method = NULL;
      const char *auth_data = NULL;
      const char *auth_answer = NULL;
      struct GNUNET_HashCode auth_nonce;

      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_string ("auth_method",
                                 &auth_method),
        GNUNET_JSON_spec_string ("auth_data",
                                 &auth_data),
        GNUNET_JSON_spec_fixed_auto ("auth_nonce",
                                     &auth_nonce),
        GNUNET_JSON_spec_mark_optional (
        GNUNET_JSON_spec_string ("auth_answer",
                                 &auth_answer),
                                 NULL),
        GNUNET_JSON_spec_end ()
      };*/

      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_fixed_auto ("auth_hash",
                                     &auth_hash),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK != GNUNET_JSON_parse (provider_i,
                                          spec,
                                          NULL,
                                          NULL))
      {
        GNUNET_break (0);
        GNUNET_JSON_parse_free (spec);
        return;
      }

      /*FROSIX_compute_auth_hash (&auth_hash,
                                      auth_data,
                                      &auth_nonce);*/

      json_t *j_hash;
      j_hash = GNUNET_JSON_PACK (
        GNUNET_JSON_pack_data_auto ("auth_hash",
                                    &auth_hash));

      json_object_update_new (temp_provider,
                              j_hash);

      GNUNET_JSON_parse_free (spec);
    }


    json_object_set (temp_provider,
                     "provider_signature",
                     json_object_get (provider_i,
                                      "provider_signature"));

    json_object_set (temp_provider,
                     "provider_public_key",
                     json_object_get (provider_i,
                                      "provider_public_key"));

    json_array_append_new (temp_providers,
                           temp_provider);
  }

  json_object_set_new (result,
                       "providers",
                       temp_providers);

  enum TALER_ErrorCode error_code = TALER_EC_NONE;
  cb (cb_cls,
      error_code,
      result);
}


enum GNUNET_GenericReturnValue
FROSIX_verify_public_key (const json_t *arguments)
{
  struct FROST_PublicKey pk;
  json_t *providers;

  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_fixed_auto ("public_key",
                                 &pk),
    GNUNET_JSON_spec_json ("providers",
                           &providers),
    GNUNET_JSON_spec_end ()
  };

  if (GNUNET_OK != GNUNET_JSON_parse (arguments,
                                      spec,
                                      NULL,
                                      NULL))
  {
    GNUNET_break (0);
    GNUNET_JSON_parse_free (spec);
    return GNUNET_NO;
  }

  unsigned int len = json_array_size (providers);

  for (unsigned int i = 0; i < len; i++)
  {
    struct FROSIX_ChallengeHashP ch;
    struct GNUNET_CRYPTO_EddsaSignature sig;
    struct GNUNET_CRYPTO_EddsaPublicKey provider_pk;

    struct GNUNET_JSON_Specification provider_spec[] = {
      GNUNET_JSON_spec_fixed_auto ("auth_hash",
                                   &ch),
      GNUNET_JSON_spec_fixed_auto ("provider_signature",
                                   &sig),
      GNUNET_JSON_spec_fixed_auto ("provider_public_key",
                                   &provider_pk),
      GNUNET_JSON_spec_end ()
    };

    if (GNUNET_OK != GNUNET_JSON_parse (json_array_get (providers,
                                                        i),
                                        provider_spec,
                                        NULL,
                                        NULL))
    {
      GNUNET_break (0);
      GNUNET_JSON_parse_free (provider_spec);
      GNUNET_JSON_parse_free (spec);
      return GNUNET_NO;
    }

    struct FROSIX_DkgKeySignaturePS ks = {
      .purpose.purpose = htonl (104),
      .purpose.size = htonl (sizeof (ks)),
      .public_key = pk,
      .auth_hash = ch,
    };

    if (GNUNET_OK != GNUNET_CRYPTO_eddsa_verify (
          104,
          &ks,
          &sig,
          &provider_pk))
    {
      GNUNET_JSON_parse_free (provider_spec);
      GNUNET_JSON_parse_free (spec);
      return GNUNET_NO;
    }

    GNUNET_JSON_parse_free (provider_spec);
  }

  GNUNET_JSON_parse_free (spec);
  return GNUNET_OK;
}