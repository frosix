/*
  This file is part of Frosix
  Copyright (C) 2020, 2021 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Frosix; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file reducer/frosix_api_delete-key.c
 * @brief frosix reducer key delete api
 * @author Christian Grothoff
 * @author Dominik Meister
 * @author Dennis Neufeld
 * @author Joel Urech
 */

#include "platform.h"
#include "frost_verify.h"
#include "frosix.h"
#include "frosix_api.h"
#include <taler/taler_merchant_service.h>

/**
 * FIXME
*/
struct FROSIX_KeyDeleteProvider
{
  /**
   * URL of the provider.
  */
  char *backend_url;

  /**
   * Encryption key
  */
  struct FROSIX_EncryptionKey enc_key;
};

/**
 * FIXME
*/
struct FROSIX_KeyDeleteData
{
  /**
   * Pointer to a list of providers, length is @e num_of_providers
  */
  struct FROSIX_KeyDeleteProvider *providers;

  /**
   * How many providers do we have?
  */
  uint8_t num_of_providers;
};


/**
 * State of a sign procedure
*/
struct FROSIX_KeyDeleteState
{
  /**
   * FIXME
  */
  struct FROSIX_KeyDeleteData *key_del_data;

  /**
   * Function to call when we are done.
   */
  FROSIX_ActionCallback cb;

  /**
   * Closure for @e cb.
   */
  void *cb_cls;

  /**
   * Redux action we returned to our controller.
   */
  struct FROSIX_ReduxAction ra;

  /**
   * Number of provider /config operations in @e ba_head that
   * are still awaiting completion.
   */
  uint8_t counter;

  /**
   * Last error code
  */
  enum TALER_ErrorCode error_code;

  /**
    * Array of config requests, with length `num_of_participants`
  */
  struct FROSIX_KeyDeleteOperation **kdo;
};


/**
 * Function to free all initialized memory of a provider from a
 * `struct FROSIX_KeyDeleteProvider`
 *
 * @param fp the struct to clean up
*/
static void
free_provider_struct (struct FROSIX_KeyDeleteProvider *kp)
{
  if (NULL != kp->backend_url)
    GNUNET_free (kp->backend_url);
}


/**
 * Function to free all initialized memory of a provider from a
 * `struct FROSIX_KeyDeleteData`
 *
 * @param dd the struct to clean up
*/
static void
free_data_struct (struct FROSIX_KeyDeleteData *dd)
{
  if (NULL != dd->providers)
  {
    for (unsigned int i = 0; i < dd->num_of_providers; i++)
    {
      if (NULL != &dd->providers[i])
        free_provider_struct (&dd->providers[i]);
    }

    GNUNET_free (dd->providers);
  }

  GNUNET_free (dd);
}


/**
 * Function called when signature process is being aborted.
 * Frees all initialized memory!
 *
 * @param cls a `struct FROSIX_KeyDeleteState`
*/
static void
key_delete_cancel_cb (void *cls)
{
  struct FROSIX_KeyDeleteState *kds = cls;

  if (NULL != kds->key_del_data)
    free_data_struct (kds->key_del_data);

  /* free operations */
  if (NULL != kds->kdo)
    GNUNET_free (kds->kdo);

  GNUNET_free (kds);
}


/**
 * Callback to process a POST /sig-commitment request
 *
 * @param cls closure
 * @param scd the decoded response body
*/
static void
key_delete_request_cb (
  void *cls,
  const struct FROSIX_KeyDeleteDetails *kdd)
{
  GNUNET_assert (NULL != cls);
  struct FROSIX_KeyDeleteState *kds = cls;

  /* set error code */
  kds->error_code = kdd->ec;

  /* check if this is a valid callback */
  GNUNET_assert (0 < kds->counter);
  GNUNET_assert (0 != kdd->http_status);

  /* call next dkg-commitment request */
  kds->counter--;
  if (0 == kds->counter)
  {
    /* we are the last, go ahead with sig-share */
    free_data_struct (kds->key_del_data);

    kds->cb (kds->cb_cls,
             kds->error_code,
             NULL);
  }
}


/**
 * FIXME
*/
static void
start_key_delete (struct FROSIX_KeyDeleteState *kds)
{
  kds->kdo = GNUNET_malloc (
    kds->key_del_data->num_of_providers * sizeof (char *));

  kds->counter = 0;
  for (unsigned int i = 0; i < kds->key_del_data->num_of_providers; i++)
  {
    struct FROSIX_KeyDeleteProvider *kp = &kds->key_del_data->providers[i];

    /* hash encryption key to get request id*/
    struct FROSIX_DkgRequestIdP request_id;
    FROSIX_hash_encryption_key (&request_id.id,
                                &kp->enc_key);

    kds->kdo[i] = FROSIX_key_delete (
      FROSIX_REDUX_ctx_,
      kp->backend_url,
      &request_id,
      &key_delete_request_cb,
      kds);

    kds->counter++;
  }
}


/**
 * Helper function to parse a keygen input
 *
 * @param[in,out] kdd A initialized struct
 * @param[in] input Input from the cli
*/
enum GNUNET_GenericReturnValue
parse_key_delete_input (struct FROSIX_KeyDeleteData *kdd,
                        const json_t *input)
{
  json_t *providers = NULL;

  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_uint8 ("number_of_participants",
                            &kdd->num_of_providers),
    GNUNET_JSON_spec_json ("providers",
                           &providers),
    GNUNET_JSON_spec_end ()
  };

  if (GNUNET_OK != GNUNET_JSON_parse (input,
                                      spec,
                                      NULL,
                                      NULL))
  {
    GNUNET_break (0);
    GNUNET_JSON_parse_free (spec);
    return GNUNET_NO;
  }

  /* get number of providers */
  size_t parsed_providers_len = json_array_size (providers);

  /* validate number of providers */
  GNUNET_assert (kdd->num_of_providers == parsed_providers_len);
  GNUNET_assert (254 > kdd->num_of_providers);
  GNUNET_assert (1 < kdd->num_of_providers);

  /* initialize providers */
  kdd->providers =  GNUNET_new_array (kdd->num_of_providers,
                                      struct FROSIX_KeyDeleteProvider);

  /* parse provider data */
  for (unsigned int i = 0; i < kdd->num_of_providers; i++)
  {
    struct GNUNET_JSON_Specification prov_spec[] = {
      GNUNET_JSON_spec_string ("backend_url",
                               (const
                                char**) &kdd->providers[i].backend_url),
      GNUNET_JSON_spec_fixed_auto ("encryption_key",
                                   &kdd->providers[i].enc_key),
      GNUNET_JSON_spec_end ()
    };

    if (GNUNET_OK != GNUNET_JSON_parse (json_array_get (providers,
                                                        i),
                                        prov_spec,
                                        NULL,
                                        NULL))
    {
      GNUNET_break (0);
      GNUNET_JSON_parse_free (prov_spec);
      GNUNET_JSON_parse_free (spec);
      return GNUNET_NO;
    }

    GNUNET_JSON_parse_free (prov_spec);
  }

  GNUNET_JSON_parse_free (spec);

  return GNUNET_OK;
}

/**
 *
*/
struct FROSIX_ReduxAction *
FROSIX_redux_key_delete_start (json_t *input,
                               FROSIX_ActionCallback cb,
                               void *cb_cls)
{
  struct FROSIX_KeyDeleteData *key_del_data = GNUNET_new (struct
                                                          FROSIX_KeyDeleteData);

  /* parse arguments from cli */
  if (GNUNET_OK != parse_key_delete_input (key_del_data,
                                           input))
  {
    free_data_struct (key_del_data);

    // FIXME: Return some useful error message
    return NULL;
  }

  struct FROSIX_KeyDeleteState *kds = GNUNET_new (struct FROSIX_KeyDeleteState);

  kds->cb = cb;
  kds->cb_cls = cb_cls;
  kds->key_del_data = key_del_data;
  kds->ra.cleanup = &key_delete_cancel_cb;
  kds->ra.cleanup_cls = kds;

  /* get commitments from all parsed providers */
  start_key_delete (kds);

  return &kds->ra;
}