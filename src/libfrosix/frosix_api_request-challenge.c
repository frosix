/*
  This file is part of Frosix
  Copyright (C) 2020, 2021 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Frosix; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file reducer/frosix_api_authenticate.c
 * @brief frosix reducer sign api
 * @author Christian Grothoff
 * @author Dominik Meister
 * @author Dennis Neufeld
 * @author Joel Urech
 */

#include "platform.h"
#include "frost_verify.h"
#include "frosix.h"
#include "frosix_api.h"
#include <taler/taler_merchant_service.h>


/**
 * FIXME
*/
struct FROSIX_Provider
{
  /**
   * URL of the provider.
  */
  char *backend_url;

  /**
   * Index of the provider
  */
  uint8_t provider_index;

  /**
   * Authentication method.
  */
  char *auth_method;

  /**
   * Authentication data.
  */
  char *auth_data;

  /**
   * Salt for the hash of the authentication data
  */
  struct GNUNET_HashCode auth_nonce;

  /**
   * Encryption key
  */
  struct FROSIX_EncryptionKey enc_key;
};

/**
 * Struct to store all parsed data from the cli and /config and /seed
*/
struct FROSIX_ChallengeRequestData
{
  /**
   * Our hashed message.
  */
  struct FROST_MessageHash message_hash;

  /**
   * Our provider
  */
  struct FROSIX_Provider provider;
};

/**
 * State of a challenge request procedure
*/
struct FROSIX_ChallengeRequestState
{
  /**
   * State we are updating.
   */
  struct FROSIX_ChallengeRequestData *challenge_data;

  /**
   * Function to call when we are done.
   */
  FROSIX_ActionCallback cb;

  /**
   * Closure for @e cb.
   */
  void *cb_cls;

  /**
   * Redux action we returned to our controller.
   */
  struct FROSIX_ReduxAction ra;

  /**
   * Last error code
  */
  enum TALER_ErrorCode error_code;

  /**
   * Response as JSON
  */
  json_t *response;

  /**
   * Pointer to our AuthChallengeRequestOperation
  */
  struct FROSIX_ChallengeRequestOperation *ao;
};


/**
 * Function to free all initialized memory of a provider from a
 * `struct FROSIX_Provider`
 *
 * @param fp the struct to clean up
*/
static void
free_provider_struct (struct FROSIX_Provider *fp)
{
  if (NULL != fp->backend_url)
    GNUNET_free (fp->backend_url);

  if (NULL != fp->auth_method)
    GNUNET_free (fp->auth_method);

  if (NULL != fp->auth_data)
    GNUNET_free (fp->auth_data);
}


/**
 * Function called when challenge-request process is being aborted.
 * Frees all initialized memory!
 *
 * @param cls a `struct FROSIX_ChallengeRequestState`
*/
static void
challenge_request_cancel_cb (void *cls)
{
  struct FROSIX_ChallengeRequestState *cs = cls;

  if (NULL != cs->challenge_data)
  {
    free_provider_struct (&cs->challenge_data->provider);
    GNUNET_free (cs->challenge_data);
  }

  GNUNET_free (cs);
}


/**
 * FIXME
*/
static void
return_feedback (struct FROSIX_ChallengeRequestState *cs)
{
  /* return empty */
  cs->cb (cs->cb_cls,
          cs->error_code,
          cs->response);
}


/**
 * Callback to process a POST /auth-challenge request
 *
 * @param cls closure
 * @param scd the decoded response body
*/
static void
challenge_request_cb (void *cls,
                      const struct FROSIX_ChallengeRequestDetails *crd)
{
  GNUNET_assert (NULL != cls);
  struct FROSIX_ChallengeRequestState *cs = cls;

  /* set error code */
  cs->error_code = crd->ec;
  cs->response = json_deep_copy (crd->response);

  /* call callback and give a return message to the user */
  return_feedback (cs);
}


/**
 * FIXME
*/
static void
start_challenge_request (struct FROSIX_ChallengeRequestState *cs)
{
  /* calculate request id*/
  struct FROSIX_ChallengeRequestIdP request_id;
  FROSIX_compute_challenge_request_id (
    &request_id,
    &cs->challenge_data->provider.enc_key,
    &cs->challenge_data->message_hash);

  cs->ao = FROSIX_auth_challenge_post (
    FROSIX_REDUX_ctx_,
    cs->challenge_data->provider.backend_url,
    &request_id,
    &cs->challenge_data->provider.enc_key,
    cs->challenge_data->provider.auth_method,
    cs->challenge_data->provider.auth_data,
    &cs->challenge_data->provider.auth_nonce,
    &cs->challenge_data->message_hash,
    &challenge_request_cb,
    cs);
}


/**
 * Helper function to parse a keygen input
 *
 * @param[in,out] dkg_data A initialized struct
 * @param[in] arguments Input from the cli
*/
enum GNUNET_GenericReturnValue
parse_challenge_request_arguments (
  struct FROSIX_ChallengeRequestData *challenge_data,
  const json_t *arguments,
  const json_t *input)
{
  json_t *providers = NULL;

  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_json ("providers",
                           &providers),
    GNUNET_JSON_spec_end ()
  };

  if (GNUNET_OK != GNUNET_JSON_parse (arguments,
                                      spec,
                                      NULL,
                                      NULL))
  {
    GNUNET_break (0);
    GNUNET_JSON_parse_free (spec);
    return GNUNET_NO;
  }

  /* check number of parsed providers */
  size_t max_num = json_array_size (providers);

  /* enforce that only one provider is selected */
  GNUNET_assert (1 == json_object_size (input));

  struct GNUNET_JSON_Specification selection_spec[] = {
    GNUNET_JSON_spec_uint8 ("provider_index",
                            &challenge_data->provider.provider_index),
    GNUNET_JSON_spec_end ()
  };

  if (GNUNET_OK != GNUNET_JSON_parse (input,
                                      selection_spec,
                                      NULL,
                                      NULL))
  {
    GNUNET_break (0);
    GNUNET_JSON_parse_free (selection_spec);
    GNUNET_JSON_parse_free (spec);
    return GNUNET_NO;
  }

  GNUNET_JSON_parse_free (selection_spec);

  /* check if our selected provider is in the keyfile */
  GNUNET_assert (max_num > challenge_data->provider.provider_index);
  GNUNET_assert (0 < challenge_data->provider.provider_index);
  uint8_t parsed_provider_index;

  /* parse provider data */
  struct GNUNET_JSON_Specification prov_spec[] = {
    GNUNET_JSON_spec_uint8 ("provider_index",
                            &parsed_provider_index),
    GNUNET_JSON_spec_string (
      "backend_url",
      (const char**) &challenge_data->provider.backend_url),
    GNUNET_JSON_spec_fixed_auto ("encryption_key",
                                 &challenge_data->provider.enc_key),
    GNUNET_JSON_spec_string (
      "auth_method",
      (const char**) &challenge_data->provider.auth_method),
    GNUNET_JSON_spec_string (
      "auth_data",
      (const char**) &challenge_data->provider.auth_data),
    GNUNET_JSON_spec_fixed_auto ("auth_nonce",
                                 &challenge_data->provider.auth_nonce),
    GNUNET_JSON_spec_end ()
  };

  if (GNUNET_OK != GNUNET_JSON_parse (
        json_array_get (
          providers,
          challenge_data->provider.provider_index - 1),
        prov_spec,
        NULL,
        NULL))
  {
    GNUNET_break (0);
    GNUNET_JSON_parse_free (prov_spec);
    GNUNET_JSON_parse_free (spec);
    return GNUNET_NO;
  }

  GNUNET_JSON_parse_free (prov_spec);
  GNUNET_JSON_parse_free (spec);

  /* check if parsed provider has correct index */
  GNUNET_assert (parsed_provider_index ==
                 challenge_data->provider.provider_index);

  return GNUNET_OK;
}


/**
 *
*/
struct FROSIX_ReduxAction *
FROSIX_redux_challenge_request_start (const json_t *arguments,
                                      const json_t *input,
                                      const char *message,
                                      FROSIX_ActionCallback cb,
                                      void *cb_cls)
{
  /* initialize signature data struct */
  struct FROSIX_ChallengeRequestData *challenge_data =
    GNUNET_new (struct FROSIX_ChallengeRequestData);

  /* parse arguments from cli */
  if (GNUNET_OK != parse_challenge_request_arguments (challenge_data,
                                                      arguments,
                                                      input))
  {
    // free_challenge_request_data_struct (challenge_data);

    // FIXME: Return some useful error message
    return NULL;
  }

  /* hash message */
  FROST_message_to_hash (&challenge_data->message_hash,
                         message,
                         strlen (message));

  /* lets create a state */
  struct FROSIX_ChallengeRequestState *cs = GNUNET_new (struct
                                                        FROSIX_ChallengeRequestState);
  cs->cb = cb;
  cs->cb_cls = cb_cls;
  cs->challenge_data = challenge_data;
  cs->ra.cleanup = &challenge_request_cancel_cb;
  cs->ra.cleanup_cls = cs;

  /* get commitments from all parsed providers */
  start_challenge_request (cs);

  return &cs->ra;
}