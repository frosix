/*
  This file is part of Frosix
  Copyright (C) 2020, 2021 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Frosix; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file reducer/frosix_api_sign_redux.c
 * @brief frosix reducer sign api
 * @author Christian Grothoff
 * @author Dominik Meister
 * @author Dennis Neufeld
 * @author Joel Urech
 */

#include "platform.h"
#include "frost_verify.h"
#include "frosix.h"
#include "frosix_api.h"
#include <taler/taler_merchant_service.h>


/**
 * FIXME
*/
struct FROSIX_Provider
{
  /**
   * URL of the provider.
  */
  char *backend_url;

  /**
   * Index of the provider
  */
  uint8_t provider_index;

  /**
   * Authentication data.
  */
  struct GNUNET_HashCode auth_data;

  /**
   *
  */
  struct GNUNET_CRYPTO_Edx25519PublicKey auth_pub;

  /**
   *
  */
  struct GNUNET_CRYPTO_Edx25519Signature auth_sig;

  /**
   *
  */
  const char *auth_method;

  /**
   * Encryption key
  */
  struct FROSIX_EncryptionKey enc_key;

  /**
   * Commitment for this signature process.
  */
  struct FROST_Commitment commitment;

  /**
   * The resulting signature share of this provider.
  */
  struct FROST_SignatureShare sig_share;
};

/**
 * Struct to store all parsed data from the cli and /config and /seed
*/
struct FROSIX_SignatureData
{
  /**
   * What is the threshold value?
  */
  uint8_t threshold;

  /**
   * Public key to verify the resulting signature
  */
  struct FROST_PublicKey public_key;

  /**
   * Our hashed message.
  */
  struct FROST_MessageHash message_hash;

  /**
   * Pointer to a list of providers, length is @e threshold
  */
  struct FROSIX_Provider *providers;

  /**
   * Resulting signature
  */
  struct FROST_Signature signature;
};

/**
 * State of a sign procedure
*/
struct FROSIX_SignatureState
{
  /**
   * State we are updating.
   */
  struct FROSIX_SignatureData *sig_data;

  /**
   * Function to call when we are done.
   */
  FROSIX_ActionCallback cb;

  /**
   * Closure for @e cb.
   */
  void *cb_cls;

  /**
   * Redux action we returned to our controller.
   */
  struct FROSIX_ReduxAction ra;

  /**
   * Number of provider /config operations in @e ba_head that
   * are still awaiting completion.
   */
  uint8_t counter;

  /**
   * Last error code
  */
  enum TALER_ErrorCode error_code;

  /**
    * Array of config requests, with length `num_of_participants`
  */
  struct FROSIX_ConfigOperation **co;

  /**
   * Array of signature commitment requests with length @e threshold.
  */
  struct FROSIX_SigCommitmentRequestOperation **sco;

  /**
   * Array of signature share requests with length @e threshold.
  */
  struct FROSIX_SigShareRequestOperation **sso;

};


/**
 * Function to free all initialized memory of a provider from a
 * `struct FROSIX_Provider`
 *
 * @param fp the struct to clean up
*/
static void
free_provider_struct (struct FROSIX_Provider *fp)
{
  if (NULL != fp->backend_url)
    GNUNET_free (fp->backend_url);
}


/**
 * Function to free all initialized memory of a provider from a
 * `struct FROSIX_SignatureData`
 *
 * @param dd the struct to clean up
*/
static void
free_signature_data_struct (struct FROSIX_SignatureData *sd)
{
  if (NULL != sd->providers)
  {
    for (unsigned int i = 0; i < sd->threshold; i++)
    {
      if (NULL != &sd->providers[i])
        free_provider_struct (&sd->providers[i]);
    }

    GNUNET_free (sd->providers);
  }
}


/**
 * Function called when signature process is being aborted.
 * Frees all initialized memory!
 *
 * @param cls a `struct FROSIX_SignatureState`
*/
static void
sign_cancel_cb (void *cls)
{
  struct FROSIX_SignatureState *ss = cls;

  if (NULL != ss->sig_data)
  {
    free_signature_data_struct (ss->sig_data);
    GNUNET_free (ss->sig_data);
  }


  /* free operations */
  if (NULL != ss->co)
    GNUNET_free (ss->co);

  if (NULL != ss->sco)
    GNUNET_free (ss->sco);

  if (NULL != ss->sso)
    GNUNET_free (ss->sso);

  GNUNET_free (ss);

}


/**
 * FIXME
*/
static void
check_and_prepare_signature (const struct FROSIX_SignatureState *ss)
{
  /* build array of commitments - needed for verification */
  struct FROST_Commitment commitments[ss->sig_data->threshold];
  for (unsigned int i = 0; i < ss->sig_data->threshold; i++)
  {
    struct FROSIX_Provider *fp = &ss->sig_data->providers[i];

    commitments[i].identifier = fp->commitment.identifier;

    memcpy (&commitments[i].hiding_commitment,
            &fp->commitment.hiding_commitment,
            sizeof (fp->commitment.hiding_commitment));

    memcpy (&commitments[i].binding_commitment,
            &fp->commitment.binding_commitment,
            sizeof (fp->commitment.binding_commitment));
  }

  /* verify received signature share */
  for (unsigned int i = 0; i < ss->sig_data->threshold; i++)
  {
    struct FROSIX_Provider *fp = &ss->sig_data->providers[i];

    GNUNET_assert (FROST_verify_signature_share (
                     &fp->commitment,
                     &fp->sig_share,
                     commitments,
                     ss->sig_data->threshold,
                     &ss->sig_data->public_key,
                     &ss->sig_data->message_hash));
  }

  /* copy all signature shares together */
  struct FROST_SignatureShare sig_shares[ss->sig_data->threshold];
  for (unsigned int i = 0; i < ss->sig_data->threshold; i++)
  {
    struct FROSIX_Provider *fp = &ss->sig_data->providers[i];

    sig_shares[i].identifier = fp->sig_share.identifier;

    memcpy (&sig_shares[i].sig_share,
            &fp->sig_share.sig_share,
            sizeof (fp->sig_share.sig_share));

    memcpy (&sig_shares[i].pk_i,
            &fp->sig_share.pk_i,
            sizeof (fp->sig_share.pk_i));
  }

  /* build signature */
  FROST_compose_signature (&ss->sig_data->signature,
                           commitments,
                           sig_shares,
                           ss->sig_data->threshold,
                           &ss->sig_data->message_hash);

  /* verify signature */
  GNUNET_assert (FROST_verify_signature (&ss->sig_data->public_key,
                                         &ss->sig_data->signature,
                                         &ss->sig_data->message_hash));

  /* build json */
  json_t *result;

  result = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_data_auto ("message_hash",
                                &ss->sig_data->message_hash),
    GNUNET_JSON_pack_data_auto ("public_key",
                                &ss->sig_data->public_key),
    GNUNET_JSON_pack_data_auto ("signature_r",
                                &ss->sig_data->signature.r),
    GNUNET_JSON_pack_data_auto ("signature_z",
                                &ss->sig_data->signature.z)
    );

  /* free all initialized data */
  free_signature_data_struct (ss->sig_data);

  ss->cb (ss->cb_cls,
          ss->error_code,
          result);
}


/**
 * Callback to process a POST /sig-share request
 *
 * @param cls closure
 * @param ssd the decoded response body
*/
static void
signature_share_request_cb (void *cls,
                            const struct FROSIX_SigShareRequestDetails *ssd)
{
  GNUNET_assert (NULL != cls);
  struct FROSIX_SignatureState *ss = cls;

  /* set error code */
  ss->error_code = ssd->ec;

  /* check if this is a valid callback */
  GNUNET_assert (0 < ss->counter);
  GNUNET_assert (0 != ssd->http_status);

  /* check if provider index is valid */
  GNUNET_assert (0 < ssd->sig_share.identifier);
  GNUNET_assert (255 > ssd->sig_share.identifier);

  struct FROSIX_Provider *fp = &ss->sig_data->providers[ssd->array_index];

  /* Fill return values in our central struct */
  fp->sig_share.identifier = ssd->sig_share.identifier;

  memcpy (&fp->sig_share.sig_share,
          &ssd->sig_share.sig_share,
          sizeof (ssd->sig_share.sig_share));

  memcpy (&fp->sig_share.pk_i,
          &ssd->sig_share.pk_i,
          sizeof (ssd->sig_share.pk_i));

  /* call next dkg-commitment request */
  ss->counter--;
  if (0 == ss->counter)
  {
    /* we are the last, check received shares and build final signature */
    check_and_prepare_signature (ss);
  }
}


/**
 * FIXME
*/
static void
start_signature_shares (struct FROSIX_SignatureState *ss)
{
  ss->sso = GNUNET_malloc (ss->sig_data->threshold * sizeof (char *));
  ss->counter = 0;

  /* build commitment array */
  struct FROST_Commitment commitments[ss->sig_data->threshold];
  for (unsigned int i = 0; i < ss->sig_data->threshold; i++)
  {
    struct FROSIX_Provider *fp = &ss->sig_data->providers[i];

    commitments[i].identifier = fp->commitment.identifier;

    FROST_point_copy_to (&commitments[i].hiding_commitment,
                         &fp->commitment.hiding_commitment);

    FROST_point_copy_to (&commitments[i].binding_commitment,
                         &fp->commitment.binding_commitment);
  }

  for (unsigned int i = 0; i < ss->sig_data->threshold; i++)
  {
    struct FROSIX_Provider *fp = &ss->sig_data->providers[i];

    /* hash encryption key */
    struct FROST_HashCode enc_key_hash;
    FROSIX_hash_encryption_key (&enc_key_hash,
                                &fp->enc_key);

    /* compute request id */
    struct FROSIX_SigRequestIdP request_id;
    FROSIX_compute_signature_request_id (
      &request_id,
      &enc_key_hash,
      &ss->sig_data->message_hash);

    ss->sso[i] = FROSIX_sig_share_request (
      FROSIX_REDUX_ctx_,
      fp->backend_url,
      &request_id,
      fp->provider_index,
      i,
      &fp->enc_key,
      &ss->sig_data->message_hash,
      commitments,
      ss->sig_data->threshold,
      &signature_share_request_cb,
      ss);

    ss->counter++;
  }
}



/**
 * Callback to process a POST /sig-commitment request
 *
 * @param cls closure
 * @param scd the decoded response body
*/
static void
signature_commitment_request_cb (void *cls,
                                 const struct
                                 FROSIX_SigCommitmentRequestDetails *scd)
{
  GNUNET_assert (NULL != cls);
  struct FROSIX_SignatureState *ss = cls;

  /* set error code */
  ss->error_code = scd->ec;

  /* check if we have to abort the signing process */
  if (0 == scd->http_status)
  {
    ss->cb (ss,
            ss->error_code,
            NULL);
    return;
  }


  /* check if this is a valid callback */
  GNUNET_assert (0 < ss->counter);

  /* check if provider index is valid */
  GNUNET_assert (0 < scd->sig_commitment.identifier);
  GNUNET_assert (255 > scd->sig_commitment.identifier);

  /* copy commitment in our struct */
  struct FROSIX_Provider *fp = &ss->sig_data->providers[scd->array_index];

  /* Fill return values in our central struct */
  fp->commitment.identifier = scd->sig_commitment.identifier;

  FROST_point_copy_to (&fp->commitment.hiding_commitment,
                       &scd->sig_commitment.hiding_commitment);

  FROST_point_copy_to (&fp->commitment.binding_commitment,
                       &scd->sig_commitment.binding_commitment);

  /* call next dkg-commitment request */
  ss->counter--;
  if (0 == ss->counter)
  {
    /* we are the last, go ahead with sig-share */
    start_signature_shares (ss);
  }
}


/**
 * FIXME
*/
static void
start_signature_commitments (struct FROSIX_SignatureState *ss)
{
  ss->sco = GNUNET_malloc (ss->sig_data->threshold * sizeof (char *));
  ss->counter = 0;
  for (unsigned int i = 0; i < ss->sig_data->threshold; i++)
  {
    struct FROSIX_Provider *fp = &ss->sig_data->providers[i];

    /* hash encryption key */
    struct FROST_HashCode enc_key_hash;
    FROSIX_hash_encryption_key (&enc_key_hash,
                                &fp->enc_key);

    /* compute request id */
    struct FROSIX_SigRequestIdP request_id;
    FROSIX_compute_signature_request_id (
      &request_id,
      &enc_key_hash,
      &ss->sig_data->message_hash);

    if (0 == strcmp (fp->auth_method,
                     "question"))
    {
      ss->sco[i] = FROSIX_sig_commitment_request (
        FROSIX_REDUX_ctx_,
        fp->backend_url,
        &request_id,
        fp->provider_index,
        i,
        &enc_key_hash,
        NULL,
        &fp->auth_pub,
        &fp->auth_sig,
        fp->auth_method,
        &ss->sig_data->message_hash,
        &signature_commitment_request_cb,
        ss);
    }
    else
    {
      ss->sco[i] = FROSIX_sig_commitment_request (
        FROSIX_REDUX_ctx_,
        fp->backend_url,
        &request_id,
        fp->provider_index,
        i,
        &enc_key_hash,
        &fp->auth_data,
        NULL,
        NULL,
        fp->auth_method,
        &ss->sig_data->message_hash,
        &signature_commitment_request_cb,
        ss);
    }


    ss->counter++;
  }
}


/**
 * Helper function to parse a keygen input
 *
 * @param[in,out] dkg_data A initialized struct
 * @param[in] arguments Input from the cli
*/
enum GNUNET_GenericReturnValue
parse_sign_arguments (struct FROSIX_SignatureData *sig_data,
                      const json_t *arguments,
                      json_t *input)
{
  json_t *providers = NULL;
  uint8_t max_num = 0;

  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_uint8 ("number_of_participants",
                            &max_num),
    GNUNET_JSON_spec_uint8 ("threshold",
                            &sig_data->threshold),
    GNUNET_JSON_spec_fixed_auto ("public_key",
                                 &sig_data->public_key),
    GNUNET_JSON_spec_json ("providers",
                           &providers),
    GNUNET_JSON_spec_end ()
  };

  if (GNUNET_OK != GNUNET_JSON_parse (arguments,
                                      spec,
                                      NULL,
                                      NULL))
  {
    GNUNET_break (0);
    GNUNET_JSON_parse_free (spec);
    return GNUNET_NO;
  }

  /* get number of providers */
  size_t num_of_providers = json_object_size (input);

  /* validate number of providers */
  GNUNET_assert (sig_data->threshold == num_of_providers);
  GNUNET_assert (254 > sig_data->threshold);
  GNUNET_assert (0 < sig_data->threshold);

  /* initialize providers */
  sig_data->providers =  GNUNET_new_array (sig_data->threshold,
                                           struct FROSIX_Provider);

  uint8_t provider_mapping[sig_data->threshold];

  /* get involved providers */
  const char *key;
  json_t *value;
  unsigned int counter = 0;
  json_object_foreach (input,
                       key,
                       value)
  {
    /* try to convert key value (string) to unsigned int */
    int key_as_int = atoi (key);

    GNUNET_assert (0 < key_as_int);
    GNUNET_assert (254 > key_as_int);
    GNUNET_assert (max_num >= key_as_int);

    provider_mapping[counter] = (uint8_t) key_as_int;
    counter++;
  }

  /* parse provider data */
  for (unsigned int i = 0; i < sig_data->threshold; i++)
  {
    struct GNUNET_HashCode auth_nonce;
    uint8_t provider_index = provider_mapping[i];
    struct GNUNET_JSON_Specification prov_spec[] = {
      GNUNET_JSON_spec_uint8 ("provider_index",
                              &sig_data->providers[i].provider_index),
      GNUNET_JSON_spec_string ("backend_url",
                               (const
                                char**) &sig_data->providers[i].backend_url),
      GNUNET_JSON_spec_fixed_auto ("encryption_key",
                                   &sig_data->providers[i].enc_key),
      GNUNET_JSON_spec_string ("auth_method",
                               &sig_data->providers[i].auth_method),
      GNUNET_JSON_spec_fixed_auto ("auth_nonce",
                                   &auth_nonce),
      GNUNET_JSON_spec_end ()
    };

    if (GNUNET_OK != GNUNET_JSON_parse (json_array_get (providers,
                                                        provider_index - 1),
                                        prov_spec,
                                        NULL,
                                        NULL))
    {
      GNUNET_break (0);
      GNUNET_JSON_parse_free (prov_spec);
      GNUNET_JSON_parse_free (spec);
      return GNUNET_NO;
    }

    GNUNET_JSON_parse_free (prov_spec);

    /* convert int index to a string - this is our key */
    char index[5];
    snprintf (index,
              4,
              "%d",
              sig_data->providers[i].provider_index);

    /* parse challenge solution / answer of security question */
    if (0 == strcmp (sig_data->providers[i].auth_method,
                     "question"))
    {
      const char *answer;

      struct GNUNET_JSON_Specification q_spec[] = {
        GNUNET_JSON_spec_string (index,
                                 &answer),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK != GNUNET_JSON_parse (input,
                                          q_spec,
                                          NULL,
                                          NULL))
      {
        GNUNET_break (0);
        GNUNET_JSON_parse_free (q_spec);
        return GNUNET_NO;
      }

      GNUNET_JSON_parse_free (q_spec);

      unsigned int amplifier = 1;
      /* compute key pair to sign signature */
      struct GNUNET_CRYPTO_Edx25519PrivateKey priv;
      struct FROSIX_ChallengeHashP auth_hash;
      FROSIX_hash_pow (&auth_hash.hash,
                       &priv,
                       &sig_data->providers[i].auth_pub,
                       &auth_nonce,
                       answer,
                       amplifier);

      // Sign data
      struct FROSIX_AuthSignaturePS as = {
        .purpose.purpose = htonl (72),
        .purpose.size = htonl (sizeof (as)),
        .mh = sig_data->message_hash,
      };

      GNUNET_CRYPTO_edx25519_sign (&priv,
                                   &as,
                                   &sig_data->providers[i].auth_sig);
    }
    else
    {
      /* parse answer as number */
      uint64_t code;

      struct GNUNET_JSON_Specification c_spec[] = {
        GNUNET_JSON_spec_uint64 (index,
                                 &code),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK != GNUNET_JSON_parse (input,
                                          c_spec,
                                          NULL,
                                          NULL))
      {
        GNUNET_break (0);
        GNUNET_JSON_parse_free (c_spec);
        return GNUNET_NO;
      }

      GNUNET_JSON_parse_free (c_spec);

      /* compute challenge hash */
      FROSIX_hash_answer (code,
                          &sig_data->providers[i].auth_data);
    }
  }

  GNUNET_JSON_parse_free (spec);

  /* check if list of providers is sorted */
  for (unsigned int i = 1; i < sig_data->threshold; i++)
  {
    GNUNET_assert (sig_data->providers[i].provider_index >
                   sig_data->providers[i - 1].provider_index);
  }

  return GNUNET_OK;
}


/**
 *
*/
struct FROSIX_ReduxAction *
FROSIX_redux_sign_start (const json_t *arguments,
                         json_t *input,
                         const char *message,
                         FROSIX_ActionCallback cb,
                         void *cb_cls)
{
  /* initialize signature data struct */
  struct FROSIX_SignatureData *sig_data = GNUNET_new (struct
                                                      FROSIX_SignatureData);

  /* hash message */
  FROST_message_to_hash (&sig_data->message_hash,
                         message,
                         strlen (message));

  /* parse arguments from cli */
  if (GNUNET_OK != parse_sign_arguments (sig_data,
                                         arguments,
                                         input))
  {
    free_signature_data_struct (sig_data);

    // FIXME: Return some useful error message
    return NULL;
  }

  /* lets create a state */
  struct FROSIX_SignatureState *ss = GNUNET_new (struct FROSIX_SignatureState);
  ss->cb = cb;
  ss->cb_cls = cb_cls;
  ss->sig_data = sig_data;
  ss->ra.cleanup = &sign_cancel_cb;
  ss->ra.cleanup_cls = ss;

  /* get commitments from all parsed providers */
  start_signature_commitments (ss);

  return &ss->ra;
}