/*
  This file is part of Frosix
  Copyright (C) 2020, 2021 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Frosix; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file libfrosix/frosix_api.h
 * @brief anastasis reducer api, internal data structures
 * @author Christian Grothoff
 * @author Dominik Meister
 * @author Dennis Neufeld
 */
#ifndef FROSIX_API_REDUX_H
#define FROSIX_API_REDUX_H

#include <jansson.h>


/**
 * CURL context to be used by all operations.
 */
extern struct GNUNET_CURL_Context *FROSIX_REDUX_ctx_;


/**
 * Function to return a json error response.
 *
 * @param cb callback to give error to
 * @param cb_cls callback closure
 * @param ec error code
 * @param detail error detail
 */
void
FROSIX_redux_fail_ (FROSIX_ActionCallback cb,
                    void *cb_cls,
                    enum TALER_ErrorCode ec,
                    const char *detail);




#endif
