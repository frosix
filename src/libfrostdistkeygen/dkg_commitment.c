/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file dkg_commitment.c
 * @brief helper functions to initialize and free a dkg commitment struct
 * @author Joel Urech
*/
#include "keygen.h"
#include <gnunet/gnunet_util_lib.h>


enum GNUNET_GenericReturnValue
FROST_initialize_dkg_commitment (struct FROST_DkgCommitment *dkg_commitment,
                                 uint8_t my_index,
                                 uint8_t threshold)
{
  /* check params */
  if (GNUNET_OK != FROST_validate_dkg_params (my_index,
                                              threshold,
                                              254))
    return GNUNET_NO;

  /* abort if calling function misbehaves*/
  GNUNET_assert (NULL != dkg_commitment);

  /* init memory and set initial values */
  dkg_commitment->identifier = my_index;
  dkg_commitment->share_comm
    = GNUNET_malloc (threshold * sizeof (struct FROST_Point));

  dkg_commitment->shares_commitments_length = threshold;
  FROST_scalar_zero (&dkg_commitment->zkp.z);
  FROST_point_identity (&dkg_commitment->zkp.r);

  return GNUNET_OK;
}


void
FROST_free_dkg_commitment (struct FROST_DkgCommitment dkg_commitments[],
                           size_t length)
{
  for (unsigned int i = 0; i < length; i++)
  {
    GNUNET_free (dkg_commitments[i].share_comm);
  }
}
