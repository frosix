/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file keygen_common.h
 * @brief Implementation of some functions which are used several times in
 * different steps of the distributed key generation process.
 * @author Joel Urech
*/
#ifndef FROST_KEYGEN_COMMON_H
#define FROST_KEYGEN_COMMON_H

#include "high_common.h"

/**
 * Struct for the challenge of the dkg zkp
*/
struct FROST_DkgChallenge
{
  /**
   * The challenge is a scalar
  */
  struct FROST_Scalar c;
};

/**
 * Struct for a secret key during a key generation process
*/
struct FROST_DkgSecretKey
{
  /**
   * Our secret key is a scalar
  */
  struct FROST_Scalar sk;
};

/**
 * @brief Generates the challenge of the zero knowledge proof of the secret
 * \f$a_{i0}\f$
 * \f$c_i = H(i,"Context-String",g^{a_{i0}},R_i)\f$
 *
 * @param[out] challenge The resulting challenge, a scalar in the range of the
 * order of the ristretto255-group.
 * @param[in] generator_index Index of the participant who has the secret
 * \f$a_{i0}\f$
 * @param[in] s_pub The value \f$g^{a_{i0}}\f$
 * @param[in] zkp The value \f$R_i = g^k\f$ where k is a random nonce only
 * known to the participant who has the secret \f$a_{i0}\f$
 * @param[in] additional_data If not NULL, this data will be included in the
 * challenge of the @a zkp.
*/
void
FROST_generate_dkg_challenge_ (struct FROST_DkgChallenge *challenge,
                               uint8_t generator_index,
                               const struct FROST_DkgShareCommitment *s_pub,
                               const struct FROST_DkgZkp *zkp,
                               const struct FROST_HashCode *additional_data);

/**
 * @brief Generates a random threshold-1 degree polynomial and the
 * corresponding commitment values.
 *
 * @param[out] shares Coefficients of the random generated polynomial
 * @param[out] dkg_commitment A commitment value for every share
 * \f$g^{a_{ij}}\f$, 0 <= j <= threshold-1.
 * To use this function in a trusted dealer setup, there are no dkg commitments
 * and therefore submit the value NULL.
 * @param[in] secret A secret value, which will be added as the constant value
 * of the polynomial.
 * @param[in] coeff_masterkey Source of high entropy to derive the coefficients
 * from and compute the secret polynomial.
 * @param num_of_participants How many are participating?
 * @param threshold How many participants are required to restore the secret
 * (sign a message)?
*/
void
FROST_generate_shares_ (struct FROST_DkgShare shares[],
                        struct FROST_DkgCommitment *dkg_commitment,
                        const struct FROST_DkgSecretKey *secret,
                        const struct FROST_ShortHashCode *coeff_masterkey,
                        uint8_t num_of_participants,
                        uint8_t threshold);

#endif
