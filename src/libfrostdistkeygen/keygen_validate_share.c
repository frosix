/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file keygen_validate_share.c
 * @brief Implements the validation of a share.
 * @author Joel Urech
*/
#include "keygen.h"
#include "keygen_common.h"

enum GNUNET_GenericReturnValue
FROST_keygen_validate_share (
  const struct FROST_DkgCommitment *commitment,
  const struct FROST_DkgShare *share,
  uint8_t my_index)
{
  /* check if share is 0 - we can not multiply the curve generator with 0! */
  {
    struct FROST_Scalar zero;
    FROST_scalar_zero (&zero);

    if (0 == memcmp (&zero.scalarbytes,
                     &share->share.scalarbytes,
                     sizeof (share->share)))
      return GNUNET_NO;
  }

  /* g^share_i*/
  struct FROST_Point g_secret_share;
  FROST_base_mul_scalar (&g_secret_share,
                         &share->share);

  // initialize com_result
  struct FROST_Point com_result;
  FROST_point_identity (&com_result);

  // initialize index of receiver
  struct FROST_Scalar receiver_index;
  FROST_scalar_set_uint8 (&receiver_index,
                          my_index);

  /* reversed loop over all commitments */
  for (int i = commitment->shares_commitments_length - 1; i >= 0; i--)
  {
    // com_result += schare_com[i]
    FROST_point_add_point (&com_result,
                           &com_result,
                           &commitment->share_comm[i].sc);

    // we ignore the multiplication for commitment '0'
    if (i != 0)
    {
      // com_result *= receiver_index
      FROST_point_mul_scalar (&com_result,
                              &com_result,
                              &receiver_index);
    }
  }

  /* compare provided and computed points */
  return FROST_point_cmp (&com_result,
                          &g_secret_share);
}
