/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file keygen_begin.c
 * @brief Implementation of the first round of the distributed key generation
 * @author Joel Urech
*/
#include "keygen.h"
#include "keygen_common.h"


enum GNUNET_GenericReturnValue
FROST_keygen_begin (struct FROST_DkgCommitment *dkg_commitment,
                    struct FROST_DkgShare dkg_shares[],
                    const struct FROST_DkgContextString *context_string,
                    const struct FROST_HashCode *additional_data,
                    uint8_t my_index,
                    uint8_t num_of_participants,
                    uint8_t threshold)
{
  /* check params */
  if (GNUNET_OK != FROST_validate_dkg_params (my_index,
                                              threshold,
                                              num_of_participants))
    return GNUNET_NO;

  /* check if calling function misbehaves */
  GNUNET_assert (NULL != dkg_commitment);
  GNUNET_assert (NULL != dkg_shares);
  GNUNET_assert (NULL != context_string);

  /* derive secret value 'x0' from context_string */
  struct FROST_DkgSecretKey secret;
  FROST_kdf_scalar_to_curve (&secret.sk,
                             0,
                             &context_string->con_str);

  /* Generate commitments and shares */
  FROST_generate_shares_ (dkg_shares,
                          dkg_commitment,
                          &secret,
                          &context_string->con_str,
                          num_of_participants,
                          threshold);

  /* Generate the signature / zero knowledge proof of secret 'x0' */

  // hash secret value and map back to a scalar -> our 'rnd'-value
  struct FROST_HashState r_h_state;
  FROST_hash_init (&r_h_state);
  FROST_hash_scalar_update (&r_h_state,
                            &secret.sk);
  FROST_hash_fixed_update (&r_h_state,
                           "FROST-DKG-ZKP",
                           strlen ("FROST-DKG-ZKP"));
  struct FROST_HashCode r_h;
  FROST_hash_final (&r_h_state,
                    &r_h);

  struct FROST_Scalar r;
  FROST_hash_to_scalar (&r,
                        &r_h);

  // compute signature / zkp
  FROST_base_mul_scalar (&dkg_commitment->zkp.r,
                         &r);

  struct FROST_DkgShareCommitment s_pub;
  FROST_base_mul_scalar (&s_pub.sc,
                         &secret.sk);

  struct FROST_DkgChallenge challenge;
  FROST_generate_dkg_challenge_ (&challenge,
                                 my_index,
                                 &s_pub,
                                 &dkg_commitment->zkp,
                                 additional_data);

  FROST_scalar_mul_scalar (&dkg_commitment->zkp.z,
                           &secret.sk,
                           &challenge.c);
  FROST_scalar_add_scalar (&dkg_commitment->zkp.z,
                           &dkg_commitment->zkp.z,
                           &r);

  return GNUNET_OK;
}
