/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file validate_params.c
 * @brief Does a validation of the submitted parameter for the key generation.
 * @author Joel Urech
*/
#include "keygen.h"

enum GNUNET_GenericReturnValue
FROST_validate_dkg_params (uint8_t identifier,
                           uint8_t threshold,
                           uint8_t num_of_participants)
{
  if (identifier <= 0
      || identifier >= 255
      || identifier > num_of_participants
      || threshold > num_of_participants
      || threshold <= 0
      || threshold >= 254
      || num_of_participants <= 0
      || num_of_participants >= 255)
    return GNUNET_NO;
  return GNUNET_OK;
}