/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file keygen_finalize.c
 * @brief Implementation of the finalization of the distributed key generation process
 * @author Joel Urech
*/
#include "keygen.h"

/**
 * With the shares of all other participants, this function computes the
 * individual key pair and derives the group public key from the commitments.
*/
void
FROST_keygen_finalize (struct FROST_KeyPair *keypair,
                       uint8_t my_index,
                       const struct FROST_DkgShare shares[],
                       const struct FROST_DkgCommitment commitments[],
                       uint8_t num_of_participants)
{
  // id
  keypair->identifier = my_index;

  // sk_i
  FROST_scalar_zero (&keypair->my_sk);
  for (int i = 0; i < num_of_participants; i++)
  {
    FROST_scalar_add_scalar (&keypair->my_sk,
                             &keypair->my_sk,
                             &shares[i].share);
  }

  // pk_i
  FROST_base_mul_scalar (&keypair->my_pk,
                         &keypair->my_sk);

  // set pk from keypair to 0
  FROST_point_identity (&keypair->group_pk.pk);

  for (int i = 0; i < num_of_participants; i++)
  {
    FROST_point_add_point (&keypair->group_pk.pk,
                           &keypair->group_pk.pk,
                           &commitments[i].share_comm[0].sc);
  }
}
