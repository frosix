/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file keygen_validate_commitment.c
 * @brief Implements the validation of the commitments
 * @author Joel Urech
*/
#include "keygen.h"
#include "keygen_common.h"

/**
 * @brief Validates the zero knowledge proof of a commitment.
 *
 * @param[in] challenge The already calculated challenge for the zkp.
 * @param[in] comm A commitment to validate.
*/
static enum GNUNET_GenericReturnValue
is_valid_zkp (const struct FROST_Scalar *challenge,
              const struct FROST_DkgCommitment *comm)
{
  // g^z - (s_pub * challenge)
  struct FROST_Point g_z;
  FROST_base_mul_scalar (&g_z, &comm->zkp.z);

  struct FROST_Point s_challenge;
  FROST_point_mul_scalar (&s_challenge, &comm->share_comm[0].sc,
                          challenge);

  struct FROST_Point r;
  FROST_point_sub_point (&r, &g_z, &s_challenge);

  return FROST_point_cmp (&comm->zkp.r, &r);
}


enum GNUNET_GenericReturnValue
FROST_keygen_validate_commitment (
  const struct FROST_DkgCommitment *dkg_commitment,
  const struct FROST_HashCode *additional_data,
  uint8_t num_of_participants)
{
  /* Check if other party pretends to have an invalid identifier */
  if (0 == dkg_commitment->identifier
      || num_of_participants < dkg_commitment->identifier)
    return GNUNET_NO;

  /* check every element if it is a valid encoded point */
  if (0 >= dkg_commitment->shares_commitments_length
      || dkg_commitment->shares_commitments_length >= 254)
    return GNUNET_NO;
  for (unsigned int i = 0; i < dkg_commitment->shares_commitments_length; i++)
  {
    if (GNUNET_OK != FROST_is_valid_point (&dkg_commitment->share_comm[i].sc))
      return GNUNET_NO;
  }

  struct FROST_DkgChallenge challenge;
  FROST_generate_dkg_challenge_ (&challenge,
                                 dkg_commitment->identifier,
                                 &dkg_commitment->
                                 share_comm[0],
                                 &dkg_commitment->zkp,
                                 additional_data);

  return is_valid_zkp (&challenge.c, dkg_commitment);
}
