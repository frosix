/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file keygen_common.c
 * @brief Implementation of some functions which are used several times in different steps
 * of the distributed key generation process.
 * @author Joel Urech
*/
#include "keygen.h"
#include "keygen_common.h"
#include "frost_low.h"


/**
 * @brief Generates commitments for each of the provided coefficients
 *
 * @param[out] dkg_commitment The resulting commitment to the polynomial
 * @param[in] secret The secret value of the polynomial at coordiante x=0
 * @param[in] coefficients All generated coefficients of the polynomial
 * @param[in] coefficients_len Length of array coefficients
*/
static void
generate_commitments (struct FROST_DkgCommitment *dkg_commitment,
                      const struct FROST_DkgSecretKey *secret,
                      const struct FROST_Coefficient coefficients[],
                      uint8_t coefficients_len)
{
  // Generate commitments
  FROST_base_mul_scalar (&dkg_commitment->share_comm[0].sc,
                         &secret->sk);

  for (int i = 0; i < coefficients_len; i++)
  {
    FROST_base_mul_scalar (&dkg_commitment->share_comm[i + 1].sc,
                           &coefficients[i].coeff);
  }
}


void
FROST_generate_dkg_challenge_ (struct FROST_DkgChallenge *challenge,
                               uint8_t generator_index,
                               const struct FROST_DkgShareCommitment *s_pub,
                               const struct FROST_DkgZkp *zkp,
                               const struct FROST_HashCode *additional_data)
{
  struct FROST_HashState state;
  struct FROST_HashCode hash;
  FROST_hash_init (&state);
  FROST_hash_uint8_update (&state,
                           generator_index);
  FROST_hash_point_update (&state,
                           &s_pub->sc);
  FROST_hash_point_update (&state,
                           &zkp->r);
  if (NULL != additional_data) /* additional data is optional */
  {
    FROST_hash_hash_update (&state,
                            additional_data);
  }
  FROST_hash_fixed_update (&state,
                           "DKG-Challenge",
                           strlen ("DKG-Challenge"));
  FROST_hash_final (&state,
                    &hash);
  FROST_hash_to_scalar (&challenge->c,
                        &hash);
}


void
FROST_generate_shares_ (struct FROST_DkgShare shares[],
                        struct FROST_DkgCommitment *dkg_commitment,
                        const struct FROST_DkgSecretKey *secret,
                        const struct FROST_ShortHashCode *coeff_masterkey,
                        uint8_t num_of_participants,
                        uint8_t threshold)
{
  uint8_t numcoeffs = threshold - 1;

  // Sample t-1 participants values
  struct FROST_Coefficient coefficients[numcoeffs];

  for (uint8_t i = 0; i < numcoeffs; i++)
  {
    if (NULL == coeff_masterkey)
    {
      FROST_scalar_random (&coefficients[i].coeff);
    }
    else {
      // Derive new subkey
      FROST_kdf_scalar_to_curve (&coefficients[i].coeff,
                                 i + 1, /* id '0' is our secret! */
                                 coeff_masterkey);
    }
  }

  // Horner's method to evaluate the polynomial at the point x=share_index
  // [1 ... n] (i must not be 0)
  for (uint8_t i = 1; i <= num_of_participants; i++)
  {
    struct FROST_Scalar share_index;
    struct FROST_Scalar value;
    FROST_scalar_set_uint8 (&share_index,
                            i);
    FROST_scalar_zero (&value);

    // ]t-1 ... 0] => [t-2 ... 0]
    for (int j = numcoeffs - 1; j >= 0; j--)
    {
      // Add coeff and multiply with share_index
      FROST_scalar_add_scalar (&value,
                               &value,
                               &coefficients[j].coeff);
      FROST_scalar_mul_scalar (&value,
                               &value,
                               &share_index);
    }

    // Add secret as the *constant* yi (*x^0)
    FROST_scalar_add_scalar (&shares[i - 1].share,
                             &value,
                             &secret->sk);

    // set participant identifier
    shares[i - 1].identifier = i;
  }

  // We have no commitments with trusted dealer
  if (NULL != dkg_commitment)
  {
    generate_commitments (dkg_commitment,
                          secret,
                          coefficients,
                          numcoeffs);
  }
}
