#!/bin/bash
# This file is in the public domain.

set -eu

# Exit, with status code "skip" (no 'real' failure)
function exit_skip() {
    echo " SKIP: $1"
    exit 77
}

# Exit, with error message (hard failure)
function exit_fail() {
    echo " FAIL: $@"
    exit 1
}

# Cleanup to run whenever we exit
function cleanup()
{
    for n in $(jobs -p)
    do
        kill $n 2> /dev/null || true
    done
    rm -f "$SIGN_DOCUMENT" "$SIGNATURE"
    wait
}

# Install cleanup handler (except for kill -9)
trap cleanup EXIT

#echo -n "Testing for jq"
#jq -h > /dev/null || exit_skip "jq required"
#echo " FOUND"
echo -n "Testing for wget"
wget -h > /dev/null || exit_skip "wget required"
echo " FOUND"
echo -n "Testing for frosix-httpd ..."
frosix-httpd -h > /dev/null || exit_skip "frosix-httpd required"
echo " FOUND"
echo -n "Testing for frosix-reducer ..."
frosix-reducer -h > /dev/null || exit_skip "frosix-reducer required"
echo " FOUND"

# Files and config
SIGN_DOCUMENT=$(mktemp sign_documentXXXXXX.json)
SIGNATURE=$(mktemp signatureXXXXXX.json)

PLIST="test-frosix_providerlist.json"

CONF_1="test-frosix_1.conf"
CONF_2="test-frosix_2.conf"
CONF_3="test-frosix_3.conf"
CONF_4="test-frosix_4.conf"
CONF_5="test-frosix_5.conf"

BURL_1="http://localhost:9967"
BURL_2="http://localhost:9968"
BURL_3="http://localhost:9969"
BURL_4="http://localhost:9970"
BURL_5="http://localhost:9971"

AUTH='{"1":"Tobias","3":"Tobias","5":"Tobias"}'
MSG="My very important message!"

# Init db
echo -n "Initialize frosix database ..."
TARGET_DB_1=`frosix-config -c "${CONF_1}" -s frosixdb-postgres -o CONFIG | sed -e "s/^postgres:\/\/\///"`
TARGET_DB_2=`frosix-config -c "${CONF_2}" -s frosixdb-postgres -o CONFIG | sed -e "s/^postgres:\/\/\///"`
TARGET_DB_3=`frosix-config -c "${CONF_3}" -s frosixdb-postgres -o CONFIG | sed -e "s/^postgres:\/\/\///"`
TARGET_DB_4=`frosix-config -c "${CONF_4}" -s frosixdb-postgres -o CONFIG | sed -e "s/^postgres:\/\/\///"`
TARGET_DB_5=`frosix-config -c "${CONF_5}" -s frosixdb-postgres -o CONFIG | sed -e "s/^postgres:\/\/\///"`

dropdb "${TARGET_DB_1}" >/dev/null 2>/dev/null || true
createdb "${TARGET_DB_1}" || exit_skip "Could not create database ${TARGET_DB_1}"
frosix-dbinit -c "${CONF_1}" &> dbinit_1.log
dropdb "${TARGET_DB_2}" >/dev/null 2>/dev/null || true
createdb "${TARGET_DB_2}"|| exit_skip "Could not create database ${TARGET_DB_2}"
frosix-dbinit -c "${CONF_2}" &> dbinit_2.log
dropdb "${TARGET_DB_3}" >/dev/null 2>/dev/null || true
createdb "${TARGET_DB_3}" || exit_skip "Could not create database ${TARGET_DB_3}"
frosix-dbinit -c "${CONF_3}" &> dbinit_3.log
dropdb "${TARGET_DB_4}" >/dev/null 2>/dev/null || true
createdb "${TARGET_DB_4}" || exit_skip "Could not create database ${TARGET_DB_4}"
frosix-dbinit -c "${CONF_4}" &> dbinit_4.log
dropdb "${TARGET_DB_5}" >/dev/null 2>/dev/null || true
createdb "${TARGET_DB_5}" || exit_skip "Could not create database ${TARGET_DB_5}"
frosix-dbinit -c "${CONF_5}" &> dbinit_5.log

echo " OK"

echo -n "Start frosix-httpd ..."
frosix-httpd -L DEBUG -c "${CONF_1}" &> httpd_1.log &
frosix-httpd -L DEBUG -c "${CONF_2}" &> httpd_2.log &
frosix-httpd -L DEBUG -c "${CONF_3}" &> httpd_3.log &
frosix-httpd -L DEBUG -c "${CONF_4}" &> httpd_4.log &
frosix-httpd -L DEBUG -c "${CONF_5}" &> httpd_5.log &

echo " OK"

echo -n "Waiting for frosix services ..."

# Wait for frosix to be available
for n in $(seq 1 50)
do
    echo -n "."
    sleep 0.1
    OK=0
    # bankfrosix_01
    wget --tries=1 --timeout=1 "${BURL_1}/config" -o /dev/null -O /dev/null >/dev/null || continue
    # bankfrosix_02
    wget --tries=1 --timeout=1 "${BURL_2}/config" -o /dev/null -O /dev/null >/dev/null || continue
    # bankfrosix_03
    wget --tries=1 --timeout=1 "${BURL_3}/config" -o /dev/null -O /dev/null >/dev/null || continue
    # bankfrosix_04
    wget --tries=1 --timeout=1 "${BURL_4}/config" -o /dev/null -O /dev/null >/dev/null || continue
    # bankfrosix_05
    wget --tries=1 --timeout=1 "${BURL_5}/config" -o /dev/null -O /dev/null >/dev/null || continue
    OK=1
    break
done
if [ 1 != $OK ]
then
    exit_skip "Failed to launch frosix service"
fi

echo " OK"

# Start with key generation
echo -n "Start distributed key generation ..."
frosix-cli keygen -o "${SIGN_DOCUMENT}" < "${PLIST}"
RET_KG=$?

if [ "$RET_KG" != "0" ]
then
  exit_fail "Key generation failed! Expected return code 0, got $RET_KG"
fi

echo " OK"

# Sign a message
echo -n "Sign a message ..."
frosix-cli sign -a "${AUTH}" -m "${MSG}" -o "${SIGNATURE}" < "${SIGN_DOCUMENT}"
RET_S=$?

if [ "$RET_S" != "0" ]
then
  exit_fail "Signing failed! Expected return code 0, got $RET_S"
fi

echo " OK"

# Verify the signature
echo -n "Verify the signature ..."
frosix-cli verify -m "${MSG}" < "${SIGNATURE}"
RET_V=$?

if [ "$RET_V" != "0" ]
then
  exit_fail "Verification of signature failed!"
fi

exit 0
