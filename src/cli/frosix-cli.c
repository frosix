/*
  This file is part of Frosix
  Copyright (C) 2020,2021,2022 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file cli/frosix-cli.c
 * @brief command line tool for Frosix client
 * @author Christian Grothoff
 * @author Dennis Neufeld
 * @author Dominik Meister
 * @author Joel Urech
 */

#include "platform.h"
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "frosix.h"
#include <taler/taler_util.h>
#include <taler/taler_error_codes.h>
#include <taler/taler_json_lib.h>
#include "frosix_util_lib.h"

/**
 * Closure for #GNUNET_CURL_gnunet_scheduler_reschedule().
 */
static struct GNUNET_CURL_RescheduleContext *rc;

/**
 * Curl context for communication with Frosix backend
 */
static struct GNUNET_CURL_Context *ctx;

/**
 * Input to -a option given.
 */
static char *input = NULL;

/**
 * Output filename, if given.
 */
static char *output_filename = NULL;

/**
 * Message we want to sign, -m option.
*/
static char *message = NULL;

/**
 * JSON containing additional arguments
 */
static json_t *arguments;

/**
 * JSON containing data from stdin
*/
static json_t *std_input;

/**
 * Handle to an ongoing action.
 */
static struct FROSIX_ReduxAction *ra;

/**
 * Return value from main.
 */
static int global_ret;


/**
 * Persist a json state, report errors.
 *
 * @param state to persist
 * @param filename where to write the state to, NULL for stdout
 */
static void
persist_new_state (json_t *state,
                   const char *filename)
{
  if (NULL != filename)
  {
    if (0 !=
        json_dump_file (state,
                        filename,
                        JSON_INDENT (1)))
    {
      GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                  "Could not dump state to `%s'\n",
                  filename);
      return;
    }
    return;
  }
  {
    char *state_str = json_dumps (state,
                                  JSON_INDENT (1));
    if (-1 >=
        fprintf (stdout,
                 "%s",
                 state_str))
    {
      GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                  "Could not dump state to stdout\n");
      GNUNET_free (state_str);
      return;
    }
    GNUNET_free (state_str);
  }
}


/**
 * @brief Shutdown the application.
 *
 * @param cls closure
 */
static void
shutdown_task (void *cls)
{
  (void) cls;

  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Shutdown initiated\n");
  if (NULL != ra)
  {
    FROSIX_redux_action_cancel (ra);
  }
  if (NULL != ctx)
  {
    GNUNET_CURL_fini (ctx);
    ctx = NULL;
  }
  if (NULL != rc)
  {
    GNUNET_CURL_gnunet_rc_destroy (rc);
    rc = NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Shutdown complete\n");
}


/**
 * Function called with the results of #FROSIX_redux_action().
 *
 * @param cls closure
 * @param error_code Error code
 * @param result_state new state as result
 */
static void
action_cb (void *cls,
           enum TALER_ErrorCode error_code,
           json_t *result_state)
{
  (void) cls;
  // ra = NULL;
  if (NULL != result_state)

    persist_new_state (result_state,
                       output_filename);

  if (TALER_EC_NONE != error_code)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Redux failed with error %d: %s\n",
                error_code,
                TALER_ErrorCode_get_hint (error_code));
    json_dumpf (result_state,
                stderr,
                JSON_INDENT (2));
    fprintf (stderr, "action_cb - error\n");
  }

  /* free json argument and result */
  json_decref (result_state);

  GNUNET_SCHEDULER_shutdown ();
  global_ret = (TALER_EC_NONE != error_code) ? 1 : 0;
}


/**
 * @brief Start the application
 *
 * @param cls closure
 * @param args arguments left
 * @param cfgfile config file name
 * @param cfg handle for the configuration file
 */
static void
run (void *cls,
     char *const *args,
     const char *cfgfile,
     const struct GNUNET_CONFIGURATION_Handle *cfg)
{
  (void) cls;
  json_error_t error;

  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Starting frosix-reducer\n");
  GNUNET_SCHEDULER_add_shutdown (&shutdown_task,
                                 NULL);

  /* action processing */
  {
    const char *action = args[0];

    if (NULL == action)
    {
      GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                  "You must specify an action as the first argument (or `-k' or `-s')\n");
      GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                  "Example: frosix-reducer --keygen -a 'json-formatted-argument'\n");
      GNUNET_SCHEDULER_shutdown ();
      global_ret = 1;
      return;
    }

    /*** KEYGEN ***/
    if (0 == strcasecmp (action,
                         "keygen"))
    {
      /* load json file from std input */
      std_input = json_loadf (stdin,
                              JSON_DECODE_ANY,
                              &error);
      if (NULL == std_input)
      {
        GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                    "Failed to parse arguments on line %u:%u: %s!\n",
                    error.line,
                    error.column,
                    error.text);
        GNUNET_SCHEDULER_shutdown ();
        global_ret = 1;
        return;
      }

      /* initialize HTTP client event loop */
      ctx = GNUNET_CURL_init (&GNUNET_CURL_gnunet_scheduler_reschedule,
                              &rc);
      rc = GNUNET_CURL_gnunet_rc_create (ctx);

      FROSIX_redux_init (ctx);

      ra = FROSIX_redux_keygen_start (std_input,
                                      &action_cb,
                                      cls);
    }

    /*** REQUEST-CHALLENGE ***/
    if (0 == strcasecmp (action,
                         "request-challenge"))
    {
      /* check if -a and -m flag, abort if not found */
      if (NULL != input && NULL != message)
      {
        std_input = json_loadf (stdin,
                                JSON_DECODE_ANY,
                                &error);
        if (NULL == std_input)
        {
          GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                      "Failed to parse arguments on line %u:%u: %s!\n",
                      error.line,
                      error.column,
                      error.text);
          GNUNET_SCHEDULER_shutdown ();
          global_ret = 1;
          return;
        }

        arguments = json_loads (input,
                                JSON_DECODE_ANY,
                                &error);
        if (NULL == arguments)
        {
          GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                      "Failed to parse arguments on line %u:%u: %s!\n",
                      error.line,
                      error.column,
                      error.text);
          GNUNET_SCHEDULER_shutdown ();
          global_ret = 1;
          return;
        }
      }
      else {
        GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                    "Failed to parse arguments on line %u:%u: %s!\n",
                    error.line,
                    error.column,
                    error.text);
        GNUNET_SCHEDULER_shutdown ();
        global_ret = 1;
        return;
      }

      /* initialize HTTP client event loop */
      ctx = GNUNET_CURL_init (&GNUNET_CURL_gnunet_scheduler_reschedule,
                              &rc);
      rc = GNUNET_CURL_gnunet_rc_create (ctx);

      FROSIX_redux_init (ctx);

      ra = FROSIX_redux_challenge_request_start (std_input,
                                                 arguments,
                                                 message,
                                                 &action_cb,
                                                 cls);
    }

    /*** SIGN ***/
    if (0 == strcasecmp (action,
                         "sign"))
    {
      /* check if -a and -m flag, abort if not found */
      if (NULL != input && NULL != message)
      {
        std_input = json_loadf (stdin,
                                JSON_DECODE_ANY,
                                &error);
        if (NULL == std_input)
        {
          GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                      "Failed to parse arguments on line %u:%u: %s!\n",
                      error.line,
                      error.column,
                      error.text);
          GNUNET_SCHEDULER_shutdown ();
          global_ret = 1;
          return;
        }

        arguments = json_loads (input,
                                JSON_DECODE_ANY,
                                &error);
        if (NULL == arguments)
        {
          GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                      "Failed to parse arguments on line %u:%u: %s!\n",
                      error.line,
                      error.column,
                      error.text);
          GNUNET_SCHEDULER_shutdown ();
          global_ret = 1;
          return;
        }
      }
      else {
        GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                    "Failed to parse arguments on line %u:%u: %s!\n",
                    error.line,
                    error.column,
                    error.text);
        GNUNET_SCHEDULER_shutdown ();
        global_ret = 1;
        return;
      }

      /* initialize HTTP client event loop */
      ctx = GNUNET_CURL_init (&GNUNET_CURL_gnunet_scheduler_reschedule,
                              &rc);
      rc = GNUNET_CURL_gnunet_rc_create (ctx);

      FROSIX_redux_init (ctx);

      ra = FROSIX_redux_sign_start (std_input,
                                    arguments,
                                    message,
                                    &action_cb,
                                    cls);
    }

    /*** VERIFY ***/
    if (0 == strcasecmp (action,
                         "verify"))
    {
      /* check if -m flag, abort if not found */
      if (NULL != message)
      {
        std_input = json_loadf (stdin,
                                JSON_DECODE_ANY,
                                &error);
        if (NULL == std_input)
        {
          GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                      "Failed to parse arguments on line %u:%u: %s!\n",
                      error.line,
                      error.column,
                      error.text);
          GNUNET_SCHEDULER_shutdown ();
          global_ret = 1;
          return;
        }
      }
      else {
        GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                    "Failed to parse arguments on line %u:%u: %s!\n",
                    error.line,
                    error.column,
                    error.text);
        GNUNET_SCHEDULER_shutdown ();
        global_ret = 1;
        return;
      }

      /* verify signature and return OK */
      if (GNUNET_OK != FROSIX_verify_signature (message,
                                                std_input))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                    "Failed to verify signature!\n");
        global_ret = 1;
      }
      else {
        GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                    "Signature verified!\n");
      }
    }

    /*** EXPORT-PK ***/
    if (0 == strcasecmp (action,
                         "export-pk"))
    {
      std_input = json_loadf (stdin,
                              JSON_DECODE_ANY,
                              &error);
      if (NULL == std_input)
      {
        GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                    "Failed to parse arguments on line %u:%u: %s!\n",
                    error.line,
                    error.column,
                    error.text);
        GNUNET_SCHEDULER_shutdown ();
        global_ret = 1;
        return;
      }

      FROSIX_export_public_key (std_input,
                                &action_cb,
                                cls);
    }

    /*** VERIFY-PK ***/
    if (0 == strcasecmp (action,
                         "verify-pk"))
    {
      std_input = json_loadf (stdin,
                              JSON_DECODE_ANY,
                              &error);
      if (NULL == std_input)
      {
        GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                    "Failed to parse arguments on line %u:%u: %s!\n",
                    error.line,
                    error.column,
                    error.text);
        GNUNET_SCHEDULER_shutdown ();
        global_ret = 1;
        return;
      }

      /* verify signature and return OK */
      if (GNUNET_OK != FROSIX_verify_public_key (std_input))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                    "Failed to verify public key!\n");
        global_ret = 1;
      }
      else {
        GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                    "Public key verified!\n");
      }
    }

    /*** DELETE-KEY ***/
    if (0 == strcasecmp (action,
                         "delete-key"))
    {
      /* load json file from std input */
      std_input = json_loadf (stdin,
                              JSON_DECODE_ANY,
                              &error);
      if (NULL == std_input)
      {
        GNUNET_log (GNUNET_ERROR_TYPE_MESSAGE,
                    "Failed to parse arguments on line %u:%u: %s!\n",
                    error.line,
                    error.column,
                    error.text);
        GNUNET_SCHEDULER_shutdown ();
        global_ret = 1;
        return;
      }

      /* initialize HTTP client event loop */
      ctx = GNUNET_CURL_init (&GNUNET_CURL_gnunet_scheduler_reschedule,
                              &rc);
      rc = GNUNET_CURL_gnunet_rc_create (ctx);

      FROSIX_redux_init (ctx);

      ra = FROSIX_redux_key_delete_start (std_input,
                                          &action_cb,
                                          cls);
    }
  }
}


int
main (int argc,
      char *const *argv)
{
  /* the available command line options */
  struct GNUNET_GETOPT_CommandLineOption options[] = {
    GNUNET_GETOPT_option_string ('a',
                                 "arguments",
                                 "JSON",
                                 "pass a JSON string containing arguments to cli",
                                 &input),
    GNUNET_GETOPT_option_string ('o',
                                 "output",
                                 "Filename",
                                 "define output filename",
                                 &output_filename),
    GNUNET_GETOPT_option_string ('m',
                                 "message",
                                 "Message to sign",
                                 "The message to sign",
                                 &message),
    GNUNET_GETOPT_OPTION_END
  };
  enum GNUNET_GenericReturnValue ret;

  /* FIRST get the libtalerutil initialization out
     of the way. Then throw that one away, and force
     the SYNC defaults to be used! */
  (void) TALER_project_data_default ();
  GNUNET_OS_init (FROSIX_project_data_default ());
  ret = GNUNET_PROGRAM_run (argc,
                            argv,
                            "frosix-cli",
                            "This is an application for using Frosix.\n",
                            options,
                            &run,
                            NULL);
  if (GNUNET_SYSERR == ret)
    return 3;
  if (GNUNET_NO == ret)
    return 0;
  return global_ret;
}


/* end of frosix-cli-redux.c */
