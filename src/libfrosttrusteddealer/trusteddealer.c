/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file trusteddealer.c
 * @brief Implementation of the trusted dealer key generation
 * @author Joel Urech
*/
#include "frost_high.h"
#include "keygen.h"
#include "keygen_common.h"

/**
 * Generates a rnd key pair
 *
 * @param[out] sk the complete secret key
 * @param[out] pk corresponding public key
*/
static void
generate_keypair (struct FROST_DkgSecretKey *sk,
                  struct FROST_PublicKey *pk)
{
  FROST_scalar_random (&sk->sk);
  FROST_base_mul_scalar (&pk->pk, &sk->sk);
}


void
FROST_trusted_dealer_keygen (struct FROST_KeyPair key_pairs[],
                             uint8_t num_of_participants,
                             uint8_t threshold)
{
  // Generate keypair
  struct FROST_DkgSecretKey secret_key;
  struct FROST_PublicKey public_key;
  generate_keypair (&secret_key, &public_key);

  struct FROST_DkgShare participant_shares[num_of_participants];
  FROST_generate_shares_ (participant_shares,
                          NULL,
                          &secret_key,
                          NULL,
                          num_of_participants, threshold);

  for (int i = 0; i < num_of_participants; i++)
  {
    key_pairs[i].identifier = i + 1;
    FROST_point_copy_to (&key_pairs[i].group_pk.pk, &public_key.pk);
    FROST_scalar_copy_to (&key_pairs[i].my_sk,
                          &participant_shares[i].share);
    FROST_base_mul_scalar (&key_pairs[i].my_pk,
                           &participant_shares[i].share);
  }
}
