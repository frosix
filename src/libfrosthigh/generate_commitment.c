/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Frosix

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file generate_commitment.c
 * Derives a nonce and a corresponding commitment from the given message hash
 * and the secret key.
*/
#include "frost_high.h"


void
FROST_get_random_seed (struct FROST_CommitmentSeed *seed)
{
  FROST_scalar_random (&seed->scal);
}


void
FROST_generate_nonce_and_commitment (
  struct FROST_Nonce *nonce,
  struct FROST_Commitment *commitment,
  const struct FROST_MessageHash *message_hash,
  const struct FROST_CommitmentSeed *seed)
{
  /* get hash of message and secret key, this is our kdf master key */
  struct FROST_ShortHashState shs;
  struct FROST_ShortHashCode kdf_masterkey;

  FROST_short_hash_init (&shs);
  FROST_short_hash_update_fixed (&shs,
                                 message_hash,
                                 sizeof (*message_hash));
  FROST_short_hash_update_fixed (&shs,
                                 seed,
                                 sizeof (*seed));
  FROST_short_hash_final (&shs,
                          &kdf_masterkey);

  /* get nonce values with the kdf master key */
  FROST_kdf_scalar_to_curve (&nonce->hiding_nonce,
                             1,
                             &kdf_masterkey);
  FROST_kdf_scalar_to_curve (&nonce->binding_nonce,
                             2,
                             &kdf_masterkey);

  /* get commitment values from the nonce values */
  FROST_base_mul_scalar (&commitment->hiding_commitment,
                         &nonce->hiding_nonce);
  FROST_base_mul_scalar (&commitment->binding_commitment,
                         &nonce->binding_nonce);
}
