#include <stdio.h>
#include <string.h>
#include "frost_high.h"
#include "keygen.h"
#include "frost_verify.h"
#include <time.h>
#include <gnunet/gnunet_util_lib.h>

static void
distributed_key_generation (struct FROST_KeyPair key_pairs[],
                            const struct FROST_DkgContextString *context_string,
                            uint8_t num_of_participants,
                            uint8_t threshold)
{
  /* === Create shares and commitments === */
  struct FROST_DkgCommitment dkg_commitments[num_of_participants];
  struct FROST_DkgShare dkg_shares[num_of_participants][num_of_participants];

  for (int i = 0; i < num_of_participants; i++)
  {
    uint8_t my_index = i + 1;
    FROST_initialize_dkg_commitment (&dkg_commitments[i], my_index,
                                     threshold);



    FROST_keygen_begin (&dkg_commitments[i],
                        (struct FROST_DkgShare *) &dkg_shares[i],
                        context_string,
                        NULL,
                        my_index,
                        num_of_participants,
                        threshold);
  }

  /* === Send commitments === */

  /* === Verify commitments received === */
  for (int i = 0; i < num_of_participants; i++)
  {
    GNUNET_assert (1 == FROST_keygen_validate_commitment (
                     &dkg_commitments[i],
                     NULL,
                     num_of_participants));
  }


  /* === Send shares === */
  /* === Receive shares === */

  /* === Compute my key share === */
  for (int i = 0; i < num_of_participants; i++)
  {
    uint8_t my_index = i + 1; // receivers-index

    // Rearrange shares
    struct FROST_DkgShare sorted_shares[num_of_participants];
    for (int j = 0; j < num_of_participants; j++)
    {
      if (j != my_index - 1)
      {
        // Verify received share against commitments
        GNUNET_assert (FROST_keygen_validate_share (&dkg_commitments[j],
                                                    &dkg_shares[j][i],
                                                    my_index));
      }

      FROST_scalar_copy_to (&sorted_shares[j].share,
                            &dkg_shares[j][i].share);
      sorted_shares[j].identifier = i + 1;
    }

    FROST_keygen_finalize (&key_pairs[i], my_index, sorted_shares,
                           dkg_commitments, num_of_participants);
  }

  // Free dkg commitments
  FROST_free_dkg_commitment (dkg_commitments,
                             num_of_participants);
}


static int
get_my_key_pair (const struct FROST_KeyPair shares[],
                 uint8_t id,
                 uint8_t n)
{
  for (int i = 0; i < n; i++)
  {
    if (id == shares[i].identifier)
    {
      return i;
    }
  }
  return -1;
}

// https://www.tutorialspoint.com/c_standard_library/c_function_qsort.htm
static int
cmp_int (const void *a, const void *b)
{
  return (*(uint8_t*) a - *(uint8_t*) b);
}


/*
 */
static void
get_rnd_participants (uint8_t participants[],
                      uint8_t t,
                      uint8_t n)
{
  GNUNET_assert (n >= t);

  memset (participants, 0, t * sizeof (uint8_t));
  for (int i = 0; i < t; i++)
  {
    int check = 1;
    while (check != 0)
    {
      check = 0;
      uint32_t x = randombytes_random () % n + 1;

      for (int j = 0; j < t; j++)
      {
        if (x == participants[j] || x == 0)
        {
          check = 1;
        }
      }
      if (0 == check)
      {
        uint8_t y = (uint8_t) x;
        participants[i] = y;
      }
    }
  }

  qsort (participants, t, sizeof (uint8_t), cmp_int);
}


int
main (void)
{
  GNUNET_log_setup ("WARNING",
                    NULL,
                    NULL);
  // Init crypto library
  if (0 != FROST_init ())
    return 1;

  // ToDo: what if t is '0'
  uint8_t t = 7;
  uint8_t n = 19;

  // === Generate shares and public key ===
  struct FROST_KeyPair key_pairs[n];

  struct FROST_Scalar rnd;
  FROST_scalar_random (&rnd);

  struct FROST_ShortHashState state;
  FROST_short_hash_init (&state);
  FROST_short_hash_update_fixed (&state,
                                 &rnd,
                                 sizeof (rnd));

  struct FROST_DkgContextString context_string;
  FROST_short_hash_final (&state,
                          &context_string.con_str);

  // FROST_trusted_dealer_keygen (key_pairs, n, t);
  distributed_key_generation (key_pairs, &context_string, n, t);


  // === SA: Get a message ===
  char input[] = "This is a very important message!!!";

  struct FROST_MessageHash mh;
  FROST_message_to_hash (&mh, input, strlen (input));


  // === SA: Choose participants ===
  // ToDo: Better solution for choosing rnd participants?
  uint8_t participants[t];
  get_rnd_participants (participants, t, n);


  // === Round One / Preprocess - All: Generate nonces and commitments ===
  struct FROST_Commitment commitment_list[t];
  struct FROST_Nonce nonces[t];
  for (uint8_t i = 0; i < t; i++)
  {
    int index = get_my_key_pair (key_pairs, participants[i], n);

    struct FROST_CommitmentSeed seed;
    FROST_get_random_seed (&seed);

    FROST_generate_nonce_and_commitment (&nonces[i],
                                         &commitment_list[i],
                                         &mh,
                                         &seed);

    commitment_list[i].identifier = key_pairs[index].identifier;
  }

  // SA prepares the commitments and sends them to all participants, together
  // with the message
  // FROST_send_to_participant

  // SA prepares to collect all shares
  struct FROST_SignatureShare sig_shares[t];


  // === Round Two / Sign - ALL: ===
  for (uint8_t i = 0; i < t; i++)
  {
    int index = get_my_key_pair (key_pairs, participants[i], n);

    // Validate all commitments, received from SA
    for (uint8_t j = 0; j < t; j++)
    {
      GNUNET_assert (FROST_validate_commitment (&commitment_list[j]));
    }


    // Sign message
    GNUNET_assert (FROST_sign_message_hash (&sig_shares[i],
                                            &mh,
                                            commitment_list,
                                            t,
                                            &key_pairs[index],
                                            &nonces[i]));

    // Send sig_share to SA
  }


  // === SA: verify all signature shares ===
  for (uint8_t i = 0; i < t; i++)
  {
    GNUNET_assert (FROST_verify_signature_share (&commitment_list[i],
                                                 &sig_shares[i],
                                                 commitment_list, t,
                                                 &key_pairs[i].group_pk,
                                                 &mh));
  }

  // === SA: aggregate all sig shares and computes final signature ===
  struct FROST_Signature sig;
  FROST_compose_signature (&sig, commitment_list, sig_shares, t, &mh);


  // === SA: publish sig ===

  // === Verify signature ===
  if (GNUNET_OK == FROST_verify_signature (&key_pairs[0].group_pk,
                                           &sig,
                                           &mh))
    return 0;

  return 1;
}
