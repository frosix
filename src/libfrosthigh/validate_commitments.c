/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Frosix

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file validate_commitments.c
 * @brief Implements the validation of the commitments
 * @author Joel Urech
*/
#include "frost_high.h"

/**
 * Checks, if a commitment consists out of two valid ristretto255-encoded elements.
*/
enum GNUNET_GenericReturnValue
FROST_validate_commitment (
  const struct FROST_Commitment *commitment)
{
  if (GNUNET_OK != FROST_is_valid_point (&commitment->hiding_commitment)
      || GNUNET_OK != FROST_is_valid_point (&commitment->binding_commitment))
    return GNUNET_NO;

  return GNUNET_OK;
}
