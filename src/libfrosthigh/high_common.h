/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Frosix

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file high_common.h
 * @brief Provides functions which are used for different tasks during the signing or key generation process.
 * @author Joel Urech
*/
#ifndef FROST_HIGH_COMMON_H
#define FROST_HIGH_COMMON_H

#include "frost_high.h"

/**
 * Struct for a binding factor
*/
struct FROST_BindingFactor
{
  /**
   * For which participant is this binding factor?
  */
  uint8_t identifier;
  /**
   * The binding factor
  */
  struct FROST_Scalar binding_factor;
};

/**
 * Struct for a group commitment
*/
struct FROST_GroupCommitment
{
  /**
   * Our group commitment is a point on the curve
  */
  struct FROST_Point commitment;
};

/**
 * Struct for the lagrange coefficient
*/
struct FROST_Coefficient
{
  /**
   * Our lagrange coefficient is a FROST_Scalar
  */
  struct FROST_Scalar coeff;
};

/**
 * Struct for the signature challenge
*/
struct FROST_Challenge
{
  /**
   * Our challenge is a hash, mapped to a scalar in range of the order of the ristretto255 group
  */
  struct FROST_Scalar challenge;
};

/**
 * @brief Computes the set of binding values for all participating participants.
 *
 * @param[out] binding_factors The resulting set of binding factors
 * @param[in] commitments Array of all commitments
 * @param commitments_len Length of commitments array
 * @param[in] mh The message we have to sign
 */
void
FROST_compute_binding_factors_ (
  struct FROST_BindingFactor binding_factors[],
  const struct FROST_Commitment commitments[],
  uint8_t commitments_len,
  const struct FROST_MessageHash *mh);

/**
 * @brief Computes the group commitment.
 *
 * @param[out] group_commitment The resulting group commitment
 * @param[in] commitments List of all commitments
 * @param[in] binding_factors Set of all binding factors
 * @param comm_and_bind_len Number of elements in commitments and binding_factors
 */
void FROST_compute_group_commitment_ (
  struct FROST_GroupCommitment *group_commitment,
  const struct FROST_Commitment commitments[],
  const struct FROST_BindingFactor binding_factors[],
  uint8_t comm_and_bind_len);

/**
 * @brief Returns the binding factor of a specific participant.
 *
 * @param[out] binding_factor The found binding factor
 * @param[in] binding_factors Set of all binding factors
 * @param binding_factors_len Number of elements in binding_factors
 * @param participant The participant for whom we are looking for the binding factor
 * @return GNUNET_OK if a binding factor was found, otherwise GNUNET_NO
*/
enum GNUNET_GenericReturnValue
FROST_binding_factor_for_participant_ (
  struct FROST_BindingFactor *binding_factor,
  const struct FROST_BindingFactor binding_factors[],
  uint8_t binding_factors_len,
  uint8_t participant);

/**
 * @brief Computes the lagrange coefficient for a specific participant.
 *
 * @param[out] coefficient The resulting lagrange coefficient
 * @param[in] participant The participant for which we need the lagrange coefficient
 * @param[in] commitments List of all commitments. We only need the identifiers of the participants from it
 * @param commitments_len Length of commitment list
 * @return 1 if success, otherwise 0
*/
enum GNUNET_GenericReturnValue
FROST_compute_lagrange_coefficient_ (
  struct FROST_Coefficient *coefficient,
  uint8_t participant,
  const struct FROST_Commitment commitments[],
  size_t commitments_len);

/**
 * @brief Computes the signature challenge
 *
 * @param[out] challenge The resulting challenge
 * @param[in] group_commitment A previously computed group commitment
 * @param[in] group_pk Public key of the signing group
 * @param[in] mh Our hash of the message we have to sign
*/
void
FROST_compute_challenge_ (
  struct FROST_Challenge *challenge,
  const struct FROST_GroupCommitment *group_commitment,
  const struct FROST_PublicKey *group_pk,
  const struct FROST_MessageHash *mh);

#endif
