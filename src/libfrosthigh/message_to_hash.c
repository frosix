/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Frosix

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file message_to_hash.c
 * @brief Implements a simple hashing function to convert some input bytes into a hash.
 * @author Joel Urech
*/
#include "frost_high.h"

/**
 * We expect our message to be a hash value. This function provides an interface to hash abritrary messages.
*/
void
FROST_message_to_hash (
  struct FROST_MessageHash *message_hash,
  const void *msg,
  size_t msg_len)
{
  struct FROST_HashState state;
  FROST_hash_init (&state);
  FROST_hash_fixed_update (&state, msg, msg_len);

  FROST_hash_final (&state, &message_hash->hash);
}
