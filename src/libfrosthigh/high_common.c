/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file high_common.c
 * @brief Implementations of function which are used for different tasks during the signing or key generation process.
 * @author Joel Urech
*/
#include "high_common.h"

/**
 * @brief Returns all participants from the commitment list.
 * Pseudo code of this function: https://www.ietf.org/archive/id/draft-irtf-cfrg-frost-12.html#name-list-operations
 *
 * @param participants The resulting list of all participating participants
 * @param commitments The commitment list which we got from the 'SA'
 * @param commitments_len Length of the commitment list
*/
static void
participants_from_commitment_list (
  uint8_t participants[],
  const struct FROST_Commitment commitments[],
  uint8_t commitments_len)
{
  for (uint8_t i = 0; i < commitments_len; i++)
  {
    participants[i] = commitments[i].identifier;
  }
}

/**
 * Computes the set of binding values for all participating participants.
 * Pseudo code of this function: https://www.ietf.org/archive/id/draft-irtf-cfrg-frost-12.html#name-binding-factors-computation
*/
void
FROST_compute_binding_factors_ (
  struct FROST_BindingFactor binding_factors[],
  const struct FROST_Commitment commitments[],
  uint8_t commitments_len,
  const struct FROST_MessageHash *mh)
{
  // H(group_commitment_list)
  struct FROST_HashState hash_state;
  FROST_hash_init (&hash_state);

  for (uint8_t i = 0; i < commitments_len; i++)
  {
    FROST_hash_uint8_update (&hash_state, commitments[i].identifier);
    FROST_hash_point_update (&hash_state,
                             &commitments[i].hiding_commitment);
    FROST_hash_point_update (&hash_state,
                             &commitments[i].binding_commitment);
  }

  struct FROST_HashCode group_commitments_hash;
  FROST_hash_final (&hash_state, &group_commitments_hash);

  // 2. H(identifier || message || group_commitment_hash)
  for (uint8_t i = 0; i < commitments_len; i++)
  {
    struct FROST_HashState state;
    FROST_hash_init (&state);

    FROST_hash_uint8_update (&state, commitments[i].identifier);
    FROST_hash_hash_update (&state, &mh->hash);
    FROST_hash_hash_update (&state, &group_commitments_hash);

    struct FROST_HashCode rho_input;
    FROST_hash_final (&state, &rho_input);

    FROST_hash_to_scalar (&binding_factors[i].binding_factor, &rho_input);

    // Set index
    binding_factors[i].identifier = commitments[i].identifier;
  }
}

/**
 * Returns the binding factor of a specific participant.
 * Pseudo code of this function: https://www.ietf.org/archive/id/draft-irtf-cfrg-frost-12.html#name-list-operations
*/
enum GNUNET_GenericReturnValue
FROST_binding_factor_for_participant_ (
  struct FROST_BindingFactor *binding_factor,
  const struct FROST_BindingFactor binding_factors[],
  uint8_t binding_factors_len,
  uint8_t participant)
{
  for (uint8_t i = 0; i < binding_factors_len; i++)
  {
    if (participant == binding_factors[i].identifier)
    {
      FROST_scalar_copy_to (&binding_factor->binding_factor,
                            &binding_factors[i].binding_factor);
      binding_factor->identifier = binding_factors[i].identifier;
      return GNUNET_OK;
    }
  }
  return GNUNET_NO;     // invalid participant
}

/**
 * Computes the group commitment.
 * Pseudo code of this function: https://www.ietf.org/archive/id/draft-irtf-cfrg-frost-12.html#name-group-commitment-computatio
 *
*/
void
FROST_compute_group_commitment_ (
  struct FROST_GroupCommitment *group_commitment,
  const struct FROST_Commitment commitments[],
  const struct FROST_BindingFactor
  binding_factors[],
  uint8_t comm_and_bind_len)
{
  // Set group commitment to the identity element
  FROST_point_identity (&group_commitment->commitment);

  for (uint8_t i = 0; i < comm_and_bind_len; i++)
  {
    // Get binding factor
    struct FROST_BindingFactor binding_factor;
    FROST_binding_factor_for_participant_ (&binding_factor, binding_factors,
                                           comm_and_bind_len,
                                           binding_factors[i].identifier);

    // scalarmult binding_nonce_commitment, binding_factor
    struct FROST_Point binding_nonce;
    FROST_point_mul_scalar (&binding_nonce,
                            &commitments[i].binding_commitment,
                            &binding_factor.binding_factor);

    // Point add hiding_nonce_commitment with binding_nonce
    struct FROST_Point hiding_nonce;
    FROST_point_add_point (&hiding_nonce, &commitments[i].hiding_commitment,
                           &binding_nonce);

    // Point add group_commitment, ^
    FROST_point_add_point (&group_commitment->commitment,
                           &group_commitment->commitment, &hiding_nonce);
  }
}

/**
 * Computes the lagrange coefficient for a specific participant.
 * Pseudo code of this function: https://www.ietf.org/archive/id/draft-irtf-cfrg-frost-12.html#name-polynomials
 *
*/
enum GNUNET_GenericReturnValue
FROST_compute_lagrange_coefficient_ (
  struct FROST_Coefficient *coefficient,
  uint8_t participant,
  const struct FROST_Commitment commitments[],
  size_t commitments_len)
{
  uint8_t indices[commitments_len];
  participants_from_commitment_list (indices, commitments, commitments_len);

  // validate inputs -> invalid parameters
  if (0 == participant)    // own id is NOT 0
    return GNUNET_NO;

  int id_in_indices = 0;

  for (uint8_t i = 0; i < commitments_len; i++)
  {
    if (0 == indices[i])    // no other id is 0
      return GNUNET_NO;

    if (participant == indices[i])
      id_in_indices++;
  }

  if (1 != id_in_indices) // own id exists exactly 1 time
    return GNUNET_NO;

  // x-coordinate (id) as 32-byte unsigned char array
  struct FROST_Scalar xi;
  FROST_scalar_set_uint8 (&xi, participant);

  // Initialize numerator and denominator to '1' and as 32-byte unsigned char
  // array
  struct FROST_Scalar num;
  struct FROST_Scalar den;
  FROST_scalar_one (&num);
  FROST_scalar_one (&den);

  // Coefficient = num / den = prod(j - 0) / prod(j - i) \ j = i
  for (uint8_t j = 0; j < commitments_len; j++)
  {

    // If j == i, do nothing!
    if (indices[j] != participant)
    {
      // x-coordinate of 'j' as 32-byte unsigned char array
      struct FROST_Scalar xj;
      FROST_scalar_set_uint8 (&xj, indices[j]);

      // num *= (j - 0) = j
      FROST_scalar_mul_scalar (&num, &num, &xj);

      // den *= (j - i)
      struct FROST_Scalar den_temp;
      FROST_scalar_sub_scalar (&den_temp, &xj, &xi);
      FROST_scalar_mul_scalar (&den, &den, &den_temp);
    }
  }

  // coeff = num / den = num * inv(den)
  struct FROST_Scalar den_inv;
  FROST_scalar_invert (&den_inv, &den);
  FROST_scalar_mul_scalar (&coefficient->coeff, &num, &den_inv);

  return GNUNET_OK;
}

/**
 * Computes the signature challenge
 * Pseudo code of this function: https://www.ietf.org/archive/id/draft-irtf-cfrg-frost-12.html#name-signature-challenge-computa
*/
void
FROST_compute_challenge_ (
  struct FROST_Challenge *challenge,
  const struct FROST_GroupCommitment *group_commitment,
  const struct FROST_PublicKey *group_pk,
  const struct FROST_MessageHash *mh)
{
  struct FROST_HashState hash_state;
  FROST_hash_init (&hash_state);

  FROST_hash_point_update (&hash_state, &group_commitment->commitment);
  FROST_hash_point_update (&hash_state, &group_pk->pk);
  FROST_hash_hash_update (&hash_state, &mh->hash);

  struct FROST_HashCode challenge_hash;
  FROST_hash_final (&hash_state, &challenge_hash);

  FROST_hash_to_scalar (&challenge->challenge, &challenge_hash);

  return;
}
