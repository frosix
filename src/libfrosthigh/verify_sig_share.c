/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Frosix

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file verify_sig_share.c
 * @brief Implements the function to verify a signature share against a commitment
 * @author Joel Urech
*/
#include "frost_high.h"
#include "high_common.h"

/**
 * Gives the 'SA' the possibility to verify each received signature share against the commitment from round1.
*/
enum GNUNET_GenericReturnValue
FROST_verify_signature_share (
  const struct FROST_Commitment *commitment_i,
  const struct FROST_SignatureShare *signature_share_i,
  const struct FROST_Commitment commitments[],
  uint8_t commitments_len,
  const struct FROST_PublicKey *public_key,
  const struct FROST_MessageHash *message_hash)
{
  // === Compute the binding factors ===
  struct FROST_BindingFactor binding_factors[commitments_len];
  FROST_compute_binding_factors_ (binding_factors, commitments,
                                  commitments_len, message_hash);

  struct FROST_BindingFactor binding_factor;
  if (0
      == FROST_binding_factor_for_participant_ (&binding_factor,
                                                binding_factors,
                                                commitments_len,
                                                commitment_i->identifier))
    return GNUNET_NO;

  // === Compute the group commitment ===
  struct FROST_GroupCommitment group_commitment;
  FROST_compute_group_commitment_ (&group_commitment, commitments,
                                   binding_factors, commitments_len);

  // === compute the commitment share
  struct FROST_Point comm_share;
  FROST_point_mul_scalar (&comm_share, &commitment_i->binding_commitment,
                          &binding_factor.binding_factor);
  FROST_point_add_point (&comm_share, &comm_share,
                         &commitment_i->hiding_commitment);

  // === Compute challenge ===
  struct FROST_Challenge challenge;
  FROST_compute_challenge_ (&challenge, &group_commitment, public_key,
                            message_hash);

  // === Compute coefficient
  struct FROST_Coefficient coeff;
  if (GNUNET_OK
      != FROST_compute_lagrange_coefficient_ (&coeff,
                                              commitment_i->identifier,
                                              commitments, commitments_len))
    return GNUNET_NO;

  // Compute relation values
  struct FROST_Point l;
  FROST_base_mul_scalar (&l, &signature_share_i->sig_share);

  struct FROST_Scalar challenge_lambda;
  FROST_scalar_mul_scalar (&challenge_lambda, &challenge.challenge,
                           &coeff.coeff);

  struct FROST_Point r;
  FROST_point_mul_scalar (&r, &signature_share_i->pk_i, &challenge_lambda);
  FROST_point_add_point (&r, &r, &comm_share);

  return FROST_point_cmp (&l,
                          &r);
}
