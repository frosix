/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Frosix

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file sign_message_hash.c
 * @brief Implements the signing of a message
 * @author Joel Urech
*/
#include <gnunet/gnunet_util_lib.h>
#include "frost_high.h"
#include "high_common.h"

/**
 * @brief Internal function to compute the signature share
 *
 * @param sig_share The resulting signature share
 * @param my_key_pair Secret key material
 * @param nonce The generated nonce from round1
 * @param binding_factors Binding factors of all participating participants
 * @param binding_factors_len Length of the binding factors array
 * @param coefficient Our lagrange coefficient
 * @param challenge The group challenge
 * @return #GNUNET_OK
 *         #GNUNET_NO
*/
static enum GNUNET_GenericReturnValue
compute_sig_share (
  struct FROST_SignatureShare *sig_share,
  const struct FROST_KeyPair *my_key_pair,
  const struct FROST_Nonce *nonce,
  const struct FROST_BindingFactor binding_factors[],
  size_t binding_factors_len,
  const struct FROST_Coefficient *coefficient,
  const struct FROST_Challenge *challenge)
{
  // get my_binding_factor
  struct FROST_BindingFactor my_binding_factor;
  if (GNUNET_OK
      != FROST_binding_factor_for_participant_ (&my_binding_factor,
                                                binding_factors,
                                                binding_factors_len,
                                                my_key_pair->identifier))
    return GNUNET_NO;


  // binding_nonce * binding_factor
  struct FROST_Scalar binding;
  FROST_scalar_mul_scalar (&binding, &nonce->binding_nonce,
                           &my_binding_factor.binding_factor);

  // lambda_i * sk_i * challenge
  struct FROST_Scalar challenge_sk;

  FROST_scalar_mul_scalar (&challenge_sk, &coefficient->coeff,
                           &my_key_pair->my_sk);
  FROST_scalar_mul_scalar (&challenge_sk, &challenge_sk,
                           &challenge->challenge);

  // hiding + binding
  FROST_scalar_add_scalar (&sig_share->sig_share, &nonce->hiding_nonce,
                           &binding);

  // + challenge
  FROST_scalar_add_scalar (&sig_share->sig_share, &sig_share->sig_share,
                           &challenge_sk);

  // Set index
  sig_share->identifier = my_key_pair->identifier;

  // Set pk of sk_share
  FROST_base_mul_scalar (&sig_share->pk_i, &my_key_pair->my_sk);

  return GNUNET_OK;
}


enum GNUNET_GenericReturnValue
FROST_sign_message_hash (struct FROST_SignatureShare *signature_share,
                         const struct FROST_MessageHash *message_hash,
                         const struct FROST_Commitment commitments[],
                         size_t commitments_len,
                         const struct FROST_KeyPair *my_key_pair,
                         const struct FROST_Nonce *my_nonce)
{
  // Compute the binding factor(s)
  struct FROST_BindingFactor binding_factors[commitments_len];
  FROST_compute_binding_factors_ (binding_factors, commitments,
                                  commitments_len, message_hash);

  // Compute the group commitment
  struct FROST_GroupCommitment group_commitment;
  FROST_compute_group_commitment_ (&group_commitment, commitments,
                                   binding_factors, commitments_len);

  // Compute the per-message challenge
  struct FROST_Challenge challenge;
  FROST_compute_challenge_ (&challenge, &group_commitment,
                            &my_key_pair->group_pk, message_hash);

  // Compute coefficient
  struct FROST_Coefficient coeff_i;

  if (GNUNET_OK != FROST_compute_lagrange_coefficient_ (&coeff_i,
                                                        my_key_pair->identifier,
                                                        commitments,
                                                        commitments_len))
    return GNUNET_NO;

  // Compute the signature share
  return compute_sig_share (signature_share, my_key_pair, my_nonce,
                            binding_factors, commitments_len, &coeff_i,
                            &challenge);
}
