/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file compose_signature.c
 * @brief Allows the 'Signatur Aggregator' to aggregate all received signature shares (z-value)
 * and together with the commitments, computes the challenge (r-value).
 * @author Joel Urech
*/
#include "frost_high.h"
#include "high_common.h"

/**
 * Aggregates the signature shares from all participants, computes the group commitment aka 'R' value
 * and returns the final signature over the given message.
 */
void
FROST_compose_signature (struct FROST_Signature *signature,
                         const struct FROST_Commitment commitments[],
                         const struct FROST_SignatureShare signature_shares[],
                         uint8_t commitments_and_sig_shares_len,
                         const struct FROST_MessageHash *message_hash)
{
  FROST_scalar_zero (&signature->z);

  for (uint8_t i = 0; i < commitments_and_sig_shares_len; i++)
  {
    FROST_scalar_add_scalar (&signature->z, &signature->z,
                             &signature_shares[i].sig_share);
  }

  // === Compute the binding factors ===
  struct FROST_BindingFactor binding_factors[commitments_and_sig_shares_len];
  FROST_compute_binding_factors_ (binding_factors, commitments,
                                  commitments_and_sig_shares_len,
                                  message_hash);

  // Compute the group commitment
  struct FROST_GroupCommitment group_commitment;
  FROST_compute_group_commitment_ (&group_commitment, commitments,
                                   binding_factors,
                                   commitments_and_sig_shares_len);

  // Copy group_commitment to sig
  FROST_point_copy_to (&signature->r, &group_commitment.commitment);
}
