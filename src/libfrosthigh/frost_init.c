/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Frosix

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file frost_init.c
 * @brief FROST_init() has always to be called first! It initializes the underlying libsodium library
 * @author Joel Urech
*/
#include "frost_high.h"

/**
 * @brief Initializes the underlying libsodium library.
 * Always call this function first, before using libfrost!
 *
 * @result Returns 0 on success, -1 on failure and 1 if the library had already been initialized
*/
int
FROST_init ()
{
  return FROST_low_init ();
}
