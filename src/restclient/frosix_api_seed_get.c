/*
  This file is part of ANASTASIS
  Copyright (C) 2014-2019 Anastasis SARL

  ANASTASIS is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  ANASTASIS is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with ANASTASIS; see the file COPYING.LGPL.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file restclient/frosix_api_seed_get.c
 * @brief Implementation of the /policy GET and POST
 * @author Christian Grothoff
 * @author Dennis Neufeld
 * @author Dominik Meister
 */
#include "platform.h"
#include <curl/curl.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include "frosix_service.h"
#include "frosix_api_curl_defaults.h"


void
FROSIX_seed_get_cancel (struct FROSIX_SeedGetOperation *sgo)
{
  if (NULL != sgo->job)
  {
    GNUNET_CURL_job_cancel (sgo->job);
    sgo->job = NULL;
  }
  GNUNET_free (sgo->url);
  GNUNET_free (sgo);
}


/**
 * Process GET /seed response
 */
static void
handle_seed_get_finished (void *cls,
                          long response_code,
                          const void *data,
                          size_t data_size)
{
  struct FROSIX_SeedGetOperation *sgo = cls;

  sgo->job = NULL;
  switch (response_code)
  {
  case 0:
    /* Hard error */
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Backend didn't even return from GET /seed\n");
    break;
  case MHD_HTTP_OK:
    {
      struct FROSIX_ProviderSeed ps;

      if (sizeof (*ps.seed) != data_size)
      {
        GNUNET_break_op (0);
        response_code = 0;
        break;
      }
      /* Sucess, call callback with all details! */
      ps.provider_index = sgo->provider_index;
      memset (&ps.seed, 0, sizeof (ps.seed));
      ps.seed = data;

      sgo->cb (sgo->cb_cls,
               response_code,
               &ps);
      sgo->cb = NULL;
      FROSIX_seed_get_cancel (sgo);
      return;
    }
  case MHD_HTTP_BAD_REQUEST:
    /* This should never happen, either us or the anastasis server is buggy
       (or API version conflict); just pass JSON reply to the application */
    break;
  case MHD_HTTP_NOT_FOUND:
    /* Nothing really to verify */
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    /* Server had an internal issue; we should retry, but this API
       leaves this to the application */
    break;
  default:
    /* unexpected response code */
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u\n",
                (unsigned int) response_code);
    GNUNET_break (0);
    response_code = 0;
    break;
  }
  sgo->cb (sgo->cb_cls,
           response_code,
           NULL);
  sgo->cb = NULL;
  FROSIX_seed_get_cancel (sgo);
}


struct FROSIX_SeedGetOperation *
FROSIX_seed_get (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  uint8_t provider_index,
  FROSIX_SeedGetCallback cb,
  void *cb_cls)
{
  struct FROSIX_SeedGetOperation *sgo;

  GNUNET_assert (NULL != cb);
  sgo = GNUNET_new (struct FROSIX_SeedGetOperation);
  sgo->url = TALER_url_join (backend_url,
                             "seed",
                             NULL);
  sgo->provider_index = provider_index;

  sgo->ctx = ctx;
  sgo->cb = cb;
  sgo->cb_cls = cb_cls;
  {
    CURL *eh;

    eh = FROSIX_curl_easy_get_ (sgo->url);
    sgo->job = GNUNET_CURL_job_add_raw (ctx,
                                        eh,
                                        NULL,
                                        &handle_seed_get_finished,
                                        sgo);
  }

  if (NULL == sgo->job)
  {
    GNUNET_free (sgo->url);
    GNUNET_free (sgo);
    return NULL;
  }

  return sgo;
}
