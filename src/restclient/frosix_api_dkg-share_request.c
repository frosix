/*
  This file is part of ANASTASIS
  Copyright (C) 2014-2019 Anastasis SARL

  ANASTASIS is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  ANASTASIS is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with ANASTASIS; see the file COPYING.LGPL.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file restclient/frosix_api_dkg-share_request.c
 * @brief Implementation of the /dkg-share POST
 * @author Christian Grothoff
 * @author Dennis Neufeld
 * @author Dominik Meister
 * @author Joel Urech
 */
#include "platform.h"
#include <curl/curl.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include "frosix_service.h"
#include "frost_high.h"
#include "frosix_api_curl_defaults.h"
#include <gnunet/gnunet_json_lib.h>
#include <taler/taler_json_lib.h>


/**
 * @brief A Contract Operation Handle
 */
struct FROSIX_DkgShareRequestOperation
{
  /**
   * The url for this request, including parameters.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Provider index
  */
  uint8_t provider_index;

  /**
   * The CURL context to connect to the backend
  */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Function to call with the result.
   */
  FROSIX_DkgShareRequestCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;
};


void
FROSIX_dkg_share_request_cancel (
  struct FROSIX_DkgShareRequestOperation *dso)
{
  if (NULL != dso->job)
  {
    GNUNET_CURL_job_cancel (dso->job);
    dso->job = NULL;
  }
  GNUNET_free (dso->url);
  GNUNET_free (dso);
}


/**
 * Callback to process POST /dkg-share response
 *
 * @param cls the `struct FROSIX_DkgShareRequestOperation`
 * @param response_code HTTP response code, 0 on error
 * @param data response body
 * @param data_size number of byte in @a data
*/
static void
handle_dkg_share_request_finished (void *cls,
                                   long response_code,
                                   const void *response)
{
  struct FROSIX_DkgShareRequestOperation *dso = cls;
  struct FROSIX_DkgShareRequestDetails dsd;
  const json_t *json_response = response;

  dso->job = NULL;
  dsd.http_status = response_code;
  dsd.ec = TALER_EC_NONE;

  switch (response_code)
  {
  case 0:
    /* Hard error */
    GNUNET_break (0);
    dsd.dss = FROSIX_DSS_HTTP_ERROR;
    dsd.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_CREATED:
  case MHD_HTTP_OK:
    {
      dsd.dss = FROSIX_DSS_SUCCESS;
      dsd.provider_index = dso->provider_index;

      /* We got a result, lets parse it */
      json_t *secret_shares = NULL;
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_json ("secret_shares",
                               &secret_shares),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json_response,
                             spec,
                             NULL,
                             NULL))
      {
        /* Parsing failed! */
        GNUNET_break_op (0);
        dsd.http_status = 0;
        GNUNET_JSON_parse_free (spec);
        break;
      }

      /* check if size of array is in allowed range */
      if (0 >= json_array_size (secret_shares)
          || json_array_size (secret_shares) >= 254)
      {
        GNUNET_break_op (0);
        dsd.http_status = 0;
        GNUNET_JSON_parse_free (spec);
        break;
      }

      dsd.shares_len = json_array_size (secret_shares);

      /* allocate memory */
      dsd.shares = GNUNET_malloc (dsd.shares_len * sizeof (*dsd.shares));
      GNUNET_assert (NULL != dsd.shares);

      for (unsigned int i = 0; i < dsd.shares_len; i++)
      {
        struct GNUNET_JSON_Specification share_spec[] = {
          GNUNET_JSON_spec_uint8 ("target",
                                  &dsd.shares[i].target),
          GNUNET_JSON_spec_uint8 ("issuer",
                                  &dsd.shares[i].issuer),
          GNUNET_JSON_spec_fixed_auto ("secret_share",
                                       &dsd.shares[i].encrypted_share),
          GNUNET_JSON_spec_fixed_auto ("ephemeral_key",
                                       &dsd.shares[i].ephemeral_key),
          GNUNET_JSON_spec_end ()
        };

        if (GNUNET_OK !=
            GNUNET_JSON_parse (json_array_get (secret_shares,
                                               i),
                               share_spec,
                               NULL,
                               NULL))
        {
          GNUNET_break_op (0);
          GNUNET_JSON_parse_free (share_spec);
          GNUNET_JSON_parse_free (spec);
          dsd.http_status = 0;
          break;
        }

        GNUNET_JSON_parse_free (share_spec);
      }

      GNUNET_JSON_parse_free (spec);

      break;
    }
  case MHD_HTTP_BAD_REQUEST:
    /* We have a conflict with the API */
    GNUNET_break (0);
    dsd.dss = FROSIX_DSS_CLIENT_ERROR;
    dsd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    /* The provider has a problem! */
    GNUNET_break (0);
    dsd.dss = FROSIX_DSS_SERVER_ERROR;
    dsd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  default:
    /* Unexpected response code */
    GNUNET_break (0);
    dsd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  }

  /* return to callback function with the data from the response */
  dso->cb (dso->cb_cls,
           &dsd);
  dso->cb = NULL;

  FROSIX_dkg_share_request_cancel (dso);
}


/**
 * Handle HTTP header received by curl.
 *
 * @param buffer one line of HTTP header data
 * @param size size of an item
 * @param userdata our `struct FROSIX_DkgShareRequestOperation`
 * @return `size * nitems`
*/
static size_t
handle_header (char *buffer,
               size_t size,
               size_t nitems,
               void *userdata)
{
  // struct FROSIX_DkgShareRequestOperation *dso = userdata;
  size_t total = size * nitems;
  char *ndup;
  const char *hdr_type;
  char *hdr_val;
  char *sp;

  ndup = GNUNET_strndup (buffer,
                         total);

  hdr_type = strtok_r (ndup,
                       ":",
                       &sp);
  if (NULL == hdr_type)
  {
    GNUNET_free (ndup);
    return total;
  }
  hdr_val = strtok_r (NULL,
                      "\n\r",
                      &sp);
  if (NULL == hdr_val)
  {
    GNUNET_free (ndup);
    return total;
  }
  if (' ' == *hdr_val)
    hdr_val++;
  /* ... FIXME */
  return total;
}


struct FROSIX_DkgShareRequestOperation *
FROSIX_dkg_share_request (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct FROSIX_DkgRequestIdP *uuid,
  uint8_t identifier,
  uint8_t threshold,
  uint8_t num_of_participants,
  const struct FROSIX_DkgContextStringP *context_string,
  const struct FROSIX_ChallengeHashP *challenge_hash,
  const struct FROSIX_DkgCommitment dkg_commitments[],
  size_t len,
  const struct GNUNET_CRYPTO_EddsaPublicKey providers_public_keys[],
  FROSIX_DkgShareRequestCallback cb,
  void *cb_cls)
{
  struct FROSIX_DkgShareRequestOperation *dso;
  CURL *eh;
  char *json_str;

  /* check if we got a callback function, abort if not */
  GNUNET_assert (NULL != cb);

  dso = GNUNET_new (struct FROSIX_DkgShareRequestOperation);

  {
    /* prepare URL */
    char *uuid_str;
    char *path;

    uuid_str = GNUNET_STRINGS_data_to_string_alloc (uuid,
                                                    sizeof (*uuid));

    GNUNET_asprintf (&path,
                     "dkg-shares/%s",
                     uuid_str);

    dso->url = TALER_url_join (backend_url,
                               path,
                               NULL);

    GNUNET_free (path);
    GNUNET_free (uuid_str);
  }

  dso->provider_index = identifier;

  /* array for all providers public keys */
  json_t *json_public_keys = json_array ();

  for (unsigned int i = 0; i < num_of_participants; i++)
  {
    /* single public key */
    json_t *public_key;
    public_key = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_data_auto (NULL,
                                  &providers_public_keys[i]));

    /* add to public key array */
    GNUNET_assert (0 ==
                   json_array_append_new (
                     json_public_keys,
                     public_key));
  }

  {
    /* array for all dkg commitments */
    json_t *commitments = json_array ();

    for (unsigned int i = 0; i < len; i++)
    {
      /* dkg commitment values */
      json_t *commitment_values = json_array ();
      for (unsigned int j = 0;
           j < dkg_commitments[i].commitment.shares_commitments_length;
           j++)
      {
        /* single commitment value */
        json_t *temp_value = json_object ();
        temp_value = GNUNET_JSON_PACK (
          GNUNET_JSON_pack_data_auto (
            NULL,
            &dkg_commitments[i].commitment.share_comm[j]));

        /* add to commitment_values array */
        GNUNET_assert (0 ==
                       json_array_append_new (
                         commitment_values,
                         temp_value));
      }

      /* single dkg commitment */
      json_t *dkg_commitment;
      dkg_commitment = GNUNET_JSON_PACK (
        GNUNET_JSON_pack_uint64 ("provider_index",
                                 dkg_commitments[i].commitment.identifier),
        GNUNET_JSON_pack_array_steal ("dkg_commitment",
                                      commitment_values),
        GNUNET_JSON_pack_data_auto ("zkp_r",
                                    &dkg_commitments[i].commitment.zkp.r),
        GNUNET_JSON_pack_data_auto ("zkp_z",
                                    &dkg_commitments[i].commitment.zkp.z),
        GNUNET_JSON_pack_data_auto ("public_key",
                                    &dkg_commitments[i].encryption_public_key));

      /* add to commitments array */
      GNUNET_assert (0 ==
                     json_array_append_new (
                       commitments,
                       dkg_commitment));
    }

    /* pack the json object for the request body */
    json_t *dkg_share_data;
    dkg_share_data = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_uint64 ("provider_index",
                               identifier),
      GNUNET_JSON_pack_uint64 ("threshold",
                               threshold),
      GNUNET_JSON_pack_uint64 ("num_of_participants",
                               num_of_participants),
      GNUNET_JSON_pack_data_auto ("context_string",
                                  context_string),
      GNUNET_JSON_pack_data_auto ("auth_hash",
                                  challenge_hash),
      GNUNET_JSON_pack_array_steal ("providers_public_keys",
                                    json_public_keys),
      GNUNET_JSON_pack_array_steal ("dkg_commitments",
                                    commitments));

    json_str = json_dumps (dkg_share_data,
                           JSON_COMPACT);

    /* check if we have a json object, abort if it was not successful */
    GNUNET_assert (NULL != json_str);

    json_decref (dkg_share_data);
  }

  /* prepare curl options and fire the request */
  dso->ctx = ctx;
  dso->cb = cb;
  dso->cb_cls = cb_cls;

  eh = FROSIX_curl_easy_get_ (dso->url);

  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_POSTFIELDS,
                                   json_str));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_POSTFIELDSIZE,
                                   strlen (json_str)));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_HEADERFUNCTION,
                                   &handle_header));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_HEADERDATA,
                                   dso));

  dso->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_dkg_share_request_finished,
                                  dso);

  return dso;
}