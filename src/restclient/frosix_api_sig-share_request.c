/*
  This file is part of ANASTASIS
  Copyright (C) 2014-2019 Anastasis SARL

  ANASTASIS is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  ANASTASIS is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with ANASTASIS; see the file COPYING.LGPL.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file restclient/frosix_api_sig-share_request.c
 * @brief Implementation of the /sig-share POST
 * @author Christian Grothoff
 * @author Dennis Neufeld
 * @author Dominik Meister
 * @author Joel Urech
 */
#include "platform.h"
#include <curl/curl.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include "frosix_service.h"
#include "frost_high.h"
#include "frosix_api_curl_defaults.h"
#include <gnunet/gnunet_json_lib.h>
#include <taler/taler_json_lib.h>


/**
 * @brief A Contract Operation Handle
 */
struct FROSIX_SigShareRequestOperation
{
  /**
   * The url for this request, including parameters.
   */
  char *url;

  /**
   * Position in the array.
  */
  uint8_t array_index;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * The CURL context to connect to the backend
  */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Function to call with the result.
   */
  FROSIX_SigShareRequestCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Identifier of the provider
  */
  uint8_t provider_index;
};


void
FROSIX_sig_share_request_cancel (
  struct FROSIX_SigShareRequestOperation *sso)
{
  if (NULL != sso->job)
  {
    GNUNET_CURL_job_cancel (sso->job);
    sso->job = NULL;
  }
  GNUNET_free (sso->url);
  GNUNET_free (sso);
}


/**
 * Callback to process POST /sig-share response
 *
 * @param cls the `struct FROSIX_SigShareRequestOperation`
 * @param response_code HTTP response code, 0 on error
 * @param data response body
 * @param data_size number of byte in @a data
*/
static void
handle_sig_share_request_finished (void *cls,
                                   long response_code,
                                   const void *response)
{
  struct FROSIX_SigShareRequestOperation *sso = cls;
  struct FROSIX_SigShareRequestDetails ssd;
  const json_t *json_response = response;

  sso->job = NULL;
  ssd.http_status = response_code;
  ssd.ec = TALER_EC_NONE;
  ssd.array_index = sso->array_index;
  ssd.sig_share.identifier = sso->provider_index;

  switch (response_code)
  {
  case 0:
    /* Hard error */
    GNUNET_break (0);
    ssd.sss = FROSIX_SSS_HTTP_ERROR;
    ssd.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_CREATED:
  case MHD_HTTP_OK:
    {
      ssd.sss = FROSIX_SSS_SUCCESS;

      /* We got a result, lets parse it */
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_fixed_auto ("signature_share",
                                     &ssd.sig_share.sig_share),
        GNUNET_JSON_spec_fixed_auto ("public_key_share",
                                     &ssd.sig_share.pk_i),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json_response,
                             spec,
                             NULL,
                             NULL))
      {
        /* Parsing failed! */
        GNUNET_break_op (0);
        ssd.http_status = 0;
        GNUNET_JSON_parse_free (spec);
        break;
      }

      GNUNET_JSON_parse_free (spec);
      break;
    }
  case MHD_HTTP_BAD_REQUEST:
    /* We have a conflict with the API */
    GNUNET_break (0);
    ssd.sss = FROSIX_SSS_CLIENT_ERROR;
    ssd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    /* The provider has a problem! */
    GNUNET_break (0);
    ssd.sss = FROSIX_SSS_SERVER_ERROR;
    ssd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  default:
    /* Unexpected response code */
    GNUNET_break (0);
    ssd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  }

  /* return to callback function with the data from the response */
  sso->cb (sso->cb_cls,
           &ssd);
  sso->cb = NULL;

  FROSIX_sig_share_request_cancel (sso);
  return;
}


/**
 * Handle HTTP header received by curl.
 *
 * @param buffer one line of HTTP header data
 * @param size size of an item
 * @param userdata our `struct FROSIX_SigShareRequestOperation`
 * @return `size * nitems`
*/
static size_t
handle_header (char *buffer,
               size_t size,
               size_t nitems,
               void *userdata)
{
  // struct FROSIX_SigShareRequestOperation *sso = userdata;
  size_t total = size * nitems;
  char *ndup;
  const char *hdr_type;
  char *hdr_val;
  char *sp;

  ndup = GNUNET_strndup (buffer,
                         total);

  hdr_type = strtok_r (ndup,
                       ":",
                       &sp);
  if (NULL == hdr_type)
  {
    GNUNET_free (ndup);
    return total;
  }
  hdr_val = strtok_r (NULL,
                      "\n\r",
                      &sp);
  if (NULL == hdr_val)
  {
    GNUNET_free (ndup);
    return total;
  }
  if (' ' == *hdr_val)
    hdr_val++;
  /* ... FIXME */
  return total;
}


struct FROSIX_SigShareRequestOperation *
FROSIX_sig_share_request (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct FROSIX_SigRequestIdP *uuid,
  uint8_t provider_index,
  uint8_t array_index,
  const struct FROSIX_EncryptionKey *encryption_key,
  const struct FROST_MessageHash *message_hash,
  const struct FROST_Commitment commitments[],
  size_t len,
  FROSIX_SigShareRequestCallback cb,
  void *cb_cls)
{
  struct FROSIX_SigShareRequestOperation *sso;
  CURL *eh;
  char *json_str;

  /* check if we got a callback function, abort if not */
  GNUNET_assert (NULL != cb);

  sso = GNUNET_new (struct FROSIX_SigShareRequestOperation);
  sso->array_index = array_index;
  sso->provider_index = provider_index;

  {
    /* prepare URL */
    char *uuid_str;
    char *path;

    uuid_str = GNUNET_STRINGS_data_to_string_alloc (uuid,
                                                    sizeof (*uuid));

    GNUNET_asprintf (&path,
                     "sig-share/%s",
                     uuid_str);

    sso->url = TALER_url_join (backend_url,
                               path,
                               NULL);

    GNUNET_free (path);
    GNUNET_free (uuid_str);
  }

  {
    /* array for all packed commitments */
    json_t *commits = json_array ();

    for (unsigned int i = 0; i < len; i++)
    {
      /* single commitment */
      json_t *commit;
      commit = GNUNET_JSON_PACK (
        GNUNET_JSON_pack_uint64 ("identifier",
                                 commitments[i].identifier),
        GNUNET_JSON_pack_data_auto ("hiding_commitment",
                                    &commitments[i].hiding_commitment),
        GNUNET_JSON_pack_data_auto ("binding_commitment",
                                    &commitments[i].binding_commitment));

      /* add to commitments array */
      GNUNET_assert (0 ==
                     json_array_append_new (
                       commits,
                       commit));
    }

    {
      /* pack the json object for the request body */
      json_t *sig_share_data;
      sig_share_data = GNUNET_JSON_PACK (
        GNUNET_JSON_pack_data_auto ("encryption_key",
                                    encryption_key),
        GNUNET_JSON_pack_data_auto ("message_hash",
                                    message_hash),
        GNUNET_JSON_pack_array_steal ("commitments",
                                      commits));

      json_str = json_dumps (sig_share_data,
                             JSON_COMPACT);

      /* check if we have a json object, abort if it was not successful */
      GNUNET_assert (NULL != json_str);

      json_decref (sig_share_data);
    }
  }

  /* prepare curl options and fire the request */
  sso->ctx = ctx;
  sso->cb = cb;
  sso->cb_cls = cb_cls;

  eh = FROSIX_curl_easy_get_ (sso->url);

  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_POSTFIELDS,
                                   json_str));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_POSTFIELDSIZE,
                                   strlen (json_str)));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_HEADERFUNCTION,
                                   &handle_header));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_HEADERDATA,
                                   sso));

  sso->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_sig_share_request_finished,
                                  sso);

  return sso;
}