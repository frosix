/*
  This file is part of ANASTASIS
  Copyright (C) 2014-2019 Anastasis SARL

  ANASTASIS is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  ANASTASIS is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with ANASTASIS; see the file COPYING.LGPL.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file restclient/frosix_api_dkg-key_delete.c
 * @brief Implementation of the /dkg-key Delete
 * @author Christian Grothoff
 * @author Dennis Neufeld
 * @author Dominik Meister
 * @author Joel Urech
 */
#include "platform.h"
#include <curl/curl.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include "frosix_service.h"
#include "frost_high.h"
#include "frosix_api_curl_defaults.h"
#include <gnunet/gnunet_json_lib.h>
#include <taler/taler_json_lib.h>


/**
 * @brief A Contract Operation Handle
 */
struct FROSIX_KeyDeleteOperation
{
  /**
   * The url for this request, including parameters.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * The CURL context to connect to the backend
  */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Function to call with the result.
   */
  FROSIX_KeyDeleteCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;
};


void
FROSIX_key_delete_cancel (
  struct FROSIX_KeyDeleteOperation *kdo)
{
  if (NULL != kdo->job)
  {
    GNUNET_CURL_job_cancel (kdo->job);
    kdo->job = NULL;
  }
  GNUNET_free (kdo->url);
  GNUNET_free (kdo);
}


/**
 * Callback to process DELETE /dkg-key response
 *
 * @param cls the `struct FROSIX_DkgKeyStoreOperation`
 * @param response_code HTTP response code, 0 on error
 * @param data response body
 * @param data_size number of byte in @a data
*/
static void
handle_key_delete_finished (void *cls,
                            long response_code,
                            const void *response)
{
  struct FROSIX_KeyDeleteOperation *kdo = cls;
  struct FROSIX_KeyDeleteDetails kdd;
  const json_t *json_response = response;

  kdo->job = NULL;
  kdd.http_status = response_code;
  kdd.ec = TALER_EC_NONE;

  switch (response_code)
  {
  case 0:
    /* Hard error */
    GNUNET_break (0);
    kdd.kds = FROSIX_KDS_HTTP_ERROR;
    kdd.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_NO_CONTENT:
  case MHD_HTTP_OK:
    {
      kdd.kds = FROSIX_KDS_SUCCESS;
      break;
    }
  case MHD_HTTP_BAD_REQUEST:
    /* We have a conflict with the API */
    GNUNET_break (0);
    kdd.kds = FROSIX_KDS_CLIENT_ERROR;
    kdd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    /* The provider has a problem! */
    GNUNET_break (0);
    kdd.kds = FROSIX_KDS_SERVER_ERROR;
    kdd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  default:
    /* Unexpected response code */
    GNUNET_break (0);
    kdd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  }

  /* return to callback function with the data from the response */
  kdo->cb (kdo->cb_cls,
           &kdd);
  kdo->cb = NULL;

  FROSIX_key_delete_cancel (kdo);
}


/**
 * Handle HTTP header received by curl.
 *
 * @param buffer one line of HTTP header data
 * @param size size of an item
 * @param userdata our `struct FROSIX_DkgKeyStoreOperation`
 * @return `size * nitems`
*/
static size_t
handle_header (char *buffer,
               size_t size,
               size_t nitems,
               void *userdata)
{
  // struct FROSIX_DkgKeyStoreOperation *dko = userdata;
  size_t total = size * nitems;
  char *ndup;
  const char *hdr_type;
  char *hdr_val;
  char *sp;

  ndup = GNUNET_strndup (buffer,
                         total);

  hdr_type = strtok_r (ndup,
                       ":",
                       &sp);
  if (NULL == hdr_type)
  {
    GNUNET_free (ndup);
    return total;
  }
  hdr_val = strtok_r (NULL,
                      "\n\r",
                      &sp);
  if (NULL == hdr_val)
  {
    GNUNET_free (ndup);
    return total;
  }
  if (' ' == *hdr_val)
    hdr_val++;
  /* ... FIXME */
  return total;
}


struct FROSIX_KeyDeleteOperation *
FROSIX_key_delete (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct FROSIX_DkgRequestIdP *uuid,
  FROSIX_KeyDeleteCallback cb,
  void *cb_cls)
{
  struct FROSIX_KeyDeleteOperation *kdo;
  CURL *eh;

  /* check if we got a callback function, abort if not */
  GNUNET_assert (NULL != cb);

  kdo = GNUNET_new (struct FROSIX_KeyDeleteOperation);
  {
    /* prepare URL */
    char *uuid_str;
    char *path;

    uuid_str = GNUNET_STRINGS_data_to_string_alloc (uuid,
                                                    sizeof (*uuid));

    GNUNET_asprintf (&path,
                     "dkg-key/%s",
                     uuid_str);

    kdo->url = TALER_url_join (backend_url,
                               path,
                               NULL);

    GNUNET_free (path);
    GNUNET_free (uuid_str);
  }

  /* prepare curl options and fire the request */
  kdo->ctx = ctx;
  kdo->cb = cb;
  kdo->cb_cls = cb_cls;

  eh = FROSIX_curl_easy_get_ (kdo->url);

  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_CUSTOMREQUEST,
                                   "DELETE"));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_HEADERDATA,
                                   kdo));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_HEADERFUNCTION,
                                   &handle_header));
  kdo->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_key_delete_finished,
                                  kdo);

  return kdo;
}