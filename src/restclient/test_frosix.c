/**
 *
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "frosix_service.h"
#include "frosix_backend.h"
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "platform.h"
#include "frost_high.h"

#define HASHCONTEXTVAL "FROSIX-DKG-ID"


/**
 * State for a "config" CMD.
 */
struct ConfigState
{
  /**
   * The interpreter state.
   */
  struct GNUNET_CURL_Context *ctx;

  struct GNUNET_CURL_RescheduleContext *rc;

  /**
   * URL of the frosix backend.
   */
  const char *frosix_url;

  /**
   * Expected status code.
   */
  unsigned int http_status;

  /**
   * The /config GET operation handle.
   */
  struct FROSIX_ConfigOperation *so;

  /**
   * The salt value from server.
   */
  struct FROSIX_ProviderSaltP provider_salt;
};

static void
config_cb (void *cls,
           unsigned int http_status,
           const struct FROSIX_Config *config)
{
  fprintf (stderr, "test\n");
  struct ConfigState *ss = cls;

  ss->so = NULL;
  if (http_status != ss->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u",
                http_status);
    return;
  }
  if (NULL == config)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Config is NULL");
    return;
  }
  ss->provider_salt = config->provider_salt;

}


int
main (void)
{
  GNUNET_log_setup ("WARNING",
                    NULL,
                    NULL);

  struct ConfigState *ss;
  ss = GNUNET_new (struct ConfigState);
  ss->frosix_url = "http://localhost:9977/";
  ss->http_status = MHD_HTTP_OK;
  ss->ctx = GNUNET_CURL_init (&GNUNET_CURL_gnunet_scheduler_reschedule,
                              &ss->rc);

  /*ss->so = FROSIX_get_config (&ss->ctx,
                              ss->frosix_url,
                              &config_cb,
                              ss);*/

  FROST_init ();

  uint8_t identifier = 1;
  uint8_t num_of_participants = 5;
  uint8_t threshold = 3;

  /* Challenge Method */
  struct FROST_Scalar scalar_method;
  FROST_scalar_random (&scalar_method);

  struct FROST_HashCode challenge_method;
  struct FROST_HashState state_method;
  FROST_hash_init (&state_method);
  FROST_hash_scalar_update (&state_method, &scalar_method);
  FROST_hash_final (&state_method, &challenge_method);

  /*Convert to crockford 32 and print*/
  char *chall_method;
  chall_method = GNUNET_STRINGS_data_to_string_alloc (&challenge_method,
                                                      sizeof (challenge_method));
  fprintf (stderr, "challenge_method: %s\n", chall_method);

  /* Challenge Data */
  struct FROST_Scalar scalar_data;
  FROST_scalar_random (&scalar_data);

  struct FROST_HashCode challenge_data;
  struct FROST_HashState state_data;
  FROST_hash_init (&state_data);
  FROST_hash_scalar_update (&state_data, &scalar_data);
  FROST_hash_final (&state_data, &challenge_data);

  /*Convert to crockford 32 and print*/
  char *chall_data;
  chall_data = GNUNET_STRINGS_data_to_string_alloc (&challenge_data,
                                                    sizeof (challenge_data));
  fprintf (stderr, "challenge_data: %s\n", chall_data);

  /* Context String*/
  struct FROST_Scalar scalar_context;
  FROST_scalar_random (&scalar_context);

  struct FROST_HashCode context_string;
  struct FROST_HashState state_context;
  FROST_hash_init (&state_context);
  FROST_hash_scalar_update (&state_context, &scalar_context);
  FROST_hash_final (&state_context, &context_string);

  /*Convert to crockford 32 and print*/
  char *chall_context;
  chall_context = GNUNET_STRINGS_data_to_string_alloc (&context_string,
                                                       sizeof (context_string));
  fprintf (stderr, "context_string: %s\n", chall_context);

  /* ID */
  char *secret = "gUfO1KGOKYIFlFQg";
  struct FROSIX_ProviderSaltP provider_salt;

  GNUNET_CRYPTO_kdf (&provider_salt,
                     sizeof (provider_salt),
                     "frosix-provider-salt",
                     strlen ("frosix-provider-salt"),
                     secret,
                     strlen (secret),
                     NULL,
                     0);

  struct FROST_HashState id_state;
  struct FROST_HashCode id;

  FROST_hash_init (&id_state);
  FROST_hash_hash_update (&id_state,
                          &context_string);
  FROST_hash_hash_update (&id_state,
                          &challenge_method);
  FROST_hash_hash_update (&id_state,
                          &challenge_data);
  FROST_hash_uint8_update (&id_state,
                           identifier);
  FROST_hash_uint8_update (&id_state,
                           num_of_participants);
  FROST_hash_uint8_update (&id_state,
                           threshold);
  FROST_hash_fixed_update (&id_state,
                           &provider_salt.salt,
                           sizeof (provider_salt.salt));
  FROST_hash_fixed_update (&id_state,
                           HASHCONTEXTVAL,
                           sizeof (HASHCONTEXTVAL));
  FROST_hash_final (&id_state,
                    &id);

  /*Convert to crockford 32 and print*/
  char *crock_id;
  crock_id = GNUNET_STRINGS_data_to_string_alloc (&id,
                                                  sizeof (id));
  fprintf (stderr, "caller_id: %s\n", crock_id);

  /* Encryption Key */
  struct FROST_Scalar enc_key;
  FROST_scalar_random (&enc_key);

  /* Convert to crockford 32 and print */
  char *crock_key;
  crock_key = GNUNET_STRINGS_data_to_string_alloc (&enc_key,
                                                   sizeof (enc_key));
  fprintf (stderr, "encryption_key: %s\n", crock_key);

  /* Random signing keys */
  struct GNUNET_CRYPTO_EddsaPrivateKey private_key;
  struct GNUNET_CRYPTO_EddsaPublicKey public_key;
  GNUNET_CRYPTO_eddsa_key_create (&private_key);
  GNUNET_CRYPTO_eddsa_key_get_public (&private_key,
                                      &public_key);

  /* Convert to crockford 32 and pring */
  char *priv_key;
  char *pub_key;
  priv_key = GNUNET_STRINGS_data_to_string_alloc (&private_key,
                                                  sizeof (private_key));
  pub_key = GNUNET_STRINGS_data_to_string_alloc (&public_key,
                                                 sizeof (public_key));

  fprintf (stderr, "private signing key: %s\n", priv_key);
  fprintf (stderr, "public signing key: %s\n", pub_key);


  free (chall_method);
  free (chall_data);
  free (chall_context);
  free (crock_id);
  free (crock_key);
  free (priv_key);
  free (pub_key);

  struct FROST_HashState sig_id_state;
  struct FROST_HashCode sig_id_hash;
  FROST_hash_init (&sig_id_state);

  return 0;
}