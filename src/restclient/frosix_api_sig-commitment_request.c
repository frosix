/*
  This file is part of ANASTASIS
  Copyright (C) 2014-2019 Anastasis SARL

  ANASTASIS is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  ANASTASIS is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with ANASTASIS; see the file COPYING.LGPL.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file restclient/frosix_api_sig-commitment_request.c
 * @brief Implementation of the /sig-commitment POST
 * @author Christian Grothoff
 * @author Dennis Neufeld
 * @author Dominik Meister
 * @author Joel Urech
 */
#include "platform.h"
#include <curl/curl.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include "frosix_service.h"
#include "frost_high.h"
#include "frosix_api_curl_defaults.h"
#include <gnunet/gnunet_json_lib.h>
#include <taler/taler_json_lib.h>


/**
 * @brief A Contract Operation Handle
 */
struct FROSIX_SigCommitmentRequestOperation
{
  /**
   * The url for this request, including parameters.
   */
  char *url;

  /**
   * Index of the provider.
  */
  uint8_t provider_index;

  /**
   * Index in the provider array.
  */
  uint8_t array_index;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * The CURL context to connect to the backend
  */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Function to call with the result.
   */
  FROSIX_SigCommitmentRequestCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;
};


void
FROSIX_sig_commitment_request_cancel (
  struct FROSIX_SigCommitmentRequestOperation *sco)
{
  if (NULL != sco->job)
  {
    GNUNET_CURL_job_cancel (sco->job);
    sco->job = NULL;
  }
  GNUNET_free (sco->url);
  GNUNET_free (sco);
}


/**
 * Callback to process POST /sig-commitment response
 *
 * @param cls the `struct FROSIX_SigCommitmentRequestOperation`
 * @param response_code HTTP response code, 0 on error
 * @param data response body
 * @param data_size number of byte in @a data
*/
static void
handle_sig_commitment_request_finished (void *cls,
                                        long response_code,
                                        const void *response)
{
  struct FROSIX_SigCommitmentRequestOperation *sco = cls;
  struct FROSIX_SigCommitmentRequestDetails scd;
  const json_t *json_response = response;

  sco->job = NULL;
  scd.http_status = response_code;
  scd.ec = TALER_EC_NONE;
  scd.array_index = sco->array_index;
  scd.sig_commitment.identifier = sco->provider_index;

  switch (response_code)
  {
  case 0:
    /* Hard error */
    GNUNET_break (0);
    scd.scs = FROSIX_SCS_HTTP_ERROR;
    scd.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_CREATED:
  case MHD_HTTP_OK:
    {
      scd.scs = FROSIX_SCS_SUCCESS;

      /* We got a result, lets parse it */
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_fixed_auto ("hiding_commitment",
                                     &scd.sig_commitment.hiding_commitment),
        GNUNET_JSON_spec_fixed_auto ("binding_commitment",
                                     &scd.sig_commitment.binding_commitment),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json_response,
                             spec,
                             NULL,
                             NULL))
      {
        /* Parsing failed! */
        GNUNET_break_op (0);
        scd.http_status = 0;
        GNUNET_JSON_parse_free (spec);
        break;
      }

      GNUNET_JSON_parse_free (spec);
      break;
    }
  case MHD_HTTP_BAD_REQUEST:
    /* We have a conflict with the API */
    scd.http_status = 0;
    GNUNET_break (0);
    scd.scs = FROSIX_SCS_CLIENT_ERROR;
    scd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    /* The provider has a problem! */
    scd.http_status = 0;
    GNUNET_break (0);
    scd.scs = FROSIX_SCS_SERVER_ERROR;
    scd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  default:
    /* Unexpected response code */
    scd.http_status = 0;
    GNUNET_break (0);
    scd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  }

  /* return to callback function with the data from the response */
  sco->cb (sco->cb_cls,
           &scd);
  sco->cb = NULL;

  FROSIX_sig_commitment_request_cancel (sco);
  return;
}

/**
 * Handle HTTP header received by curl.
 *
 * @param buffer one line of HTTP header data
 * @param size size of an item
 * @param userdata our `struct FROSIX_DkgCommitmentRequestOperation`
 * @return `size * nitems`
*/
static size_t
handle_header (char *buffer,
               size_t size,
               size_t nitems,
               void *userdata)
{
  // struct FROSIX_DkgCommitmentRequestOperation *dco = userdata;
  size_t total = size * nitems;
  return total;

  /*char *ndup;
  const char *hdr_type;
  char *hdr_val;
  char *sp;

  ndup = GNUNET_strndup (buffer,
                         total);

  hdr_type = strtok_r (ndup,
                       ":",
                       &sp);
  if (NULL == hdr_type)
  {
    GNUNET_free (ndup);
    return total;
  }
  hdr_val = strtok_r (NULL,
                      "\n\r",
                      &sp);
  if (NULL == hdr_val)
  {
    GNUNET_free (ndup);
    return total;
  }
  if (' ' == *hdr_val)
    hdr_val++;

  return total;*/
}


struct FROSIX_SigCommitmentRequestOperation *
FROSIX_sig_commitment_request (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct FROSIX_SigRequestIdP *uuid,
  uint8_t provider_index,
  uint8_t array_index,
  const struct FROST_HashCode *encryption_key_hash,
  const struct GNUNET_HashCode *auth_data,
  const struct GNUNET_CRYPTO_Edx25519PublicKey *auth_pub,
  const struct GNUNET_CRYPTO_Edx25519Signature *auth_sig,
  const char *auth_method,
  const struct FROST_MessageHash *message_hash,
  FROSIX_SigCommitmentRequestCallback cb,
  void *cb_cls)
{
  struct FROSIX_SigCommitmentRequestOperation *sco;
  CURL *eh;
  char *json_str;

  /* check if we got a callback function, abort if not */
  GNUNET_assert (NULL != cb);

  sco = GNUNET_new (struct FROSIX_SigCommitmentRequestOperation);
  sco->array_index = array_index;
  sco->provider_index = provider_index;

  {
    /* prepare URL */
    char *uuid_str;
    char *path;

    uuid_str = GNUNET_STRINGS_data_to_string_alloc (uuid,
                                                    sizeof (*uuid));

    GNUNET_asprintf (&path,
                     "sig-commitment/%s",
                     uuid_str);

    sco->url = TALER_url_join (backend_url,
                               path,
                               NULL);

    GNUNET_free (path);
    GNUNET_free (uuid_str);
  }

  if (0 == strcmp (auth_method,
                   "question"))
  {
    /* pack the json object for the request body */
    json_t *sig_commitment_data;
    sig_commitment_data = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_data_auto ("encryption_key_hash",
                                  encryption_key_hash),
      GNUNET_JSON_pack_string ("auth_method",
                               auth_method),
      GNUNET_JSON_pack_data_auto ("auth_pub",
                                  auth_pub),
      GNUNET_JSON_pack_data_auto ("auth_sig",
                                  auth_sig),
      GNUNET_JSON_pack_data_auto ("message_hash",
                                  message_hash));

    json_str = json_dumps (sig_commitment_data,
                           JSON_COMPACT);

    /* check if we have a json object, abort if it was not successful */
    GNUNET_assert (NULL != json_str);

    json_decref (sig_commitment_data);
  }
  else
  {
    /* pack the json object for the request body */
    json_t *sig_commitment_data;
    sig_commitment_data = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_data_auto ("encryption_key_hash",
                                  encryption_key_hash),
      GNUNET_JSON_pack_string ("auth_method",
                               auth_method),
      GNUNET_JSON_pack_data_auto ("auth_data",
                                  auth_data),
      GNUNET_JSON_pack_data_auto ("message_hash",
                                  message_hash));

    json_str = json_dumps (sig_commitment_data,
                           JSON_COMPACT);

    /* check if we have a json object, abort if it was not successful */
    GNUNET_assert (NULL != json_str);

    json_decref (sig_commitment_data);
  }

  /* prepare curl options and fire the request */
  sco->ctx = ctx;
  sco->cb = cb;
  sco->cb_cls = cb_cls;

  eh = FROSIX_curl_easy_get_ (sco->url);

  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_POSTFIELDS,
                                   json_str));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_POSTFIELDSIZE,
                                   strlen (json_str)));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_HEADERFUNCTION,
                                   &handle_header));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_HEADERDATA,
                                   sco));

  sco->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_sig_commitment_request_finished,
                                  sco);

  return sco;
}