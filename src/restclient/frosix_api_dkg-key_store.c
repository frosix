/*
  This file is part of ANASTASIS
  Copyright (C) 2014-2019 Anastasis SARL

  ANASTASIS is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  ANASTASIS is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with ANASTASIS; see the file COPYING.LGPL.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file restclient/frosix_api_dkg-key_store.c
 * @brief Implementation of the /dkg-key POST
 * @author Christian Grothoff
 * @author Dennis Neufeld
 * @author Dominik Meister
 * @author Joel Urech
 */
#include "platform.h"
#include <curl/curl.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include "frosix_service.h"
#include "frost_high.h"
#include "frosix_api_curl_defaults.h"
#include <gnunet/gnunet_json_lib.h>
#include <taler/taler_json_lib.h>


/**
 * @brief A Contract Operation Handle
 */
struct FROSIX_DkgKeyStoreOperation
{
  /**
   * The url for this request, including parameters.
   */
  char *url;

  /**
   * FIXME
  */
  uint8_t provider_index;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * The CURL context to connect to the backend
  */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Function to call with the result.
   */
  FROSIX_DkgKeyStoreCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;
};


void
FROSIX_dkg_key_store_cancel (
  struct FROSIX_DkgKeyStoreOperation *dko)
{
  if (NULL != dko->job)
  {
    GNUNET_CURL_job_cancel (dko->job);
    dko->job = NULL;
  }
  GNUNET_free (dko->url);
  GNUNET_free (dko);
}


/**
 * Callback to process POST /dkg-key response
 *
 * @param cls the `struct FROSIX_DkgKeyStoreOperation`
 * @param response_code HTTP response code, 0 on error
 * @param data response body
 * @param data_size number of byte in @a data
*/
static void
handle_dkg_key_store_finished (void *cls,
                               long response_code,
                               const void *response)
{
  struct FROSIX_DkgKeyStoreOperation *dko = cls;
  struct FROSIX_DkgKeyStoreDetails dkd;
  const json_t *json_response = response;

  dko->job = NULL;
  dkd.http_status = response_code;
  dkd.ec = TALER_EC_NONE;

  switch (response_code)
  {
  case 0:
    /* Hard error */
    GNUNET_break (0);
    dkd.dks = FROSIX_DKS_HTTP_ERROR;
    dkd.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_CREATED:
  case MHD_HTTP_OK:
    {
      dkd.dks = FROSIX_DKS_SUCCESS;
      dkd.provider_index = dko->provider_index;

      /* We got a result, lets parse it */
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_fixed_auto ("public_key",
                                     &dkd.details.public_key),
        GNUNET_JSON_spec_fixed_auto ("signature",
                                     &dkd.details.signature),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json_response,
                             spec,
                             NULL,
                             NULL))
      {
        /* Parsing failed! */
        GNUNET_break_op (0);
        dkd.http_status = 0;
        GNUNET_JSON_parse_free (spec);
        break;
      }

      GNUNET_JSON_parse_free (spec);
      break;
    }
  case MHD_HTTP_BAD_REQUEST:
    /* We have a conflict with the API */
    GNUNET_break (0);
    dkd.dks = FROSIX_DKS_CLIENT_ERROR;
    dkd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    /* The provider has a problem! */
    GNUNET_break (0);
    dkd.dks = FROSIX_DKS_SERVER_ERROR;
    dkd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  default:
    /* Unexpected response code */
    GNUNET_break (0);
    dkd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  }

  /* return to callback function with the data from the response */
  dko->cb (dko->cb_cls,
           &dkd);
  dko->cb = NULL;

  FROSIX_dkg_key_store_cancel (dko);
}


/**
 * Handle HTTP header received by curl.
 *
 * @param buffer one line of HTTP header data
 * @param size size of an item
 * @param userdata our `struct FROSIX_DkgKeyStoreOperation`
 * @return `size * nitems`
*/
static size_t
handle_header (char *buffer,
               size_t size,
               size_t nitems,
               void *userdata)
{
  // struct FROSIX_DkgKeyStoreOperation *dko = userdata;
  size_t total = size * nitems;
  char *ndup;
  const char *hdr_type;
  char *hdr_val;
  char *sp;

  ndup = GNUNET_strndup (buffer,
                         total);

  hdr_type = strtok_r (ndup,
                       ":",
                       &sp);
  if (NULL == hdr_type)
  {
    GNUNET_free (ndup);
    return total;
  }
  hdr_val = strtok_r (NULL,
                      "\n\r",
                      &sp);
  if (NULL == hdr_val)
  {
    GNUNET_free (ndup);
    return total;
  }
  if (' ' == *hdr_val)
    hdr_val++;
  /* ... FIXME */
  return total;
}


struct FROSIX_DkgKeyStoreOperation *
FROSIX_dkg_key_store (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct FROSIX_DkgRequestIdP *uuid,
  uint8_t identifier,
  uint8_t threshold,
  uint8_t num_of_participants,
  const struct FROSIX_DkgContextStringP *context_string,
  const struct FROSIX_ChallengeHashP *challenge_hash,
  const struct GNUNET_CRYPTO_EddsaPublicKey providers_public_keys[],
  const struct FROSIX_EncryptionKey *encryption_key,
  unsigned int expiration,
  const struct FROSIX_DkgKeyStoreShare secret_shares[],
  size_t len,
  FROSIX_DkgKeyStoreCallback cb,
  void *cb_cls)
{
  struct FROSIX_DkgKeyStoreOperation *dko;
  CURL *eh;
  char *json_str;

  /* check if we got a callback function, abort if not */
  GNUNET_assert (NULL != cb);

  dko = GNUNET_new (struct FROSIX_DkgKeyStoreOperation);

  {
    /* prepare URL */
    char *uuid_str;
    char *path;

    uuid_str = GNUNET_STRINGS_data_to_string_alloc (uuid,
                                                    sizeof (*uuid));

    GNUNET_asprintf (&path,
                     "dkg-key/%s",
                     uuid_str);

    dko->url = TALER_url_join (backend_url,
                               path,
                               NULL);

    GNUNET_free (path);
    GNUNET_free (uuid_str);
  }

  dko->provider_index = identifier;

  /* array for all providers public keys */
  json_t *json_public_keys = json_array ();

  for (unsigned int i = 0; i < num_of_participants; i++)
  {
    /* single public key */
    json_t *public_key;
    public_key = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_data_auto (NULL,
                                  &providers_public_keys[i]));

    /* add to public key array */
    GNUNET_assert (0 ==
                   json_array_append_new (
                     json_public_keys,
                     public_key));
  }

  {
    /* array for all secret shares */
    json_t *shares = json_array ();

    for (unsigned int i = 0; i < len; i++)
    {
      /* single secret share */
      json_t *share;
      share = GNUNET_JSON_PACK (
        GNUNET_JSON_pack_uint64 ("provider_index",
                                 secret_shares[i].identifier),
        GNUNET_JSON_pack_data_auto ("secret_share",
                                    &secret_shares[i].secret_share),
        GNUNET_JSON_pack_data_auto ("ephemeral_key",
                                    &secret_shares[i].ephemeral_key));

      /* add to secret shares array */
      GNUNET_assert (0 ==
                     json_array_append_new (
                       shares,
                       share));
    }

    /* pack the json object for the request body */
    json_t *dkg_key_data;
    dkg_key_data = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_uint64 ("provider_index",
                               identifier),
      GNUNET_JSON_pack_uint64 ("threshold",
                               threshold),
      GNUNET_JSON_pack_data_auto ("context_string",
                                  context_string),
      GNUNET_JSON_pack_data_auto ("auth_hash",
                                  challenge_hash),
      GNUNET_JSON_pack_array_steal ("providers_public_keys",
                                    json_public_keys),
      GNUNET_JSON_pack_data_auto ("pre_encryption_key",
                                  encryption_key),
      GNUNET_JSON_pack_uint64 ("expiration",
                               expiration),
      GNUNET_JSON_pack_array_steal ("secret_shares",
                                    shares));

    json_str = json_dumps (dkg_key_data,
                           JSON_COMPACT);

    /* check if we have a json object, abort if it was not successful */
    GNUNET_assert (NULL != json_str);

    json_decref (dkg_key_data);
  }

  /* prepare curl options and fire the request */
  dko->ctx = ctx;
  dko->cb = cb;
  dko->cb_cls = cb_cls;

  eh = FROSIX_curl_easy_get_ (dko->url);

  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_POSTFIELDS,
                                   json_str));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_POSTFIELDSIZE,
                                   strlen (json_str)));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_HEADERFUNCTION,
                                   &handle_header));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_HEADERDATA,
                                   dko));

  dko->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_dkg_key_store_finished,
                                  dko);

  return dko;
}