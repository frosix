/*
  This file is part of ANASTASIS
  Copyright (C) 2014-2019 Anastasis SARL

  ANASTASIS is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  ANASTASIS is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with ANASTASIS; see the file COPYING.LGPL.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file restclient/frosix_api_dkg-commitment_request.c
 * @brief Implementation of the /dkg-commitment POST
 * @author Christian Grothoff
 * @author Dennis Neufeld
 * @author Dominik Meister
 * @author Joel Urech
 */
#include "platform.h"
#include <curl/curl.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include "frosix_service.h"
#include "frost_high.h"
#include "frosix_api_curl_defaults.h"
#include <gnunet/gnunet_json_lib.h>
#include <taler/taler_json_lib.h>


/**
 * @brief A commitment request operation handle
 */
struct FROSIX_DkgCommitmentRequestOperation
{
  /**
   * The url for this request, including parameters.
   */
  char *url;

  /**
   *
  */
  uint8_t provider_index;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * The CURL context to connect to the backend
  */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Function to call with the result.
   */
  FROSIX_DkgCommitmentRequestCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;
};


void
FROSIX_dkg_commitment_request_cancel (
  struct FROSIX_DkgCommitmentRequestOperation *dco)
{
  if (NULL != dco->job)
  {
    GNUNET_CURL_job_cancel (dco->job);
    dco->job = NULL;
  }
  GNUNET_free (dco->url);
  GNUNET_free (dco);
}


/**
 * Callback to process POST /dkg-commitment response
 *
 * @param cls the `struct FROSIX_DkgCommitmentRequestOperation`
 * @param response_code HTTP response code, 0 on error
 * @param data response body
 * @param data_size number of byte in @a data
*/
static void
handle_dkg_commitment_request_finished (void *cls,
                                        long response_code,
                                        const void *response)
{
  struct FROSIX_DkgCommitmentRequestOperation *dco = cls;
  struct FROSIX_DkgCommitmentRequestDetails dcd;
  const json_t *json_response = response;

  dco->job = NULL;
  dcd.http_status = response_code;
  dcd.ec = TALER_EC_NONE;

  switch (response_code)
  {
  case 0:
    /* Hard error */
    GNUNET_break (0);
    dcd.dcs = FROSIX_DCS_HTTP_ERROR;
    dcd.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_CREATED:
    {
      dcd.dcs = FROSIX_DCS_SUCCESS;
      dcd.provider_index = dco->provider_index;

      /* temp commitment struct */
      struct FROST_DkgCommitment commitment;

      /* We got a result, lets parse it */
      json_t *dkg_commitment = NULL;
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_uint8 ("provider_index",
                                &dcd.dkg_commitment.identifier),
        GNUNET_JSON_spec_json ("dkg_commitment",
                               &dkg_commitment),
        GNUNET_JSON_spec_fixed_auto ("zkp_r",
                                     &commitment.zkp.r),
        GNUNET_JSON_spec_fixed_auto ("zkp_z",
                                     &commitment.zkp.z),
        GNUNET_JSON_spec_fixed_auto ("public_key",
                                     &dcd.public_key),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json_response,
                             spec,
                             NULL,
                             NULL))
      {
        /* Parsing failed! */
        GNUNET_break_op (0);
        dcd.http_status = 0;
        GNUNET_JSON_parse_free (spec);
        break;
      }

      /* check if number of commitment values is in allowed range */
      if (0 >= json_array_size (dkg_commitment)
          || json_array_size (dkg_commitment) >= 254)
      {
        GNUNET_break_op (0);
        dcd.http_status = 0;
        GNUNET_JSON_parse_free (spec);
        break;
      }

      dcd.dkg_commitment.shares_commitments_length = \
        (uint8_t) json_array_size (dkg_commitment);

      /* initialize struct dkg_commitment */
      FROST_initialize_dkg_commitment (
        &dcd.dkg_commitment,
        dcd.dkg_commitment.identifier,
        dcd.dkg_commitment.shares_commitments_length);

      /* copy back from temp commitment struct */
      memcpy (&dcd.dkg_commitment.zkp.r,
              &commitment.zkp.r,
              sizeof (commitment.zkp.r));
      memcpy (&dcd.dkg_commitment.zkp.z,
              &commitment.zkp.z,
              sizeof (commitment.zkp.z));

      /* parse commitment values */
      for (unsigned int i = 0; i < dcd.dkg_commitment.shares_commitments_length;
           i++)
      {
        struct GNUNET_JSON_Specification comm_spec[] = {
          GNUNET_JSON_spec_fixed_auto (NULL,
                                       &dcd.dkg_commitment.share_comm[i]),
          GNUNET_JSON_spec_end ()
        };

        if (GNUNET_OK !=
            GNUNET_JSON_parse (json_array_get (dkg_commitment,
                                               i),
                               comm_spec,
                               NULL,
                               NULL))
        {
          GNUNET_break_op (0);
          GNUNET_JSON_parse_free (comm_spec);
          GNUNET_JSON_parse_free (spec);
          FROST_free_dkg_commitment (&dcd.dkg_commitment,
                                     1);
          dcd.http_status = 0;
          break;
        }

        GNUNET_JSON_parse_free (comm_spec);
      }

      GNUNET_JSON_parse_free (spec);
      break;
    }
  case MHD_HTTP_BAD_REQUEST:
    /* We have a conflict with the API */
    GNUNET_break (0);
    dcd.dcs = FROSIX_DCS_CLIENT_ERROR;
    dcd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    /* The provider has a problem! */
    GNUNET_break (0);
    dcd.dcs = FROSIX_DCS_SERVER_ERROR;
    dcd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  default:
    /* Unexpected response code */
    GNUNET_break (0);
    dcd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  }

  /* return to callback function with the data from the response */
  dco->cb (dco->cb_cls,
           &dcd);
  dco->cb = NULL;

  FROST_free_dkg_commitment (&dcd.dkg_commitment,
                             1);

  FROSIX_dkg_commitment_request_cancel (dco);
}


/**
 * Handle HTTP header received by curl.
 *
 * @param buffer one line of HTTP header data
 * @param size size of an item
 * @param userdata our `struct FROSIX_DkgCommitmentRequestOperation`
 * @return `size * nitems`
*/
static size_t
handle_header (char *buffer,
               size_t size,
               size_t nitems,
               void *userdata)
{
  size_t total = size * nitems;
  return total;
}


struct FROSIX_DkgCommitmentRequestOperation *
FROSIX_dkg_commitment_request (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct FROSIX_DkgRequestIdP *uuid,
  uint8_t identifier,
  uint8_t threshold,
  uint8_t num_of_participants,
  const struct FROSIX_DkgContextStringP *context_string,
  const struct FROSIX_ChallengeHashP *challenge_hash,
  const struct GNUNET_CRYPTO_EddsaPublicKey providers_public_keys[],
  FROSIX_DkgCommitmentRequestCallback cb,
  void *cb_cls)
{
  struct FROSIX_DkgCommitmentRequestOperation *dco;
  CURL *eh;
  char *json_str;

  /* check if we got a callback function, abort if not */
  GNUNET_assert (NULL != cb);

  dco = GNUNET_new (struct FROSIX_DkgCommitmentRequestOperation);

  {
    /* prepare URL */
    char *uuid_str;
    char *path;

    uuid_str = GNUNET_STRINGS_data_to_string_alloc (uuid,
                                                    sizeof (*uuid));

    GNUNET_asprintf (&path,
                     "dkg-commitment/%s",
                     uuid_str);

    dco->url = TALER_url_join (backend_url,
                               path,
                               NULL);

    GNUNET_free (path);
    GNUNET_free (uuid_str);
  }

  dco->provider_index = identifier;

  /* array for all providers public keys */
  json_t *json_public_keys = json_array ();


  for (unsigned int i = 0; i < num_of_participants; i++)
  {
    /* single public key */
    json_t *public_key;
    public_key = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_data_auto (NULL,
                                  &providers_public_keys[i]));

    /* add to public key array */
    GNUNET_assert (0 ==
                   json_array_append_new (
                     json_public_keys,
                     public_key));
  }

  {
    /* pack the json object for the request body */
    json_t *dkg_commitment_data;
    dkg_commitment_data = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_uint64 ("provider_index",
                               identifier),
      GNUNET_JSON_pack_uint64 ("threshold",
                               threshold),
      GNUNET_JSON_pack_data_auto ("context_string",
                                  context_string),
      GNUNET_JSON_pack_data_auto ("auth_hash",
                                  challenge_hash),
      GNUNET_JSON_pack_array_steal ("providers_public_keys",
                                    json_public_keys));
    json_str = json_dumps (dkg_commitment_data,
                           JSON_COMPACT);

    json_decref (dkg_commitment_data);

    /* check if we have a json object, abort if it was not successful */
    GNUNET_assert (NULL != json_str);
  }

  /* prepare curl options and fire the request */
  dco->ctx = ctx;
  dco->cb = cb;
  dco->cb_cls = cb_cls;

  eh = FROSIX_curl_easy_get_ (dco->url);

  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_POSTFIELDS,
                                   json_str));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_POSTFIELDSIZE,
                                   strlen (json_str)));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_HEADERFUNCTION,
                                   &handle_header));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_HEADERDATA,
                                   dco));

  dco->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_dkg_commitment_request_finished,
                                  dco);

  return dco;
}