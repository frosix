/*
  This file is part of Frosix
  Copyright (C) 2014-2019 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with Frosix; see the file COPYING.LGPL.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file restclient/frosix_api_auth-challenge_request.c
 * @brief Implementation of the /auth-challenge POST
 * @author Christian Grothoff
 * @author Dennis Neufeld
 * @author Dominik Meister
 * @author Joel Urech
 */
#include "platform.h"
#include <curl/curl.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include "frosix_service.h"
#include "frosix_api_curl_defaults.h"
#include <taler/taler_json_lib.h>

/**
 * @brief A Contract Operation Handle
 */
struct FROSIX_ChallengeRequestOperation
{
  /**
   * The url for this request, including parameters.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * The CURL context to connect to the backend
  */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Function to call with the result.
   */
  FROSIX_ChallengeRequestCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;
};


void
FROSIX_auth_challenge_post_cancel (struct FROSIX_ChallengeRequestOperation *aco)
{
  if (NULL != aco->job)
  {
    GNUNET_CURL_job_cancel (aco->job);
    aco->job = NULL;
  }
  GNUNET_free (aco->url);
  GNUNET_free (aco);
}


/**
 * Process POST /auth-challenge response
 */
static void
handle_auth_challenge_post_finished (void *cls,
                                     long response_code,
                                     const void *response)
{
  struct FROSIX_ChallengeRequestOperation *aco = cls;
  struct FROSIX_ChallengeRequestDetails acd;
  const json_t *json_response = response;

  aco->job = NULL;
  acd.http_status = response_code;
  acd.ec = TALER_EC_NONE;

  switch (response_code)
  {
  case 0:
    /* Hard error */
    GNUNET_break (0);
    acd.crs = FROSIX_CRS_HTTP_ERROR;
    acd.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_CREATED:
  case MHD_HTTP_OK:
    {
      acd.crs = FROSIX_CRS_SUCCESS;
      acd.response = json_deep_copy (json_response);
      break;
    }
  case MHD_HTTP_BAD_REQUEST:
    /* We have a conflict with the API */
    GNUNET_break (0);
    acd.crs = FROSIX_CRS_CLIENT_ERROR;
    acd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    /* The provider has a problem! */
    GNUNET_break (0);
    acd.crs = FROSIX_CRS_SERVER_ERROR;
    acd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  default:
    /* Unexpected response code */
    GNUNET_break (0);
    acd.ec = TALER_JSON_get_error_code2 (json_response,
                                         json_object_size (json_response));
    break;
  }

  /* return to callback function with the data from the response */
  aco->cb (aco->cb_cls,
           &acd);
  aco->cb = NULL;

  FROSIX_auth_challenge_post_cancel (aco);
  return;
}


/**
 * Handle HTTP header received by curl.
 *
 * @param buffer one line of HTTP header data
 * @param size size of an item
 * @param userdata our `struct FROSIX_SigCommitmentRequestOperation`
 * @return `size * nitems`
*/
static size_t
handle_header (char *buffer,
               size_t size,
               size_t nitems,
               void *userdata)
{
  // struct FROSIX_SigCommitmentRequestOperation *sco = userdata;
  size_t total = size * nitems;
  char *ndup;
  const char *hdr_type;
  char *hdr_val;
  char *sp;

  ndup = GNUNET_strndup (buffer,
                         total);

  hdr_type = strtok_r (ndup,
                       ":",
                       &sp);
  if (NULL == hdr_type)
  {
    GNUNET_free (ndup);
    return total;
  }
  hdr_val = strtok_r (NULL,
                      "\n\r",
                      &sp);
  if (NULL == hdr_val)
  {
    GNUNET_free (ndup);
    return total;
  }
  if (' ' == *hdr_val)
    hdr_val++;
  /* ... FIXME */
  return total;
}


struct FROSIX_ChallengeRequestOperation *
FROSIX_auth_challenge_post (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct FROSIX_ChallengeRequestIdP *uuid,
  const struct FROSIX_EncryptionKey *encryption_key,
  const char *auth_method,
  const char *auth_data,
  const struct GNUNET_HashCode *auth_nonce,
  const struct FROST_MessageHash *message_hash,
  FROSIX_ChallengeRequestCallback cb,
  void *cb_cls)
{
  struct FROSIX_ChallengeRequestOperation *aco;
  CURL *eh;
  char *json_str;

  /* check if we got a callback function, abort if not */
  GNUNET_assert (NULL != cb);

  aco = GNUNET_new (struct FROSIX_ChallengeRequestOperation);

  {
    /* prepare URL */
    char *uuid_str;
    char *path;

    uuid_str = GNUNET_STRINGS_data_to_string_alloc (uuid,
                                                    sizeof (*uuid));

    GNUNET_asprintf (&path,
                     "auth-challenge/%s",
                     uuid_str);

    aco->url = TALER_url_join (backend_url,
                               path,
                               NULL);

    GNUNET_free (path);
    GNUNET_free (uuid_str);
  }

  {
    /* pack the json object for the request body */
    json_t *auth_challenge_data;
    auth_challenge_data = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_data_auto ("encryption_key",
                                  encryption_key),
      GNUNET_JSON_pack_string ("auth_method",
                               auth_method),
      GNUNET_JSON_pack_string ("auth_data",
                               auth_data),
      GNUNET_JSON_pack_data_auto ("auth_nonce",
                                  auth_nonce),
      GNUNET_JSON_pack_data_auto ("message_hash",
                                  message_hash));

    json_str = json_dumps (auth_challenge_data,
                           JSON_COMPACT);

    /* check if we have a json object, abort if it was not successful */
    GNUNET_assert (NULL != json_str);

    json_decref (auth_challenge_data);
  }

  /* prepare curl options and fire the request */
  aco->ctx = ctx;
  aco->cb = cb;
  aco->cb_cls = cb_cls;

  eh = FROSIX_curl_easy_get_ (aco->url);

  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_POSTFIELDS,
                                   json_str));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_POSTFIELDSIZE,
                                   strlen (json_str)));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_HEADERFUNCTION,
                                   &handle_header));
  GNUNET_assert (CURLE_OK ==
                 curl_easy_setopt (eh,
                                   CURLOPT_HEADERDATA,
                                   aco));

  aco->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_auth_challenge_post_finished,
                                  aco);

  return aco;
}
