/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file frost_low.c
 * @brief Wrapper of all used functions from the crypto library libsodium
 * @author Joel Urech
*/
#include <string.h>
#include <gnunet/gnunet_util_lib.h>
#include "frost_low.h"

int
FROST_low_init ()
{
  return sodium_init ();
}


// Points
void
FROST_point_mul_scalar (struct FROST_Point *result,
                        const struct FROST_Point *pt,
                        const struct FROST_Scalar *scal)
{
  GNUNET_assert (0 == crypto_scalarmult_ristretto255 (result->xcoord,
                                                      scal->scalarbytes,
                                                      pt->xcoord));
}


void
FROST_point_add_point (struct FROST_Point *result,
                       const struct FROST_Point *p,
                       const struct FROST_Point *q)
{
  GNUNET_assert (0 == crypto_core_ristretto255_add (result->xcoord,
                                                    p->xcoord,
                                                    q->xcoord));
}


void
FROST_point_sub_point (struct FROST_Point *result,
                       const struct FROST_Point *p,
                       const struct FROST_Point *q)
{
  GNUNET_assert (0 == crypto_core_ristretto255_sub (result->xcoord,
                                                    p->xcoord,
                                                    q->xcoord));
}


void
FROST_point_identity (struct FROST_Point *result)
{
  memset (result->xcoord,
          0,
          sizeof (*result));
}


void
FROST_base_mul_scalar (struct FROST_Point *result,
                       const struct FROST_Scalar *scal)
{
  GNUNET_assert (0 == crypto_scalarmult_ristretto255_base (result->xcoord,
                                                           scal->scalarbytes));
}


void
FROST_scalar_add_scalar (struct FROST_Scalar *result,
                         const struct FROST_Scalar *xscal,
                         const struct FROST_Scalar *yscal)
{
  crypto_core_ristretto255_scalar_add (result->scalarbytes,
                                       xscal->scalarbytes,
                                       yscal->scalarbytes);
}


void
FROST_scalar_mul_scalar (struct FROST_Scalar *result,
                         const struct FROST_Scalar *xscal,
                         const struct FROST_Scalar *yscal)
{
  crypto_core_ristretto255_scalar_mul (result->scalarbytes,
                                       xscal->scalarbytes,
                                       yscal->scalarbytes);
}


void
FROST_scalar_sub_scalar (struct FROST_Scalar *result,
                         const struct FROST_Scalar *xscal,
                         const struct FROST_Scalar *yscal)
{
  crypto_core_ristretto255_scalar_sub (result->scalarbytes,
                                       xscal->scalarbytes,
                                       yscal->scalarbytes);
}


void
FROST_scalar_invert (struct FROST_Scalar *result,
                     const struct FROST_Scalar *scal)
{
  GNUNET_assert (0 == crypto_core_ristretto255_scalar_invert (
                   result->scalarbytes,
                   scal->scalarbytes));
}


void
FROST_point_random (struct FROST_Point *result)
{
  crypto_core_ristretto255_random (result->xcoord);
}


void
FROST_scalar_random (struct FROST_Scalar *result)
{
  crypto_core_ristretto255_scalar_random (result->scalarbytes);
}


void
FROST_hash_init (struct FROST_HashState *state)
{
  GNUNET_assert (0 == crypto_hash_sha512_init (&state->state));
}


void
FROST_hash_fixed_update (struct FROST_HashState *state,
                         const void *msg,
                         size_t len)
{
  GNUNET_assert (0 == crypto_hash_sha512_update (&state->state,
                                                 msg,
                                                 len));
}


void
FROST_hash_point_update (struct FROST_HashState *state,
                         const struct FROST_Point *pt)
{
  GNUNET_assert (0 == crypto_hash_sha512_update (&state->state,
                                                 pt->xcoord,
                                                 sizeof (*pt)));
}


void
FROST_hash_hash_update (struct FROST_HashState *state,
                        const struct FROST_HashCode *hash)
{
  GNUNET_assert (0 == crypto_hash_sha512_update (&state->state,
                                                 hash->hashbytes,
                                                 sizeof (*hash)));
}


void
FROST_hash_scalar_update (struct FROST_HashState *state,
                          const struct FROST_Scalar *scal)
{
  GNUNET_assert (0 == crypto_hash_sha512_update (&state->state,
                                                 scal->scalarbytes,
                                                 sizeof (*scal)));
}


void
FROST_hash_uint8_update (struct FROST_HashState *state,
                         uint8_t identifier)
{
  GNUNET_assert (0 == crypto_hash_sha512_update (&state->state,
                                                 &identifier,
                                                 sizeof (identifier)));
}


void
FROST_hash_final (struct FROST_HashState *state,
                  struct FROST_HashCode *result)
{
  GNUNET_assert (0 == crypto_hash_sha512_final (&state->state,
                                                result->hashbytes));
}


void
FROST_hash_to_curve (struct FROST_Point *result,
                     const struct FROST_HashCode *hash)
{
  GNUNET_assert (0 == crypto_core_ristretto255_from_hash (result->xcoord,
                                                          hash->hashbytes));
}


enum GNUNET_GenericReturnValue
FROST_hash_cmp (const struct FROST_HashCode *first,
                const struct FROST_HashCode *second)
{
  if (0 == memcmp (&first->hashbytes,
                   &second->hashbytes,
                   sizeof (*first)))
    return GNUNET_OK;
  return GNUNET_NO;
}


void
FROST_hash_to_scalar (struct FROST_Scalar *result,
                      const struct FROST_HashCode *hash)
{
  crypto_core_ristretto255_scalar_reduce (result->scalarbytes,
                                          hash->hashbytes);
}


void
FROST_short_hash_init (struct FROST_ShortHashState *state)
{
  GNUNET_assert (0 == crypto_hash_sha256_init (&state->state));
}


void
FROST_short_hash_update_fixed (struct FROST_ShortHashState *state,
                               const void *msg,
                               size_t len)
{
  GNUNET_assert (0 == crypto_hash_sha256_update (&state->state,
                                                 msg,
                                                 len));
}


void
FROST_short_hash_final (struct FROST_ShortHashState *state,
                        struct FROST_ShortHashCode *result)
{
  GNUNET_assert (0 == crypto_hash_sha256_final (&state->state,
                                                result->hashbytes));
}


void
FROST_point_copy_to (struct FROST_Point *destination,
                     const struct FROST_Point *origin)
{
  memcpy (&destination->xcoord,
          &origin->xcoord,
          sizeof (*destination));
}


void
FROST_scalar_copy_to (struct FROST_Scalar *destination,
                      const struct FROST_Scalar *origin)
{
  memcpy (&destination->scalarbytes,
          &origin->scalarbytes,
          sizeof (*destination));
}


void
FROST_scalar_zero (struct FROST_Scalar *scal)
{
  memset (scal->scalarbytes,
          0,
          sizeof (*scal));
}


void
FROST_scalar_one (struct FROST_Scalar *scal)
{
  memset (scal->scalarbytes,
          0,
          sizeof (*scal));
  uint8_t one = 1;
  memcpy (scal->scalarbytes,
          &one,
          sizeof (one));
}


void
FROST_scalar_set_uint8 (struct FROST_Scalar *result,
                        uint8_t number)
{
  FROST_scalar_zero (result);
  memcpy (result,
          &number,
          sizeof (number));
}


enum GNUNET_GenericReturnValue
FROST_point_cmp (const struct FROST_Point *p,
                 const struct FROST_Point *q)
{
  if (0 == memcmp (p,
                   q,
                   crypto_core_ristretto255_BYTES))
    return GNUNET_OK;
  return GNUNET_NO;
}


enum GNUNET_GenericReturnValue
FROST_is_valid_point (const struct FROST_Point *p)
{
  struct FROST_Point identity;
  FROST_point_identity (&identity);

  if (1 == crypto_core_ristretto255_is_valid_point (p->xcoord)
      && GNUNET_NO == FROST_point_cmp (&identity,
                                       p))
    return GNUNET_OK;
  return GNUNET_NO;
}


void
FROST_kdf_scalar_to_curve (struct FROST_Scalar *subkey,
                           uint64_t subkey_id,
                           const struct FROST_ShortHashCode *masterkey)
{
  struct FROST_HashCode subkey_raw;
  GNUNET_assert (0 == crypto_kdf_derive_from_key (subkey_raw.hashbytes,
                                                  sizeof (subkey_raw),
                                                  subkey_id,
                                                  "FROSTLOW",
                                                  masterkey->hashbytes));

  FROST_hash_to_scalar (subkey,
                        &subkey_raw);
}


enum GNUNET_GenericReturnValue
FROST_pow_hash (struct FROST_HashCode *hash,
                const char *answer,
                size_t length,
                const struct FROST_PowSalt *salt,
                uint8_t difficulty)
{
  if (0 != crypto_pwhash (hash->hashbytes,
                          sizeof (*hash),
                          answer,
                          length,
                          salt->bytes,
                          crypto_pwhash_OPSLIMIT_MODERATE * difficulty,
                          crypto_pwhash_MEMLIMIT_MODERATE,
                          crypto_pwhash_ALG_ARGON2ID13))
    return GNUNET_NO;
  return GNUNET_OK;
}