/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file backend/frosix-httpd_dkg-shares.c
 * @brief functions to handle incoming requests on /dkg-shares
 * @author Joel Urech
 */
#include "frosix-httpd_dkg.h"
#include "frosix-httpd.h"
#include "frosix_database_plugin.h"
#include "frosix_service.h"
#include "keygen.h"
#include <taler/taler_util.h>
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_db_lib.h>

struct DkgSharesContext
{
  /**
   *
  */
  uint8_t provider_index;

  /**
   *
  */
  uint8_t threshold;

  /**
   *
  */
  uint8_t num_of_participants;

  /**
   *
  */
  struct FROSIX_DkgContextStringP context_string;

  /**
   *
  */
  struct FROSIX_ChallengeHashP auth_hash;

  /**
   *
  */
  struct FROSIX_DkgRequestIdP request_id;

  /**
   *
  */
  struct FROSIX_ProviderSaltP provider_salt;

  /**
   *
  */
  struct FROSIX_SecretProviderSaltP secret_provider_salt;

  /**
   *
  */
  struct FROST_DkgCommitment *dkg_commitments;

  /**
   * Our handler context
  */
  struct TM_HandlerContext *hc;

  /**
   * Uploaded JSON data, NULL if upload is not yet complete.
  */
  json_t *json;

  /**
   * Post parser context.
   */
  void *post_ctx;

  /**
   * Connection handle for closing or resuming
   */
  struct MHD_Connection *connection;

  /**
   * When should this request time out?
  */
  struct GNUNET_TIME_Absolute timeout;
};

/**
 * Return the generated dkg shares
 *
 * @param connection
 * @param dkg_shares
 * @return
*/
static MHD_RESULT
return_dkg_shares (
  struct MHD_Connection *connection,
  const struct FROST_DkgShare dkg_shares[],
  const struct GNUNET_CRYPTO_EddsaPublicKey pub_keys[],
  uint8_t provider_index,
  uint8_t num_of_participants)
{
  // Prepare secret shares
  json_t *shares;
  shares = json_array ();
  GNUNET_assert (NULL != shares);
  int counter = 0;

  for (int i = 0; i < num_of_participants; i++)
  {
    if (provider_index - 1 != i)
    {
      uint8_t target = i + 1;
      json_t *share;
      share = json_object ();

      /* get key material */
      struct GNUNET_CRYPTO_EcdhePrivateKey priv_enc_key;
      GNUNET_CRYPTO_ecdhe_key_create (&priv_enc_key);

      struct GNUNET_CRYPTO_EcdhePublicKey pub_enc_key;
      GNUNET_CRYPTO_ecdhe_key_get_public (&priv_enc_key,
                                          &pub_enc_key);

      struct GNUNET_HashCode key_material;
      if (GNUNET_OK != GNUNET_CRYPTO_ecdh_eddsa (&priv_enc_key,
                                                 &pub_keys[counter],
                                                 &key_material))
      {
        GNUNET_break (0);
        return MHD_NO;
      }

      /* derive key */
      struct GNUNET_CRYPTO_SymmetricSessionKey sym_key;
      struct GNUNET_CRYPTO_SymmetricInitializationVector iv;
      GNUNET_CRYPTO_hash_to_aes_key (&key_material,
                                     &sym_key,
                                     &iv);

      /* encrypt share */
      struct FROSIX_EncryptedShareP encrypted_share;
      ssize_t len = GNUNET_CRYPTO_symmetric_encrypt (&dkg_shares[i].share,
                                                     sizeof (dkg_shares[i].share),
                                                     &sym_key,
                                                     &iv,
                                                     &encrypted_share);

      if (-1 == len || sizeof (encrypted_share) != len)
      {
        GNUNET_break (0);
        return MHD_NO;
      }

      /* pack data in json */
      share = GNUNET_JSON_PACK (
        GNUNET_JSON_pack_uint64 ("target",
                                 target),
        GNUNET_JSON_pack_uint64 ("issuer",
                                 provider_index),
        GNUNET_JSON_pack_data_auto ("secret_share",
                                    &encrypted_share),
        GNUNET_JSON_pack_data_auto ("ephemeral_key",
                                    &pub_enc_key)
        );

      GNUNET_assert (0 ==
                     json_array_append (
                       shares,
                       share));
      counter++;
    }
  }

  return TALER_MHD_REPLY_JSON_PACK (
    connection,
    MHD_HTTP_OK,
    GNUNET_JSON_pack_array_steal ("secret_shares",
                                  shares));
}

static MHD_RESULT
store_commitments (struct DkgSharesContext *dc)
{
  /* copy commitments to a single byte array */
  struct FROSIX_DkgCommitmentsRaw commits;
  commits.length = (sizeof (dc->provider_index)
                    + (sizeof (struct FROST_Point)
                       * dc->threshold))
                   * (dc->num_of_participants - 1);
  commits.ptr_commits = calloc (1, commits.length);

  unsigned int offset = 0;

  for (int i = 0; i < dc->num_of_participants - 1; i++)
  {
    memcpy ((char *) commits.ptr_commits + offset,
            &dc->dkg_commitments[i].identifier,
            1);
    offset += 1;

    for (int j = 0; j < dc->threshold; j++)
    {
      memcpy ((char *) commits.ptr_commits + offset,
              &dc->dkg_commitments[i].share_comm[j].sc.xcoord,
              sizeof (struct FROST_Point));
      offset += sizeof (struct FROST_Point);
    }
  }

  /* store commitments */
  enum GNUNET_DB_QueryStatus qs;
  qs = db->store_dkg_commitment (db->cls,
                                 &dc->request_id,
                                 &commits);
  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
  case GNUNET_DB_STATUS_SOFT_ERROR:
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    GNUNET_break (0);
    free (commits.ptr_commits);
    return TALER_MHD_reply_with_error (dc->connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_INVARIANT_FAILURE,
                                       "store dkg-commitment");
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    break;
  }

  /* free initialized memory */
  free (commits.ptr_commits);
  return MHD_HTTP_OK;
}


MHD_RESULT
FH_handler_dkg_shares_post (
  struct MHD_Connection *connection,
  struct TM_HandlerContext *hc,
  const struct FROSIX_DkgRequestIdP *dkg_id,
  const struct FROSIX_ProviderSaltP *provider_salt,
  const struct FROSIX_SecretProviderSaltP *secret_provider_salt,
  const char *dkg_commitment_data,
  size_t *dkg_commitment_data_size)
{
  enum GNUNET_GenericReturnValue res;
  struct DkgSharesContext *dc = hc->ctx;
  json_t *json_ppk = NULL;
  json_t *commitments = NULL;

  if (NULL == dc)
  {
    dc = GNUNET_new (struct DkgSharesContext);
    dc->connection = connection;
    dc->request_id = *dkg_id;
    dc->provider_salt = *provider_salt;
    dc->secret_provider_salt = *secret_provider_salt;
    hc->ctx = dc;
  }

  /* parse request body */
  if (NULL == dc->json)
  {
    res = TALER_MHD_parse_post_json (dc->connection,
                                     &dc->post_ctx,
                                     dkg_commitment_data,
                                     dkg_commitment_data_size,
                                     &dc->json);
    if (GNUNET_SYSERR == res)
    {
      GNUNET_break (0);
      return MHD_NO;
    }
    if ((GNUNET_NO == res ||
         (NULL == dc->json)))
    {
      return MHD_YES;
    }
  }

  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_uint8 ("provider_index",
                            &dc->provider_index),
    GNUNET_JSON_spec_uint8 ("threshold",
                            &dc->threshold),
    GNUNET_JSON_spec_fixed_auto ("context_string",
                                 &dc->context_string),
    GNUNET_JSON_spec_fixed_auto ("auth_hash",
                                 &dc->auth_hash),
    GNUNET_JSON_spec_json ("providers_public_keys",
                           &json_ppk),
    GNUNET_JSON_spec_json ("dkg_commitments",
                           &commitments),
    GNUNET_JSON_spec_end ()
  };

  res = TALER_MHD_parse_json_data (connection,
                                   dc->json,
                                   spec);

  if (GNUNET_SYSERR == res)
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break (0);
    return MHD_NO;
  }
  if (GNUNET_NO == res)
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Unable to parse request body");
  }

  /* check number of submitted provider public keys*/
  if (254 < json_array_size (json_ppk))
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Too many provider public keys");
  }

  /* set number of participants */
  dc->num_of_participants = json_array_size (json_ppk);

  /* validate provider index, threshold and num_of_participants */
  if (GNUNET_OK != FROST_validate_dkg_params (dc->provider_index,
                                              dc->threshold,
                                              dc->num_of_participants))
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Protocol parameters out of bound");
  }

  /* Check if called id is matching the send data */
  if (GNUNET_OK != FROSIX_dkg_validate_request_id_ (&dc->request_id,
                                                    &dc->context_string,
                                                    &dc->auth_hash,
                                                    &dc->provider_salt,
                                                    dc->provider_index,
                                                    dc->num_of_participants,
                                                    dc->threshold))
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Unable to validate request ID");
  }

  /* parse provider public keys */
  struct GNUNET_CRYPTO_EddsaPublicKey
    providers_public_keys[dc->num_of_participants];
  for (unsigned int i = 0; i < dc->num_of_participants; i++)
  {
    struct GNUNET_JSON_Specification ppk_spec[] = {
      GNUNET_JSON_spec_fixed_auto (NULL,
                                   &providers_public_keys[i]),
      GNUNET_JSON_spec_end ()
    };

    res = TALER_MHD_parse_json_array (connection,
                                      json_ppk,
                                      ppk_spec,
                                      i,
                                      -1);

    if (GNUNET_SYSERR == res)
    {
      GNUNET_JSON_parse_free (spec);
      GNUNET_JSON_parse_free (ppk_spec);
      GNUNET_break (0);
      return MHD_NO;
    }
    if (GNUNET_NO == res)
    {
      GNUNET_JSON_parse_free (spec);
      GNUNET_JSON_parse_free (ppk_spec);
      GNUNET_break_op (0);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         "Unable to parse request body");
    }

    GNUNET_JSON_parse_free (ppk_spec);
  }

  /* check if there are num_of_participants - 1 commitments */
  if (dc->num_of_participants - 1 != json_array_size (commitments))
  {
    GNUNET_break_op (0);
    GNUNET_JSON_parse_free (spec);

    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Number of commitments not matching");
  }

  /* parse all commitments */
  struct FROST_DkgCommitment dkg_commitments[dc->num_of_participants - 1];
  dc->dkg_commitments = dkg_commitments;
  json_t *coms = json_array ();

  // Place to store the delivered public keys to later encrypt our secret shares
  // against
  struct GNUNET_CRYPTO_EddsaPublicKey pub_sig_keys[dc->num_of_participants - 1];

  for (unsigned int i = 0; i < dc->num_of_participants - 1; i++)
  {
    if (GNUNET_OK != FROST_initialize_dkg_commitment (&dc->dkg_commitments[i],
                                                      dc->provider_index,
                                                      dc->threshold))
    {
      GNUNET_break (0);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                         "Initialize DKG commitment");
    }

    json_t *json_temp = NULL;
    struct GNUNET_JSON_Specification com_spec[] = {
      GNUNET_JSON_spec_uint8 ("provider_index",
                              &dc->dkg_commitments[i].identifier),
      GNUNET_JSON_spec_json ("dkg_commitment",
                             &json_temp),
      GNUNET_JSON_spec_fixed_auto ("zkp_r",
                                   &dc->dkg_commitments[i].zkp.r),
      GNUNET_JSON_spec_fixed_auto ("zkp_z",
                                   &dc->dkg_commitments[i].zkp.z),
      GNUNET_JSON_spec_fixed_auto ("public_key",
                                   &pub_sig_keys[i]),
      GNUNET_JSON_spec_end ()
    };

    res = TALER_MHD_parse_json_array (connection,
                                      commitments,
                                      com_spec,
                                      i,
                                      -1);

    if (GNUNET_SYSERR == res)
    {
      GNUNET_JSON_parse_free (spec);
      GNUNET_JSON_parse_free (com_spec);
      FROST_free_dkg_commitment (dkg_commitments,
                                 dc->num_of_participants - 1);
      GNUNET_break (0);
      return MHD_NO;
    }
    if (GNUNET_NO == res)
    {
      GNUNET_JSON_parse_free (spec);
      GNUNET_JSON_parse_free (com_spec);
      FROST_free_dkg_commitment (dkg_commitments,
                                 dc->num_of_participants - 1);
      GNUNET_break_op (0);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         "Unable to parse request body");
    }

    /* check if there are `threshold` commitment values */
    if (dc->threshold != json_array_size (json_temp))
    {
      GNUNET_JSON_parse_free (spec);
      GNUNET_JSON_parse_free (com_spec);
      FROST_free_dkg_commitment (dkg_commitments,
                                 dc->num_of_participants - 1);
      GNUNET_break_op (0);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         "Number of commitment values not equal to threshold value");
    }
    dc->dkg_commitments[i].shares_commitments_length = dc->threshold;

    /* add temp value to json array */
    json_array_append_new (coms,
                           json_temp);

    /* parse the commitment values */
    for (unsigned int j = 0; j < dc->threshold; j++)
    {
      struct GNUNET_JSON_Specification coms_spec[] = {
        GNUNET_JSON_spec_fixed_auto (NULL,
                                     &dc->dkg_commitments[i].share_comm[j]),
        GNUNET_JSON_spec_end ()
      };

      res = TALER_MHD_parse_json_array (connection,
                                        json_array_get (coms,
                                                        i),
                                        coms_spec,
                                        j,
                                        -1);

      if (GNUNET_SYSERR == res)
      {
        GNUNET_JSON_parse_free (spec);
        GNUNET_JSON_parse_free (com_spec);
        GNUNET_JSON_parse_free (coms_spec);
        FROST_free_dkg_commitment (dkg_commitments,
                                   dc->num_of_participants - 1);
        GNUNET_break (0);
        return MHD_NO;
      }
      if (GNUNET_NO == res)
      {
        GNUNET_JSON_parse_free (spec);
        GNUNET_JSON_parse_free (com_spec);
        GNUNET_JSON_parse_free (coms_spec);
        FROST_free_dkg_commitment (dkg_commitments,
                                   dc->num_of_participants - 1);
        GNUNET_break_op (0);
        return TALER_MHD_reply_with_error (connection,
                                           MHD_HTTP_BAD_REQUEST,
                                           TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                           "Unable to parse request body");
      }

      /* free json spec */
      GNUNET_JSON_parse_free (coms_spec);
    }

    /* free json spec */
    GNUNET_JSON_parse_free (com_spec);
  }

  /* free json spec */
  GNUNET_JSON_parse_free (spec);

  /* check if commitments are valid */
  for (unsigned int i = 0; i < dc->num_of_participants - 1; i++)
  {
    /* hash received pub key */
    struct FROST_HashState h_s;
    struct FROST_HashCode pub_key_hash;
    FROST_hash_init (&h_s);
    FROST_hash_fixed_update (&h_s,
                             &pub_sig_keys[i].q_y,
                             sizeof (pub_sig_keys[i].q_y));
    FROST_hash_final (&h_s,
                      &pub_key_hash);

    if (GNUNET_OK != FROST_keygen_validate_commitment (&dc->dkg_commitments[i],
                                                       &pub_key_hash,
                                                       dc->num_of_participants))
    {
      FROST_free_dkg_commitment (dkg_commitments,
                                 dc->num_of_participants - 1);
      GNUNET_break_op (0);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         "Could not validate commitment");
    }
  }

  /* check if commitments are already stored */
  enum GNUNET_DB_QueryStatus qs;
  qs = db->lookup_dkg_commitment (db->cls,
                                  &dc->request_id);

  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
  case GNUNET_DB_STATUS_SOFT_ERROR:
    FROST_free_dkg_commitment (dkg_commitments,
                               dc->num_of_participants - 1);
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_INVARIANT_FAILURE,
                                       "lookup dkg-commitment");
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    /* store commitments */
    MHD_RESULT mhd_res;
    mhd_res = store_commitments (dc);
    if (MHD_HTTP_OK != mhd_res)
    {
      FROST_free_dkg_commitment (dkg_commitments,
                                 dc->num_of_participants - 1);
      return mhd_res;
    }
    break;
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    // commitments are already stored - go ahead
    break;
  }

  FROST_free_dkg_commitment (dkg_commitments,
                             dc->num_of_participants - 1);

  /* derive the final context string */
  struct FROST_DkgContextString cs_h;
  if (GNUNET_YES != FROSIX_dkg_derive_context_string_ (
        &cs_h,
        dc->provider_index,
        dc->threshold,
        &dc->context_string,
        &dc->auth_hash,
        providers_public_keys,
        dc->num_of_participants,
        &dc->secret_provider_salt))
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                       "Derive context string");
  }

  /* create shares */
  struct FROST_DkgShare dkg_shares[dc->num_of_participants];

  if (GNUNET_OK != FROSIX_dkg_shares_generate_ (dkg_shares,
                                                &cs_h,
                                                NULL,
                                                dc->provider_index,
                                                dc->threshold,
                                                dc->num_of_participants))
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                       "Generate DKG shares");
  }

  res = return_dkg_shares (connection,
                           dkg_shares,
                           pub_sig_keys,
                           dc->provider_index,
                           dc->num_of_participants);

  return res;

}