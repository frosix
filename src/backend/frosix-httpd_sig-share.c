/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file backend/frosix-httpd_sign-share.c
 * @brief functions to handle incoming requests on /sign-share
 * @author Joel Urech
 */
#include "frosix-httpd_sig.h"
#include "frosix-httpd.h"
#include "frosix_database_plugin.h"
#include "frost_high.h"
#include "frost_verify.h"
#include <taler/taler_util.h>
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_db_lib.h>
#include "frosix_authorization_plugin.h"

/**
 * What is the maximum frequency at which we allow
 * clients to attempt to answer security questions?
 */
#define MAX_QUESTION_FREQ GNUNET_TIME_relative_multiply ( \
    GNUNET_TIME_UNIT_SECONDS, 30)

/**
 * How many retries do we allow per code?
 */
#define INITIAL_RETRY_COUNTER 3

struct SignShareContext
{
  /**
   *
  */
  uint32_t identifier;

  /**
   *
  */
  uint8_t threshold;

  /**
   *
  */
  struct FROST_Commitment *commitments;

  /**
   *
  */
  struct FROSIX_EncryptionKey encryption_key;


  /**
   *
  */
  struct FROST_HashCode enc_key_hash;

  /**
   *
  */
  struct FROST_MessageHash message_hash;

  /**
   *
  */
  struct FROSIX_SigRequestIdP id;

  /**
   * Our handler context
  */
  struct TM_HandlerContext *hc;

  /**
   * Uploaded JSON data, NULL if upload is not yet complete.
  */
  json_t *json;

  /**
   * Post parser context.
   */
  void *post_ctx;

  /**
   * Connection handle for closing or resuming
   */
  struct MHD_Connection *connection;
};


/**
 * FIXME
*/
static struct FROST_Commitment *
get_my_commitment (uint32_t identifier,
                   struct FROST_Commitment commitments[],
                   uint8_t len)
{
  for (unsigned int i = 0; i < len; i++)
  {
    if (identifier == commitments[i].identifier)
      return &commitments[i];
  }
  return NULL;
}


static MHD_RESULT
return_sig_share (struct SignShareContext *gc)
{
  /* get key data from DB */
  struct FROSIX_EncryptionNonceP nonce;
  struct FROSIX_KeyDataEncrypted enc_key_data;

  enum GNUNET_DB_QueryStatus qs_key;
  qs_key = db->get_key_data (db->cls,
                             &gc->enc_key_hash,
                             &gc->identifier,
                             &nonce,
                             &enc_key_data);

  switch (qs_key)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
  case GNUNET_DB_STATUS_SOFT_ERROR:
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    GNUNET_free (gc->commitments);
    GNUNET_break (0);
    /* FROSIX_EC_KEY_NOT_FOUND */
    return TALER_MHD_reply_with_error (gc->connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       "key_data_select");
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    break;
  }

  /* validate identifier from db */
  if (gc->identifier <= 0 || gc->identifier >= 255 )
  {
    GNUNET_free (gc->commitments);
    GNUNET_break_op (0);
    /* FROSIX_EC_KEY_DATA_CORRUPT */
    return TALER_MHD_reply_with_error (gc->connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Got invalid data from DB");
  }

  /* get commitment */
  struct FROST_Commitment *my_comm = get_my_commitment (gc->identifier,
                                                        gc->commitments,
                                                        gc->threshold);

  if (NULL == my_comm)
  {
    GNUNET_free (gc->commitments);
    GNUNET_break_op (0);
    /* FROSIX_COMMITMENT NOT_FOUND */
    return TALER_MHD_reply_with_error (gc->connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Commitment not found");
  }

  /* compute seed id in db */
  struct GNUNET_HashCode db_seed_id;
  FROSIX_compute_db_commitment_id (&db_seed_id,
                                   &gc->enc_key_hash,
                                   my_comm);

  /* get commitment seed from DB */
  struct FROST_CommitmentSeed seed;

  enum GNUNET_DB_QueryStatus qs_seed;
  qs_seed = db->get_and_delete_commitment_seed (db->cls,
                                                &db_seed_id,
                                                &seed);

  switch (qs_seed)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
  case GNUNET_DB_STATUS_SOFT_ERROR:
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    GNUNET_free (gc->commitments);
    GNUNET_break (0);
    /* FROSIX_COMMITMENT_NOT_FOUND */
    return TALER_MHD_reply_with_error (gc->connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       "get_commitment_seed");
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    break;
  }

  /* compute nonce and commitment */
  struct FROST_Nonce com_nonce;
  struct FROST_Commitment commitment;
  FROST_generate_nonce_and_commitment (&com_nonce,
                                       &commitment,
                                       &gc->message_hash,
                                       &seed);

  /* FIXME check if computed commitment equals send commitment */

  /* decrypt key data from db */
  struct FROSIX_KeyDataRaw decrypted;
  if (GNUNET_OK != FROSIX_secretbox_keydata_open (&decrypted,
                                                  &enc_key_data,
                                                  &nonce,
                                                  &gc->encryption_key))
  {
    GNUNET_free (gc->commitments);
    GNUNET_break_op (0);
    /* FROSIX_EC_DECRYPTION_FAILED */
    return TALER_MHD_reply_with_error (gc->connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Unable to decrypt data from DB");
  }

  /* create key pair */
  struct FROST_KeyPair key_pair;
  key_pair.identifier = gc->identifier;

  FROSIX_raw_key_to_struct (&key_pair,
                            &decrypted);

  /* compute signature share */
  struct FROST_SignatureShare sig_share;
  FROST_sign_message_hash (&sig_share,
                           &gc->message_hash,
                           gc->commitments,
                           gc->threshold,
                           &key_pair,
                           &com_nonce);

  GNUNET_free (gc->commitments);

  return TALER_MHD_REPLY_JSON_PACK (
    gc->connection,
    MHD_HTTP_OK,
    GNUNET_JSON_pack_data_auto ("signature_share",
                                &sig_share.sig_share),
    GNUNET_JSON_pack_data_auto ("public_key_share",
                                &sig_share.pk_i));
}


MHD_RESULT
FH_handler_sig_share_post (
  struct MHD_Connection *connection,
  struct TM_HandlerContext *hc,
  const struct FROSIX_SigRequestIdP *id,
  const char *sign_share_data,
  size_t *sign_share_data_size)
{
  enum GNUNET_GenericReturnValue res;
  struct SignShareContext *dc = hc->ctx;
  json_t *json_commitments = NULL;

  if (NULL == dc)
  {
    dc = GNUNET_new (struct SignShareContext);
    dc->connection = connection;
    dc->id = *id;
    hc->ctx = dc;
  }

  /* parse request body */
  if (NULL == dc->json)
  {
    res = TALER_MHD_parse_post_json (dc->connection,
                                     &dc->post_ctx,
                                     sign_share_data,
                                     sign_share_data_size,
                                     &dc->json);
    if (GNUNET_SYSERR == res)
    {
      GNUNET_break (0);
      return MHD_NO;
    }
    if ((GNUNET_NO == res ||
         (NULL == dc->json)))
    {
      return MHD_YES;
    }
  }

  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_fixed_auto ("message_hash",
                                 &dc->message_hash),
    GNUNET_JSON_spec_fixed_auto ("encryption_key",
                                 &dc->encryption_key),
    GNUNET_JSON_spec_json ("commitments",
                           &json_commitments),
    GNUNET_JSON_spec_end ()
  };

  res = TALER_MHD_parse_json_data (connection,
                                   dc->json,
                                   spec);

  if (GNUNET_SYSERR == res)
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break (0);
    return MHD_NO;
  }
  if (GNUNET_NO == res)
  {
    GNUNET_JSON_parse_free (spec);
    /* FROSIX_EC_PARAMETER_MALFORMED */
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Unable to parse request body");
  }

  /* hash encryption key */
  FROSIX_hash_encryption_key (&dc->enc_key_hash,
                              &dc->encryption_key);

  /* validate id */
  {
    struct FROSIX_SigRequestIdP req_id;
    FROSIX_compute_signature_request_id (&req_id,
                                         &dc->enc_key_hash,
                                         &dc->message_hash);
    if (GNUNET_NO == FROST_hash_cmp (&req_id.id,
                                     &id->id))
    {
      GNUNET_JSON_parse_free (spec);
      /* FROSIX_EC_REQUEST_ID_NOT_MATCHING */
      GNUNET_break_op (0);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         "ID in URL not matching data in body");
    }
  }

  /* get number of provided commitments */
  dc->threshold = json_array_size (json_commitments);
  if (0 >= dc->threshold || dc->threshold >= 253)
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break_op (0);
    /* FROSIX_EC_NO_OR_TOO_MANY_COMMITMENTS */
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "No or too many commitments");
  }

  /* parse commitments */
  dc->commitments = GNUNET_new_array (dc->threshold,
                                      struct FROST_Commitment);

  for (int i = 0; i < dc->threshold; i++)
  {
    struct GNUNET_JSON_Specification comm_spec[] = {
      GNUNET_JSON_spec_uint8 ("identifier",
                              &dc->commitments[i].identifier),
      GNUNET_JSON_spec_fixed_auto ("hiding_commitment",
                                   &dc->commitments[i].hiding_commitment),
      GNUNET_JSON_spec_fixed_auto ("binding_commitment",
                                   &dc->commitments[i].binding_commitment),
      GNUNET_JSON_spec_end ()
    };

    res = TALER_MHD_parse_json_array (connection,
                                      json_commitments,
                                      comm_spec,
                                      i,
                                      -1);

    if (GNUNET_SYSERR == res)
    {
      GNUNET_free (dc->commitments);
      GNUNET_JSON_parse_free (spec);
      GNUNET_JSON_parse_free (comm_spec);
      GNUNET_break (0);
      return MHD_NO;
    }
    if (GNUNET_NO == res)
    {
      GNUNET_free (dc->commitments);
      GNUNET_JSON_parse_free (spec);
      GNUNET_JSON_parse_free (comm_spec);
      GNUNET_break_op (0);
      /* FROSIX_EC_COMMITMENTS_MALFORMED */
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         "Unable to parse request body");
    }

    GNUNET_JSON_parse_free (comm_spec);
  }

  GNUNET_JSON_parse_free (spec);

  /* validate commitments */
  for (unsigned int i = 0; i < dc->threshold; i++)
  {
    if (dc->commitments[i].identifier <= 0 || dc->commitments[i].identifier >=
        255 )
    {
      GNUNET_free (dc->commitments);
      GNUNET_break_op (0);
      /* FROSIX_EC_COMMITMENT_IDENTIFIER_WRONG */
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         "Unable to validate commitments");
    }

    if (GNUNET_OK != FROST_validate_commitment (&dc->commitments[i]))
    {
      GNUNET_free (dc->commitments);
      GNUNET_break_op (0);
      /* FROSIX_EC_COMMITMENT_VALIDATION_FAILED (return id of failing
         commitment?) */
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         "Unable to validate commitments");
    }
  }

  return return_sig_share (dc);
}