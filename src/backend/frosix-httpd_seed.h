/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file backend/frosix-httpd_seed.h
 * @brief function to handle incoming requests on /policy
 * @author Joel Urech
 */
#ifndef FROSIX_HTTPD_SEED_H
#define FROSIX_HTTPD_SEED_H
#include "frosix-httpd.h"
#include <microhttpd.h>

#define FROSIX_SEED_SIZE 64

/**
 * Manages a /seed call.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @return MHD result code
 */
MHD_RESULT
FH_seed_get (struct FH_RequestHandler *rh,
             struct MHD_Connection *connection);

#endif