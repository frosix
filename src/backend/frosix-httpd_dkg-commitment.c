/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file backend/frosix-httpd_commitment.c
 * @brief functions to handle incoming requests on /dkg-commitment
 * @author Joel Urech
 */
#include "frosix-httpd_dkg.h"
#include "frosix-httpd.h"
#include "frosix_service.h"
#include "keygen.h"
#include <taler/taler_util.h>
#include <gnunet/gnunet_util_lib.h>

#define HASHCONTEXT "FROSIX-DKG"

/**
 * Context for an dkg commitment operation
*/
struct DkgCommitmentContext
{
  /**
   * Our unique identifier value in the dkg process.
  */
  uint8_t provider_index;

  /**
   * Number of providers which has to collaborate in order to generate a valid signature.
  */
  uint8_t threshold;

  /**
   * Total number of providers participating in the distributed key generation process.
  */
  uint8_t num_of_participants;

  /**
   * High entropy string, which should be only used for one distributed key generation process.
   * The resulting commitment will be deterministically derived from this context string.
  */
  struct FROSIX_DkgContextStringP context_string;

  /**
   * Salted hash of the chosen challenge method.
  */
  struct FROSIX_ChallengeHashP auth_hash;

  /**
   * Id of the request, hash of all submitted values.
  */
  struct FROSIX_DkgRequestIdP request_id;

  /**
   *
  */
  struct FROSIX_ProviderSaltP provider_salt;

  /**
   *
  */
  struct FROSIX_SecretProviderSaltP secret_provider_salt;

  /**
   *
  */
  struct GNUNET_CRYPTO_EddsaPublicKey pub_sig_key;

  /**
   *
  */
  struct GNUNET_CRYPTO_EddsaPrivateKey priv_sig_key;

  /**
   * Our handler context
  */
  struct TM_HandlerContext *hc;

  /**
   * Uploaded JSON data, NULL if upload is not yet complete.
  */
  json_t *json;

  /**
   * Post parser context.
   */
  void *post_ctx;

  /**
   * Connection handle for closing or resuming
   */
  struct MHD_Connection *connection;

  /**
   * When should this request time out?
  */
  struct GNUNET_TIME_Absolute timeout;

};


/**
 * Return the generated dkg_commitment
 *
 * @param connection
 * @param dkg_comm
 * @return
*/
static MHD_RESULT
return_dkg_commitment (
  struct MHD_Connection *connection,
  const struct GNUNET_CRYPTO_EddsaPublicKey *pub_sig_key,
  const struct FROST_DkgCommitment *dkg_comm)
{
  // Prepare commit
  json_t *commits;
  commits = json_array ();
  GNUNET_assert (NULL != commits);

  for (int i = 0; i < dkg_comm->shares_commitments_length; i++)
  {
    GNUNET_assert (0 ==
                   json_array_append (
                     commits,
                     GNUNET_JSON_from_data_auto (&dkg_comm->share_comm[i])));
  }

  // Return everything
  return TALER_MHD_REPLY_JSON_PACK (
    connection,
    MHD_HTTP_CREATED,
    GNUNET_JSON_pack_uint64 ("provider_index",
                             dkg_comm->identifier),
    GNUNET_JSON_pack_array_steal ("dkg_commitment",
                                  commits),
    GNUNET_JSON_pack_data_auto ("zkp_r",
                                &dkg_comm->zkp.r),
    GNUNET_JSON_pack_data_auto ("zkp_z",
                                &dkg_comm->zkp.z),
    GNUNET_JSON_pack_data_auto ("public_key",
                                pub_sig_key));
}

MHD_RESULT
FH_handler_dkg_commitment_post (
  struct MHD_Connection *connection,
  struct TM_HandlerContext *hc,
  const struct FROSIX_DkgRequestIdP *dkg_id,
  const struct FROSIX_ProviderSaltP *provider_salt,
  const struct FROSIX_SecretProviderSaltP *secret_provider_salt,
  const struct GNUNET_CRYPTO_EddsaPrivateKey *priv_sig_key,
  const struct GNUNET_CRYPTO_EddsaPublicKey *pub_sig_key,
  const char *dkg_commitment_data,
  size_t *dkg_commitment_data_size)
{
  enum GNUNET_GenericReturnValue res;
  struct DkgCommitmentContext *dc = hc->ctx;
  json_t *json_ppk = NULL;

  if (NULL == dc)
  {
    dc = GNUNET_new (struct DkgCommitmentContext);
    dc->connection = connection;
    dc->request_id = *dkg_id;
    dc->provider_salt = *provider_salt;
    dc->secret_provider_salt = *secret_provider_salt;
    dc->priv_sig_key = *priv_sig_key;
    dc->pub_sig_key = *pub_sig_key;
    hc->ctx = dc;
  }

  /* parse request body */
  if (NULL == dc->json)
  {
    res = TALER_MHD_parse_post_json (connection,
                                     &dc->post_ctx,
                                     dkg_commitment_data,
                                     dkg_commitment_data_size,
                                     &dc->json);
    if (GNUNET_SYSERR == res)
    {
      GNUNET_break (0);
      return MHD_NO;
    }
    if ((GNUNET_NO == res ||
         (NULL == dc->json)))
    {
      return MHD_YES;
    }
  }

  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_uint8 ("provider_index",
                            &dc->provider_index),
    GNUNET_JSON_spec_uint8 ("threshold",
                            &dc->threshold),
    GNUNET_JSON_spec_fixed_auto ("context_string",
                                 &dc->context_string),
    GNUNET_JSON_spec_fixed_auto ("auth_hash",
                                 &dc->auth_hash),
    GNUNET_JSON_spec_json ("providers_public_keys",
                           &json_ppk),
    GNUNET_JSON_spec_end ()
  };

  res = TALER_MHD_parse_json_data (connection,
                                   dc->json,
                                   spec);
  if (GNUNET_SYSERR == res)
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                       "Unable to parse request body");
  }
  if (GNUNET_NO == res)
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Unable to parse request body");
  }

  /* check number of submitted provider public keys*/
  if (254 < json_array_size (json_ppk))
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Too many provider public keys");
  }

  /* set number of participants */
  dc->num_of_participants = json_array_size (json_ppk);

  /* validate provider index, threshold and num_of_participants */
  if (GNUNET_OK != FROST_validate_dkg_params (dc->provider_index,
                                              dc->threshold,
                                              dc->num_of_participants))
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Parameters out ouf bound");
  }

  /* Check if called id is matching the send data */
  if (GNUNET_OK != FROSIX_dkg_validate_request_id_ (
        &dc->request_id,
        &dc->context_string,
        &dc->auth_hash,
        &dc->provider_salt,
        dc->provider_index,
        dc->num_of_participants,
        dc->threshold))
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "ID in URL not matching data in body");
  }

  /* parse providers public keys */
  struct GNUNET_CRYPTO_EddsaPublicKey
    providers_public_keys[dc->num_of_participants];
  for (unsigned int i = 0; i < dc->num_of_participants; i++)
  {
    struct GNUNET_JSON_Specification ppk_spec[] = {
      GNUNET_JSON_spec_fixed_auto (NULL,
                                   &providers_public_keys[i]),
      GNUNET_JSON_spec_end ()
    };

    res = TALER_MHD_parse_json_array (connection,
                                      json_ppk,
                                      ppk_spec,
                                      i,
                                      -1);

    if (GNUNET_SYSERR == res)
    {
      GNUNET_JSON_parse_free (spec);
      GNUNET_JSON_parse_free (ppk_spec);
      GNUNET_break (0);
      return MHD_NO;
    }
    if (GNUNET_NO == res)
    {
      GNUNET_JSON_parse_free (spec);
      GNUNET_JSON_parse_free (ppk_spec);
      GNUNET_break_op (0);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         "Unable to parse request body");
    }

    GNUNET_JSON_parse_free (ppk_spec);
  }

  GNUNET_JSON_parse_free (spec);

  /* FIXME: check if any of the submitted public key is ours */

  /* derive the final context string */
  struct FROST_DkgContextString cs_h;
  if (GNUNET_YES != FROSIX_dkg_derive_context_string_ (
        &cs_h,
        dc->provider_index,
        dc->threshold,
        &dc->context_string,
        &dc->auth_hash,
        providers_public_keys,
        dc->num_of_participants,
        &dc->secret_provider_salt))
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                       "Derive context string");
  }

  /* hash public key */
  struct FROST_HashState h_s;
  struct FROST_HashCode pub_key_hash;
  FROST_hash_init (&h_s);
  FROST_hash_fixed_update (&h_s,
                           &dc->pub_sig_key,
                           sizeof (dc->pub_sig_key));
  FROST_hash_final (&h_s,
                    &pub_key_hash);

  /* create shares with zkp including hashed pk */
  struct FROST_DkgCommitment dkg_commitment;

  if (GNUNET_OK != FROSIX_dkg_commitment_generate_ (&dkg_commitment,
                                                    &cs_h,
                                                    &pub_key_hash,
                                                    dc->provider_index,
                                                    dc->threshold,
                                                    dc->num_of_participants))
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                       "Generate DKG commitment");
  }

  res = return_dkg_commitment (connection,
                               &dc->pub_sig_key,
                               &dkg_commitment);

  FROST_free_dkg_commitment (&dkg_commitment,
                             1);

  return res;
}