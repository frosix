/*
  This file is part of Frosix
  Copyright (C) 2020, 2021 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file backend/frosix-httpd_terms.h
 * @brief headers for /terms handler
 * @author Christian Grothoff
 * @author Dennis Neufeld
 * @author Dominik Meister
 */
#ifndef FROSIX_HTTPD_TERMS_H
#define FROSIX_HTTPD_TERMS_H
#include <microhttpd.h>
#include "frosix-httpd.h"

/**
 * Manages a /terms call.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @return MHD result code
 */
MHD_RESULT
FH_handler_terms (struct FH_RequestHandler *rh,
                  struct MHD_Connection *connection);


/**
 * Handle a "/privacy" request.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @return MHD result code
 */
MHD_RESULT
FH_handler_privacy (struct FH_RequestHandler *rh,
                    struct MHD_Connection *connection);

/**
 * Load our terms of service as per configuration.
 *
 * @param cfg configuration to process
 */
void
FH_load_terms (const struct GNUNET_CONFIGURATION_Handle *cfg);


#endif

/* end of frosix-httpd_terms.h */
