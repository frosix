/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file backend/frosix-httpd_dkg.h
 * @brief functions to handle incoming requests on /dkg-*
 * @author Joel Urech
 */
#ifndef FROSIX_HTTPD_DKG_H
#define FROSIX_HTTPD_DKG_H
#include "frosix-httpd.h"
#include "keygen.h"
#include "frosix_service.h"
#include "frosix_util_lib.h"
#include <microhttpd.h>
#include <taler/taler_mhd_lib.h>


/**
 *
 *
 * @param[out] dkg_comm
 * @param[in] context_string
 * @param[in] additional_data
 * @param identifier
 * @param threshold
 * @param num_of_participants
 * @return GNUNET_OK or GNUNET_NO
*/
enum GNUNET_GenericReturnValue
FROSIX_dkg_commitment_generate_ (
  struct FROST_DkgCommitment *dkg_comm,
  const struct FROST_DkgContextString *context_string,
  const struct FROST_HashCode *additional_data,
  uint8_t identifier,
  uint8_t threshold,
  uint8_t num_of_participants);

/**
 * FIXME
*/
enum GNUNET_GenericReturnValue
FROSIX_dkg_shares_generate_ (
  struct FROST_DkgShare dkg_shares[],
  const struct FROST_DkgContextString *context_string,
  const struct FROST_HashCode *additional_data,
  uint8_t identifier,
  uint8_t threshold,
  uint8_t num_of_participants);

/**
 *
*/
enum GNUNET_GenericReturnValue
FROSIX_dkg_derive_context_string_ (
  struct FROST_DkgContextString *resulting_context_string,
  uint8_t provider_index,
  uint8_t threshold,
  const struct FROSIX_DkgContextStringP *input_context_string,
  const struct FROSIX_ChallengeHashP *auth_hash,
  const struct GNUNET_CRYPTO_EddsaPublicKey provider_public_keys[],
  size_t length,
  const struct FROSIX_SecretProviderSaltP*secret_provider_salt);


/**
 *
*/
enum GNUNET_GenericReturnValue
FROSIX_dkg_validate_request_id_ (
  const struct FROSIX_DkgRequestIdP *request_id,
  const struct FROSIX_DkgContextStringP *context_string,
  const struct FROSIX_ChallengeHashP *auth_hash,
  const struct FROSIX_ProviderSaltP *secret_provider_salt,
  uint8_t identifier,
  uint8_t num_of_participants,
  uint8_t threshold);

/**
 * FIXME
*/
MHD_RESULT
FH_handler_dkg_commitment_post (
  struct MHD_Connection *connection,
  struct TM_HandlerContext *hc,
  const struct FROSIX_DkgRequestIdP *dkg_id,
  const struct FROSIX_ProviderSaltP *provider_salt,
  const struct FROSIX_SecretProviderSaltP *secret_provider_salt,
  const struct GNUNET_CRYPTO_EddsaPrivateKey *priv_sig_key,
  const struct GNUNET_CRYPTO_EddsaPublicKey *pub_sig_key,
  const char *dkg_commitment_data,
  size_t *dkg_commitment_data_size);

/**
 * FIXME
*/
MHD_RESULT
FH_handler_dkg_shares_post (
  struct MHD_Connection *connection,
  struct TM_HandlerContext *hc,
  const struct FROSIX_DkgRequestIdP *dkg_id,
  const struct FROSIX_ProviderSaltP *provider_salt,
  const struct FROSIX_SecretProviderSaltP *secret_provider_salt,
  const char *dkg_commitment_data,
  size_t *dkg_commitment_data_size);

/**
 * FIXME
*/
MHD_RESULT
FH_handler_dkg_key_post (
  struct MHD_Connection *connection,
  struct TM_HandlerContext *hc,
  const struct FROSIX_DkgRequestIdP *dkg_id,
  const struct FROSIX_ProviderSaltP *provider_salt,
  const struct FROSIX_SecretProviderSaltP *secret_provider_salt,
  const struct GNUNET_CRYPTO_EddsaPrivateKey *priv_sig_key,
  const char *dkg_commitment_data,
  size_t *dkg_commitment_data_size);

/**
 * FIXME
*/
MHD_RESULT
FH_handler_key_delete (
  struct MHD_Connection *connection,
  struct TM_HandlerContext *hc,
  const struct FROSIX_DkgRequestIdP *id);

#endif