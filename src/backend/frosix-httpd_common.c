/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file backend/frosix-httpd_dkg-commitment_common.c
 * @brief functions to handle incoming requests on /seed
 * @author Joel Urech
 */
#include "frosix-httpd_dkg.h"
#include "frosix_service.h"
#include "keygen.h"
#include "frost_low.h"
#include <taler/taler_util.h>
#include <gnunet/gnunet_util_lib.h>


static enum GNUNET_GenericReturnValue
generate_commitment_and_shares (
  struct FROST_DkgCommitment *dkg_comm,
  struct FROST_DkgShare dkg_shares[],
  const struct FROST_DkgContextString *context_string,
  const struct FROST_HashCode *additional_data,
  uint8_t identifier,
  uint8_t threshold,
  uint8_t num_of_participants)
{
  if (GNUNET_OK != FROST_initialize_dkg_commitment (dkg_comm,
                                                    identifier,
                                                    threshold))
    return GNUNET_NO;

  return FROST_keygen_begin (dkg_comm,
                             dkg_shares,
                             context_string,
                             additional_data,
                             identifier,
                             num_of_participants,
                             threshold);
}

enum GNUNET_GenericReturnValue
FROSIX_dkg_commitment_generate_ (
  struct FROST_DkgCommitment *dkg_comm,
  const struct FROST_DkgContextString *context_string,
  const struct FROST_HashCode *additional_data,
  uint8_t identifier,
  uint8_t threshold,
  uint8_t num_of_participants)
{
  struct FROST_DkgShare dkg_shares[num_of_participants];

  enum GNUNET_GenericReturnValue ret = generate_commitment_and_shares (dkg_comm,
                                                                       dkg_shares,
                                                                       context_string,
                                                                       additional_data,
                                                                       identifier,
                                                                       threshold,
                                                                       num_of_participants);

  return ret;
}

enum GNUNET_GenericReturnValue
FROSIX_dkg_shares_generate_ (
  struct FROST_DkgShare dkg_shares[],
  const struct FROST_DkgContextString *context_string,
  const struct FROST_HashCode *additional_data,
  uint8_t identifier,
  uint8_t threshold,
  uint8_t num_of_participants)
{
  struct FROST_DkgCommitment dkg_comm;

  enum GNUNET_GenericReturnValue ret =
    generate_commitment_and_shares (&dkg_comm,
                                    dkg_shares,
                                    context_string,
                                    additional_data,
                                    identifier,
                                    threshold,
                                    num_of_participants);

  FROST_free_dkg_commitment (&dkg_comm,
                             1);

  return ret;
}

enum GNUNET_GenericReturnValue
FROSIX_dkg_derive_context_string_ (
  struct FROST_DkgContextString *resulting_context_string,
  uint8_t provider_index,
  uint8_t threshold,
  const struct FROSIX_DkgContextStringP *input_context_string,
  const struct FROSIX_ChallengeHashP *challenge_hash,
  const struct GNUNET_CRYPTO_EddsaPublicKey provider_public_keys[],
  size_t length,
  const struct FROSIX_SecretProviderSaltP *secret_provider_salt)
{
  /* get hash of all public keys, concatenated with provider index and threshold
     */
  struct GNUNET_HashContext *hash_context = GNUNET_CRYPTO_hash_context_start ();

  for (unsigned int i = 0; i < length; i++)
  {
    GNUNET_CRYPTO_hash_context_read (hash_context,
                                     &provider_public_keys[i],
                                     sizeof (*provider_public_keys));
  }
  GNUNET_CRYPTO_hash_context_read (hash_context,
                                   &provider_index,
                                   sizeof (provider_index));
  GNUNET_CRYPTO_hash_context_read (hash_context,
                                   &threshold,
                                   sizeof (threshold));

  struct GNUNET_HashCode pk_hash;
  GNUNET_CRYPTO_hash_context_finish (hash_context,
                                     &pk_hash);

  /* derive entropy for our key generation */
  return GNUNET_CRYPTO_kdf (resulting_context_string,
                            sizeof (*resulting_context_string),
                            secret_provider_salt,
                            sizeof (*secret_provider_salt),
                            input_context_string,
                            sizeof (*input_context_string),
                            challenge_hash,
                            sizeof (*challenge_hash),
                            &pk_hash,
                            sizeof (pk_hash),
                            NULL);
}

enum GNUNET_GenericReturnValue
FROSIX_dkg_validate_request_id_ (
  const struct FROSIX_DkgRequestIdP *request_id,
  const struct FROSIX_DkgContextStringP *context_string,
  const struct FROSIX_ChallengeHashP *challenge_hash,
  const struct FROSIX_ProviderSaltP *provider_salt,
  uint8_t identifier,
  uint8_t num_of_participants,
  uint8_t threshold)
{
  /* is request_id == H(context_string ||
                    auth_data_hash_salted ||
                    identifier ||
                    num_of_participants ||
                    threshold ||
                    public_server_salt ||
                    "FROSIX-DKG" )*/
  struct FROST_HashState check_id_state;
  struct FROST_HashCode check_id;

  FROST_hash_init (&check_id_state);
  FROST_hash_hash_update (&check_id_state,
                          &context_string->hash);
  FROST_hash_fixed_update (&check_id_state,
                           &challenge_hash->hash,
                           sizeof (*challenge_hash));
  FROST_hash_uint8_update (&check_id_state,
                           identifier);
  FROST_hash_uint8_update (&check_id_state,
                           num_of_participants);
  FROST_hash_uint8_update (&check_id_state,
                           threshold);
  FROST_hash_fixed_update (&check_id_state,
                           provider_salt,
                           sizeof (*provider_salt));
  FROST_hash_fixed_update (&check_id_state,
                           "FROSIX-DKG-ID",
                           strlen ("FROSIX-DKG-ID"));
  FROST_hash_final (&check_id_state,
                    &check_id);

  return FROST_hash_cmp (&request_id->id,
                         &check_id);
}

void
FROSIX_raw_key_to_struct (struct FROST_KeyPair *key_pair,
                          const struct FROSIX_KeyDataRaw *decrypted)
{
  memcpy (&key_pair->my_sk,
          &decrypted->bytes[0],
          sizeof (key_pair->my_sk));
  memcpy (&key_pair->group_pk,
          &decrypted->bytes[sizeof (key_pair->my_sk)],
          sizeof (key_pair->my_pk));
  FROST_base_mul_scalar (&key_pair->my_pk,
                         &key_pair->my_sk);
}


void
FROSIX_compute_db_commitment_id (struct GNUNET_HashCode *db_id,
                                 const struct FROST_HashCode *enc_key_hash,
                                 const struct FROST_Commitment *commitment)
{
  struct GNUNET_HashContext *hc = GNUNET_CRYPTO_hash_context_start ();
  GNUNET_CRYPTO_hash_context_read (hc,
                                   enc_key_hash,
                                   sizeof (*enc_key_hash));
  GNUNET_CRYPTO_hash_context_read (hc,
                                   &commitment->hiding_commitment,
                                   sizeof (commitment->hiding_commitment));
  GNUNET_CRYPTO_hash_context_read (hc,
                                   &commitment->binding_commitment,
                                   sizeof (commitment->binding_commitment));
  GNUNET_CRYPTO_hash_context_finish (hc,
                                     db_id);
}