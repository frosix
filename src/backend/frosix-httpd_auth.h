/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file backend/frosix-httpd_auth.h
 * @brief functions to handle incoming requests on /auth-challenge
 * @author Joel Urech
 */
#ifndef FROSIX_HTTPD_AUTH_H
#define FROSIX_HTTPD_AUTH_H
#include "frosix-httpd.h"
#include "frosix_crypto.h"
#include "frosix_util_lib.h"
#include <microhttpd.h>
#include <taler/taler_mhd_lib.h>

MHD_RESULT
FH_handler_auth_challenge_post (
  struct MHD_Connection *connection,
  struct TM_HandlerContext *hc,
  const struct FROSIX_ChallengeRequestIdP *id,
  const char *dkg_commitment_data,
  size_t *dkg_commitment_data_size);

#endif