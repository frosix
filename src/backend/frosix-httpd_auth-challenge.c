/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file backend/frosix-httpd_commitment.c
 * @brief functions to handle incoming requests on /auth-challenge
 * @author Joel Urech
 */
#include "frosix-httpd_auth.h"
#include "frosix-httpd.h"
#include "frost_high.h"
#include "frost_low.h"
#include <taler/taler_util.h>
#include <gnunet/gnunet_util_lib.h>
#include "frosix_database_plugin.h"
#include "frosix_util_lib.h"
#include "frosix_authorization_lib.h"

#define HASHCONTEXT "FROSIX-DKG"

/**
 * Context for an dkg commitment operation
*/
struct ChallengeContext
{
  /**
   *
  */
  struct FROSIX_EncryptionKey encryption_key;

  /**
   *
  */
  struct FROST_MessageHash message_hash;

  /**
   *
  */
  struct GNUNET_HashCode challenge_salt;

  /**
   *
  */
  const char *challenge_method;

  /**
   *
  */
  const char *challenge_data;

  /**
   * Id of the request, hash of all submitted values.
  */
  struct FROSIX_ChallengeRequestIdP request_id;

  /**
   * Our handler context
  */
  struct TM_HandlerContext *hc;

  /**
   * Uploaded JSON data, NULL if upload is not yet complete.
  */
  json_t *json;

  /**
   * Post parser context.
   */
  void *post_ctx;

  /**
   * Connection handle for closing or resuming
   */
  struct MHD_Connection *connection;

  /**
   * Reference to the authorization plugin which was loaded
  */
  struct FROSIX_AuthorizationPlugin *authorization;

  /**
   * Status of the authorization
   */
  struct FROSIX_AUTHORIZATION_State *as;

  /**
   * Random authorization code we are using.
   */
  uint64_t code;

  /**
   * FIXME
  */
  struct FROSIX_ChallengeIdP challenge_id;
};


/**
 * Generate a response telling the client that answering this
 * challenge failed because the rate limit has been exceeded.
 *
 * @param gc request to answer for
 * @return MHD status code
 */
static MHD_RESULT
reply_rate_limited (const struct ChallengeContext *cc)
{
  return TALER_MHD_REPLY_JSON_PACK (
    cc->connection,
    MHD_HTTP_TOO_MANY_REQUESTS,
    TALER_MHD_PACK_EC (TALER_EC_GENERIC_TIMEOUT),
    GNUNET_JSON_pack_uint64 ("request_limit",
                             cc->authorization->retry_counter),
    GNUNET_JSON_pack_time_rel ("request_frequency",
                               cc->authorization->code_rotation_period));
}


/**
 * Run the authorization method-specific 'process' function and continue
 * based on its result with generating an HTTP response.
 *
 * @param connection the connection we are handling
 * @param gc our overall handler context
 */
static MHD_RESULT
run_authorization_process (struct MHD_Connection *connection,
                           struct ChallengeContext *gc)
{
  enum FROSIX_AUTHORIZATION_ChallengeResult ret;
  enum GNUNET_DB_QueryStatus qs;

  if (NULL == gc->authorization->challenge)
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (gc->connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_METHOD_INVALID,
                                       "challenge method not implemented for authorization method");
  }
  ret = gc->authorization->challenge (gc->as,
                                      connection);
  switch (ret)
  {
  case FROSIX_AUTHORIZATION_CRES_SUCCESS:
    /* Challenge sent successfully */
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Authorization request %llu for %s sent successfully\n",
                (unsigned long long) gc->code,
                TALER_B2S (&gc->challenge_id));
    qs = db->mark_challenge_sent (db->cls,
                                  &gc->challenge_id,
                                  gc->code);
    GNUNET_break (0 < qs);
    gc->authorization->cleanup (gc->as);
    gc->as = NULL;
    return MHD_YES;
  case FROSIX_AUTHORIZATION_CRES_FAILED:
    gc->authorization->cleanup (gc->as);
    gc->as = NULL;
    return MHD_YES;
  case FROSIX_AUTHORIZATION_CRES_SUSPENDED:
    /* connection was suspended */
    // gc_suspended (gc);
    return MHD_YES;
  case FROSIX_AUTHORIZATION_CRES_SUCCESS_REPLY_FAILED:
    /* Challenge sent successfully */
    qs = db->mark_challenge_sent (db->cls,
                                  &gc->challenge_id,
                                  gc->code);
    GNUNET_break (0 < qs);
    gc->authorization->cleanup (gc->as);
    gc->as = NULL;
    return MHD_NO;
  case FROSIX_AUTHORIZATION_CRES_FAILED_REPLY_FAILED:
    gc->authorization->cleanup (gc->as);
    gc->as = NULL;
    return MHD_NO;
  }
  GNUNET_break (0);
  return MHD_NO;
}


MHD_RESULT
FH_handler_auth_challenge_post (
  struct MHD_Connection *connection,
  struct TM_HandlerContext *hc,
  const struct FROSIX_ChallengeRequestIdP *id,
  const char *auth_challenge_data,
  size_t *auth_challenge_data_size)
{
  enum GNUNET_GenericReturnValue res;
  struct ChallengeContext *cc = hc->ctx;

  if (NULL == cc)
  {
    cc = GNUNET_new (struct ChallengeContext);
    cc->connection = connection;
    hc->ctx = cc;
  }

  /* parse request body */
  if (NULL == cc->json)
  {
    res = TALER_MHD_parse_post_json (connection,
                                     &cc->post_ctx,
                                     auth_challenge_data,
                                     auth_challenge_data_size,
                                     &cc->json);
    if (GNUNET_SYSERR == res)
    {
      GNUNET_break (0);
      return MHD_NO;
    }
    if ((GNUNET_NO == res ||
         (NULL == cc->json)))
    {
      return MHD_YES;
    }
  }

  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_fixed_auto ("encryption_key",
                                 &cc->encryption_key),
    GNUNET_JSON_spec_string ("auth_method",
                             &cc->challenge_method),
    GNUNET_JSON_spec_string ("auth_data",
                             &cc->challenge_data),
    GNUNET_JSON_spec_fixed_auto ("auth_nonce",
                                 &cc->challenge_salt),
    GNUNET_JSON_spec_fixed_auto ("message_hash",
                                 &cc->message_hash),
    GNUNET_JSON_spec_end ()
  };

  res = TALER_MHD_parse_json_data (connection,
                                   cc->json,
                                   spec);
  if (GNUNET_SYSERR == res)
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                       "Unable to parse request body");
  }
  if (GNUNET_NO == res)
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Unable to parse request body");
  }

  /* validate request id */
  {
    struct FROSIX_ChallengeRequestIdP req_id;
    FROSIX_compute_challenge_request_id (&req_id,
                                         &cc->encryption_key,
                                         &cc->message_hash);

    if (GNUNET_OK != FROST_hash_cmp (&req_id.id,
                                     &id->id))
    {
      GNUNET_JSON_parse_free (spec);
      GNUNET_break_op (0);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         "ID in URL not matching data in body");
    }
  }

  {
    /* hash encryption_key to get id from entry in db */
    struct FROST_HashCode enc_key_hash;
    FROSIX_hash_encryption_key (&enc_key_hash,
                                &cc->encryption_key);

    struct FROSIX_ChallengeHashP auth_hash_db;
    enum GNUNET_DB_QueryStatus qs;
    qs = db->get_auth_hash (db->cls,
                            &enc_key_hash,
                            &auth_hash_db);

    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
    case GNUNET_DB_STATUS_SOFT_ERROR:
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      GNUNET_break (0);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_DB_FETCH_FAILED,
                                         "auth_data_select");
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      break;
    }

    /* hash received auth data */
    struct FROSIX_ChallengeHashP auth_hash_req;
    FROSIX_compute_auth_hash (&auth_hash_req,
                              cc->challenge_data,
                              &cc->challenge_salt);

    /* compare auth hash from db with hash of received data*/
    if (GNUNET_OK != FROSIX_gnunet_hash_cmp (&auth_hash_req.hash,
                                             &auth_hash_db.hash))
    {
      GNUNET_break_op (0);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         "Got invalid data from DB");
    }

    /* compute challenge id */
    FROSIX_compute_challenge_id (&cc->challenge_id,
                                 &enc_key_hash,
                                 &cc->message_hash);
  }

  /* try to load authorization plugin */
  cc->authorization
    = FROSIX_authorization_plugin_load (cc->challenge_method,
                                        db,
                                        FH_cfg);

  if (NULL == cc->authorization)
  {
    MHD_RESULT ret;

    ret = TALER_MHD_reply_with_error (
      connection,
      MHD_HTTP_INTERNAL_SERVER_ERROR,
      TALER_EC_GENERIC_METHOD_INVALID,
      cc->challenge_method);
    return ret;
  }

  /* use plugin to check if challenge data is valid! */
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "No challenge provided, creating fresh challenge\n");
  {
    enum GNUNET_GenericReturnValue ret;

    ret = cc->authorization->validate (cc->authorization->cls,
                                       connection,
                                       cc->challenge_method,
                                       cc->challenge_data,
                                       strlen (cc->challenge_data));
    switch (ret)
    {
    case GNUNET_OK:
      /* data valid, continued below */
      break;
    case GNUNET_NO:
      /* data invalid, reply was queued */
      return MHD_YES;
    case GNUNET_SYSERR:
      /* data invalid, reply was NOT queued */
      return MHD_NO;
    }
  }

  /* Setup challenge and begin authorization process */
  {
    struct GNUNET_TIME_Timestamp transmission_date;
    enum GNUNET_DB_QueryStatus qs;

    qs = db->create_challenge_code (db->cls,
                                    &cc->challenge_id,
                                    cc->authorization->code_rotation_period,
                                    cc->authorization->code_validity_period,
                                    cc->authorization->retry_counter,
                                    &transmission_date,
                                    &cc->code);

    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
    case GNUNET_DB_STATUS_SOFT_ERROR:
      GNUNET_break (0);
      return TALER_MHD_reply_with_error (cc->connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_DB_FETCH_FAILED,
                                         "create_challenge_code");
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      /* 0 == retry_counter of existing challenge => rate limit exceeded */
      return reply_rate_limited (cc);
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      /* challenge code was stored successfully*/
      GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                  "Created fresh challenge\n");
      break;
    }

    if (GNUNET_TIME_relative_cmp (
          GNUNET_TIME_absolute_get_duration (
            transmission_date.abs_time),
          <,
          cc->authorization->code_retransmission_frequency) )
    {
      /* Too early for a retransmission! */
      return TALER_MHD_REPLY_JSON_PACK (
        cc->connection,
        MHD_HTTP_OK,
        GNUNET_JSON_pack_string ("challenge_type",
                                 "TAN_ALREADY_SENT"));
    }
  }

  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Beginning authorization process\n");
  cc->as = cc->authorization->start (cc->authorization->cls,
                                     &FH_trigger_daemon,
                                     NULL,
                                     &cc->challenge_id,
                                     cc->code,
                                     cc->challenge_data,
                                     strlen (cc->challenge_data));

  if (NULL == cc->as)
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (cc->connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_UNEXPECTED_REQUEST_ERROR,
                                       NULL);
  }

  return run_authorization_process (connection,
                                    cc);
}