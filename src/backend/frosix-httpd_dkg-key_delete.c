/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file backend/frosix-httpd_key.c
 * @brief functions to handle incoming requests on /key
 * @author Joel Urech
 */
#include "frosix-httpd_dkg.h"
#include "frosix-httpd.h"
#include "frosix-httpd_mhd.h"
#include "frosix_database_plugin.h"
#include <taler/taler_util.h>
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_db_lib.h>
#include "gnunet/gnunet_curl_lib.h"

struct KeyDeleteContext
{
  /**
   * Our handler context
  */
  struct TM_HandlerContext *hc;

  /**
   * Post parser context.
   */
  void *post_ctx;

  /**
   * Connection handle for closing or resuming
   */
  struct MHD_Connection *connection;

  /**
   * When should this request time out?
  */
  struct GNUNET_TIME_Absolute timeout;
};


MHD_RESULT
FH_handler_key_delete (
  struct MHD_Connection *connection,
  struct TM_HandlerContext *hc,
  const struct FROSIX_DkgRequestIdP *id)
{
  struct KeyDeleteContext *dc = hc->ctx;

  if (NULL == dc)
  {
    dc = GNUNET_new (struct KeyDeleteContext);
    dc->connection = connection;
    hc->ctx = dc;
  }

  enum GNUNET_DB_QueryStatus qs;
  qs = db->delete_key_data (db->cls,
                            &id->id);

  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
  case GNUNET_DB_STATUS_SOFT_ERROR:
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       "key_delete");
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    break;
  }

  struct FH_RequestHandler h204 = {
    "", NULL, "text/html",
    "<html><title>204: no content</title></html>", 0,
    &TMH_MHD_handler_static_response, MHD_HTTP_NO_CONTENT
  };

  return TMH_MHD_handler_static_response (&h204,
                                          connection);
}