/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file backend/frosix-httpd_get.c
 * @brief functions to handle incoming requests on /seed
 * @author Joel Urech
 */
#include "frosix-httpd_seed.h"
#include "frosix-httpd.h"
#include <sodium.h>

struct FROSIX_RndBytes
{
  uint8_t bytes[FROSIX_SEED_SIZE];
};

/**
 * @brief Wraps libsodium to get rnd bytes.
 * The amount of bytes is defined in the global FROSIX_SEED_SIZE
 *
 * @param[out] rnd_bytes Pointer to the struct to fill with rnd bytes.
*/
static void
get_rnd_bytes (struct FROSIX_RndBytes *rnd_bytes)
{
  randombytes_buf (rnd_bytes->bytes, FROSIX_SEED_SIZE);
}


MHD_RESULT
FH_seed_get (struct FH_RequestHandler *rh,
             struct MHD_Connection *connection)
{
  struct MHD_Response *resp;
  MHD_RESULT ret;
  struct FROSIX_RndBytes *rnd_bytes;

  rnd_bytes = calloc (1, FROSIX_SEED_SIZE);
  if (NULL == rnd_bytes)
    return MHD_NO;

  get_rnd_bytes (rnd_bytes);

  resp = MHD_create_response_from_buffer (FROSIX_SEED_SIZE,
                                          rnd_bytes,
                                          MHD_RESPMEM_MUST_FREE);

  TALER_MHD_add_global_headers (resp);
  ret = MHD_queue_response (connection,
                            MHD_HTTP_OK,
                            resp);
  MHD_destroy_response (resp);
  return ret;
}