/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file backend/frosix-httpd_dkg-key.c
 * @brief functions to handle incoming requests on /dkg-key
 * @author Joel Urech
 */
#include "frosix-httpd_dkg.h"
#include "frosix-httpd.h"
#include "frosix_database_plugin.h"
#include "frosix_service.h"
#include "keygen.h"
#include <taler/taler_util.h>
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_db_lib.h>


struct DkgKeyContext
{
  /**
   *
  */
  uint8_t provider_index;

  /**
   *
  */
  uint8_t threshold;

  /**
   *
  */
  uint8_t num_of_participants;

  /**
   *
  */
  struct FROSIX_DkgContextStringP context_string;

  /**
   *
  */
  struct FROSIX_ChallengeHashP auth_hash;

  /**
   *
  */
  struct FROSIX_EncryptionKey pre_enc_key;

  /**
   *
  */
  uint8_t expiration;


  /**
   *
  */
  struct FROSIX_DkgRequestIdP request_id;

  /**
   *
  */
  struct FROSIX_ProviderSaltP provider_salt;

  /**
   *
  */
  struct FROSIX_SecretProviderSaltP secret_provider_salt;

  /**
   *
  */
  struct GNUNET_CRYPTO_EddsaPrivateKey priv_sig_key;

  /**
   *
  */
  struct FROST_DkgShare *dkg_shares;

  /**
   * Our handler context
  */
  struct TM_HandlerContext *hc;

  /**
   * Uploaded JSON data, NULL if upload is not yet complete.
  */
  json_t *json;

  /**
   * Post parser context.
   */
  void *post_ctx;

  /**
   * Connection handle for closing or resuming
   */
  struct MHD_Connection *connection;

  /**
   * When should this request time out?
  */
  struct GNUNET_TIME_Absolute timeout;
};


static enum GNUNET_GenericReturnValue
derive_encryption_key (
  struct FROSIX_EncryptionKey *enc_key,
  const struct FROSIX_EncryptionKey *pre_enc_key,
  const struct FROST_PublicKey *pk,
  const struct FROSIX_ProviderSaltP *provider_salt,
  uint8_t provider_index)
{
  return GNUNET_CRYPTO_kdf (enc_key,
                            sizeof (*enc_key),
                            "FROSIX-KEY",
                            strlen ("FROSIX-KEY"),
                            pre_enc_key,
                            sizeof (*pre_enc_key),
                            pk,
                            sizeof (*pk),
                            provider_salt,
                            sizeof (*provider_salt),
                            &provider_index,
                            sizeof (provider_index),
                            NULL,
                            0);
}

static void
generate_key_pair (struct FROST_KeyPair *key_pair,
                   struct FROST_DkgShare other_shares[],
                   const struct FROST_DkgShare my_shares[],
                   const struct FROST_DkgCommitment dkg_commitments[],
                   uint8_t my_index,
                   uint8_t threshold,
                   uint8_t num_of_participants)
{
  memcpy (&other_shares[my_index - 1].share,
          &my_shares[my_index - 1].share,
          sizeof (my_shares[my_index - 1].share));

  FROST_keygen_finalize (key_pair,
                         my_index,
                         other_shares,
                         dkg_commitments,
                         num_of_participants);
}

static MHD_RESULT
store_key_data (struct DkgKeyContext *dc,
                struct FROST_HashCode *enc_key_hash,
                struct FROSIX_EncryptionKey *enc_key,
                struct FROST_KeyPair *key_pair)
{
  /* Copy key data together */
  struct FROSIX_KeyDataRaw raw_data;
  memcpy (&raw_data.bytes[0],
          &key_pair->my_sk,
          sizeof (key_pair->my_sk));

  memcpy (&raw_data.bytes[sizeof key_pair->my_sk],
          &key_pair->group_pk,
          sizeof (key_pair->group_pk));

  /* Generate nonce */
  struct FROSIX_EncryptionNonceP nonce;
  FROSIX_secretbox_nonce_randombytes (&nonce);

  /* Encrypt key data */
  struct FROSIX_KeyDataEncrypted ciphertext;
  FROSIX_secretbox_keydata (&ciphertext,
                            &raw_data,
                            &nonce,
                            enc_key);

  /* store key data */
  enum GNUNET_DB_QueryStatus qs;
  qs = db->store_key (db->cls,
                      enc_key_hash,
                      &nonce,
                      &ciphertext,
                      &dc->auth_hash,
                      dc->expiration,
                      dc->provider_index);
  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
  case GNUNET_DB_STATUS_SOFT_ERROR:
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (dc->connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_INVARIANT_FAILURE,
                                       "store key");
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    break;
  }
  return MHD_HTTP_OK;
}


MHD_RESULT
FH_handler_dkg_key_post (
  struct MHD_Connection *connection,
  struct TM_HandlerContext *hc,
  const struct FROSIX_DkgRequestIdP *dkg_id,
  const struct FROSIX_ProviderSaltP *provider_salt,
  const struct FROSIX_SecretProviderSaltP *secret_provider_salt,
  const struct GNUNET_CRYPTO_EddsaPrivateKey *priv_sig_key,
  const char *dkg_key_data,
  size_t *dkg_key_data_size)
{
  enum GNUNET_GenericReturnValue res;
  struct DkgKeyContext *dc = hc->ctx;
  json_t *json_ppk = NULL;
  json_t *shares = NULL;
  size_t share_n_comm_length;

  if (NULL == dc)
  {
    dc = GNUNET_new (struct DkgKeyContext);
    dc->connection = connection;
    dc->request_id = *dkg_id;
    dc->provider_salt = *provider_salt;
    dc->secret_provider_salt = *secret_provider_salt;
    dc->priv_sig_key = *priv_sig_key;
    hc->ctx = dc;
  }

  /* parse request body */
  if (NULL == dc->json)
  {
    res = TALER_MHD_parse_post_json (dc->connection,
                                     &dc->post_ctx,
                                     dkg_key_data,
                                     dkg_key_data_size,
                                     &dc->json);
    if (GNUNET_SYSERR == res)
    {
      GNUNET_break (0);
      return MHD_NO;
    }
    if ((GNUNET_NO == res ||
         (NULL == dc->json)))
    {
      return MHD_YES;
    }
  }

  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_uint8 ("provider_index",
                            &dc->provider_index),
    GNUNET_JSON_spec_uint8 ("threshold",
                            &dc->threshold),
    GNUNET_JSON_spec_fixed_auto ("context_string",
                                 &dc->context_string),
    GNUNET_JSON_spec_fixed_auto ("auth_hash",
                                 &dc->auth_hash),
    GNUNET_JSON_spec_json ("providers_public_keys",
                           &json_ppk),
    GNUNET_JSON_spec_fixed_auto ("pre_encryption_key",
                                 &dc->pre_enc_key),
    GNUNET_JSON_spec_json ("secret_shares",
                           &shares),
    GNUNET_JSON_spec_uint8 ("expiration",
                            &dc->expiration),
    GNUNET_JSON_spec_end ()
  };

  res = TALER_MHD_parse_json_data (connection,
                                   dc->json,
                                   spec);

  if (GNUNET_SYSERR == res)
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break (0);
    return MHD_NO;
  }
  if (GNUNET_NO == res)
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Unable to parse request body");
  }

  /* check number of submitted provider public keys*/
  if (254 < json_array_size (json_ppk))
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Too many provider public keys");
  }

  /* set number of participants */
  dc->num_of_participants = json_array_size (json_ppk);

  /* validate provider_index, threshold and num_of_participants */
  if (GNUNET_OK != FROST_validate_dkg_params (dc->provider_index,
                                              dc->threshold,
                                              dc->num_of_participants))
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Protocol parameters out of bound");
  }

  /* Check if called id is matching the send data */
  if (GNUNET_OK != FROSIX_dkg_validate_request_id_ (&dc->request_id,
                                                    &dc->context_string,
                                                    &dc->auth_hash,
                                                    &dc->provider_salt,
                                                    dc->provider_index,
                                                    dc->num_of_participants,
                                                    dc->threshold))
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Unable to validate request ID");
  }

  /* parse provider public keys */
  struct GNUNET_CRYPTO_EddsaPublicKey
    providers_public_keys[dc->num_of_participants];
  for (unsigned int i = 0; i < dc->num_of_participants; i++)
  {
    struct GNUNET_JSON_Specification ppk_spec[] = {
      GNUNET_JSON_spec_fixed_auto (NULL,
                                   &providers_public_keys[i]),
      GNUNET_JSON_spec_end ()
    };

    res = TALER_MHD_parse_json_array (connection,
                                      json_ppk,
                                      ppk_spec,
                                      i,
                                      -1);

    if (GNUNET_SYSERR == res)
    {
      GNUNET_JSON_parse_free (spec);
      GNUNET_JSON_parse_free (ppk_spec);
      GNUNET_break (0);
      return MHD_NO;
    }
    if (GNUNET_NO == res)
    {
      GNUNET_JSON_parse_free (spec);
      GNUNET_JSON_parse_free (ppk_spec);
      GNUNET_break_op (0);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         "Unable to parse request body");
    }

    GNUNET_JSON_parse_free (ppk_spec);
  }

  /* check if there are 'num_of_participants - 1' shares */
  if (dc->num_of_participants - 1 != json_array_size (shares))
  {
    GNUNET_break_op (0);
    GNUNET_JSON_parse_free (spec);

    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Number of shares not matching");
  }

  share_n_comm_length = dc->num_of_participants - 1;

  /* initialize shares and commitments */
  struct FROST_DkgShare dkg_shares[dc->num_of_participants];
  struct FROST_DkgCommitment dkg_commitments[dc->num_of_participants];

  {
    /* parse all shares */
    struct FROSIX_EncryptedShareP enc_shares[share_n_comm_length];
    struct GNUNET_CRYPTO_EcdhePublicKey pub_enc_keys[share_n_comm_length];

    unsigned int pi_counter = 0;
    for (int i = 0; i < share_n_comm_length; i++)
    {
      if (dc->provider_index - 1 == pi_counter)
        pi_counter++;

      struct GNUNET_JSON_Specification share_spec[] = {
        GNUNET_JSON_spec_uint8 ("provider_index",
                                &dkg_shares[pi_counter].identifier),
        GNUNET_JSON_spec_fixed_auto ("secret_share",
                                     &enc_shares[i]),
        GNUNET_JSON_spec_fixed_auto ("ephemeral_key",
                                     &pub_enc_keys[i]),
        GNUNET_JSON_spec_end ()
      };

      res = TALER_MHD_parse_json_array (connection,
                                        shares,
                                        share_spec,
                                        i,
                                        -1);

      if (GNUNET_SYSERR == res)
      {
        GNUNET_JSON_parse_free (spec);
        GNUNET_JSON_parse_free (share_spec);
        GNUNET_break (0);
        return MHD_NO;
      }
      if (GNUNET_NO == res)
      {
        GNUNET_JSON_parse_free (spec);
        GNUNET_JSON_parse_free (share_spec);
        GNUNET_break_op (0);
        return TALER_MHD_reply_with_error (connection,
                                           MHD_HTTP_BAD_REQUEST,
                                           TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                           "Unable to parse request body");
      }

      /* free json spec */
      GNUNET_JSON_parse_free (share_spec);

      pi_counter++;
    }

    /* free json spec */
    GNUNET_JSON_parse_free (spec);

    /* get commitments from db */
    {
      struct FROSIX_DkgCommitmentsRaw commits;
      commits.length = (sizeof (dc->provider_index)
                        + (sizeof (struct FROST_Point)
                           * dc->threshold))
                       * (share_n_comm_length);

      enum GNUNET_DB_QueryStatus qs;
      qs = db->get_dkg_commitment (db->cls,
                                   &dc->request_id,
                                   &commits);

      switch (qs)
      {
      case GNUNET_DB_STATUS_HARD_ERROR:
      case GNUNET_DB_STATUS_SOFT_ERROR:
      case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
        GNUNET_break (0);
        return TALER_MHD_reply_with_error (connection,
                                           MHD_HTTP_INTERNAL_SERVER_ERROR,
                                           TALER_EC_GENERIC_DB_FETCH_FAILED,
                                           "get_dkg_commitments");
      case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
        break;
      }

      // FIXME: check how many bytes we got from the db

      /* parse result to struct */
      for (int i = 0; i < dc->num_of_participants; i++)
      {
        if (GNUNET_OK != FROST_initialize_dkg_commitment (&dkg_commitments[i],
                                                          dc->provider_index,
                                                          dc->threshold))
        {
          FROST_free_dkg_commitment (dkg_commitments,
                                     dc->num_of_participants);
          GNUNET_break (0);
          return TALER_MHD_reply_with_error (connection,
                                             MHD_HTTP_INTERNAL_SERVER_ERROR,
                                             TALER_EC_GENERIC_DB_FETCH_FAILED,
                                             "Initialize DKG commitment");
        }
      }

      unsigned int offset = 0;
      unsigned int comm_counter = 0;
      for (uint8_t i = 0; i < share_n_comm_length; i++)
      {
        if (dc->provider_index - 1 == comm_counter)
          comm_counter++;

        memcpy (&dkg_commitments[comm_counter].identifier,
                (char *) commits.ptr_commits + offset,
                1);
        offset += 1;

        for (int j = 0; j < dc->threshold; j++)
        {
          memcpy (&dkg_commitments[comm_counter].share_comm[j].sc.xcoord,
                  (char *) commits.ptr_commits + offset,
                  sizeof (struct FROST_Point));
          offset += sizeof (struct FROST_Point);
        }

        dkg_commitments[comm_counter].shares_commitments_length = dc->threshold;

        comm_counter++;
      }
    }


    /* check if shares are valid */
    unsigned int share_counter = 0;
    for (int i = 0; i < share_n_comm_length; i++)
    {
      if (dc->provider_index - 1 == i)
        share_counter++;

      /* get key material */
      struct GNUNET_HashCode key_material;
      if (GNUNET_OK != GNUNET_CRYPTO_eddsa_ecdh (&dc->priv_sig_key,
                                                 &pub_enc_keys[i],
                                                 &key_material))
      {
        GNUNET_break_op (0);
        FROST_free_dkg_commitment (dkg_commitments,
                                   dc->num_of_participants);
        return TALER_MHD_reply_with_error (connection,
                                           MHD_HTTP_BAD_REQUEST,
                                           TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                           "Unable to compute encryption key");
      }

      /* derive key */
      struct GNUNET_CRYPTO_SymmetricSessionKey sym_key;
      struct GNUNET_CRYPTO_SymmetricInitializationVector iv;
      GNUNET_CRYPTO_hash_to_aes_key (&key_material,
                                     &sym_key,
                                     &iv);

      /* decrypt share */
      ssize_t len = GNUNET_CRYPTO_symmetric_decrypt (&enc_shares[i],
                                                     sizeof (enc_shares[i]),
                                                     &sym_key,
                                                     &iv,
                                                     &dkg_shares[share_counter].
                                                     share);

      if ( -1 == len || sizeof (dkg_shares[share_counter].share) != len)
      {
        GNUNET_break_op (0);
        FROST_free_dkg_commitment (dkg_commitments,
                                   dc->num_of_participants);
        return TALER_MHD_reply_with_error (connection,
                                           MHD_HTTP_BAD_REQUEST,
                                           TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                           "Unable to decrypt encrypted share");
      }

      if (GNUNET_OK != FROST_keygen_validate_share (
            &dkg_commitments[share_counter],
            &dkg_shares[share_counter],
            dc->provider_index))
      {
        GNUNET_break_op (0);
        FROST_free_dkg_commitment (dkg_commitments,
                                   dc->num_of_participants);
        return TALER_MHD_reply_with_error (connection,
                                           MHD_HTTP_BAD_REQUEST,
                                           TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                           "Unable to validate share");
      }
      share_counter++;
    }
  }

  /* derive the final context string */
  struct FROST_DkgContextString cs_h;
  if (GNUNET_YES != FROSIX_dkg_derive_context_string_ (
        &cs_h,
        dc->provider_index,
        dc->threshold,
        &dc->context_string,
        &dc->auth_hash,
        providers_public_keys,
        dc->num_of_participants,
        &dc->secret_provider_salt))
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                       "Derive context string");
  }

  /* calculate own commitment and share */
  if (GNUNET_OK != FROSIX_dkg_commitment_generate_ (
        &dkg_commitments[dc->provider_index - 1],
        &cs_h,
        NULL,
        dc->provider_index,
        dc->threshold,
        dc->num_of_participants))
  {
    FROST_free_dkg_commitment (dkg_commitments,
                               dc->num_of_participants);
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                       "Generate DKG commitment");
  }

  struct FROST_DkgShare my_shares[dc->num_of_participants];
  if (GNUNET_OK != FROSIX_dkg_shares_generate_ (
        my_shares,
        &cs_h,
        NULL,
        dc->provider_index,
        dc->threshold,
        dc->num_of_participants))
  {
    FROST_free_dkg_commitment (dkg_commitments,
                               dc->num_of_participants);
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                       "Generate DKG shares");
  }

  /* calculate key share */
  struct FROST_KeyPair key_pair;
  generate_key_pair (&key_pair,
                     dkg_shares,
                     my_shares,
                     dkg_commitments,
                     dc->provider_index,
                     dc->threshold,
                     dc->num_of_participants);

  /* free initialized memory */
  FROST_free_dkg_commitment (dkg_commitments,
                             dc->num_of_participants);

  /* Derive symmetric key */
  struct FROSIX_EncryptionKey encryption_key;
  if (GNUNET_OK != derive_encryption_key (&encryption_key,
                                          &dc->pre_enc_key,
                                          &key_pair.group_pk,
                                          &dc->provider_salt,
                                          dc->provider_index))
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                       "Derive encryption key");
  }

  /* check if key is already stored */
  /* hash encryption key */
  struct FROST_HashCode enc_key_hash;
  FROSIX_hash_encryption_key (&enc_key_hash,
                              &encryption_key);

  enum GNUNET_DB_QueryStatus qs;
  qs = db->lookup_key (db->cls,
                       &enc_key_hash);

  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
  case GNUNET_DB_STATUS_SOFT_ERROR:
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_INVARIANT_FAILURE,
                                       "lookup key");
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    /* store key data */
    MHD_RESULT mhd_res;
    mhd_res = store_key_data (dc,
                              &enc_key_hash,
                              &encryption_key,
                              &key_pair);
    if (MHD_HTTP_OK != mhd_res)
    {
      return mhd_res;
    }
    break;
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    // key is already stored - go ahead
    break;
  }

  // Sign data
  struct FROSIX_DkgKeySignaturePS ks = {
    .purpose.purpose = htonl (104),
    .purpose.size = htonl (sizeof (ks)),
    .public_key = key_pair.group_pk,
    .auth_hash = dc->auth_hash,
  };

  struct GNUNET_CRYPTO_EddsaSignature sig;
  GNUNET_CRYPTO_eddsa_sign (priv_sig_key,
                            &ks,
                            &sig);

  return TALER_MHD_REPLY_JSON_PACK (
    connection,
    MHD_HTTP_OK,
    GNUNET_JSON_pack_data_auto ("public_key",
                                &key_pair.group_pk),
    GNUNET_JSON_pack_data_auto ("signature",
                                &sig));
}