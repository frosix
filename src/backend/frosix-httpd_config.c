/*
  This file is part of Frosix
  Copyright (C) 2020, 2021 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file backend/frosix-httpd_config.c
 * @brief headers for /terms handler
 * @author Christian Grothoff
 * @author Dennis Neufeld
 * @author Dominik Meister
 */
#include "platform.h"
#include <jansson.h>
#include "frosix-httpd_config.h"
#include "frosix-httpd.h"
#include "frosix_authorization_plugin.h"
#include "frosix_authorization_lib.h"
#include <taler/taler_json_lib.h>


/**
 * Add enabled methods and their fees to the ``/config`` response.
 *
 * @param[in,out] cls a `json_t` array to build
 * @param section configuration section to inspect
 */
static void
add_methods (void *cls,
             const char *section)
{
  json_t *method_arr = cls;
  struct FROSIX_AuthorizationPlugin *p;
  json_t *method;

  if (0 != strncasecmp (section,
                        "authorization-",
                        strlen ("authorization-")))
    return;
  if (GNUNET_YES !=
      GNUNET_CONFIGURATION_get_value_yesno (FH_cfg,
                                            section,
                                            "ENABLED"))
    return;
  section += strlen ("authorization-");
  p = FROSIX_authorization_plugin_load (section,
                                        db,
                                        FH_cfg);
  if (NULL == p)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Failed to load authorization plugin `%s'\n",
                section);
    return;
  }
  method = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_string ("type",
                             section),
    TALER_JSON_pack_amount ("cost",
                            &p->cost));
  GNUNET_assert (
    0 ==
    json_array_append_new (method_arr,
                           method));
}


MHD_RESULT
FH_handler_config (struct FH_RequestHandler *rh,
                   struct MHD_Connection *connection)
{
  json_t *method_arr = json_array ();

  GNUNET_assert (NULL != method_arr);
  {
    json_t *method;

    method = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_string ("type",
                               "question"),
      TALER_JSON_pack_amount ("cost",
                              &FH_question_cost));
    GNUNET_assert (
      0 ==
      json_array_append_new (method_arr,
                             method));
  }
  GNUNET_CONFIGURATION_iterate_sections (FH_cfg,
                                         &add_methods,
                                         method_arr);
  return TALER_MHD_REPLY_JSON_PACK (
    connection,
    MHD_HTTP_OK,
    GNUNET_JSON_pack_string ("name",
                             "frosix"),
    GNUNET_JSON_pack_string ("version",
                             "0:0:0"),
    GNUNET_JSON_pack_string ("business_name",
                             FH_business_name),
    GNUNET_JSON_pack_array_steal ("methods",
                                  method_arr),
    TALER_JSON_pack_amount ("annual_fee",
                            &FH_annual_fee),
    TALER_JSON_pack_amount ("signature_creation_fee",
                            &FH_signature_creation_fee),
    GNUNET_JSON_pack_data_auto ("provider_salt",
                                &FH_provider_salt),
    GNUNET_JSON_pack_data_auto ("public_key",
                                &FH_pub_sig_key));
}


/* end of frosix-httpd_config.c */
