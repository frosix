/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file backend/frosix-httpd_sign-commitment.c
 * @brief functions to handle incoming requests on /sign-commitment
 * @author Joel Urech
 */
#include "frosix-httpd_sig.h"
#include "frosix-httpd.h"
#include "frosix_database_plugin.h"
#include "frost_high.h"
#include "frost_low.h"
#include <taler/taler_util.h>
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_db_lib.h>
#include "frosix_authorization_plugin.h"

/**
 * What is the maximum frequency at which we allow
 * clients to attempt to answer security questions?
 */
#define MAX_QUESTION_FREQ GNUNET_TIME_relative_multiply ( \
    GNUNET_TIME_UNIT_SECONDS, 30)

/**
 * How many retries do we allow per code?
 */
#define INITIAL_RETRY_COUNTER 3

struct SignCommitmentContext
{
  /**
   *
  */
  uint32_t identifier;

  /**
   *
  */
  struct FROST_HashCode enc_key_hash;

  /**
   *
  */
  struct FROST_MessageHash message_hash;

  /**
   *
  */
  const char *auth_method;

  /**
   *
  */
  struct GNUNET_HashCode auth_data;

  /**
   *
  */
  bool auth_data_parsed;

  /**
   *
  */
  struct GNUNET_CRYPTO_Edx25519Signature auth_sig;

  /**
   *
  */
  bool auth_sig_parsed;

  /**
   *
  */
  struct GNUNET_CRYPTO_Edx25519PublicKey auth_pub;

  /**
   *
  */
  bool auth_pub_parsed;

  /**
   *
  */
  struct FROSIX_SigRequestIdP id;

  /**
   * Reference to the authorization plugin which was loaded
  */
  struct FROSIX_AuthorizationPlugin *authorization;


  /**
   * Our handler context
  */
  struct TM_HandlerContext *hc;

  /**
   * Uploaded JSON data, NULL if upload is not yet complete.
  */
  json_t *json;

  /**
   * Post parser context.
   */
  void *post_ctx;

  /**
   * Connection handle for closing or resuming
   */
  struct MHD_Connection *connection;
};


static MHD_RESULT
return_sig_commitment (struct SignCommitmentContext *dc,
                       struct MHD_Connection *connection)
{
  /* generate random seed */
  struct FROST_CommitmentSeed seed;
  FROST_get_random_seed (&seed);

  /* compute commitments */
  struct FROST_Nonce com_nonce;
  struct FROST_Commitment commitment;
  FROST_generate_nonce_and_commitment (&com_nonce,
                                       &commitment,
                                       &dc->message_hash,
                                       &seed);

  /* compute commitment_id for storing in db */
  struct GNUNET_HashCode db_id;
  FROSIX_compute_db_commitment_id (&db_id,
                                   &dc->enc_key_hash,
                                   &commitment);

  /* store seed in db */
  enum GNUNET_DB_QueryStatus qs;
  qs = db->store_commitment_seed (db->cls,
                                  &db_id,
                                  &seed);

  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
  case GNUNET_DB_STATUS_SOFT_ERROR:
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (dc->connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_INVARIANT_FAILURE,
                                       "store commitment seed");
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    break;
  }

  /* parse commitments to json */
  return TALER_MHD_REPLY_JSON_PACK (
    connection,
    MHD_HTTP_OK,
    GNUNET_JSON_pack_data_auto ("hiding_commitment",
                                &commitment.hiding_commitment),
    GNUNET_JSON_pack_data_auto ("binding_commitment",
                                &commitment.binding_commitment));
}


/**
 * Generate a response telling the client that answering this
 * challenge failed because the rate limit has been exceeded.
 *
 * @param gc request to answer for
 * @return MHD status code
 */
static MHD_RESULT
reply_rate_limited (const struct SignCommitmentContext *gc)
{
  if (NULL != gc->authorization)
    return TALER_MHD_REPLY_JSON_PACK (
      gc->connection,
      MHD_HTTP_TOO_MANY_REQUESTS,
      TALER_MHD_PACK_EC (TALER_EC_GENERIC_TIMEOUT),
      GNUNET_JSON_pack_uint64 ("request_limit",
                               gc->authorization->retry_counter),
      GNUNET_JSON_pack_time_rel ("request_frequency",
                                 gc->authorization->code_rotation_period));
  /* must be security question */
  return TALER_MHD_REPLY_JSON_PACK (
    gc->connection,
    MHD_HTTP_TOO_MANY_REQUESTS,
    TALER_MHD_PACK_EC (TALER_EC_GENERIC_TIMEOUT),
    GNUNET_JSON_pack_uint64 ("request_limit",
                             INITIAL_RETRY_COUNTER),
    GNUNET_JSON_pack_time_rel ("request_frequency",
                               MAX_QUESTION_FREQ));
}


/**
 * Use the database to rate-limit queries to the authentication
 * procedure, but without actually storing 'real' challenge codes.
 *
 * @param[in,out] gc context to rate limit requests for
 * @return #GNUNET_OK if rate-limiting passes,
 *         #GNUNET_NO if a reply was sent (rate limited)
 *         #GNUNET_SYSERR if we failed and no reply
 *                        was queued
 */
static enum GNUNET_GenericReturnValue
rate_limit (struct SignCommitmentContext *gc)
{
  enum GNUNET_DB_QueryStatus qs;
  struct GNUNET_TIME_Timestamp rt;
  uint64_t code;
  enum FROSIX_DB_CodeStatus cs;
  bool satisfied;
  uint64_t dummy;

  struct FROSIX_ChallengeIdP challenge_id;
  GNUNET_CRYPTO_hash (&gc->id,
                      sizeof (gc->id),
                      &challenge_id.hash);

  rt = GNUNET_TIME_UNIT_FOREVER_TS;
  qs = db->create_challenge_code (db->cls,
                                  &challenge_id,
                                  MAX_QUESTION_FREQ,
                                  GNUNET_TIME_UNIT_HOURS,
                                  INITIAL_RETRY_COUNTER,
                                  &rt,
                                  &code);
  if (0 > qs)
  {
    GNUNET_break (0 < qs);
    return (MHD_YES ==
            TALER_MHD_reply_with_error (gc->connection,
                                        MHD_HTTP_INTERNAL_SERVER_ERROR,
                                        TALER_EC_GENERIC_DB_FETCH_FAILED,
                                        "create_challenge_code (for rate limiting)"))
           ? GNUNET_NO
           : GNUNET_SYSERR;
  }
  if (GNUNET_DB_STATUS_SUCCESS_NO_RESULTS == qs)
  {
    return (MHD_YES ==
            reply_rate_limited (gc))
           ? GNUNET_NO
           : GNUNET_SYSERR;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Using intentionally wrong answer to produce rate-limiting\n");
  /* decrement trial counter */
  cs = db->verify_challenge_code (db->cls,
                                  &challenge_id,
                                  &challenge_id.hash, /* always use wrong answer
                                                         */
                                  &dummy,
                                  &satisfied);
  switch (cs)
  {
  case FROSIX_DB_CODE_STATUS_CHALLENGE_CODE_MISMATCH:
    /* good, what we wanted */
    return GNUNET_OK;
  case FROSIX_DB_CODE_STATUS_HARD_ERROR:
  case FROSIX_DB_CODE_STATUS_SOFT_ERROR:
    GNUNET_break (0);
    return (MHD_YES ==
            TALER_MHD_reply_with_error (gc->connection,
                                        MHD_HTTP_INTERNAL_SERVER_ERROR,
                                        TALER_EC_GENERIC_DB_FETCH_FAILED,
                                        "verify_challenge_code"))
           ? GNUNET_NO
           : GNUNET_SYSERR;
  case FROSIX_DB_CODE_STATUS_NO_RESULTS:
    return (MHD_YES ==
            reply_rate_limited (gc))
           ? GNUNET_NO
           : GNUNET_SYSERR;
  case FROSIX_DB_CODE_STATUS_VALID_CODE_STORED:
    /* this should be impossible, we used code+1 */
    GNUNET_assert (0);
  }
  return GNUNET_SYSERR;
}


/**
 * Handle special case of a security question where we do not
 * generate a code. Rate limits answers against brute forcing.
 *
 * @param[in,out] gc request to handle
 * @param decrypted_truth hash to check against
 * @param decrypted_truth_size number of bytes in @a decrypted_truth
 * @return MHD status code
 */
static MHD_RESULT
handle_security_question (struct SignCommitmentContext *gc)
{
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Handling security question challenge\n");
  /* rate limit */
  /*{
    enum GNUNET_GenericReturnValue ret;

    ret = rate_limit (gc);
    if (GNUNET_OK != ret)
      return (GNUNET_NO == ret) ? MHD_YES : MHD_NO;
  }*/
  /* check reply matches truth */
  /* get auth data from DB */
  struct FROSIX_ChallengeHashP auth_hash;

  enum GNUNET_DB_QueryStatus qs;
  qs = db->get_auth_hash (db->cls,
                          &gc->enc_key_hash,
                          &auth_hash);
  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
  case GNUNET_DB_STATUS_SOFT_ERROR:
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    GNUNET_break (0);
    /* FROSIX_EC_KEY_NOT_FOUND */
    return TALER_MHD_reply_with_error (gc->connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       "auth_data_select");
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    break;
  }

  /* check if hash of submitted public key matches our stored hash */
  struct GNUNET_HashCode db_auth_answer;
  struct GNUNET_HashContext *hc = GNUNET_CRYPTO_hash_context_start ();
  GNUNET_CRYPTO_hash_context_read (hc,
                                   &gc->auth_pub,
                                   sizeof (gc->auth_pub));
  GNUNET_CRYPTO_hash_context_read (hc,
                                   "FROSIX",
                                   strlen ("FROSIX"));
  GNUNET_CRYPTO_hash_context_finish (hc,
                                     &db_auth_answer);

  if (GNUNET_OK != FROSIX_gnunet_hash_cmp (&db_auth_answer,
                                           &auth_hash.hash))
  {
    GNUNET_break (0);
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Authentication public key not matching\n");
    return TALER_MHD_reply_with_error (gc->connection,
                                       MHD_HTTP_FORBIDDEN,
                                       TALER_EC_GENERIC_INVALID_RESPONSE,
                                       NULL);
  }

  /* check signature of message hash with submitted public key */
  struct FROSIX_AuthSignaturePS as = {
    .purpose.purpose = htonl (72),
    .purpose.size = htonl (sizeof (as)),
    .mh = gc->message_hash,
  };

  if (GNUNET_OK != GNUNET_CRYPTO_edx25519_verify (72,
                                                  &as,
                                                  &gc->auth_sig,
                                                  &gc->auth_pub))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Signature of message not verified\n");
    return TALER_MHD_reply_with_error (gc->connection,
                                       MHD_HTTP_FORBIDDEN,
                                       TALER_EC_GENERIC_INVALID_RESPONSE,
                                       NULL);
  }

  /* good, return the key share */
  return return_sig_commitment (gc,
                                gc->connection);
}


MHD_RESULT
FH_handler_sig_commitment_post (
  struct MHD_Connection *connection,
  struct TM_HandlerContext *hc,
  const struct FROSIX_SigRequestIdP *id,
  const char *sign_commitment_data,
  size_t *sign_commitment_data_size)
{
  enum GNUNET_GenericReturnValue res;
  struct SignCommitmentContext *dc = hc->ctx;

  if (NULL == dc)
  {
    dc = GNUNET_new (struct SignCommitmentContext);
    dc->connection = connection;
    dc->id = *id;
    hc->ctx = dc;
  }

  /* parse request body */
  if (NULL == dc->json)
  {
    res = TALER_MHD_parse_post_json (dc->connection,
                                     &dc->post_ctx,
                                     sign_commitment_data,
                                     sign_commitment_data_size,
                                     &dc->json);
    if (GNUNET_SYSERR == res)
    {
      GNUNET_break (0);
      return MHD_NO;
    }
    if ((GNUNET_NO == res ||
         (NULL == dc->json)))
    {
      return MHD_YES;
    }
  }
  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_fixed_auto ("message_hash",
                                 &dc->message_hash),
    GNUNET_JSON_spec_fixed_auto ("encryption_key_hash",
                                 &dc->enc_key_hash),
    GNUNET_JSON_spec_string ("auth_method",
                             &dc->auth_method),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_fixed_auto ("auth_data",
                                   &dc->auth_data),
      &dc->auth_data_parsed),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_fixed_auto ("auth_sig",
                                   &dc->auth_sig),
      &dc->auth_sig_parsed),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_fixed_auto ("auth_pub",
                                   &dc->auth_pub),
      &dc->auth_pub_parsed),
    GNUNET_JSON_spec_end ()
  };

  res = TALER_MHD_parse_json_data (connection,
                                   dc->json,
                                   spec);

  if (GNUNET_SYSERR == res)
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break (0);
    return MHD_NO;
  }
  if (GNUNET_NO == res)
  {
    GNUNET_JSON_parse_free (spec);
    GNUNET_break_op (0);
    /* FROSIX_EC_PARAMETER_MALFORMED */
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "Unable to parse request body");
  }

  GNUNET_JSON_parse_free (spec);

  /* validate id */
  {
    struct FROSIX_SigRequestIdP req_id;
    FROSIX_compute_signature_request_id (&req_id,
                                         &dc->enc_key_hash,
                                         &dc->message_hash);
    if (GNUNET_NO == FROST_hash_cmp (&req_id.id,
                                     &id->id))
    {
      GNUNET_JSON_parse_free (spec);
      /* FROSIX_EC_REQUEST_ID_NOT_MATCHING */
      GNUNET_break_op (0);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         "ID in URL not matching data in body");
    }
  }

  /* authenticate */
  if (0 == strcmp ("question",
                   dc->auth_method))
  {
    if (&dc->auth_sig_parsed && &dc->auth_pub_parsed)
    {
      return handle_security_question (dc);
    }
    else
    {
      GNUNET_break (0);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         NULL);
    }
  }
  else
  {
    if (! &dc->auth_data_parsed)
    {
      GNUNET_break (0);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         NULL);
    }

    enum FROSIX_DB_CodeStatus cs;
    bool satisfied = false;
    uint64_t code;

    struct FROSIX_ChallengeIdP challenge_id;
    FROSIX_compute_challenge_id (&challenge_id,
                                 &dc->enc_key_hash,
                                 &dc->message_hash);

    /* random code, check against database */
    cs = db->verify_challenge_code (db->cls,
                                    &challenge_id,
                                    &dc->auth_data,
                                    &code,
                                    &satisfied);
    switch (cs)
    {
    case FROSIX_DB_CODE_STATUS_CHALLENGE_CODE_MISMATCH:
      GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                  "Provided response does not match our stored challenge\n");
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_FORBIDDEN,
                                         TALER_EC_GENERIC_DB_FETCH_FAILED,
                                         NULL);
    case FROSIX_DB_CODE_STATUS_HARD_ERROR:
    case FROSIX_DB_CODE_STATUS_SOFT_ERROR:
      GNUNET_break (0);
      return TALER_MHD_reply_with_error (dc->connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_DB_FETCH_FAILED,
                                         "verify_challenge_code");
    case FROSIX_DB_CODE_STATUS_NO_RESULTS:
      GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                  "Specified challenge code %s was not issued\n",
                  GNUNET_h2s (&dc->auth_data));
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_FORBIDDEN,
                                         TALER_EC_GENERIC_DB_FETCH_FAILED,
                                         "specific challenge code was not issued");
    case FROSIX_DB_CODE_STATUS_VALID_CODE_STORED:
      return return_sig_commitment (dc,
                                    connection);
    default:
      GNUNET_break (0);
      return MHD_NO;
    }
  }
}