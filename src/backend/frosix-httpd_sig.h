/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file backend/frosix-httpd_sign.h
 * @brief functions to handle incoming requests on /sign-*
 * @author Joel Urech
 */
#ifndef FROSIX_HTTPD_SIGN_H
#define FROSIX_HTTPD_SIGN_H
#include "frosix-httpd.h"
#include "keygen.h"
#include "frosix_util_lib.h"
#include "frosix_crypto.h"
#include <microhttpd.h>
#include <taler/taler_mhd_lib.h>

MHD_RESULT
FH_handler_sig_commitment_post (
  struct MHD_Connection *connection,
  struct TM_HandlerContext *hc,
  const struct FROSIX_SigRequestIdP *id,
  const char *sign_commitment_data,
  size_t *sign_commitment_data_size);


MHD_RESULT
FH_handler_sig_share_post (
  struct MHD_Connection *connection,
  struct TM_HandlerContext *hc,
  const struct FROSIX_SigRequestIdP *id,
  const char *sign_share_data,
  size_t *sign_share_data_size);


/**
 * FIXME
*/
void
FROSIX_raw_key_to_struct (struct FROST_KeyPair *key_pair,
                          const struct FROSIX_KeyDataRaw *decrypted);


/**
 * FIXME
*/
void
FROSIX_compute_db_commitment_id (struct GNUNET_HashCode *db_id,
                                 const struct FROST_HashCode *enc_key_hash,
                                 const struct FROST_Commitment *commitment);

#endif