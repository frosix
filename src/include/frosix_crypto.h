/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Frosix; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file include/frosix_crypto.h
 * @brief frosix crypto functions
 * @author Joel Urech
 */
#ifndef FROSIX_CRYPTO_H
#define FROSIX_CRYPTO_H

#include "frost_low.h"
#include "frost_high.h"
#include <sodium.h>
#include <taler/taler_json_lib.h>
#include <gnunet/gnunet_util_lib.h>
#include <stdbool.h>


/*** Frosix Crypto ***/

/**
 * This encryption key is used to encrypt and decrypt the key data stored
 * in the provider's database. The encryption key itself is not stored in
 * the database and is only saved in the `Frosix Sign Document`.
*/
struct FROSIX_EncryptionKey
{
  unsigned char bytes[crypto_secretbox_KEYBYTES];
};


/**
 * Random nonce for the encryption of the key data stored in the provider's
 * database. This nonce is also stored in the database.
*/
struct FROSIX_EncryptionNonceP
{
  unsigned char bytes[crypto_secretbox_NONCEBYTES];
};


/**
 * Used in Frosix DKG to transmit the secret shares from all providers
 * to all providers, without letting the client know what the secret share is.
*/
struct FROSIX_EncryptedShareP
{
  unsigned char bytes[FROST_SCALAR_BYTES];
};


/**
 * An ID which is used to identify a challenge.
 */
struct FROSIX_ChallengeIdP
{
  struct GNUNET_HashCode hash;
};


/**
 * High entropy context string, used in Frosix DKG as seed
 * to derive the commitment.
*/
struct FROSIX_DkgContextStringP
{
  struct FROST_HashCode hash; // FIXME
};


/**
 * Hashed authentication data, stored in the provider's database.
 * Used to verify the submitted challenge data in order to issue
 * a challenge code.
*/
struct FROSIX_ChallengeHashP
{
  struct GNUNET_HashCode hash;
};


/**
 * Specifies a salt value provided by an Frosix provider as result
 * of an /config request, used for deriving the encryption key,
 * the challenge nonce and is part of the Frosix DKG request id.
 */
struct FROSIX_ProviderSaltP
{
  struct GNUNET_CRYPTO_PowSalt salt;
};


/**
 * Specifies a nonce, used for salting the hash of the authentication data.
 */
struct FROSIX_ChallengeNonce
{
  struct GNUNET_HashCode hash;
};


/**
 * Secret long term source of high entropy of every provider, which is included
 * before deriving the commitment in Frosix DKG.
*/
struct FROSIX_SecretProviderSaltP
{
  struct GNUNET_CRYPTO_PowSalt salt;
};


/**
 * Provides a struct to sign the result of a Frosix DKG with the providers
 * secret signing key.
*/
struct FROSIX_DkgKeySignaturePS
{
  /**
   * Should contain a GNUNET_SIGNATURE_PURPOSE_XXX,
   * has `htonl (104)` at the moment.
  */
  struct GNUNET_CRYPTO_EccSignaturePurpose purpose;

  /**
   * The signature is over the resulting public key and...
  */
  struct FROST_PublicKey public_key;

  /**
   * ... the submitted salted authentication hash.
  */
  struct FROSIX_ChallengeHashP auth_hash;
};

/**
 * Provides a struct to sign the message with from the security question
 * derived secret signing key.
*/
struct FROSIX_AuthSignaturePS
{
  /**
   * Should contain a GNUNET_SIGNATURE_PURPOSE_XXX,
   * has `htonl (104)` at the moment.
  */
  struct GNUNET_CRYPTO_EccSignaturePurpose purpose;

  /**
   * The signature is over the hash of the message
  */
  struct FROST_MessageHash mh;
};


/*** Frosix DKG ***/

/**
 * Deterministically derived identifier of a HTTP POST request in Frosix DKG,
 * transmitted in the URL. The provider has to prove this id against the
 * submitted data in the POST request body.
*/
struct FROSIX_DkgRequestIdP
{
  struct FROST_HashCode id; // FIXME
};


/*** Frosix Sign ***/

/**
 * Deterministically derived identifier of a HTTP POST request in FROSIX Sign,
 * transmitted in the URL. The provider has to prove this id against the
 * submitted data in the POST request body.
*/
struct FROSIX_SigRequestIdP
{
  struct FROST_HashCode id; // FIXME
};


/*** Frosix Challenge ***/

/**
 * Deterministically derived identifier of a HTTP POST request in
 * FROSIX Challenge, transmitted in the URL. The provider has to prove this id
 * against the submitted data in the POST request body.
*/
struct FROSIX_ChallengeRequestIdP
{
  struct FROST_HashCode id; // FIXME
};


/*** Frosix Database ***/

/**
 * Just a pointer and the number of bytes at the pointers target.
 * Is used in Frosix DKG, while storing all other provider's commitment,
 * to copy all commitments at one place in memory.
 * This struct is also used to load the commitments from the database and copy
 * each commitment back in a useful struct.
*/
struct FROSIX_DkgCommitmentsRaw
{
  void *ptr_commits;
  size_t length;
};


/**
 * Byte array including the secret and the public key, before an encryption
 * or right after a decryption.
*/
struct FROSIX_KeyDataRaw
{
  unsigned char bytes[FROST_POINT_BYTES + FROST_SCALAR_BYTES];
};


/**
 * Byte array for the encrypted key data before storing in the database
 * or to load from the database.
*/
struct FROSIX_KeyDataEncrypted
{
  unsigned char bytes[crypto_secretbox_MACBYTES
                      + sizeof (struct FROSIX_KeyDataRaw)];
};


/*** Frosix Payment - not implemented yet ***/

/**
 * Random identifier used to later charge a payment.
 */
struct FROSIX_PaymentSecretP
{
  uint32_t id[8];
};


/**
 * Encrypts the key data in a FROSIX DKG to store them
 * in the providers database.
 *
 * @param[out] ciphertext The encrypted data
 * @param[in] plaintext Plaintext data to encrypt
 * @param[in] nonce Nonce value to use in the symmetric encryption
 * @param[in] key The encryption key
 * @return #GNUNET_OK if the encryption was successful
 *         #GNUNET_NO if there was a problem while encrypting
*/
enum GNUNET_GenericReturnValue
FROSIX_secretbox_keydata (struct FROSIX_KeyDataEncrypted *ciphertext,
                          const struct FROSIX_KeyDataRaw *plaintext,
                          const struct FROSIX_EncryptionNonceP *nonce,
                          const struct FROSIX_EncryptionKey *key);


/**
 * Decrypts encrypted key data from the providers database in Frosix Sign.
 *
 * @param[out] plaintext The resulting plaintext
 * @param[in] ciphertext Encrypted data to decrypt
 * @param[in] nonce Nonce value used in the symmetric encryption
 * @param[in] key Key which was used to encrypt the data in @e ciphertext
 * @return #GNUNET_OK if the decryption was successful
 *         #GNUNET_NO otherwise
*/
enum GNUNET_GenericReturnValue
FROSIX_secretbox_keydata_open (
  struct FROSIX_KeyDataRaw *plaintext,
  const struct FROSIX_KeyDataEncrypted *ciphertext,
  const struct FROSIX_EncryptionNonceP *nonce,
  const struct FROSIX_EncryptionKey *key);


/**
 * Returns a high entropy nonce to be used in FROSIX_secretbox_keydata.
 *
 * @param[out] nonce The resulting nonce value
*/
void
FROSIX_secretbox_nonce_randombytes (struct FROSIX_EncryptionNonceP *nonce);


/**
 * Returns the hash of an encryption key. This hash is used as identifier
 * in the providers databases.
 *
 * @param[out] enc_key_hash Hash of the encryption key
 * @param[in] enc_key Encryption key to hash
*/
void
FROSIX_hash_encryption_key (struct FROST_HashCode *enc_key_hash, // FIXME
                            const struct FROSIX_EncryptionKey *enc_key);


/**
 * Returns the authentication hash, derived from the submitted
 * authentication data and the authentication nonce.
 *
 * @param[out] auth_hash Resulting authentication hash
 * @param[in] auth_data Null-terminated string, containing
 * the authentication data
 * @param[in] auth_nonce Nonce value which should be used to salt
 * the resulting hash
*/
void
FROSIX_compute_auth_hash (struct FROSIX_ChallengeHashP *auth_hash,
                          const char *auth_data,
                          const struct GNUNET_HashCode *auth_nonce);


/**
 * Computes the id of a POST request to /auth-challenge.
 *
 * @param[out] request_id The resulting request id
 * @param[in] enc_key Encryption key -> FIXME Should be hash of enc key
 * @param[in] message_hash Hash of message or plaintext message -> FIXME
*/
void
FROSIX_compute_challenge_request_id (
  struct FROSIX_ChallengeRequestIdP *request_id,
  const struct FROSIX_EncryptionKey *enc_key,
  const struct FROST_MessageHash *message_hash);


/**
 * Computes the id of a POST request to /sig-commitment and /sig-share.
 *
 * @param[out] request_id The resulting request id
 * @param[in] enc_key_hash Hash of the encryption key, which was used to$
 * encrypt the data stored at the provider
 * @param[in] message_hash Hash of message or plaintext message -> FIXME
*/
void
FROSIX_compute_signature_request_id (
  struct FROSIX_SigRequestIdP *request_id,
  const struct FROST_HashCode *enc_key_hash,
  const struct FROST_MessageHash *message_hash);


/**
 * Computes the id of a POST request to /dkg-commitment, /dkg-share
 * and /dkg-key.
 *
 * @param[out] request_id The resulting request id
 * @param[in] context_string Main source of entropy for the provider
 * @param[in] challenge_hash Salted hash of the authentication data
 * @param[in] provider_salt Salt value which we received from a /config request
 * @param[in] provider_index Index of the provider in this Frosix DKG
 * @param[in] num_of_participants Number of participating providers
 * @param[in] threshold Chosen threshold value for this Frosix DKG
*/
void
FROSIX_compute_dkg_request_id (
  struct FROSIX_DkgRequestIdP *request_id,
  const struct FROSIX_DkgContextStringP *context_string,
  const struct FROSIX_ChallengeHashP *challenge_hash,
  const struct FROSIX_ProviderSaltP *provider_salt,
  uint8_t provider_index,
  uint8_t num_of_participants,
  uint8_t threshold);


/**
 * Computes an id which is used as identifier to store the challenge
 * in the database. -> FIXME should not be in frosix.h
 *
 * @param[out] challenge_id The resulting request id
 * @param[in] enc_key_hash Identifier of the corresponding auth data
 * @param[in] mh Hash of the message, to bind a request to a specific message
*/
void
FROSIX_compute_challenge_id (struct FROSIX_ChallengeIdP *challenge_id,
                             const struct FROST_HashCode *enc_key_hash, // FIXME
                             const struct FROST_MessageHash *mh);


/**
 * Compares two given `struct GNUNET_HashCode`s.
 *
 * @param[in] hc1 First hash to compare
 * @param[in] hc2 Second hash to compare
 * @return #GNUNET_OK if the hashes matches
 *         #GNUNET_NO otherwise
*/
enum GNUNET_GenericReturnValue
FROSIX_gnunet_hash_cmp (const struct GNUNET_HashCode *hc1,
                        const struct GNUNET_HashCode *hc2);


/**
 * Hash a numerical answer to compute the hash value to be submitted
 * to the server for verification. Useful for PINs and SMS-TANs and
 * other numbers submitted for challenges.
 *
 * @param code the numeric value to hash
 * @param[out] hashed_code the resulting hash value to submit to the Frosix server
 */
void
FROSIX_hash_answer (uint64_t code,
                    struct GNUNET_HashCode *hashed_code);


/**
 * FIXME
 */
void
FROSIX_nonce_to_pow_salt (struct FROST_PowSalt *salt,
                          const struct GNUNET_HashCode *nonce);


/**
 * FIXME
*/
void
FROSIX_hash_pow (struct GNUNET_HashCode *hash,
                 struct GNUNET_CRYPTO_Edx25519PrivateKey *priv,
                 struct GNUNET_CRYPTO_Edx25519PublicKey *pub,
                 const struct GNUNET_HashCode *nonce,
                 const char *auth_answer,
                 unsigned int amplifier);

#endif