/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file frost_low.h
 * @brief Wrapper of all used functions from the crypto library libsodium
 * @author Joel Urech
*/
#ifndef FROST_LOW_H
#define FROST_LOW_H

#include <sodium.h>

#define FROST_SCALAR_BYTES crypto_core_ristretto255_SCALARBYTES
#define FROST_POINT_BYTES crypto_core_ristretto255_BYTES

/**
 * FROST_Point represents a 'Ristretto' point on Curve25519
*/
struct FROST_Point
{
  /**
   * We only have to save the x-coord of our point in ristretto255-encoding
  */
  unsigned char xcoord[crypto_core_ristretto255_BYTES];
};

/**
 * FROST_Scalar represents a scalar in the range of [0..L[,
 * where L is the order of the ristretto255 group
*/
struct FROST_Scalar
{
  /**
   * The scalar bytes
  */
  unsigned char scalarbytes[crypto_core_ristretto255_SCALARBYTES];
};

/**
 * Representation of a SHA-512 hash
*/
struct FROST_HashCode
{
  /**
   * 64 bytes
  */
  unsigned char hashbytes[crypto_hash_sha512_BYTES];
};

/**
 * To construct a hash over more than one input, we need a hash state
*/
struct FROST_HashState
{
  /**
   * The internal hash state of libsodium
  */
  crypto_hash_sha512_state state;
};

/**
 * Representation of a SHA-256 hash
*/
struct FROST_ShortHashCode
{
  /**
   * 32 bytes
  */
  unsigned char hashbytes[crypto_hash_sha256_BYTES];
};

/**
 * To construct a hash over more than one input, we need a hash state
*/
struct FROST_ShortHashState
{
  /**
   * The internal hash state of libsodium
  */
  crypto_hash_sha256_state state;
};

/**
 * Salt to use in computationally intense hashing of a password.
*/
struct FROST_PowSalt
{
  /**
   * 16 bytes
  */
  unsigned char bytes[crypto_pwhash_SALTBYTES];
};


/**
 * @brief Initializes the underlying libsodium library.
 *
 * @result Returns 0 on success, -1 on failure and 1 if the library had already been initialized
*/
int
FROST_low_init (void);

// Points
/**
 * @brief Multiplies a point with a scalar
 *
 * @param[out] result The resulting point
 * @param[in] pt Input point
 * @param[in] scal Input scalar
*/
void
FROST_point_mul_scalar (struct FROST_Point *result,
                        const struct FROST_Point *pt,
                        const struct FROST_Scalar *scal);

/**
 * @brief Adds two points together
 *
 * @param[out] result The resulting point
 * @param[in] p First point
 * @param[in] q Second point
*/
void
FROST_point_add_point (struct FROST_Point *result,
                       const struct FROST_Point *p,
                       const struct FROST_Point *q);

/**
 * @brief Substracts point p from point q
 *
 * @param[out] result The resulting point
 * @param[in] p Point which will be subtracted
 * @param[in] q Point to subtract of
*/
void
FROST_point_sub_point (struct FROST_Point *result,
                       const struct FROST_Point *p,
                       const struct FROST_Point *q);

/**
 * @brief Provides the identity element
 *
 * @param[out] result The identity element
*/
void
FROST_point_identity (struct FROST_Point *result);

// Scalars
/**
 * @brief Multiplies the basepoint with a scalar
 *
 * @param[out] result The resulting point
 * @param[in] scal Input scalar
*/
void
FROST_base_mul_scalar (struct FROST_Point *result,
                       const struct FROST_Scalar *scal);

/**
 * @brief Adds two scalars together
 *
 * @param[out] result The resulting scalar
 * @param[in] xscal First scalar
 * @param[in] yscal Second scalar
*/
void
FROST_scalar_add_scalar (struct FROST_Scalar *result,
                         const struct FROST_Scalar *xscal,
                         const struct FROST_Scalar *yscal);

/**
 * @brief Multiplies two scalars
 *
 * @param[out] result The resulting scalar
 * @param[in] xscal First scalar
 * @param[in] yscal Second scalar
*/
void
FROST_scalar_mul_scalar (struct FROST_Scalar *result,
                         const struct FROST_Scalar *xscal,
                         const struct FROST_Scalar *yscal);

/**
 * @brief Subtracts yscal from xscal
 *
 * @param[out] result The resulting scalar
 * @param[in] xscal Scalar to substract from
 * @param[in] yscal Scalar which will be substracted
*/
void
FROST_scalar_sub_scalar (struct FROST_Scalar *result,
                         const struct FROST_Scalar *xscal,
                         const struct FROST_Scalar *yscal);

/**
 * @brief Returns the multiplicative inverse of scal
 *
 * @param[out] result The resulting scalar
 * @param[in] scal Input scalar
*/
void
FROST_scalar_invert (struct FROST_Scalar *result,
                     const struct FROST_Scalar *scal);

// Randomnes
/**
 * @brief Provides a random point on Curve25519.
 *
 * @param[out] result The resulting random point
*/
void
FROST_point_random (struct FROST_Point *result);

/**
 * @brief Provides a random scalar in the range of [0..L[.
 * L is the order of the ristretto255 group
 *
 * @param[out] result The resulting random scalar
*/
void
FROST_scalar_random (struct FROST_Scalar *result);

// Hash functions
/**
 * @brief Initializes a hash state (needed for more than one input)
 *
 * @param[out] state The state which has to be initialized
*/
void
FROST_hash_init (struct FROST_HashState *state);

/**
 * @brief Allows to hash a byte array of arbitrary length
 *
 * @param[in,out] state An already initialized hash state has to be provided
 * @param[in] msg Pointer to the beginning of the byte array to hash (updates the hash state)
 * @param len Length of the byte array to hash
*/
void
FROST_hash_fixed_update (struct FROST_HashState *state,
                         const void *msg,
                         size_t len);

/**
 * @brief Allows to hash a ristretto255 point
 *
 * @param[in,out] state An already initialized hash state has to be provided
 * @param[in] pt The point to hash (updates the hash state)
*/
void
FROST_hash_point_update (struct FROST_HashState *state,
                         const struct FROST_Point *pt);

/**
 * @brief Allows to hash a hash in form of the struct FROST_HashCode
 *
 * @param[in,out] state An already initialized hash state has to be provided
 * @param[in] hash The hash to hash (updates the hash state)
*/
void
FROST_hash_hash_update (struct FROST_HashState *state,
                        const struct FROST_HashCode *hash);

/**
 * @brief Allows to hash a scalar
 *
 * @param[in,out] state An already initialized hash state has to be provided
 * @param[in] scal The scalar to hash (updates the hash state)
*/
void
FROST_hash_scalar_update (struct FROST_HashState *state,
                          const struct FROST_Scalar *scal);

/**
 * @brief Allows to hash an uint8 value
 *
 * @param[in,out] state An already initialized hash state has to be provided
 * @param identifier The uint8 value to hash (updates the hash state)
*/
void
FROST_hash_uint8_update (struct FROST_HashState *state,
                         const uint8_t identifier);

/**
 * @brief Transforms an updated hash state to a final hash value
 *
 * @param[in] state An already initialized and with at least one value updated hash state has to be provided
 * @param[out] result The resulting hash value (SHA512)
*/
void
FROST_hash_final (struct FROST_HashState *state,
                  struct FROST_HashCode *result);

/**
 * @brief Maps a hash value to a point on the curve
 *
 * @param[out] result The resulting point
 * @param[in] hash Hash input
*/
void
FROST_hash_to_curve (struct FROST_Point *result,
                     const struct FROST_HashCode *hash);

/**
 * @brief Reduces a hash value to a scalar in the range of the order of the ristretto255 group
 *
 * @param[out] result The resulting scalar
 * @param[in] hash Hash input
*/
void
FROST_hash_to_scalar (struct FROST_Scalar *result,
                      const struct FROST_HashCode *hash);

/**
 * FIXME
*/
enum GNUNET_GenericReturnValue
FROST_hash_cmp (const struct FROST_HashCode *first,
                const struct FROST_HashCode *second);

/**
 * FIXME
*/
void
FROST_short_hash_init (struct FROST_ShortHashState *state);

/**
 *
*/
void
FROST_short_hash_update_fixed (struct FROST_ShortHashState *state,
                               const void *msg,
                               size_t len);

/**
 * FIXME
*/
void
FROST_short_hash_final (struct FROST_ShortHashState *state,
                        struct FROST_ShortHashCode *result);


// Helper functions
/**
 * @brief Copies the value of a point
 *
 * @param[out] destination Point to copy to
 * @param[in] origin Point to copy from
*/
void
FROST_point_copy_to (struct FROST_Point *destination,
                     const struct FROST_Point *origin);

/**
 * @brief Copies the value of a scalar
 *
 * @param[out] destination Scalar to copy to
 * @param[in] origin Scalar to copy from
*/
void
FROST_scalar_copy_to (struct FROST_Scalar *destination,
                      const struct FROST_Scalar *origin);

/**
 * @brief Sets a FROST_Scalar to 0
 *
 * @param[out] scal The scalar to set to 0
*/
void
FROST_scalar_zero (struct FROST_Scalar *scal);

/**
 * @brief Sets a FROST_Scalar to 1
 *
 * @param[out] scal The scalar to set to 1
*/
void
FROST_scalar_one (struct FROST_Scalar *scal);

/**
 * @brief Sets a FROST_Scalar to a value of an uint8
 *
 * @param[out] result The resulting scalar
 * @param number Uint8 value
*/
void
FROST_scalar_set_uint8 (struct FROST_Scalar *result,
                        uint8_t number);

/**
 * @brief Compares two point on equality
 *
 * @param[in] p First point to compare
 * @param[in] q Second point to compare
 * @return GNUNET_OK if both points are equal, GNUNET_NO otherwise
*/
enum GNUNET_GenericReturnValue
FROST_point_cmp (const struct FROST_Point *p,
                 const struct FROST_Point *q);

/**
 * @brief Provides a check if the given point is a valid ristretto255-encoded element
 *
 * @param[in] p Point to validate
 * @return GNUNET_OK or GNUNET_NO
*/
enum GNUNET_GenericReturnValue
FROST_is_valid_point (const struct FROST_Point *p);


/**
 * Takes a single high entropy key and derives a scalar, mapped to the curve
 * from it.
 *
 *
 * @param[out] subkey The resulting derived key, mapped as a scalar to the curve.
 * @param[in] subkey_id Submit a different @a subkey_id to derive a different
 * subkey.
 * @param[in] masterkey Source of entropy to derive the @a subkey from. Make
 * sure to provide 32 bytes of high entropy!
*/
void
FROST_kdf_scalar_to_curve (struct FROST_Scalar *subkey,
                           uint64_t subkey_id,
                           const struct FROST_ShortHashCode *masterkey);


/**
 * FIXME
*/
enum GNUNET_GenericReturnValue
FROST_pow_hash (struct FROST_HashCode *hash,
                const char *answer,
                size_t length,
                const struct FROST_PowSalt *salt,
                uint8_t difficulty);

#endif
