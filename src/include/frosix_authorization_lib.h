/*
  This file is part of Frosix
  Copyright (C) 2019, 2021 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file include/frosix_authorization_lib.h
 * @brief database plugin loader
 * @author Dominik Meister
 * @author Dennis Neufeld
 * @author Christian Grothoff
 */
#ifndef FROSIX_AUTHORIZATION_LIB_H
#define FROSIX_AUTHORIZATION_LIB_H

#include "frosix_authorization_plugin.h"

/**
 * Load authorization plugin.
 *
 * @param method name of the method to load
 * @param db database handle to use
 * @param FH_cfg configuration to use
 * @return plugin handle on success
 */
struct FROSIX_AuthorizationPlugin *
FROSIX_authorization_plugin_load (
  const char *method,
  struct FROSIX_DatabasePlugin *db,
  const struct GNUNET_CONFIGURATION_Handle *FH_cfg);


/**
 * Shutdown all loaded plugins.
 */
void
FROSIX_authorization_plugin_shutdown (void);

#endif
/* end of frosix_authorization_lib.h */
