/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file include/frost_verify.h
 * @brief frosix api to verify a signature
 * @author Joel Urech
*/
#ifndef FROST_VERIFY_H
#define FROST_VERIFY_H

#include "frost_high.h"

/**
 * Verifies the validity of a given signature and hash of a message against a public key.
 * Since we create signatures which are compatible with the widespread EdDSA signature scheme,
 * this verification function does the same as every implementation of a EdDSA compatible verificiation function:
 * \f$sig = (r, z)\f$
 * \f$c = H(r || pk || m)\f$
 * \f$r = g^z - pk^c\f$ ?
 *
 * @param[in] public_key The corresponding public key
 * @param[in] signature The signature to verify
 * @param[in] message_hash Hash of the message, which has been signed, as an element on the elliptic curve
 * @return GNUNET_OK if signature is valid, otherwise GNUNET_NO
 */
enum GNUNET_GenericReturnValue
FROST_verify_signature (const struct FROST_PublicKey *public_key,
                        const struct FROST_Signature *signature,
                        const struct FROST_MessageHash *message_hash);

#endif
