/*
  This file is part of Frosix
  Copyright (C) 2020, 2021 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Frosix; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file include/frosix.h
 * @brief frosix client api
 * @author Christian Grothoff
 * @author Dominik Meister
 * @author Dennis Neufeld
 * @author Joel Urech
 */
#ifndef FROSIX_H
#define FROSIX_H

#include <jansson.h>
#include "frosix_backend.h"
#include "frosix_service.h"
#include <taler/taler_mhd_lib.h>
#include <regex.h>


/**
 * Initialize reducer subsystem.
 *
 * @param ctx context to use for CURL requests.
 */
void
FROSIX_redux_init (struct GNUNET_CURL_Context *ctx);


/**
 * Signature of the callback passed to #FROSIX_redux_action()
 *
 * @param cls closure
 * @param error error code, #TALER_EC_NONE if @a new_bs is the new successful state
 * @param new_state the new state of the operation (client should json_incref() to keep an alias)
 */
typedef void
(*FROSIX_ActionCallback)(void *cls,
                         enum TALER_ErrorCode error,
                         json_t *new_state);


/**
 * Generic container for an action with asynchronous activities.
 * Handle to an ongoing action. Only valid until the #FROSIX_ActionCallback is invoked.
 */
struct FROSIX_ReduxAction
{
  /**
   * Function to call to clean up.
   */
  void (*cleanup)(void *cls);

  /**
   * Action-specific state, closure for @e cleanup.
   */
  void *cleanup_cls;
};



/**
 * Cancel ongoing redux action.
 *
 * @param ra action to cancel
 */
void
FROSIX_redux_action_cancel (struct FROSIX_ReduxAction *ra);


/**
 * FIXME
*/
struct FROSIX_ReduxAction *
FROSIX_redux_keygen_start (const json_t *arguments,
                           FROSIX_ActionCallback cb,
                           void *cb_cls);


/**
 * FIXME
*/
struct FROSIX_ReduxAction *
FROSIX_redux_challenge_request_start (const json_t *arguments,
                                      const json_t *input,
                                      const char *message,
                                      FROSIX_ActionCallback cb,
                                      void *cb_cls);


/**
 * FIXME
*/
struct FROSIX_ReduxAction *
FROSIX_redux_sign_start (const json_t *arguments,
                         json_t *input,
                         const char *message,
                         FROSIX_ActionCallback cb,
                         void *cb_cls);


/**
 * FIXME
*/
struct FROSIX_ReduxAction *
FROSIX_redux_key_delete_start (json_t *input,
                               FROSIX_ActionCallback cb,
                               void *cb_cls);


/**
 * FIXME
*/
enum GNUNET_GenericReturnValue
FROSIX_verify_signature (const char *message,
                         const json_t *arguments);


/**
 * FIXME
*/
void
FROSIX_export_public_key (const json_t *arguments,
                          FROSIX_ActionCallback cb,
                          void *cb_cls);


/**
 * FIXME
*/
enum GNUNET_GenericReturnValue
FROSIX_verify_public_key (const json_t *arguments);

#endif  /* _FROSIX_REDUX_H */
