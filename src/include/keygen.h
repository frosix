/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file include/keygen.h
 * @brief frosix api to create a key pair in a distributed key generation
 * process.
 * @author Joel Urech
*/
#ifndef FROST_KEYGEN_H
#define FROST_KEYGEN_H

#include "frost_high.h"

/**
 * A commitment for a single share in a dkg.
*/
struct FROST_DkgShareCommitment
{
  /**
   * Our commitment is g^'share'
  */
  struct FROST_Point sc;
};

/**
 * A zero knowledge proof of the secret at x-coord 0 in our polynomial.
 * In principle it is a Schnorr signature.
*/
struct FROST_DkgZkp
{
  /**
   * The nonce of our zkp.
   */
  struct FROST_Point r;
  /**
   * Proof of knowing the secret value.
   */
  struct FROST_Scalar z;
};

/**
 * In the first round of the distributed key generation process,
 * every participant has to send a special commitment to all other.
 * This structs contains all needed data for this commitment.
*/
struct FROST_DkgCommitment
{
  /**
  * Unique identifier of the participant, must be greater than 0 and less than 255.
  */
  uint8_t identifier;
  /**
   * A pointer to an array of commitments.
   * The number of commitments depends on the choosen threshold value,
   * which can vary between 2 and 253. To initialize this struct,
   * call FROST_initialize_dkg_commitment().
  */
  struct FROST_DkgShareCommitment *share_comm;
  /**
   * The length of the commitment array
  */
  uint8_t shares_commitments_length;
  /**
   * A zero knowledge proof of the underlying secret value.
  */
  struct FROST_DkgZkp zkp;
};

/**
 *
*/

/**
 * Representation of a distributed key generation secret share (not the final key share!).
 * This struct will be used in the second and third round of the distributed key generation
 * process.
*/
struct FROST_DkgShare
{
  /**
   * Unique identifier of the participant, must be greater than 0 and less than 255
  */
  uint8_t identifier;
  /**
   * A secret value, which is intended only for a particular participant.
   * It has to match one of the commitments from round 1!
  */
  struct FROST_Scalar share;
};

/**
 * FIXME
*/
struct FROST_DkgContextString
{
  /**
   * FIXME
  */
  struct FROST_ShortHashCode con_str;
};

/**
 * Trusted dealer should only be used for testing purposes.
 * It creates key material using only local entropy.
 *
 * @param[out] key_pairs An array with the resulting key material and this
 * array contains a key pair for every participant.
 * @param[in] num_of_participants In how many parts should the secret key get
 * divided? This is also the length of the `key_pairs` array.
 * @param[in] threshold How many participants have to participate in the
 * signing process? The threshold value also determines one of the big security
 * parameters of our system. Do not choose it too low!
*/
void
FROST_trusted_dealer_keygen (struct FROST_KeyPair key_pairs[],
                             uint8_t num_of_participants,
                             uint8_t threshold);


/**
 * Checks if the provided params are valid.
 *
 * @param[in] identifier unique identifier of the participant in the group
 * @param[in] threshold threshold value to create a valid signature
 * @param[in] num_of_participant total number of participant
 * @return GNUNET_OK or GNUNET_NO
*/
enum GNUNET_GenericReturnValue
FROST_validate_dkg_params (uint8_t identifier,
                           uint8_t threshold,
                           uint8_t num_of_participants);

/**
 * Call this function always before starting a dkg.
 * It will initialize the struct `FROST_DkgCommitment`, which contains an array
 * of length `threshold`.
 *
 * @param[out] dkg_commitment The commitment which should be initialized.
 * @param[in] my_index Unique identifier of the participant, must be greater
 * than 0 and less than 255.
 * @param[in] threshold How many participants have to participate in the
 * signing process? The threshold value also fixes the length of the inner
 * array.
 * @return GNUNET_OK or GNUNET_NO
*/
enum GNUNET_GenericReturnValue
FROST_initialize_dkg_commitment (struct FROST_DkgCommitment *dkg_commitment,
                                 uint8_t my_index,
                                 uint8_t threshold);

/**
 * Every participant first do a trusted dealer on his own and commits to each
 * of the t-1 generated values.
 * Additionally, every participant has to compute a zero knowledge proof of
 * the underlying secret (y-value of x=0). As a result, this function returns
 * a commitment, which should then be send to all other participants.
 * Do not send the dkg shares now!
 *
 * @a context_string should be the hash of the received context_string,
 * concatenated with the secret provider salt and other values like the salted
 * hash of the authentication method and the salted hash of the authentication
 * data.
 *
 * @param[in,out] dkg_commitment This struct must first be initialized with
 * FROST_initialize_dkg_commitment. After calling FROST_keygen_begin, the inner
 * array will be filled with a commitment to each of the t-1 share values.
 * @param[out] dkg_shares This are the precomputed shares, one share for each
 * participant. The length of this array MUST be 'num_of_participants - 1'.
 * @param[in] context_string Source of entropy to derive all 'rnd' values.
 * @param[in] addtional_data There is the possibility to include additional
 * data in form of a hash in the zero knowledge proof. NULL possible.
 * @param[in] my_index Unique identifier of the participant, must be greater
 * than 0 and less than 255.
 * @param[in] num_of_participants How many participants are participating in
 * the distributed key generation process?
 * @param[in] threshold How many participants have to participate in the
 * signing process?
 * @return GNUNET_OK or GNUNET_NO
*/
enum GNUNET_GenericReturnValue
FROST_keygen_begin (struct FROST_DkgCommitment *dkg_commitment,
                    struct FROST_DkgShare dkg_shares[],
                    const struct FROST_DkgContextString *context_string,
                    const struct FROST_HashCode *additional_data,
                    uint8_t my_index,
                    uint8_t num_of_participants,
                    uint8_t threshold);

/**
 * After receiving all commitments from all other participants, they have to be
 * validated. This means, that every value will be checked if it is a correctly
 * encoded ristretto255 element. Additionally the included zero knowledge proof
 * has to be validated too.
 *
 * @param[in] dkg_commitment A sorted list of all commitments.
 * @param[in] num_of_participants How many participants are participating in
 * the distributed key generation process?
 * @return GNUNET_OK or GNUNET_NO
*/
enum GNUNET_GenericReturnValue
FROST_keygen_validate_commitment (
  const struct FROST_DkgCommitment *dkg_commitment,
  const struct FROST_HashCode *additional_data,
  uint8_t num_of_participants);

/**
 * Finally send to each participant his share and receive from each participant
 * a share.
 * This shares has to be verified against the previously received commitments.
 *
 * @param[in] commitment The dkg commitment, including all share commitments
 * @param[in] share The share we validate with the dkg commitment
 * @param[in] my_index What is my identifier number?
 * @return GNUNET_OK or GNUNET_NO
*/
enum GNUNET_GenericReturnValue
FROST_keygen_validate_share (
  const struct FROST_DkgCommitment *commitment,
  const struct FROST_DkgShare *share,
  uint8_t my_index);

/**
 * With all these shares, this function computes the individual key pair and
 * derives the group public key from the commitments.
 *
 * @param[out] key_pair The resulting key material, keep it safe and secure!
 * @param[in] my_index Unique identifier of the participant, must be greater
 * than 0 and less than 255.
 * @param[in] shares The precomputed shares, received one share from each
 * participant.
 * @param[in] commitments The previously received and already validated
 * commitments. There has to be a corresponding value to each of the received
 * shares.
 * @param[in] num_of_participants How many participants are participating in
 * the distributed key generation process?
*/
void
FROST_keygen_finalize (struct FROST_KeyPair *key_pair,
                       uint8_t my_index,
                       const struct FROST_DkgShare shares[],
                       const struct FROST_DkgCommitment commitments[],
                       uint8_t num_of_participants);

/**
 * Counterpart to the initializing function of the struct dkg commitment.
 * This function frees allocated space.
 *
 * @param[in,out] dkg_commitment The struct which was initialized with
 * `FROST_initialize_dkg_commitment` at the beginning of the dkg process.
 * @param[in] length Number of commitments elements.
*/
void
FROST_free_dkg_commitment (struct FROST_DkgCommitment dkg_commitments[],
                           size_t length);

#endif
