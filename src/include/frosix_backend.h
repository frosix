/*
  This file is part of Frosix
  Copyright (C) 2022, 2023 Frosix

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file include/frost_high.h
 * @brief frosix high level api to create a signature share
 * @author Joel Urech
*/
#ifndef FROSIX_BACKEND_H
#define FROSIX_BACKEND_H

#include <gnunet/gnunet_util_lib.h>
#include "frost_low.h"
#include "frosix.h"

/**
 * Server to client: this is the policy version.
 */
// #define FROSIX_HTTP_HEADER_POLICY_VERSION "Frosix-Version"

/**
 * Server to client: this is the policy expiration time.
 */
// #define ANASTASIS_HTTP_HEADER_POLICY_EXPIRATION "Anastasis-Policy-Expiration"

/**
 * Client to server: use this to decrypt the truth.
 */
/*#define ANASTASIS_HTTP_HEADER_TRUTH_DECRYPTION_KEY \
  "Anastasis-Truth-Decryption-Key"*/

/**
 * Client to server: please store this meta data.
 */
// #define ANASTASIS_HTTP_HEADER_POLICY_META_DATA "Anastasis-Policy-Meta-Data"


/**
 * Client to server: I paid using this payment secret.
 */
// #define ANASTASIS_HTTP_HEADER_PAYMENT_IDENTIFIER
// "Anastasis-Payment-Identifier"

/**
 * Client to server: I am authorized to update this policy, or
 * server to client: I prove this is a valid policy.
 */
// #define ANASTASIS_HTTP_HEADER_POLICY_SIGNATURE "Anastasis-Policy-Signature"

/**
 * Server to client: Taler Payto-URI.
 */
// #define ANASTASIS_HTTP_HEADER_TALER "Taler"


GNUNET_NETWORK_STRUCT_BEGIN

/**
 * An EdDSA public key that is used to identify a user's account.
 */
/*struct FROSIX_CRYPTO_AccountPublicKeyP
{
  struct GNUNET_CRYPTO_EddsaPublicKey pub;
};*/


/**
 * An EdDSA private key that is used to identify a user's account.
 */
/*struct ANASTASIS_CRYPTO_AccountPrivateKeyP
{
  struct GNUNET_CRYPTO_EddsaPrivateKey priv;
};*/


/**
 * Specifies a TruthKey which is used to decrypt the Truth stored by the user.
 */
/*struct ANASTASIS_CRYPTO_TruthKeyP
{
  struct FROST_HashCode key;
};*/


/**
 * Specifies a salt value used to encrypt the master public key.
 */
/*struct ANASTASIS_CRYPTO_MasterSaltP
{
  struct FROST_HashCode salt;
};*/





/**
 * Specifies a policy key which is used to decrypt the master key
 */
/*struct ANASTASIS_CRYPTO_PolicyKeyP
{
  struct FROST_HashCode key;
};*/


/**
 * Nonce used for encryption, 24 bytes.
 */
/*struct ANASTASIS_CRYPTO_NonceP
{
  uint8_t nonce[crypto_secretbox_NONCEBYTES];
};*/


/**
 * Header that is prepended to a ciphertext, consisting of nonce and MAC.
 */
/*struct ANASTASIS_CRYPTO_CiphertextHeaderP
{
  uint8_t header[crypto_secretbox_NONCEBYTES + crypto_secretbox_MACBYTES];
};*/


/**
 * Specifies a key used for symmetric encryption, 32 bytes.
 */
/*struct ANASTASIS_CRYPTO_SymKeyP
{
  uint32_t key[8];
};*/


/**
 * Specifies a Key Share from an escrow provider, the combined
 * keyshares generate the EscrowMasterKey which is used to decrypt the
 * Secret from the user.
 */
/*struct ANASTASIS_CRYPTO_KeyShareP
{
  uint32_t key[8];
};*/


/**
 * Specifies an encrypted KeyShare
 */
/*struct FROSIX_CRYPTO_EncryptedKeyShareP
{
  //
   * Ciphertext.
   //
  struct ANASTASIS_CRYPTO_CiphertextHeaderP header;

  //
   * The actual key share, encrypted.
   //
  struct ANASTASIS_CRYPTO_KeyShareP keyshare;
};*/


/**
 * The escrow master key is the key used to encrypt the user secret (MasterKey).
 */
/*struct ANASTASIS_CRYPTO_EscrowMasterKeyP
{
  uint32_t key[8];
};*/


/**
 * The user identifier consists of user information and the server salt. It is used as
 * entropy source to generate the account public key and the encryption keys.
 */
/*struct ANASTASIS_CRYPTO_UserIdentifierP
{
  struct FROST_HashCode hash;
};*/





/**
 * Data signed by the account public key of a sync client to
 * authorize the upload of the backup.
 */
/* struct ANASTASIS_UploadSignaturePS
{
  //
   * Set to #TALER_SIGNATURE_ANASTASIS_POLICY_UPLOAD.
   //
  struct GNUNET_CRYPTO_EccSignaturePurpose purpose;

  //
   * Hash of the new backup.
   //
  struct FROST_HashCode new_recovery_data_hash;

}; */


/**
 * Signature made with an account's public key.
 */
/*struct FROSIX_AccountSignatureP
{*/
/**
   * We use EdDSA.
   */
/*struct GNUNET_CRYPTO_EddsaSignature eddsa_sig;
};*/


  GNUNET_NETWORK_STRUCT_END

/**
 * Result of encrypting the core secret.
 */
// struct ANASTASIS_CoreSecretEncryptionResult
// {
/**
   * Encrypted core secret.
   */
// void *enc_core_secret;

/**
   * Size of the encrypted core secret.
   */
// size_t enc_core_secret_size;

/**
   * Array of encrypted master keys.  Each key is encrypted
   * to a different policy key.
   */
// void **enc_master_keys;

/**
   * Sizes of the encrypted master keys.
   */
// size_t *enc_master_key_sizes;
// };


#endif