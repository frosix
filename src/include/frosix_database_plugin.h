/*
  This file is part of Frosix
  Copyright (C) 2019-2022 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file include/frosix_database_plugin.h
 * @brief database access for Frosix
 * @author Christian Grothoff
 */
#ifndef FROSIX_DATABASE_PLUGIN_H
#define FROSIX_DATABASE_PLUGIN_H

#include "frosix_service.h"
#include "frost_high.h"
#include "keygen.h"
#include "frosix_util_lib.h"
#include <gnunet/gnunet_db_lib.h>
#include <taler/taler_util.h>

/**
 * How long is an offer for a challenge payment valid for payment?
 */
#define FROSIX_CHALLENGE_OFFER_LIFETIME GNUNET_TIME_UNIT_HOURS

/**
 * FIXME
*/
enum FROSIX_DB_CommitmentStatus
{
  /**
  * FIXME
 */
  FROSIX_DB_COMMITMENT_STATUS_HARD_ERROR = -2,

  /**
   * FIXME
  */
  FROSIX_DB_COMMITMENT_STATUS_SOFT_ERROR = -1,

  /**
   * FIXME
  */
  FROSIX_DB_COMMITMENT_STATUS_NO_RESULTS = 0,

  /**
   * FIXME
  */
  FROSIX_DB_COMMITMENT_STATUS_ONE_RESULT = 1,
};

/**
 * FIXME
*/
enum FROSIX_DB_KeyStatus
{
  /**
  * FIXME
 */
  FROSIX_DB_KEY_STATUS_HARD_ERROR = -2,

  /**
   * FIXME
  */
  FROSIX_DB_KEY_STATUS_SOFT_ERROR = -1,

  /**
   * FIXME
  */
  FROSIX_DB_KEY_STATUS_NO_RESULTS = 0,

  /**
   * FIXME
  */
  FROSIX_DB_KEY_STATUS_ONE_RESULT = 1,
};

/**
 * Return values for checking code validity.
 */
enum FROSIX_DB_CodeStatus
{
  /**
   * Provided authentication code does not match database content.
   */
  FROSIX_DB_CODE_STATUS_CHALLENGE_CODE_MISMATCH = -3,

  /**
   * Encountered hard error talking to DB.
   */
  FROSIX_DB_CODE_STATUS_HARD_ERROR = -2,

  /**
   * Encountered serialization error talking to DB.
   */
  FROSIX_DB_CODE_STATUS_SOFT_ERROR = -1,

  /**
   * We have no challenge in the database.
   */
  FROSIX_DB_CODE_STATUS_NO_RESULTS = 0,

  /**
   * The provided challenge matches what we have in the database.
   */
  FROSIX_DB_CODE_STATUS_VALID_CODE_STORED = 1,
};


/**
 * Return values for checking account validity.
 */
enum FROSIX_DB_AccountStatus
{
  /**
   * Account is unknown, user should pay to establish it.
   */
  FROSIX_DB_ACCOUNT_STATUS_PAYMENT_REQUIRED = -3,

  /**
   * Encountered hard error talking to DB.
   */
  FROSIX_DB_ACCOUNT_STATUS_HARD_ERROR = -2,

  /**
   * Account is valid, but we have no policy stored yet.
   */
  FROSIX_DB_ACCOUNT_STATUS_NO_RESULTS = 0,

  /**
   * Account is valid, and we have a policy stored.
   */
  FROSIX_DB_ACCOUNT_STATUS_VALID_HASH_RETURNED = 1,
};


/**
 * Return values for storing data in database with payment.
 */
enum FROSIX_DB_StoreStatus
{
  /**
   * The client has stored too many policies, should pay to store more.
   */
  FROSIX_DB_STORE_STATUS_STORE_LIMIT_EXCEEDED = -4,

  /**
   * The client needs to pay to store policies.
   */
  FROSIX_DB_STORE_STATUS_PAYMENT_REQUIRED = -3,

  /**
   * Encountered hard error talking to DB.
   */
  FROSIX_DB_STORE_STATUS_HARD_ERROR = -2,

  /**
   * Despite retrying, we encountered serialization errors.
   */
  FROSIX_DB_STORE_STATUS_SOFT_ERROR = -1,

  /**
   * Database did not need an update (document exists).
   */
  FROSIX_DB_STORE_STATUS_NO_RESULTS = 0,

  /**
   * We successfully stored the document.
   */
  FROSIX_DB_STORE_STATUS_SUCCESS = 1,
};


/**
 * Function called on all pending payments for an account or challenge.
 *
 * @param cls closure
 * @param timestamp for how long have we been waiting
 * @param payment_secret payment secret / order id in the backend
 * @param amount how much is the order for
 */
typedef void
(*FROSIX_DB_PaymentPendingIterator)(
  void *cls,
  struct GNUNET_TIME_Timestamp timestamp,
  const struct FROSIX_PaymentSecretP *payment_secret,
  const struct TALER_Amount *amount);


/**
 * Function called to test if a given wire transfer
 * satisfied the authentication requirement of the
 * IBAN plugin.
 *
 * @param cls closure
 * @param credit amount that was transferred
 * @param wire_subject subject provided in the wire transfer
 * @return true if this wire transfer satisfied the authentication check
 */
typedef bool
(*FROSIX_DB_AuthIbanTransfercheck)(
  void *cls,
  const struct TALER_Amount *credit,
  const char *wire_subject);


/**
 * Function called on matching meta data.  Note that if the client did
 * not provide meta data for @a version, the function will be called
 * with @a recovery_meta_data being NULL.
 *
 * @param cls closure
 * @param version the version of the recovery document
 * @param ts timestamp when the document was uploaded
 * @param recovery_meta_data contains meta data about the encrypted recovery document
 * @param recovery_meta_data_size size of @a recovery_meta_data blob
 * @return #GNUNET_OK to continue to iterate, #GNUNET_NO to abort iteration
 */
typedef enum GNUNET_GenericReturnValue
(*FROSIX_DB_RecoveryMetaCallback)(void *cls,
                                  uint32_t version,
                                  struct GNUNET_TIME_Timestamp ts,
                                  const void *recovery_meta_data,
                                  size_t recovery_meta_data_size);


/**
 * Handle to interact with the database.
 *
 * Functions ending with "_TR" run their OWN transaction scope
 * and MUST NOT be called from within a transaction setup by the
 * caller.  Functions ending with "_NT" require the caller to
 * setup a transaction scope.  Functions without a suffix are
 * simple, single SQL queries that MAY be used either way.
 */
struct FROSIX_DatabasePlugin
{

  /**
   * Closure for all callbacks.
   */
  void *cls;

  /**
   * Name of the library which generated this plugin.  Set by the
   * plugin loader.
   */
  char *library_name;

  /**
   * Drop anastasis tables. Used for testcases.
   *
   * @param cls closure
   * @return #GNUNET_OK upon success; #GNUNET_SYSERR upon failure
   */
  enum GNUNET_GenericReturnValue
  (*drop_tables)(void *cls);

  /**
   * Connect to the database.
   *
   * @param cls closure
   * @return #GNUNET_OK upon success; #GNUNET_SYSERR upon failure
   */
  enum GNUNET_GenericReturnValue
  (*connect)(void *cls);

  /**
   * Initialize merchant tables
   *
   * @param cls closure
   * @return #GNUNET_OK upon success; #GNUNET_SYSERR upon failure
   */
  enum GNUNET_GenericReturnValue
  (*create_tables)(void *cls);

  /**
   * Function called to perform "garbage collection" on the
   * database, expiring records we no longer require.  Deletes
   * all user records that are not paid up (and by cascade deletes
   * the associated recovery documents). Also deletes expired
   * truth and financial records older than @a fin_expire.
   *
   * @param cls closure
   * @param expire_backups backups older than the given time stamp should be garbage collected
   * @param expire_pending_payments payments still pending from since before
   *            this value should be garbage collected
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*gc)(void *cls,
        struct GNUNET_TIME_Absolute expire,
        struct GNUNET_TIME_Absolute expire_pending_payments);

  /**
  * Do a pre-flight check that we are not in an uncommitted transaction.
  * If we are, try to commit the previous transaction and output a warning.
  * Does not return anything, as we will continue regardless of the outcome.
  *
  * @param cls the `struct PostgresClosure` with the plugin-specific state
  * @return #GNUNET_OK if everything is fine
  *         #GNUNET_NO if a transaction was rolled back
  *         #GNUNET_SYSERR on hard errors
  */
  enum GNUNET_GenericReturnValue
  (*preflight)(void *cls);

  /**
  * Check that the database connection is still up.
  *
  * @param pg connection to check
  */
  void
  (*check_connection) (void *cls);

  /**
  * Roll back the current transaction of a database connection.
  *
  * @param cls the `struct PostgresClosure` with the plugin-specific state
  * @return #GNUNET_OK on success
  */
  void
  (*rollback) (void *cls);

  /**
   * Start a transaction.
   *
   * @param cls the `struct PostgresClosure` with the plugin-specific state
   * @param name unique name identifying the transaction (for debugging),
   *             must point to a constant
   * @return #GNUNET_OK on success
   */
  int
  (*start) (void *cls,
            const char *name);

  /**
   * Commit the current transaction of a database connection.
   *
   * @param cls the `struct PostgresClosure` with the plugin-specific state
   * @return transaction status code
   */
  enum GNUNET_DB_QueryStatus
  (*commit)(void *cls);


  /**
   * Register callback to be invoked on events of type @a es.
   *
   * @param cls database context to use
   * @param es specification of the event to listen for
   * @param timeout how long to wait for the event
   * @param cb function to call when the event happens, possibly
   *         multiple times (until cancel is invoked)
   * @param cb_cls closure for @a cb
   * @return handle useful to cancel the listener
   */
  struct GNUNET_DB_EventHandler *
  (*event_listen)(void *cls,
                  const struct GNUNET_DB_EventHeaderP *es,
                  struct GNUNET_TIME_Relative timeout,
                  GNUNET_DB_EventCallback cb,
                  void *cb_cls);

  /**
   * Stop notifications.
   *
   * @param eh handle to unregister.
   */
  void
  (*event_listen_cancel)(struct GNUNET_DB_EventHandler *eh);


  /**
   * Notify all that listen on @a es of an event.
   *
   * @param cls database context to use
   * @param es specification of the event to generate
   * @param extra additional event data provided
   * @param extra_size number of bytes in @a extra
   */
  void
  (*event_notify)(void *cls,
                  const struct GNUNET_DB_EventHeaderP *es,
                  const void *extra,
                  size_t extra_size);


  /**
   * FIXME
  */
  enum FROSIX_DB_StoreStatus
  (*store_dkg_commitment)(
    void *cls,
    const struct FROSIX_DkgRequestIdP *dkg_id,
    const struct FROSIX_DkgCommitmentsRaw *dkg_commits);

  /**
   * FIXME
  */
  enum GNUNET_DB_QueryStatus
  (*get_dkg_commitment)(
    void *cls,
    const struct FROSIX_DkgRequestIdP *dkg_id,
    struct FROSIX_DkgCommitmentsRaw *dkg_commits);


  /**
   * FIXME
  */
  enum FROSIX_DB_CommitmentStatus
  (*lookup_dkg_commitment)(
    void *cls,
    const struct FROSIX_DkgRequestIdP *dkg_id);


  /**
   * FIXME
  */
  enum FROSIX_DB_StoreStatus
  (*store_key)(
    void *cls,
    const struct FROST_HashCode *id,
    const struct FROSIX_EncryptionNonceP *nonce,
    const struct FROSIX_KeyDataEncrypted *key_data,
    const struct FROSIX_ChallengeHashP *challenge_hash,
    uint32_t expiration,
    uint8_t identifier);

/**
 * FIXME
*/
  enum FROSIX_DB_KeyStatus
  (*lookup_key)(
    void *cls,
    const struct FROST_HashCode *id);


  /**
   * FIXME
  */
  enum GNUNET_DB_QueryStatus
  (*get_auth_hash)(
    void *cls,
    const struct FROST_HashCode *db_id,
    struct FROSIX_ChallengeHashP *challenge_hash);


  /**
   * FIXME
  */
  enum GNUNET_DB_QueryStatus
  (*get_key_data)(
    void *cls,
    const struct FROST_HashCode *db_id,
    uint32_t *identifier,
    struct FROSIX_EncryptionNonceP *nonce,
    struct FROSIX_KeyDataEncrypted *enc_key_data);


  /**
   * FIXME
  */
  enum GNUNET_DB_QueryStatus
  (*delete_key_data)(
    void *cls,
    const struct FROST_HashCode *db_id);


  /**
   * FIXME
  */
  enum GNUNET_DB_QueryStatus
  (*store_commitment_seed)(
    void *cls,
    const struct GNUNET_HashCode *db_id,
    const struct FROST_CommitmentSeed *seed);


  /**
   * FIXME
  */
  enum GNUNET_DB_QueryStatus
  (*get_and_delete_commitment_seed)(
    void *cls,
    const struct GNUNET_HashCode *db_id,
    struct FROST_CommitmentSeed *seed);


  /**
   * Verify the provided code with the code on the server.
   * If the code matches the function will return with success, if the code
   * does not match, the retry counter will be decreased by one.
   *
   * @param cls closure
   * @param truth_uuid identification of the challenge which the code corresponds to
   * @param hashed_code code which the user provided and wants to verify
   * @param[out] code set to the original numeric code
   * @param[out] satisfied set to true if the challenge is set to satisfied
   * @return transaction status
   */
  enum FROSIX_DB_CodeStatus
  (*verify_challenge_code)(
    void *cls,
    const struct FROSIX_ChallengeIdP *truth_uuid,
    const struct GNUNET_HashCode *hashed_code,
    uint64_t *code,
    bool *satisfied);


  /**
   * Set the 'satisfied' bit for the given challenge and code to
   * 'true'.
   *
   * @param cls closure
   * @param truth_uuid identification of the challenge which the code corresponds to
   * @param code code which is now satisfied
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*mark_challenge_code_satisfied)(
    void *cls,
    const struct FROSIX_ChallengeIdP *truth_uuid,
    uint64_t code);


  /**
   * Check if the 'satisfied' bit for the given challenge and code is
   * 'true' and the challenge code is not yet expired.
   *
   * @param cls closure
   * @param truth_uuid identification of the challenge which the code corresponds to
   * @param code code which is now satisfied
   * @param after after what time must the challenge have been created
   * @return transaction status,
   *        #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if the challenge code is not satisfied or expired
   *        #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT if the challenge code has been marked as satisfied
   */
  enum GNUNET_DB_QueryStatus
  (*test_challenge_code_satisfied)(
    void *cls,
    const struct FROSIX_ChallengeIdP *truth_uuid,
    const uint64_t code,
    struct GNUNET_TIME_Timestamp after);


  /**
   * Insert a new challenge code for a given challenge identified by the challenge
   * public key. The function will first check if there is already a valid code
   * for this challenge present and won't insert a new one in this case.
   *
   * @param cls closure
   * @param truth_uuid the identifier for the challenge
   * @param rotation_period for how long is the code available
   * @param validity_period for how long is the code available
   * @param retry_counter amount of retries allowed
   * @param[out] retransmission_date when to next retransmit
   * @param[out] code set to the code which will be checked for later
   * @return transaction status,
   *        #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if we are out of valid tries,
   *        #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT if @a code is now in the DB
   */
  enum GNUNET_DB_QueryStatus
  (*create_challenge_code)(
    void *cls,
    const struct FROSIX_ChallengeIdP *truth_uuid,
    struct GNUNET_TIME_Relative rotation_period,
    struct GNUNET_TIME_Relative validity_period,
    uint32_t retry_counter,
    struct GNUNET_TIME_Timestamp *retransmission_date,
    uint64_t *code);


  /**
   * Remember in the database that we successfully sent a challenge.
   *
   * @param cls closure
   * @param truth_uuid the identifier for the challenge
   * @param code the challenge that was sent
   */
  enum GNUNET_DB_QueryStatus
  (*mark_challenge_sent)(
    void *cls,
    const struct FROSIX_ChallengeIdP *truth_uuid,
    uint64_t code);


  /**
   * Store payment for challenge.
   *
   * @param cls closure
   * @param truth_key identifier of the challenge to pay
   * @param payment_secret payment secret which the user must provide with every upload
   * @param amount how much we asked for
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*record_challenge_payment)(
    void *cls,
    const struct FROSIX_ChallengeIdP *truth_uuid,
    const struct FROSIX_PaymentSecretP *payment_secret,
    const struct TALER_Amount *amount);


  /**
   * Record refund for challenge.
   *
   * @param cls closure
   * @param truth_uuid identifier of the challenge to refund
   * @param payment_secret payment secret which the user must provide with every upload
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*record_challenge_refund)(
    void *cls,
    const struct FROSIX_ChallengeIdP *truth_uuid,
    const struct FROSIX_PaymentSecretP *payment_secret);


  /**
   * Lookup for a pending payment for a certain challenge
   *
   * @param cls closure
   * @param truth_uuid identification of the challenge
   * @param[out] payment_secret set to the challenge payment secret
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*lookup_challenge_payment)(
    void *cls,
    const struct FROSIX_ChallengeIdP *truth_uuid,
    struct FROSIX_PaymentSecretP *payment_secret);


  /**
   * Update payment status of challenge
   *
   * @param cls closure
   * @param truth_uuid which challenge received a payment
   * @param payment_identifier proof of payment, must be unique and match pending payment
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*update_challenge_payment)(
    void *cls,
    const struct FROSIX_ChallengeIdP *truth_uuid,
    const struct FROSIX_PaymentSecretP *payment_identifier);


  /**
   * Store inbound IBAN payment made for authentication.
   *
   * @param cls closure
   * @param wire_reference unique identifier inside LibEuFin/Nexus
   * @param wire_subject subject of the wire transfer
   * @param amount how much was transferred
   * @param debit_account account that was debited
   * @param credit_account Anastasis operator account credited
   * @param execution_date when was the transfer made
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*record_auth_iban_payment)(
    void *cls,
    uint8_t wire_reference,
    const char *wire_subject,
    const struct TALER_Amount *amount,
    const char *debit_account,
    const char *credit_account,
    struct GNUNET_TIME_Timestamp execution_date);


  /**
   * Function to check if we are aware of a wire transfer
   * that satisfies the IBAN plugin's authentication check.
   *
   * @param cls closure
   * @param debit_account which debit account to check
   * @param earliest_date earliest date to check
   * @param cb function to call on all entries found
   * @param cb_cls closure for @a cb
   * @return transaction status,
   *    #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT if @a cb
   *      returned 'true' once
   *    #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if no
   *      wire transfers existed for which @a cb returned true
   */
  enum GNUNET_DB_QueryStatus
  (*test_auth_iban_payment)(
    void *cls,
    const char *debit_account,
    struct GNUNET_TIME_Timestamp earliest_date,
    FROSIX_DB_AuthIbanTransfercheck cb,
    void *cb_cls);


  /**
   * Function to check the last known IBAN payment.
   *
   * @param cls closure
   * @param credit_account which credit account to check
   * @param[out] last_row set to the last known row
   * @return transaction status,
   *    #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT if @a cb
   *      returned 'true' once
   *    #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if no
   *      wire transfers existed for which @a cb returned true
   */
  enum GNUNET_DB_QueryStatus
  (*get_last_auth_iban_payment_row)(
    void *cls,
    const char *credit_account,
    uint8_t *last_row);


  /**
   * Function called to remove all expired codes from the database.
   *
   * @return transaction status
   */
  enum GNUNET_DB_QueryStatus
  (*challenge_gc)(void *cls);


};
#endif
