/*
  This file is part of Frosix
  Copyright (C) 2020 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Frosix; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file include/frosix_util_lib.h
 * @brief frosix client api
 * @author Christian Grothoff
 * @author Dominik Meister
 * @author Dennis Neufeld
 */
#ifndef FROSIX_UTIL_LIB_H
#define FROSIX_UTIL_LIB_H

// FIXME
// #include "frosix_error_codes.h"
// #define GNU_TALER_ERROR_CODES_H 1
#include "frost_low.h"
#include <gnunet/gnunet_util_lib.h>
#include <taler/taler_util.h>


/**
 * Maximum value allowed for PINs. Limited to 10^15 < 2^52 to ensure the
 * numeric value survives a conversion to float by JavaScript.
 *
 * NOTE: Do not change this value, we map it to a string like
 * 42353-256-6521-241 and that mapping fails if the number
 * does not have exactly 15 digits!
 */
#define FROSIX_PIN_MAX_VALUE 1000000000000000


/**
 * Return default project data used by Frosix.
 */
const struct GNUNET_OS_ProjectData *
FROSIX_project_data_default (void);


/**
 * Initialize libfrosixsutil.
 */
void
FROSIX_OS_init (void);


/*** FOR DEBUGGING ***/
void
FROSIX_debug_print_point (const struct FROST_Point *pt);

void
FROSIX_debug_print_scalar (const struct FROST_Scalar *scal);

void
FROSIX_debug_print_crockford32 (const void *data,
                                size_t length,
                                const char *title);


/*** NOT IN USE AT THE MOMENT ***/
/**
 * Convert input string @a as into @a pin.
 *
 * @param as input of the form 42355-256-2262-265
 * @param[out] pin set to numeric pin
 * @return false if @as is malformed
 */
bool
FROSIX_scan_pin (const char *as,
                 unsigned long long *pin);


/**
 * Convert numeric pin to human-readable number for display.
 *
 * @param pin number to convert
 * @return static (!) buffer with the text to show
 */
const char *
FROSIX_pin2s (uint64_t pin);


#endif
