/*
  This file is part of Frosix
  Copyright (C) 2019 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file include/frosix_database_lib.h
 * @brief database plugin loader
 * @author Dominik Meister
 * @author Dennis Neufeld
 * @author Christian Grothoff
 */
#ifndef FROSIX_DB_LIB_H
#define FROSIX_DB_LIB_H

#include "frosix_database_plugin.h"

/**
 * Initialize the plugin.
 *
 * @param cfg configuration to use
 * @return NULL on failure
 */
struct FROSIX_DatabasePlugin *
FROSIX_DB_plugin_load (const struct GNUNET_CONFIGURATION_Handle *cfg);


/**
 * Shutdown the plugin.
 *
 * @param plugin plugin to unload
 */
void
FROSIX_DB_plugin_unload (struct FROSIX_DatabasePlugin *plugin);


#endif

/* end of frosix_database_lib.h */
