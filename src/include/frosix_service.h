/*
  This file is part of Frosix
  Copyright (C) 2019-2022 Anastasis SARL

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Frosix; see the file COPYING.LIB.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file include/frosix_service.h
 * @brief C interface of libanastasisrest, a C library to use merchant's HTTP API
 * @author Christian Grothoff
 * @author Dennis Neufeld
 * @author Dominik Meister
 */
#ifndef FROSIX_SERVICE_H
#define FROSIX_SERVICE_H

#include "keygen.h"
#include "frosix_util_lib.h"
#include "frosix_crypto.h"
#include <gnunet/gnunet_curl_lib.h>
#include <jansson.h>
#include <taler/taler_util.h>

#define FROSIX_SEED_SIZE 64

/**
 * Frosix authorization method configuration
 */
struct FROSIX_AuthorizationMethodConfig
{
  /**
   * Type of the method, i.e. "question".
   */
  const char *type;

  /**
   * Fee charged for accessing key share using this method.
   */
  struct TALER_Amount usage_fee;
};


/**
 * @brief Frosix configuration data.
 */
struct FROSIX_Config
{
  /**
   * FIXME
  */
  uint8_t provider_index;

  /**
   * Protocol version supported by the server.
   */
  const char *version;

  /**
   * Business name of the frosix provider.
   */
  const char *business_name;

  /**
   * Array of authorization methods supported by the server.
   */
  const struct FROSIX_AuthorizationMethodConfig *methods;

  /**
   * Length of the @e methods array.
   */
  unsigned int methods_length;

  /**
   * Annual fee for an account / policy upload.
   */
  struct TALER_Amount annual_fee;

  /**
   * Fee for a truth upload.
   */
  struct TALER_Amount signature_creation_fee;

  /**
   * Maximum legal liability for data loss covered by the
   * provider.
   */
  struct TALER_Amount liability_limit;

  /**
   * Provider salt.
   */
  struct FROSIX_ProviderSaltP provider_salt;

  /**
   * Public key of the provider (to verify signatures and encrypt against)
  */
  struct GNUNET_CRYPTO_EddsaPublicKey public_key;

};


/**
 * Function called with the result of a /config request.
 * Note that an HTTP status of #MHD_HTTP_OK is no guarantee
 * that @a fcfg is non-NULL. @a fcfg is non-NULL only if
 * the server provided an acceptable response.
 *
 * @param cls closure
 * @param http_status the HTTP status
 * @param fcfg configuration obtained, NULL if we could not parse it
 */
typedef void
(*FROSIX_ConfigCallback)(void *cls,
                         unsigned int http_status,
                         const struct FROSIX_Config *fcfg);


/**
 * @brief A Config Operation Handle
 */
struct FROSIX_ConfigOperation
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Must not be 0!
  */
  uint8_t provider_index;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

  /**
  * The callback to pass the backend response to
  */
  FROSIX_ConfigCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

};


/**
 * Run a GET /config request against the Frosix backend.
 *
 * @param ctx CURL context to use
 * @param base_url base URL fo the Frosix backend
 * @param cb function to call with the results
 * @param cb_cls closure for @a cb
 * @return handle to cancel the operation
 */
struct FROSIX_ConfigOperation *
FROSIX_get_config (struct GNUNET_CURL_Context *ctx,
                   const char *base_url,
                   uint8_t provider_index,
                   FROSIX_ConfigCallback cb,
                   void *cb_cls);


/**
 * Cancel ongoing #FROSIX_get_config() request.
 *
 * @param co configuration request to cancel.
 */
void
FROSIX_config_cancel (struct FROSIX_ConfigOperation *co);

/* SEED API */

/**
 *
*/
struct FROSIX_Seed
{
  uint8_t bytes[FROSIX_SEED_SIZE];
};


/**
 *
*/
struct FROSIX_ProviderSeed
{
  /**
   *
  */
  uint8_t provider_index;

  /**
   *
  */
  const struct FROSIX_Seed *seed;
};

/**
 *
*/
typedef void
(*FROSIX_SeedGetCallback) (void *cls,
                           unsigned int http_status,
                           const struct FROSIX_ProviderSeed *ps);

/**
 * @brief A Contract Operation Handle
 */
struct FROSIX_SeedGetOperation
{
  /**
   * The url for this request, including parameters.
   */
  char *url;

  /**
   * Must not be 0!
  */
  uint8_t provider_index;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  FROSIX_SeedGetCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};



/**
 *
*/
struct FROSIX_SeedGetOperation *
FROSIX_seed_get (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  uint8_t provider_index,
  FROSIX_SeedGetCallback cb,
  void *cb_cls);

/**
 *
*/
void
FROSIX_seed_get_cancel (
  struct FROSIX_SeedGetOperation *sgo);


/* CONFIG API */
/* FIXME? */


/*** DKG-COMMITMENT API ***/

/**
 * High level ways how an dkg-commitment request may conclude.
*/
enum FROSIX_DkgCommitmentStatus
{
  /**
   * Commitment was successfully computed and returned.
  */
  FROSIX_DCS_SUCCESS = 0,

  /**
   * HTTP interaction failed, see HTTP status.
  */
  FROSIX_DCS_HTTP_ERROR,

  /**
   * We had an HTTP 400 error, see details in response body.
  */
  FROSIX_DCS_CLIENT_ERROR,

  /**
   * Server had an internal error.
  */
  FROSIX_DCS_SERVER_ERROR
};


/**
 *
*/
struct FROSIX_DkgCommitmentRequestDetails
{
  /**
   * High level status of the upload operation. Determines @e details.
  */
  enum FROSIX_DkgCommitmentStatus dcs;

  /**
   * HTTP status code.
   */
  unsigned int http_status;

  /**
   * Taler error code.
   */
  enum TALER_ErrorCode ec;

  /**
   * FIXME
  */
  uint8_t provider_index;

  /**
   * The returned DKG commitment, must be initialized before use!
  */
  struct FROST_DkgCommitment dkg_commitment;

  /**
   * Public key of the provider
  */
  struct GNUNET_CRYPTO_EddsaPublicKey public_key;
};


/**
 * Handle for a POST /dkg-commitment operation
*/
struct FROSIX_DkgCommitmentRequestOperation;


/**
 * Callback to process a POST /dkg-commitment request
 *
 * @param cls closure
 * @param dcd the response body
*/
typedef void
(*FROSIX_DkgCommitmentRequestCallback) (
  void *cls,
  const struct FROSIX_DkgCommitmentRequestDetails *dcd);


/**
 * Ask for a DKG commitment, does a POST /dkg-commitment/$UUID
 *
 * @param ctx the CURL context used to connect to the backend
 * @param backend_url backend's base URL, including final "/"
 * @param uuid unique identification of the dkg-commitment request
 * @param identifier unique identifier of the provider in the group
 * @param threshold number of participants needed to create a valid signature
 * @param num_of_participants total number of participants in the group
 * @param context_string main source of entropy for the provider to create a commitment
 * @param challenge_hash salted hash of challenge method and data
 * @param providers_public_keys sorted list of public keys of all involved providers.
 * @param cb callback function of the request
 * @param cb_cls closure for the callback function
 * @return handle for the operation
*/
struct FROSIX_DkgCommitmentRequestOperation *
FROSIX_dkg_commitment_request (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct FROSIX_DkgRequestIdP *uuid,
  uint8_t identifier,
  uint8_t threshold,
  uint8_t num_of_participants,
  const struct FROSIX_DkgContextStringP *context_string,
  const struct FROSIX_ChallengeHashP *challenge_hash,
  const struct GNUNET_CRYPTO_EddsaPublicKey providers_public_keys[],
  FROSIX_DkgCommitmentRequestCallback cb,
  void *cb_cls);


/**
 * Cancel a POST /dkg-commitment request
 *
 * @param dco the dkg-commitment request operation
*/
void
FROSIX_dkg_commitment_request_cancel (
  struct FROSIX_DkgCommitmentRequestOperation *dco);


/*** DKG-SHARE API ***/

/**
 * High level ways how an dkg-commitment request may conclude.
*/
enum FROSIX_DkgShareStatus
{
  /**
   * Commitment was successfully computed and returned.
  */
  FROSIX_DSS_SUCCESS = 0,

  /**
   * HTTP interaction failed, see HTTP status.
  */
  FROSIX_DSS_HTTP_ERROR,

  /**
   * We had an HTTP 400 error, see details in response body.
  */
  FROSIX_DSS_CLIENT_ERROR,

  /**
   * Server had an internal error.
  */
  FROSIX_DSS_SERVER_ERROR
};


/**
 * FIXME
*/
struct FROSIX_DkgSecretShare
{
  /**
    * Identifier of the target of this share
   */
  uint8_t target;

  /**
   * Identifier of the issuer. This information is intended for the target provider
  */
  uint8_t issuer;

  /**
   * The encrypted secret share
  */
  struct FROSIX_EncryptedShareP encrypted_share;

  /**
   * An ephemeral public key, which is needed to decrypt the share
  */
  struct GNUNET_CRYPTO_EcdhePublicKey ephemeral_key;
};


/**
 *
*/
struct FROSIX_DkgShareRequestDetails
{
  /**
   * High level status of the upload operation. Determines @e shares.
  */
  enum FROSIX_DkgShareStatus dss;

  /**
   * HTTP status code.
   */
  unsigned int http_status;

  /**
   * Taler error code.
   */
  enum TALER_ErrorCode ec;

  /**
   * Identifier of the provider
  */
  uint8_t provider_index;

  /**
   * Pointer to an array of secret shares
  */
  struct FROSIX_DkgSecretShare *shares;

  /**
   * Number of shares we got
  */
  size_t shares_len;
};

/**
 * FIXME
*/
struct FROSIX_DkgCommitment
{
  /**
   * FIXME
  */
  struct FROST_DkgCommitment commitment;

  /**
   * FIXME
  */
  struct GNUNET_CRYPTO_EcdhePublicKey encryption_public_key;
};


/**
 * Handle for a POST /dkg-share operation
*/
struct FROSIX_DkgShareRequestOperation;


/**
 * Callback to process a POST /dkg-share request
 *
 * @param cls closure
 * @param dsd the response body
*/
typedef void
(*FROSIX_DkgShareRequestCallback) (
  void *cls,
  const struct FROSIX_DkgShareRequestDetails *dcs);


/**
 * Ask for encrypted shares in a distributed key generation. Does a POST /dkg-share/$UUID
 *
 * @param ctx the CURL context used to connect to the backend
 * @param backend_url backend's base URL, including final "/"
 * @param uuid unique identification of the dkg-commitment request
 * @param identifier unique identifier of the provider in the group
 * @param threshold number of participants needed to create a valid signature
 * @param num_of_participants total number of participants in the group
 * @param context_string main source of entropy for the provider to create a commitment
 * @param challenge_hash salted hash of challenge method and data
 * @param dkg_commitments all the commitments we have to send
 * @param len length of @a dkg_commitments array
 * @param providers_public_keys sorted list of public keys of all involved providers.
 * @param cb callback function of the request
 * @param cb_cls closure for the callback function
 * @return handle for the operation
*/
struct FROSIX_DkgShareRequestOperation *
FROSIX_dkg_share_request (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct FROSIX_DkgRequestIdP *uuid,
  uint8_t identifier,
  uint8_t threshold,
  uint8_t num_of_participants,
  const struct FROSIX_DkgContextStringP *context_string,
  const struct FROSIX_ChallengeHashP *challenge_hash,
  const struct FROSIX_DkgCommitment dkg_commitments[],
  size_t len,
  const struct GNUNET_CRYPTO_EddsaPublicKey providers_public_keys[],
  FROSIX_DkgShareRequestCallback cb,
  void *cb_cls);


/**
 * Cancel a POST /dkg-share request
 *
 * @param dso the dkg-share request operation
*/
void
FROSIX_dkg_share_request_cancel (
  struct FROSIX_DkgShareRequestOperation *dso);


/*** DKG-KEY API ***/

/**
 * High level ways how an dkg-commitment request may conclude.
*/
enum FROSIX_DkgKeyStatus
{
  /**
   * Commitment was successfully computed and returned.
  */
  FROSIX_DKS_SUCCESS = 0,

  /**
   * HTTP interaction failed, see HTTP status.
  */
  FROSIX_DKS_HTTP_ERROR,

  /**
   * We had an HTTP 400 error, see details in response body.
  */
  FROSIX_DKS_CLIENT_ERROR,

  /**
   * Server had an internal error.
  */
  FROSIX_DKS_SERVER_ERROR
};


/**
 *
*/
struct FROSIX_DkgKeyStoreDetails
{
  /**
   * High level status of the upload operation. Determines @e details.
  */
  enum FROSIX_DkgKeyStatus dks;

  /**
   * HTTP status code.
   */
  unsigned int http_status;

  /**
   * Taler error code.
   */
  enum TALER_ErrorCode ec;

  /**
   * Identifier of the provider
  */
  uint8_t provider_index;

  /**
   * The returned data
  */
  struct
  {
    /**
     * Our final public key
    */
    struct FROST_PublicKey public_key;

    /**
     * A signature over the public key and the salted hashes of the challenge
    */
    struct GNUNET_CRYPTO_EddsaSignature signature;
  } details;
};


/**
 * A share for a request to /dkg-key
*/
struct FROSIX_DkgKeyStoreShare
{
  /**
   * Identifier of the issuer of the share
  */
  uint8_t identifier;

  /**
   * Encrypted secret share
  */
  struct FROSIX_EncryptedShareP secret_share;

  /**
   * Ephemeral public key to derive the encryption key
  */
  struct GNUNET_CRYPTO_EcdhePublicKey ephemeral_key;
};


/**
 * Handle for a POST /dkg-key operation
*/
struct FROSIX_DkgKeyStoreOperation;


/**
 * Callback to process a POST /dkg-key request
 *
 * @param cls closure
 * @param dkd the response body
*/
typedef void
(*FROSIX_DkgKeyStoreCallback) (
  void *cls,
  const struct FROSIX_DkgKeyStoreDetails *dks);


/**
 * Ask to generate and store key material in a distributed key generation. Does a POST /dkg-key/$UUID
 *
 * @param ctx the CURL context used to connect to the backend
 * @param backend_url backend's base URL, including final "/"
 * @param uuid unique identification of the dkg-commitment request
 * @param identifier unique identifier of the provider in the group
 * @param threshold number of participants needed to create a valid signature
 * @param num_of_participants total number of participants in the group
 * @param context_string main source of entropy for the provider to create a commitment
 * @param challenge_hash salted hash of challenge method and data
 * @param providers_public_keys sorted list of public keys of all involved providers.
 * @param pre_encryption_key the master key to derive the encryption key, used for the data at the provider
 * @param expiration how many years the provider should store the key data
 * @param secret_shares array of secret shares
 * @param len length of @a dkg_commitments and @a provider_public_keys array
 * @param cb callback function of the request
 * @param cb_cls closure for the callback function
 * @return handle for the operation
*/
struct FROSIX_DkgKeyStoreOperation *
FROSIX_dkg_key_store (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct FROSIX_DkgRequestIdP *uuid,
  uint8_t identifier,
  uint8_t threshold,
  uint8_t num_of_participants,
  const struct FROSIX_DkgContextStringP *context_string,
  const struct FROSIX_ChallengeHashP *challenge_hash,
  const struct GNUNET_CRYPTO_EddsaPublicKey providers_public_keys[],
  const struct FROSIX_EncryptionKey *pre_encryption_key,
  unsigned int expiration,
  const struct FROSIX_DkgKeyStoreShare secret_shares[],
  size_t len,
  FROSIX_DkgKeyStoreCallback cb,
  void *cb_cls);


/**
 * Cancel a POST /dkg-key request
 *
 * @param dso the dkg-key request operation
*/
void
FROSIX_dkg_key_store_cancel (
  struct FROSIX_DkgKeyStoreOperation *dko);


/*** AUTH-CHALLENGE API ***/

/**
 * High level ways how an sig-commitment request may conclude.
*/
enum FROSIX_ChallengeRequestStatus
{
  /**
   * Commitment was successfully computed and returned.
  */
  FROSIX_CRS_SUCCESS = 0,

  /**
   * HTTP interaction failed, see HTTP status.
  */
  FROSIX_CRS_HTTP_ERROR,

  /**
   * We had an HTTP 400 error, see details in response body.
  */
  FROSIX_CRS_CLIENT_ERROR,

  /**
   * Server had an internal error.
  */
  FROSIX_CRS_SERVER_ERROR
};


/**
 *
*/
struct FROSIX_ChallengeRequestDetails
{
  /**
   * High level status of the upload operation. Determines @e details.
  */
  enum FROSIX_ChallengeRequestStatus crs;

  /**
   * HTTP status code.
   */
  unsigned int http_status;

  /**
   * Taler error code.
   */
  enum TALER_ErrorCode ec;

  /**
   * Response as JSON
  */
  json_t *response;
};


/**
 * Handle for a POST /auth-challenge operation
*/
struct FROSIX_ChallengeRequestOperation;


/**
 * Callback to process a POST /auth-challenge request
 *
 * @param cls closure
 * @param acd the response body
*/
typedef void
(*FROSIX_ChallengeRequestCallback) (
  void *cls,
  const struct FROSIX_ChallengeRequestDetails *acd);


/**
 * FIXME
*/
struct FROSIX_ChallengeRequestOperation *
FROSIX_auth_challenge_post (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct FROSIX_ChallengeRequestIdP *uuid,
  const struct FROSIX_EncryptionKey *encryption_key,
  const char *auth_method,
  const char *auth_data,
  const struct GNUNET_HashCode *auth_nonce,
  const struct FROST_MessageHash *message_hash,
  FROSIX_ChallengeRequestCallback cb,
  void *cb_cls);

/**
 * Cancel a POST /auth-challenge request
 *
 * @param aco the auth-challenge request operation
*/
void
FROSIX_challenge_request_cancel (
  struct FROSIX_ChallengeRequestOperation *aco);


/*** SIG-COMMITMENT API ***/

/**
 * High level ways how an sig-commitment request may conclude.
*/
enum FROSIX_SigCommitmentStatus
{
  /**
   * Commitment was successfully computed and returned.
  */
  FROSIX_SCS_SUCCESS = 0,

  /**
   * HTTP interaction failed, see HTTP status.
  */
  FROSIX_SCS_HTTP_ERROR,

  /**
   * We had an HTTP 400 error, see details in response body.
  */
  FROSIX_SCS_CLIENT_ERROR,

  /**
   * Server had an internal error.
  */
  FROSIX_SCS_SERVER_ERROR
};


/**
 *
*/
struct FROSIX_SigCommitmentRequestDetails
{
  /**
   * High level status of the upload operation. Determines @e details.
  */
  enum FROSIX_SigCommitmentStatus scs;

  /**
   * HTTP status code.
   */
  unsigned int http_status;

  /**
   * Taler error code.
   */
  enum TALER_ErrorCode ec;

  /**
   * Position of our provider in the array of providers.
  */
  uint8_t array_index;

  /**
   * The returned data, a commitment for all other participants
  */
  struct FROST_Commitment sig_commitment;
};


/**
 * Handle for a POST /sig-commitment operation
*/
struct FROSIX_SigCommitmentRequestOperation;


/**
 * Callback to process a POST /sig-commitment request
 *
 * @param cls closure
 * @param scd the response body
*/
typedef void
(*FROSIX_SigCommitmentRequestCallback) (
  void *cls,
  const struct FROSIX_SigCommitmentRequestDetails *scd);


/**
 * Ask for a commitment to sign a message. Does a POST /sig-commitment/$UUID
 *
 * @param ctx the CURL context used to connect to the backend
 * @param backend_url backend's base URL, including final "/"
 * @param uuid unique identification of the dkg-commitment request
 * @param identifier identifier of the provider
 * @param encryption_key key to decrypt the data stored at the provider
 * @param auth_method
 * @param auth_data
 * @param auth_nonce salt used to hash challenge data
 * @param message_hash our data we want to sign
 * @param cb callback function of the request
 * @param cb_cls closure for the callback function
 * @return handle for the operation
*/
struct FROSIX_SigCommitmentRequestOperation *
FROSIX_sig_commitment_request (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct FROSIX_SigRequestIdP *uuid,
  uint8_t identifier,
  uint8_t index_in_array,
  const struct FROST_HashCode *encryption_key_hash,
  const struct GNUNET_HashCode *auth_data,
  const struct GNUNET_CRYPTO_Edx25519PublicKey *auth_pub,
  const struct GNUNET_CRYPTO_Edx25519Signature *auth_sig,
  const char *auth_method,
  const struct FROST_MessageHash *message_hash,
  FROSIX_SigCommitmentRequestCallback cb,
  void *cb_cls);


/**
 * Cancel a POST /sig-commitment request
 *
 * @param sco the sig-commitment request operation
*/
void
FROSIX_sig_commitment_request_cancel (
  struct FROSIX_SigCommitmentRequestOperation *sco);



/*** SIG-SHARE API ***/

/**
 * High level ways how an sig-share request may conclude.
*/
enum FROSIX_SigShareStatus
{
  /**
   * Signature share was successfully computed and returned.
  */
  FROSIX_SSS_SUCCESS = 0,

  /**
   * HTTP interaction failed, see HTTP status.
  */
  FROSIX_SSS_HTTP_ERROR,

  /**
   * We had an HTTP 400 error, see details in response body.
  */
  FROSIX_SSS_CLIENT_ERROR,

  /**
   * Server had an internal error.
  */
  FROSIX_SSS_SERVER_ERROR
};


/**
 *
*/
struct FROSIX_SigShareRequestDetails
{
  /**
   * High level status of the upload operation. Determines @e details.
  */
  enum FROSIX_SigShareStatus sss;

  /**
   * HTTP status code.
   */
  unsigned int http_status;

  /**
   * Taler error code.
   */
  enum TALER_ErrorCode ec;

  /**
   * Position in the array.
  */
  uint8_t array_index;

  /**
   * The returned data, a share of the final signature
  */
  struct FROST_SignatureShare sig_share;
};


/**
 * Handle for a POST /sig-share operation
*/
struct FROSIX_SigShareRequestOperation;


/**
 * Callback to process a POST /sig-share request
 *
 * @param cls closure
 * @param ssd the response body
*/
typedef void
(*FROSIX_SigShareRequestCallback) (
  void *cls,
  const struct FROSIX_SigShareRequestDetails *ssd);


/**
 * Ask for a share of a signature. Does a POST /sig-share/$UUID
 *
 * @param ctx the CURL context used to connect to the backend
 * @param backend_url backend's base URL, including final "/"
 * @param uuid unique identification of the dkg-commitment request
 * @param identifier identifier of the provider
 * @param encryption_key key to decrypt the data stored at the provider
 * @param challenge_solution solution of the authentication challenge
 * @param message_hash our data we want to sign
 * @param commitments a sorted list of commitments of all participants
 * @param len how many commitments do we have? (should be equal to the threshold value)
 * @param cb callback function of the request
 * @param cb_cls closure for the callback function
 * @return handle for the operation
*/
struct FROSIX_SigShareRequestOperation *
FROSIX_sig_share_request (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct FROSIX_SigRequestIdP *uuid,
  uint8_t provider_index,
  uint8_t array_index,
  const struct FROSIX_EncryptionKey *encryption_key,
  const struct FROST_MessageHash *message_hash,
  const struct FROST_Commitment commitments[],
  size_t len,
  FROSIX_SigShareRequestCallback cb,
  void *cb_cls);


/**
 * Cancel a POST /sig-share request
 *
 * @param sso the sig-share request operation
*/
void
FROSIX_sig_share_request_cancel (
  struct FROSIX_SigShareRequestOperation *sso);


/*** DELETE DKG-KEY API ***/

/**
 * High level ways how an dkg-key delete request may conclude.
*/
enum FROSIX_KeyDeleteStatus
{
  /**
   * Commitment was successfully computed and returned.
  */
  FROSIX_KDS_SUCCESS = 0,

  /**
   * HTTP interaction failed, see HTTP status.
  */
  FROSIX_KDS_HTTP_ERROR,

  /**
   * We had an HTTP 400 error, see details in response body.
  */
  FROSIX_KDS_CLIENT_ERROR,

  /**
   * Server had an internal error.
  */
  FROSIX_KDS_SERVER_ERROR
};


/**
 *
*/
struct FROSIX_KeyDeleteDetails
{
  /**
   * High level status of the upload operation. Determines @e details.
  */
  enum FROSIX_KeyDeleteStatus kds;

  /**
   * HTTP status code.
   */
  unsigned int http_status;

  /**
   * Taler error code.
   */
  enum TALER_ErrorCode ec;
};


/**
 * Handle for a DELETE /dkg-key operation
*/
struct FROSIX_KeyDeleteOperation;


/**
 * Callback to process a DELETE /dkg-key request
 *
 * @param cls closure
 * @param kdd the response body
*/
typedef void
(*FROSIX_KeyDeleteCallback) (
  void *cls,
  const struct FROSIX_KeyDeleteDetails *kdd);


/**
 * Ask to generate and store key material in a distributed key generation. Does a POST /dkg-key/$UUID
 *
 * @param ctx the CURL context used to connect to the backend
 * @param backend_url backend's base URL, including final "/"
 * @param uuid unique identification of the dkg-key request
 * @param cb callback function of the request
 * @param cb_cls closure for the callback function
 * @return handle for the operation
*/
struct FROSIX_KeyDeleteOperation *
FROSIX_key_delete (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct FROSIX_DkgRequestIdP *uuid,
  FROSIX_KeyDeleteCallback cb,
  void *cb_cls);


/**
 * Cancel a DELETE /dkg-key request
 *
 * @param kdo the dkg-key request operation
*/
void
FROSIX_key_delete_cancel (
  struct FROSIX_KeyDeleteOperation *kdo);



#endif
