
\section{Cryptographic Principles}
In this chapter, all relevant cryptographic principles used in Frosix are explained and described.

\subsection{Randomness}
"Any one who considers arithmetical methods of producing random digits is, of course, in
a state of sin." \cite{neumann:1951}

In the field of cryptography, good randomness is crucial \cite[page 3]{vadhan:2012},
but it is difficult for computers to behave unpredictably.
We therefore need some mechanisms which come close to and are indistinguishable from true random:
computational indistinguishability. \cite[chapter 7.1.1]{vadhan:2012}

\subsubsection{Pseudo Random Generator}
A pseudo random number generator (PRG or PRNG) algorithm produces a long sequence of
bytes, for which there exist no efficient algorithm to distinguish it from a
true random sequence. Since an algorithm can not produce true random, a PRG
needs to be seeded with a short and perfectly random input. \cite[page 5]{vadhan:2012}

\textbf{Pseudo Random Function}
\newline To build a pseudo random function (PRF), a PRG can be used. A PRF is a
deterministic function whose output is indistinguishable from true random
and there is no efficient way to gain any information about the input seed,
unless this seed is already known. \cite[page 223]{vadhan:2012}

\subsubsection{Entropy}
Entropy is the measurement of how much randomness is in a random variable.
The notion is always a log to the base 2. For example, an entropy value of $128$
means that there are $2^{128}$ possibilities, all of which are equally likely.
This is also called uniform distribution. \cite[chapter 6.1.2]{vadhan:2012}

\subsection{Hash}
A hash function is technically speaking a compression function and is defined as a
function that takes an input with arbitrary length and returns a fix-sized
output. \cite[chapter 2]{sobti:2012}.
\custind To become useful for cryptography, a hash function must fulfill
following additional properties and is then known as cryptographic hash function \cite[chapter 5]{sobti:2012}:

\begin{itemize}
  \item \textbf{Avalanche Criterion}: A good hash function generates for two different input two completely different outputs,
  regardless how small the difference in the inputs are.
  This results in the avalanche criterion, where changing one bit in the input,
  changes every bit in the output with a probability of 0.5. \cite[chapter 5.2]{sobti:2012}
  \item \textbf{Pre-Image Resistance}: A hash function is said to be pre-image resistant if it is difficult to find the input,
  if only the output is given.
  \newline "Given $H$ and $H(x)$, it is computationally infeasible to find $x$." \cite[chapter 2.1]{sobti:2012}
  Such a hash function is also known as One Way Hash Function.
  \item \textbf{Second Pre-Image Resistance}: To be second pre-image resistant, it must be difficult to find a second input
  for a given input which leads to the same output.
  \newline "it is computationally infeasible to find any second input which has
  the same output as any specified input, i.e., given $x$, to find a 2nd-preimage $x' = x$ such that
  $h(x) = h(x')$." \cite[chapter 3.3]{cryptoeprint:2004/035}
  \item \textbf{Collision Resistance}: The strongest property of a cryptographic hash function is its collision resistance.
  With a collision resistance hash function, it is hard to find two different inputs which lead to the
  same output.
  \newline "it is computationally infeasible to find any two distinct inputs $x$, $x'$ which has the same output, i.e.,
  such that $h(x) = h(x')$." \cite[chapter 9.2.2]{appliedcryptography}
\end{itemize}

\subsubsection{HMAC}
In network communication, the message authentication code (MAC) is used to verify the integrity and authenticity
of received information. A MAC is a "cryptographic checksum", based on a shared key \cite[chapter 1.1]{bellare:1996}.
\custind The hash-based message authentication code (HMAC) scheme is based on the principle of a MAC and utilizes a cryptographic hash function ($H$), with a key ($k$) and a message ($x$) as input:

\begin{center}
  $HMAC_k(x) = H(\overline{k} \oplus opad, H(\overline{k} \oplus ipad, x))$ \cite[chapter 5.1]{bellare:1996}
\end{center}

\subsubsection{HKDF}
Goal of a key derivation function (KDF) is to derive "one or more cryptographically strong secret keys" \cite[chapter 1]{cryptoeprint:2010/264} from a probably not
uniformly distributed key. Thus, a KDF contains two phases. In the extraction phase, a pseudorandom key is extracted and in the second phase expanded to
the desired output length, with the help of a pseudo random function (PRF).
\custind In a hashed key derivation function (HKDF), the HMAC scheme is utilized in the extraction phase to get the pseudorandom key as well as
in the expansion phase as PRF. \cite[chapter 4.2]{cryptoeprint:2010/264}.
\custind Frosix makes intensive use of HKDF functions to deterministically expand high entropy inputs (see chapter \ref{chap:rnd}).

\subsubsection{PBKDF}
A password-based key derivation function (PBKDF) derives a pseudorandom key from a low-entropy password. Unlike a HKDF, a PBKDF scheme uses
an adaptable number of iterations to derive the resulting key, thus making it hard to brute-force the password. \cite[chapter 3]{RFC8018}

\textbf{Argon2}
\newline Argon2 is a PBKDF which aims to "maximize the cost of password cracking on ASICs" \cite[page 2]{argon2}.
Therefore, Argon2 is designed to not only use many iterations
in order to derive a pseudorandom key from a low-entropy password, but at the same time uses a lot of memory, making it "memory-hard" \cite[page 9]{argon2}.
\custind Frosix uses Argon2 (Argon2id) to derive a seed from the secret answer to make brute forcing as inefficient as possible.

\subsection{Public Key Cryptography}
"In a public-key cryptographic scheme, a key pair is selected so that the problem of deriving the
private key from the corresponding public key is equivalent to solving a computational problem
that is believed to be intractable." \cite[chapter 1.2]{Hankerson2004GuideTE}
\custind Public key cryptography relies either on the hardness of the factorization problem (RSA), the discrete logarithm problem (DL),
or the elliptic curve discrete logarithm problem (EC). \cite[chapter 1.2]{Hankerson2004GuideTE}

\subsubsection{Elliptic Curves}
In cryptography, an elliptic curve is built on the finite field $\mathbb{F}_p$,
and is defined by an equation of the form

\begin{center}
  $y^2 = x^3 + ax + b$, where a, b $\in \mathbb{F}_p$ and $4a^3 + 27b^2 \not\equiv 0$ (mod p) \cite[page 13]{Hankerson2004GuideTE}.
\end{center}

Every pair \((x, y) \in \mathbb{F}_p^2\) is a point on the curve if above equation is satisfied. \cite[page 13]{Hankerson2004GuideTE}
Advantages of elliptic curves are their smaller key size, compared to RSA or DSA, to achieve the same security level. \cite[page 19]{Hankerson2004GuideTE}

A finite field is the set $\mathbb{F}$, together with the two operations addition and multiplication, and fulfills
specific properties \cite[chapter 2.1]{Hankerson2004GuideTE}:

\begin{itemize}
  \item \((\mathbb{F}, +)\) is an abelian group with (additive) identity denoted by 0.
  \item \((\mathbb{F} \setminus \{0\}, \cdot)\) is an abelian group with (multiplicative) identity denoted by 1.
  \item The distributive law holds: \((a + b) \cdot c = a \cdot c + b \cdot c\) for all \(a,b,c \in \mathbb{F}\).
  \item If the set \(\mathbb{F}\) is finite, then the field is said to be finite.
\end{itemize}

\textbf{Ristretto255 Group}
\newline The ristretto255 group is an adaption of the Curve25519, "a state-of-the-art elliptic-curve-Diffie-Hellman function
suitable for a wide variety of cryptographic applications" \cite[chapter 1]{10.1007/11745853_14}.
Ristretto is based on a technique called Decaf \cite{cryptoeprint:2015/673} and allows the construction
of prime-order groups from non-prime-order elliptic curves with cofactor 8, such as Curve25519. \cite[chapter 1]{irtf-cfrg-ristretto255-decaf448-07}
\custind As recommended by the authors of FROST, Frosix uses ristretto255 as the underlying prime-order group. \cite[chapter 6]{irtf-cfrg-frost-13}

\subsubsection{ECDHE}
Elliptic Curve Diffie-Hellman Ephemeral (ECDHE) is a key agreement protocol, which allows a shared key to be established over an
untrusted channel. \cite[chapter 6.1]{secg:sec1-v2}
\custind Frosix uses ECDHE to transmit the secret shares in round 2 and 3 of the key generation protocol.

\subsubsection{Digital Signature}
A digital signature is a construct "which associates a message (in digital
form) with some originating entity" \cite[chapter 11.2]{appliedcryptography}.
\custind There exists a lot of different signature schemes, but all of them
consist of three basic algorithms: \cite[chapter 11.2.2]{appliedcryptography}
\begin{itemize}
  \item \textbf{Key Generation}: In a key generation a key pair has to be created. A key pair consists out of a private key
  \(sk\) and a public key \(pk\). The private key is chosen secretly and at random,  or at least derived from high entropy.
  Afterwards, the public key can be derived from the private key.
  \item \textbf{Signature Generation}: With the private key from the previously generated key pair, arbitrary messages can be signed.
  \item \textbf{Signature Verification}: Anybody in possession of the public key and the verification function can now
  verify, whether a signature matches a message.
\end{itemize}

\subsubsection{Schnorr Signature Scheme}
The Schnorr signature scheme is an efficient signature scheme, based on discrete logarithms: \cite{crypto-1989-1727}

\begin{itemize}
  \item primes $p$ and $q$ such that $q$ | $p-1$
  \item $\alpha \in \mathbb{Z}_p$ with order $q$, $\alpha^q = 1$ (mod $p$), $\alpha \neq 1$
  \item \textbf{Key Generation}:
  \begin{enumerate}
    \item Chose private key \(s \in \{1, 2, ..., q\}\).
    \item The corresponding public key \(v\) is the number $v = \alpha^{-s}$ (mod $p$).
  \end{enumerate}
  \item \textbf{Signature Generation}: The key pair $s, v$ and the message $m$ are provided.
  \begin{enumerate}
    \item Pick a random number \(r \in {1, ..., q}\) and compute \(x := \alpha^r\) (mod $p$).
    \item Compute \(e := h(x,m) \in \{0, ..., 2^t-1\}\).
    \item Compute \(y := r + se\) (mod q) and output the signature \((e,y)\).
  \end{enumerate}
  \item \textbf{Signature Verification}: The signature ($e$, $y$) for message $m$ and the public key $v$ are provided.
  \begin{enumerate}
    \item Compute \(\overline{x} = \alpha^y v^e\) (mod p).
    \item Check that \(e = h(\overline{x},m)\).
  \end{enumerate}
\end{itemize}

Due to a patent, which expired in 2008, Schnorr signature schemes were not common for a long time.

\textbf{EdDSA}
\newline Nowadays, the Edwards-curve Digital Signature Algorithm (EdDSA) \cite{rfc8032},
a deterministic variant of the Schnorr Signature Scheme
on elliptic Edwards-curve, is standardized and widely used.
\custind Frosix uses EdDSA signatures to let each involved provider sign the resulting public key in the key generation process.

\subsubsection{Secret Sharing}
\textbf{Shamir's Secret Sharing}
\newline Shamir's secret sharing is a method to divide some data D into n parts, based on polynomial interpolation
and has the following two properties \cite{10.7551/mitpress/12274.003.0048}:

\begin{enumerate}
  \item knowledge of any \(k\) or more \(D_i\) pieces makes \(D\) easily computable
  \item knowledge of any \(k - 1\) or fewer \(D_i\) pieces leaves \(D\) completely undetermined (in the sense that all its possible
  values are equally likely)
\end{enumerate}

\textbf{Feldman's Verifiable Secret Sharing}
\newline In Shamir's secret sharing a party can not verify if the received piece is valid.
In Feldman's verifiable secret sharing \cite{10.1109/SFCS.1987.4},
the \gls{trusted dealer} first publishes a commitment of the secret, with which each receiving party can test,
whether the afterwards sent piece is a valid part of that secret. \cite[chapter 3.2]{10.1109/SFCS.1987.4}

\subsubsection{Threshold Signature Scheme}
In a threshold signature scheme, the private key is distributed among $n$ parties, such that \(k \leq n\) parties can collaborate to generate
a valid signature.
To achieve a signature system, wherein the signing key is not combined in any single place at any time, it is mandatory to
also generate the key pair with a multiparty algorithm in a distributed key generation process. \cite[chapter 1]{cryptoeprint:2020/1390}

\subsubsection{Distributed Key Generation}
\textbf{Pedersen's Distributed Key Generation}
\newline In order to perform a key generation among a group, Torben Pedersen introduced a distributed key generation protocol (DKG) \cite{10.1007/3-540-46416-6_47},
based on verifiable secret sharing.
\custind In Pedersen's distributed key generation, each party performs a verifiable secret sharing, thus acts as a \gls{trusted dealer}.
After publishing all commitments, each party sends to every other party a secret share, like in Shamir's secret sharing.
The final share of the private key is the combinations of a self-generated secret share, combined with the secret shares received from all other parties.
\custind Therefore, two main properties can be concluded \cite[chapter 5]{10.1007/3-540-46416-6_47}:

\begin{enumerate}
  \item A trusted party for selecting and distributing the secret is no longer needed.
  \item Each member of the group can verify that his share of the secret key corresponds to the public key.
\end{enumerate}

\section{FROST Signature Scheme}
FROST is a Flexible Round-Optimized Schnorr Threshold Signature scheme.
It is optimized to be as efficient as possible, without restrictions to the security properties.
Although it can be instantiated as a single-round protocol, with a pre-processing stage,
in Frosix, FROST is instantiated as the two-round protocol variant.
\custind Signatures produced by FROST signing operations are verifiable using the standard Schnorr verification operation
and use a signature format similar to EdDSA. \cite{cryptoeprint:2020/852}

\subsubsection{FROST Key Generation}
Like many other threshold signature protocols, FROST builds upon Pedersen's DKG for key generation,
but adds a \gls{zero knowledge proof} to protect against rogue key attacks.
The zero knowledge proof is a Schnorr signature on its own, using the secret value from the verifiable secret sharing as private key.
\custind Whereas this key generation protocol is originally designed to be a two-round protocol,
in Frosix, the distributed key generation is a three-round protocol,
due to the tunneling of all communication through the Frosix client.

Below is the original \textit{FROST KeyGen} \cite[page 12]{cryptoeprint:2020/852}:

\begin{center}
  \textbf{Round 1}
\end{center}

{\large \(P_i\)}
\par \textbf{Step 1}: Every participant \(P_i\) samples \(t\) random values and uses these values as coefficients to define a degree \(t-1\) polynomial.

{\small $\displaystyle (a_{i0}, ..., a_{i(t-1)}) \xleftarrow{\$} \mathbb{Z}_q$} \\
{\small $\displaystyle f_i(x) \ =\ \sum _{j=0} ^{t-1} a_{ij}x^j$}

\textbf{Step 2}: Every \(P_i\) computes a proof of knowledge to the corresponding secret \(a_{i0}\).

{\small $\displaystyle k \xleftarrow{\$} \mathbb{Z}_q$} \\
{\small $\displaystyle R_i \ =\ g^k$} \\
{\small $\displaystyle c_i \ =\ H(i, \$context\_string, g^{a_{i0}}, R_i)$} \\
{\small $\displaystyle z_i \ =\ k + a_{i0} \cdot c_i$} \\
{\small $\displaystyle \sigma_i \ =\ (R_i, z_i)$}

\textbf{Step 3}: Every participant \(P_i\) computes a public commitment \(C\).

{\small $\displaystyle \phi_{ij} \ =\ g^{ij}, 0 \leq j \leq t-1$} \\
{\small $\displaystyle \vec{C_i} \ =\ \langle \phi_{i0}, ..., \phi_{i(t-1)} \rangle$}

\textbf{Step 4}: Every \(P_i\) broadcasts \(C_i, \sigma_i\) to all other participants.

\begin{center}
  {\small $\displaystyle (C_i, \sigma_i) \longrightarrow $}
\end{center}

\textbf{Step 5}: Upon receiving \(C_l, \sigma_l\) from participants \(1 \leq l \leq n, l \neq i\), each participant \(P_i\) verifies \(\sigma_l\).

{\small $\displaystyle c_l \ =\ H(l, \$context\_string, \phi_{l0}, R_l)$} \\
{\small $\displaystyle R_l \overset{?}{=} g^{z_l} \ \cdot \phi_{l0}^{-c_l}$}

\begin{center}
  \textbf{Round 2}
\end{center}

{\large \(P_i\)}
\par \textbf{Step 1}: Each \(P_i\) securely sends a secret share ($l, f_i(l)$) to each other participant $P_l$.

\begin{center}
  {\small $\displaystyle (l, f_i(l)) \longrightarrow$}
\end{center}

\textbf{Step 2}: Each \(P_i\) verifies their received shares.

{\small $\displaystyle g^{f_l(i)} \ \overset{?}{=} \ \prod _{k=0} ^{t-1} \phi_{lk}^{i^k mod \ q}$}

\textbf{Step 3}: Each \(P_i\) calculates their long-lived private signing share \(s_i\).

{\small $\displaystyle s_i \ =\ \sum _{l=1} ^n f_l(i)$}

\textbf{Step 4}: Each \(P_i\) calculates their public verification share \(Y_i\) and the group's public key \(Y\).

{\small $\displaystyle Y_i \ =\ g^{s_i}$} \\
{\small $\displaystyle Y \ =\ \prod _{j=1} ^n \phi_{j0}$}

\subsubsection{FROST Sign}
Since Frosix uses the two-round variant of FROST Sign,
round 1 represents a single preprocess operation.
Otherwise, the protocol below is as unmodified as possible. \cite[page 12, 15]{cryptoeprint:2020/852}

\begin{center}
  \textbf{Round 1}
\end{center}

{\large $SA$} \hfill {\large $\displaystyle P_{i}{}_{\in}{}_{S}$}
\par \textbf{Step 1}: In the first round of FROST Sign, the signature aggregator (SA) chooses the set $S$, $k$ of $n$ parties.

{\small $\displaystyle S = \{P_1,...,P_k\}, 0 < P_i \leq n$}

\textbf{Step 2}: Each party (\(P_i\)) samples two random commitment values and sends those to the SA.

\begin{flushright}
  {\small $\displaystyle (d_i,e_i) \xleftarrow{\$} \mathbb{Z}^*_q \times \mathbb{Z}^*_q$} \\
  {\small $\displaystyle (D_i,E_i) \ = \ (g^{d_i},g^{e_i})$}
\end{flushright}

\begin{center}
  {\small $\displaystyle \longleftarrow (D_i,E_i)$}
\end{center}

\begin{center}
  \textbf{Round 2}
\end{center}

{\large $SA$} \hfill {\large $\displaystyle P_{i}{}_{\in}{}_{S}$}

\textbf{Step 1}: The SA fetches all commitments, builds the set $B \ = \ \langle i,D_i,E_i \rangle, i \in S$ and sends $B$ together with the message $m$ to all parties in $S$.

{\small $\displaystyle B \ =\ \langle ( i,D_{i} ,E_{i})\rangle _{i}{}_{\in }{}_{S}$}

\begin{center}
  {\small $\displaystyle ( m,\ B) \longrightarrow $}
\end{center}

\textbf{Step 2}: After receiving $(m,\ B)$, each $P_i$ validates the message $m$, and then checks $D_l,E_l \in \mathbb{G}^*$ for each commitment in $B$, aborting if either check fails.

\textbf{Step 3}: Each $P_i$ then computes the set of binding values.

\begin{flushright}
  {\small $\displaystyle p_l \ = \ H_1(l,m,B), l \in S$}
\end{flushright}

\textbf{Step 4}: Each $P_i$ then derives the group commitment $R$ and the challenge $c$.

\begin{flushright}
  {\small $\displaystyle R \ =\ \prod _{l \in S} D_l \cdot (E_l)^{\rho_l}$} \\
  {\small $\displaystyle c \ =\ H_2(R,Y,m)$}
\end{flushright}

\textbf{Step 5}: Each $P_i$ computes their response $z_i$ using their long-lived secret share $s_i$.

\begin{flushright}
  {\small $\displaystyle z_i \ =\ d_i + (e_i \cdot \rho_i) + \lambda_i \cdot s_i \cdot c$}
\end{flushright}

\textbf{Step 6}: Each $P_i$ securely deletes $(d_i,e_i)$ from their local storage, and then returns $z_i$ to SA.

\begin{center}
  {\small $\displaystyle \longleftarrow z_i$}
\end{center}

\textbf{Step 7}: The signature aggregator SA calculates the set of binding values \(p_l\),
the set of individual commitments \(R_l\), the group commitment \(R\) and the challenge \(c\).
With these values, the SA verifies the validity of each response \(z_i\).

{\small $\displaystyle p_l \ =\ H_1(l,m,B), l \in S$} \\
{\small $\displaystyle R_l \ =\ D_l \cdot (E_l)^{\rho_l}, l \in S$}

{\small $\displaystyle R \ =\ \prod _{l \in S} R_l$} \\
{\small $\displaystyle c \ =\ H_2(R,Y,m)$}

{\small $\displaystyle g^{z_i} \overset{?}{=} R_i \cdot Y_i^{c \cdot \lambda_i}$}

\textbf{Step 8}: The signature aggregator SA computes the resulting signature.

{\small $\displaystyle z = \sum z_i$} \\
{\small $\displaystyle \sigma \ =\ (R, z)$}

\newpage
\section{Authentication}
Authentication makes up a big part of Frosix.
Fortunately, Frosix can build upon the know-how and implementation of GNU Anastasis.
At the moment, GNU Anastasis supports the following authentication methods: \cite{anastasis}

\begin{itemize}
  \item Security Question
  \item SMS
  \item Email
  \item Post
  \item Bank Transfer
  \item TOTP (Time-based one-time password)
\end{itemize}

\section{Other Implementations}
Frosix is not the first to implement the FROST signature scheme.
But Frosix is one of the first FROST based application implementing a REST API,
and probably unique, considering the integration of additional authentication.

Besides the reference implementation \cite{ckomlo:frost}, which provides a simple Rust based library, there are some further libraries available on GitHub.
Most of them are written in Rust or Go and therefore unsuitable to be used in Frosix.
\newline The following list is not exhaustive.

\begin{itemize}
  \item \textbf{Zcash Foundation} \cite{zcash:frost}: Rust based library which provides a high level API to work with various ciphers
  \item \textbf{Coinbase} \cite{coinbase:frost}: Go based library, which has been archived and is no longer supported from the authors
  \item \textbf{Taurus} \cite{taurusgroup:frost}: Go based library with focus on compatibility on Ed25519 (EdDSA), is not under active development for two years
  \item \textbf{Isis Agora Lovecruft} \cite{lovecruft:frost}: Rust based library which is not under development for two years
  \item \textbf{Cloudflare} \cite{cloudflare:frost}: a feature request for the Cloudflare Interoperable Reusable Cryptographic Library (CIRCL), written in GO
  \item \textbf{Trust-Machines} \cite{trust-machines:frost}: a library which provides an implementation of the original FROST as well as an implementation of Weighted Threshold FROST (see chapter~\ref{chap:future_dev}) in Rust
\end{itemize}

\section{Future Development}\label{chap:future_dev}
There is ongoing research, and standardization efforts on FROST and threshold signature schemes in general.

\textbf{FROST as an Internet Draft}
\newline At the moment, FROST is an "Active Internet Draft" at the Internet Engineering Task Force (IETF). \cite{irtf-cfrg-frost-13}
This helped a lot during the implementation of FROST,
because it provides pseudocode for all necessary functions.

\textbf{NIST First Call for Multi-Party Threshold Schemes}
\newline Also NIST is highly interested in standardizing threshold crypto system, among others for signing. \cite{Peralta_Brandão_2023}

\textbf{Weighted Threshold FROST: WTF}
\newline The idea of weighted threshold FROST is to optimize the required bandwidth when FROST parties control multiple
keys. \cite{weighted-frost}

\textbf{Threshold Schnorr with Stateless Deterministic Signing}
\newline In contrast to some single-signer signature schemes, like for example EdDSA, it is not trivial to implement
deterministic signatures in a threshold environment.
This is due to the danger of nonce reuse. \cite{10.5555/648120.747057}
Although there are already first approaches \cite{cryptoeprint:2021/1055}, the implementation of a stateless
deterministic threshold signing should be carefully considered.

\textbf{FROST2}
\newline There is already an optimized version of FROST, FROST2. \cite{cryptoeprint:2021/1375}
With FROST2, the number of exponentiation required for signing and verification are reduced from linear to constant in the number of signers.

\textbf{FROST3}
\newline Another paper proposes FROST3, RObust Asynchronous Threshold signatures (ROAST). 
ROAST is a wrapper protocol and can be instantiated with FROST.
The authors claim to "obtain the first (nontrivial) asynchronous Schnorr threshold signature protocol"
and that ROAST is also the "first robust protocol that can be setup to guarantee unforgeability against a
dishonest majority ($t-1 \geq n/2$)" \cite{10.1145/3548606.3560583}.
\custind What this means exactly and whether Frosix could benefit from ROAST was not further investigated.

The further development of FROST and its variants remains exciting!
