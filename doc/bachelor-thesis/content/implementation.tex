Even though C has lost popularity in recent years, Frosix is free software, written in C.
Like every programming language, C has advantages and disadvantages.
\custind The main disadvantages are the non-trivial memory management,
the freedom that everything is just a pointer and that out-of-bound memory access is not checked.
\custind Therefore, during the development of Frosix, care was taken to create \glspl{struct} where possible,
to check against the size of an array before accessing it and to ensure,
that no substantial memory leak or use after free happens.
Especially, because it is nearly impossible for someone with little development experience to write bug-free software,
minor and probably also major bugs exist.
\custind On the other hand, Frosix benefits a lot from rich and performant libraries written in C.
As it was planned from the beginning to reuse parts of the codebase from GNU Anastasis,
the question of the programming language has never arisen.

\par Frosix is organized in different components. Figure~\ref{fig:system_comp_overview} gives an overview.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_system_overview}
    \caption{System components overview}
    \label{fig:system_comp_overview}
\end{figure}

The middle box contains the core implementation of Frosix, including the implementation of the FROST protocol in the library libfrost.
At the bottom are the utilized external libraries
and on the left side are the implemented authentication methods.

\subsubsection{External Libraries}
\begin{itemize}
  \item \textbf{Libsodium}: Sodium is a modern library for virtually all cryptographic tasks.
  It is a fork of the well-known library NaCl, which is a library with implementations from reputable cryptographers.
  And most importantly for Frosix, Sodium provides an implementation of the ristretto255 group. \cite{libsodium}
  \item \textbf{Libgnunet}: GNUnet is not just a library, it is a framework for P2P networking. However,
  Frosix only uses some of GNUnet's foundational libraries, and not the actual P2P networking stack.
  The GNUnet libraries offer great functionality for logging, parsing from and to JSON (based on Libjansson)
  and provide some elegant to use cryptographic operations without the need of additional wrapping (like with Libsodium).  \cite{libgnunet}
  \item \textbf{Libcurl}: libcurl provides functionalities for the basic network communication between a Frosix client and a \Gls{provider}.
  It is probably the most used library for this purpose. \cite{libcurl}
  \item \textbf{Libmicrohttpd}: The \Glspl{provider} are based on GNU libmicrohttpd, a very lightweight HTTP server offering great flexibility. \cite{libmicrohttpd}
  \item \textbf{PostgreSQL}: PostgreSQL serves as the database for a \Gls{provider}.
  It is open source, widely used and is being developed very actively, making it the default choice if there are no special requirements. \cite{postgresql}
  \item \textbf{Libjansson}: Jansson is nowadays the standard library in C based project, if they work with JSON data.
  Frosix not only utilizes Jansson to parse from and to JSON data in the network communication, but also to parse all from the CLI incoming and outputting data. \cite{libjansson}
  \item \textbf{Libtaler}: Despite the payment system GNU Taler not being integrated in the current version of Frosix,
  some of GNU Taler's libraries are already in use because they provide additional useful functionalities. \cite{Libtaler}
\end{itemize}

\newpage
\section{System Architecture} \label{chap:sys_arch}
A general overview of the architecture of Frosix is shown in Figure~\ref{fig:system_arch_overview}.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_system_architecture}
    \caption{System architecture overview}
    \label{fig:system_arch_overview}
\end{figure}

\subsubsection{Frosix Files}
Every input and output to and from the CLI is in the form of JSON data, except purely informative messages.
Those inputs and outputs can be stored in various files, depending on the purpose.
The files can then easily be manipulated, copied, deleted or even encrypted, by hand or in an automated way.

\subsubsection{Command Line Interface}
The command line interface (CLI) of Frosix exposes all implemented functionality in various commands.
It allows the use of Frosix from a shell.

\subsubsection{Libfrosix}\label{impl:libfrosix}
In libfrosix, the input from the CLI is parsed into the different variables.
Furthermore, libfrosix is responsible for gathering and maintaining all required information,
triggering the necessary calls from the RESTclient, and handling the corresponding responses.
Also libfrosix provides the link to libfrost, in order to execute the steps from the FROST protocol.
If something goes wrong, it is up to libfrosix to abort the current command,
clean up everything and return a useful error message back to the user.

\subsubsection{Libfrost}
Libfrost is the implementation of the FROST protocol and provides an easy-to-use high level API.
Thus, libfrosix does not have to care about the complicated cryptography hidden inside the FROST protocol.
The second part of libfrost is the low level wrapping of all utilized functions from libsodium.
This is due to the fact that libsodium does not offer any typing, and therefore it is not safe to use.

\subsubsection{RESTclient}\label{impl:restclient}
As a counterpart to every REST endpoint from the Frosix Webserver,
there is an implementation of a REST startpoint on the client side.
The RESTclient encodes the inputs of each request into JSON data and sends them to a REST endpoint from a \Gls{provider}.
If there is a response, the RESTclient is responsible to decode the JSON data and pass them back to libfrosix.

\subsubsection{REST API}
The REST API is an instantiation of libmicrohttpd. It receives and forwards incoming request to the matching handlers.

\subsubsection{REST API Handlers}
There is a separate handler for every implemented REST endpoints.
The handlers include the main logic of a \Gls{provider}.
They parse the incoming data, do the verification, load and store data from the database via the database plugin
and use libfrost in order to perform the FROST protocol computation.

\subsubsection{Database}
Frosix uses a PostgreSQL database to store data both for a short duration and long-term.
All stored data have an expiration date and get eventually deleted with a regularly running garbage collection task.

\newpage
\section{Server Architecture}
A \Gls{provider} consists of a small webserver (libmicrohttpd),
some handlers to handle all the different requests,
a plugin to access the database,
a plugin to handle the different authentication methods
and libfrost (Figure~\ref{fig:server_arch}).

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_server_architecture}
    \caption{Frosix server architecture}
    \label{fig:server_arch}
\end{figure}

\subsubsection{REST API}
For detailed information about Frosix's RESTful API, see appendix A.

\subsubsection{Handlers}
\textbf{Terms Handler}
\newline Returns the terms of service of a \Gls{provider}.
At the moment, the terms of service are just a copy of the terms of service from GNU Anastasis \cite{anastasis} and must be adapted to Frosix.

\textbf{Config Handler}
\newline Returns the current configuration of a \Gls{provider}.
This includes supported authentication methods, public provider salt, public key and some more information.

\textbf{Seed Handler}
\newline Returns some high entropy bytes, which are used in the key generation process,
thus counteracting bad PRNG on client side.

\textbf{KeyGen Handlers}
\newline There are three sub-handlers, one for each round of the key generation protocol:
\begin{itemize}
  \item \textbf{Dkg-Commitment}: Returns a deterministically derived commitment.
  Whereas the size of such a request increases linearly with number of participating providers,
  the size of a commitment increases linearly with the selected threshold value.
  \item \textbf{Dkg-Shares}: Returns an encrypted share of a private key share for each other \Gls{provider}.
  The size of this request can quickly escalate, because it linearly depends on the number of
  participating \Glspl{provider}, as well as on the selected threshold value.
  The size of the response, however, depends solely on the number of involved \Glspl{provider}.
  \item \textbf{Dkg-Key}: Returns the final public key and a signature over the public key and the submitted authentication value (a salted hash).
  Only the size of the request to this handler is dependent on the number of involved \Glspl{provider}.
\end{itemize}

\textbf{Sign Handlers}
\newline There are two sub-handlers, one for each phase:
\begin{itemize}
  \item \textbf{Sig-Commitment}: Returns a commitment, derived from a random nonce and the submitted hash of the message.
  \item \textbf{Sig-Share}: Returns the signature share.
  Both the size of the request and of the response are linearly increasing with the threshold value.
\end{itemize}

\textbf{Key Delete Handler}
\newline Allows to delete the data stored in the database of a \Gls{provider}.

\textbf{Challenge Handler}
\newline A request to this handler triggers a challenge-code, which then is transferred over a different channel,
if the hash of the submitted authentication data and nonce match the stored hash.

\newpage
\section{Database}
The database schema of Frosix is shown below (Figure~\ref{fig:database_schema}).

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_database}
    \caption{Frosix database schema}
    \label{fig:database_schema}
\end{figure}

\subsubsection{Key}
This is the main table where all essential data used for authentication and signing is stored.

\begin{itemize}
  \item \textbf{key\_id}: The identifier of key data is the hash of the encryption key.
  \newline \(key\_id = H(encryption\_key)\)
  \item \textbf{identifier}: The identifier is a unique number in the signing group.
  This value can be anything between 1 and 254.
  \item \textbf{expiration}: The expiration field contains a number indication for how many years a \Gls{provider} should store this entry.
  \item \textbf{encryption\_nonce}: This field represents a high entropy value used in the encryption of the \textit{enc\_key\_data}.
  \item \textbf{enc\_key\_data}: This is the encrypted key data, including the private key share and the public key.
  \item \textbf{auth\_data}: The authentication data is a salted hash over a phone number, email address, postal address etc.
\end{itemize}

\subsubsection{Public\_Commitments}
The table \textit{pbulic\_commitments} is only used during a key generation process and stores the commitments of all other \Glspl{provider} between round 2 and 3.

\begin{itemize}
  \item \textbf{dkg\_id}: The identifier of some stored public commitments is the ID of the corresponding \textit{dkg-*} request (see appendix~\ref{appendix_a} for more details about the request IDs).
  \item \textbf{time\_stamp}: The time when the entry was created.
  This value helps to clean up the database regularly, without deleting entries which are still in use.
  \item \textbf{commitments}: This field stores a blob with the commitments from all other \Glspl{provider}.
\end{itemize}

Since there can be up to 253 commitments with each up to 253 values with a size of 32 bytes each, this table can grow quickly.
However, the usage of these values is for a short time, a few minutes at most.
This table should therefore be cleaned up regularly.

\subsubsection{Challengecode}
In contrast to the table \textit{public\_commitment}, the lifetime of these values is measured in days.
This table and the corresponding logic are part of the code reuse from GNU Anastasis \cite{anastasis} and are therefore not explained in detail.

\subsubsection{Seed}
This table stores the \gls{seed} which was used to generate the commitment in the first round of the signing process.
As with the entries in \textit{public\_commitments}, these values have a very short lifetime.

\begin{itemize}
  \item \textbf{seed\_id}: The identifier of a seed is the hash over the hash of the encryption key and the hash of the message
  \newline \(seed\_id = H(H(encryption\_key) || H(message))\).
  \item \textbf{seed}: This field contains the freshly and random generated high entropy nonce.
  \item \textbf{time\_stamp}: The time when this entry was created.
  This value helps to clean up the database regularly, without deleting entries which are still in use.
\end{itemize}

There is no relation between the tables, because every table serves a different purpose
and the identifier of an entry is related to corresponding data in a request, except for the key table.

\section{Authentication Methods}
Following authentication methods are implemented, or can at least be integrated with little effort\footnote{The approach of challenge-code based authentication methods is implemented.}:

\begin{itemize}
  \item \textbf{SMS}: A challenge-code, together with the hash of the message is sent in an SMS to the matching phone number.
  \item \textbf{Email}: A challenge-code, together with the hash of the message is sent in an email to the matching email address.
  \item \textbf{Mail}: A challenge-code, together with the hash of the message is sent in a letter to the matching postal address.
  \item \textbf{Security Question}: Does not involve a challenge-code (see chapter \ref{chap:sec_question}).
\end{itemize}

\section{Client Architecture}
Frosix client consists out of three modules:
\begin{itemize}
  \item Command line interface (CLI)
  \item Libfrosix
  \item RESTclient
\end{itemize}

The functionalities and responsibilities of Libfrosix and the RESTclient are already explained in chapter \ref{chap:sys_arch}.

\subsubsection{Command Line Interface}
The command line interface exposes useful commands to the user.

\textbf{Available Commands and Options}
\begin{itemize}
  \item \textit{keygen -o signing\_document(.json) < providerlist(.json)}:
  \newline takes a valid Frosix providerlist as input and does a distributed key generation with all listed \Glspl{provider}
  \item \textit{export-pk -o public\_key(.json) < signing\_document(.json)}:
  \newline takes a Frosix signing document and exports the public key and the signature of each involved \Gls{provider}
  \item \textit{verify-pk < public\_key(.json)}:
  \newline verifies all signatures in a Frosix public key document
  \item \textit{request-challenge -a '{"provider\_index":\$provider\_index}' -m "\$message" < signing\_document(.json)}:
  \newline requests a challenge-code for the specified \Gls{provider}
  \item \textit{sign -a '{"1":"\$solution1", "2":"\$solution2"}' -m "\$message" -o signature(.json) < signing\_document(.json)}:
  \newline signs a message with the provided challenge-codes or a secret answer
  \item \textit{verify -m "\$message" < signature(.json)}:
  \newline verifies a signature against a message
  \item \textit{delete-key < signing document(.json)}:
  \newline requests the deletion of all data stored at each \Gls{provider} from the provided Frosix signing document
  \item \textit{-o} (output):
  \newline writes the output to a file with the specified name
  \item \textit{-a} (argument):
  \newline submit further arguments, formatted as JSON
  \item \textit{-m} (message):
  \newline submit a message to sign (will always get hashed before signing)
  \item \textit{<} (shell input redirection):
  \newline redirects the content of a document as input to a command
  \item \textit{\$provider\_index}:
  \newline a value between 1 and 254 and must match a \Gls{provider} in the signing document (value is written without surrounding quotes)
  \item \$message:
  \newline the message that is to be signed or is already signed
\end{itemize}

\subsection{Frosix Files}
This chapters explains the use and structure of all Frosix files in detail.

\newpage
\subsubsection{Frosix Provider List}
A Frosix providerlist includes all necessary information to start a Frosix key generation process.
Below is a minimal example of a \textit{Frosix providerlist}.
Technically, it is possible to use only two \Glspl{provider} and a \textit{threshold-value} of 1.

\begin{verbatim}
{
  "threshold": 2,
  "expiration": 5,
  "providers": [
    {
      "url": "$PROVIDER_URL_1",
      "public_key": "$PROVIDER_PUBLIC_KEY_1",
      "auth_method": "sms",
      "auth_data": "$PHONE_NUMBER"
    },
    {
      "url": "$PROVIDER_URL_2",
      "public_key": "$PROVIDER_PUBLIC_KEY_2",
      "auth_method": "question",
      "auth_data":  "The answer to life, the universe and everything",
      "auth_answer": "42"
    },
    {
      "url": "$PROVIDER_URL_3",
      "public_key": "$PROVIDER_PUBLIC_KEY_3",
      "auth_method": "email",
      "auth_data":  "$EMAIL_ADDRESS",
    }
  ]
}
\end{verbatim}

\textbf{Frosix provider list values explained}
\begin{itemize}
  \item \textit{threshold}: defines the \textit{threshold-value}.
  \item \textit{expiration}: number of years until a \Gls{provider} should delete the private key share
  \item \textit{providers}: list of chosen \Glspl{provider}, defines total number of providers at the same time (\Glspl{provider} may occur more than once)
  \item \textit{url}: URL under which this \Gls{provider} is reachable
  \item \textit{public\_key}: public key of this provider, to detect if someone is impersonating a \Gls{provider}
  \item \textit{auth\_method}: method, which should be used for authentication before issuing a signature
  \item \textit{auth\_data}: corresponding authentication data like a phone number or an email address
  \item opt. \textit{auth\_answer}: hash of a public key, which is derived from the secret answer (only if auth\_method is \textit{question}).
\end{itemize}

Note: A \textit{Frosix provider list} can and should be deleted after being successfully used in Frosix key generation,
as it can contain sensitive information like the secret answer to a security questions.

\subsubsection{Frosix Signing Document}
A Frosix signing document contains all data required to request a challenge,
to start a Frosix sign process, to export the public key
and to delete the private key shares.

\begin{verbatim}
  {
    "public_key": "$PUBLIC_KEY",
    "number_of_participants": 3,
    "threshold": 2,
    "providers": [
      {
        "provider_index": 1,
        "provider_name": "$PROVIDER_NAME_1",
        "backend_url": "$PROVIDER_URL_1",
        "encryption_key": "$ENCRYPTION_KEY_1",
        "auth_method": "sms",
        "auth_data": "$PHONE_NUMBER",
        "auth_nonce": "$AUTH_NONCE_1",
        "auth_hash": "$AUTH_HASH_1",
        "provider_signature": "$PROVIDER_SIGNATURE_1"
        "provider_public_key": "$PROVIDER_PUBLIC_KEY_1"
      },
      {
        "provider_index": 2,
        "provider_name": "$PROVIDER_NAME_2",
        "backend_url": "$PROVIDER_URL_2",
        "encryption_key": "$ENCRYPTION_KEY_2",
        "auth_method": "question",
        "auth_data": "The answer to life, the universe and everything",
        "auth_nonce": "$AUTH_NONCE_2",
        "auth_hash": "$AUTH_HASH_2",
        "provider_signature": "$PROVIDER_SIGNATURE_2"
        "provider_public_key": "$PROVIDER_PUBLIC_KEY_2"
      },
      {
        "provider_index": 3,
        "provider_name": "$PROVIDER_NAME_3",
        "backend_url": "$PROVIDER_URL_3",
        "encryption_key": "$ENCRYPTION_KEY_3",
        "auth_method": "email",
        "auth_data": "$EMAIL_ADDRESS",
        "auth_nonce": "$AUTH_NONCE_3",
        "auth_hash": "$AUTH_HASH_3",
        "provider_signature": "$PROVIDER_SIGNATURE_3"
        "provider_public_key": "$PROVIDER_PUBLIC_KEY_3"
      }
    ]
  }
\end{verbatim}

\textbf{Frosix signing document's values explained}\label{sec:frosix_sign_document}
\begin{itemize}
  \item \textit{public\_key}: the public key to verify a signature, which is created based on this Frosix signing document
  \item \textit{number\_of\_participants}: this value could technically be omitted and is therefore just supporting information
  \item \textit{threshold}: the threshold value which defines how many \Glspl{provider} are necessary to issue a valid signature
  \item \textit{providers}: list of all \Glspl{provider}, which were part of the preceding Frosix key generation
  \item \textit{provider\_index}: unique identifier of this \Gls{provider} in the group, a value between 1 and 254
  \item \textit{provider\_name}: human-friendly name of this \Gls{provider} (received from a GET /config request)
  \item \textit{backend\_url}: URL under which this provider is reachable
  \item \textit{encryption\_key}: this value is needed to find the corresponding encrypted private key share in the \Gls{provider} 's database and to decrypt that private key share
  \item \textit{auth\_method}: method which is used for authentication
  \item \textit{auth\_data}: data which corresponds to the chosen authentication method (phone number, email address, etc.)
  \item \textit{auth\_nonce}: salt which was used to hash the authentication data
  \item \textit{auth\_hash}: the resulting hash
  \newline Note: the authentication hash is not absolutely necessary,
  except in the case where the authentication method is of type \textit{question} (since we do not want to store the secret answer in plaintext).
  \item \textit{provider\_signature}: a signature from this \Gls{provider} over the \textit{public\_key} and the \textit{auth\_hash}
  \item \textit{provider\_public\_key}: public key of this \Gls{provider} which was used to create the \textit{provider\_signature} (public key is received from a GET /config request)
\end{itemize}

Note: Even though the security of Frosix does not rely on the confidentiality of a Frosix signing document,
it should nevertheless be handled like a private signing key.
In case of disclosure, the chosen \textit{threshold-value} and the possible combinations of authentication methods can still provide strong protection.
\custind In such a situation we recommend deleting all private key shares as soon as possible and creating a new Frosix signing document.

\subsubsection{Frosix Public key}
Unlike a Frosix signing document, a Frosix public key can and should be published to those, who have to verify our signatures.

\begin{verbatim}
{
  "public_key": "$PUBLIC_KEY",
  "providers": [
    {
      "provider_name": "$PROVIDER_NAME_1",
      "backend_url": "$PROVIDER_URL_1",
      "auth_hash": "$AUTH_HASH_1",
      "provider_signature:" "$PROVIDER_SIGNATURE_1",
      "provider_public_key": "$PROVIDER_PUBLIC_KEY_1"
    },
    {
      "provider_name": "$PROVIDER_NAME_2",
      "backend_url": "$PROVIDER_URL_2",
      "auth_hash": "$AUTH_HASH_2",
      "provider_signature:" "$PROVIDER_SIGNATURE_2",
      "provider_public_key": "$PROVIDER_PUBLIC_KEY_2"
    },
    {
      "provider_name": "$PROVIDER_NAME_3",
      "backend_url": "$PROVIDER_URL_3",
      "auth_hash": "$AUTH_HASH_3",
      "provider_signature:" "$PROVIDER_SIGNATURE_3",
      "provider_public_key": "$PROVIDER_PUBLIC_KEY_3"
    }
  ]
}
\end{verbatim}

For a detailed description of those values, see chapter~\ref{sec:frosix_sign_document}.

\subsubsection{Frosix signature}

\begin{verbatim}
{
  "message_hash": "$MESSAGE_HASH",
  "public_key": "$PUBLIC_KEY",
  "signature_r": "$SIGNATURE_R_VALUE",
  "signature_z": "$SIGNATURE_Z_VALUE"
}
\end{verbatim}

\textbf{Frosix signature's values explained}\
\begin{itemize}
  \item \textit{message\_hash}: hash of the signed message, not absolutely necessary since the message itself always has to be submitted to verify the signature
  \item \textit{public\_key}: public key which corresponds to the private key used to issue this signature
  \item \textit{signature\_r}: first part of the signature
  \item \textit{signature\_z}: second part of the signature
\end{itemize}

Note: Both signature values could be combined in one value in the future.

\newpage
\section{Application Flow}
In this section, the different flows between the Frosix client and the \Glspl{provider} are described.

\subsubsection{Distributed Key Generation}
Figure \ref{fig:dkg_sequence} illustrates the Frosix key generation process.
The specifications of the REST endpoints can be found in appendix~\ref{appendix_a}.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_dkg_sequence}
    \caption{Frosix distributed key generation sequence}
    \label{fig:dkg_sequence}
\end{figure}

\begin{enumerate}
  \item The user prepares a Frosix Providerlist as input to frosix-cli keygen.
  \item The Frosix client requests the server configuration from each \Glspl{provider} (GET /config), which includes information like supported authentication methods, public key to verify a providers signature and the \Gls{provider} public server salt.
  If one of the \Glspl{provider} doesn't respond to this request the Frosix client will abort the process.
  \item After positive feedback was received from all \Glspl{provider}, the Frosix client asks each \Gls{provider} for a seed (GET /seed), to be used as partial source of entropy (see chapter design).
  \item Before starting the first round of the distributed key generation process, the Frosix client combines all received seeds together with local entropy and derives all \glspl{nonce} and context strings.
  \item In the first round of the distributed key generation process, the Frosix client requests every \Gls{provider} to derive a commitment from the submitted \gls{context string} and the \Gls{provider} \gls{secret server salt} (POST /dkg-commitment).
  \item Next, the Frosix client distributes all commitments to every participating \Gls{provider}, requesting the \Glspl{provider} to send the encrypted secret shares (POST /dkg-shares).
  \item Afterwards, those encrypted shares have to be ordered and the Frosix client transmits to each \Gls{provider} only the secret shares targeted to this specific provider (POST /dkg-key), initiating round three.
  \item Last, the Frosix client receives the public key of the generated key pair and a signature from each provider, checks if every public key is the same and verifies the signature over the public key and the authentication data, aborting if it fails.
  \item At the end, the Frosix client outputs a Frosix signing document.
\end{enumerate}

\subsubsection{Request Challenge}
Figure \ref{fig:challenge_sequence} illustrates a single challenge request sequence. This request must be done for each challenge-code independently.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_request-challenge_sequence}
    \caption{Frosix request challenge sequence}
    \label{fig:challenge_sequence}
\end{figure}

\begin{enumerate}
  \item The user provides the \textit{Frosix signing document} and specifies from which listed \Gls{provider} a challenge-code should be requested for which message.
  \item The Frosix client sends the challenge request and if the submitted authentication data matches the salted hash stored at the \Gls{provider}, a challenge-code is issued (POST /auth-challenge).
\end{enumerate}

\subsubsection{Distributed Signing}
Figure \ref{fig:sig_sequence} illustrates the sequence of a complete distributed signing process.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_sign_sequence}
    \caption{Frosix distributed signing sequence}
    \label{fig:sig_sequence}
\end{figure}

\begin{enumerate}
  \item There are always two different inputs needed to start a distributed signing process. Besides a Frosix signing document, the Frosix client needs a challenge-code or the correct answer to a \textit{security question}
  for each participating \Gls{provider}. There has to be exactly \textit{threshold-value} numbers of providers!
  \item Frosix client starts the first round of the distributed signing process with a request for a random, but message-specific commitment to each participating \Gls{provider} (POST /sig-commitment).
  \item After passing the authentication challenge and receiving a commitment from each participating \Gls{provider}, the Frosix client distributes these commitments with the request to each compute a partial signature (POST /sig-share).
  \item The Frosix client verifies now each partial signature and aggregates and outputs the final signature.
\end{enumerate}

\subsubsection{Delete Key}
Figure \ref{fig:del-key_sequence} illustrates the deletion of all private key shares, which belong to a specific public key.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_delete-key_sequence}
    \caption{Frosix delete key sequence}
    \label{fig:del-key_sequence}
\end{figure}

\begin{enumerate}
  \item To delete the private key shares the signing document has to be provided.
  \item The Frosix client then calls for each \Gls{provider} the REST endpoint DELETE /dkg-key with the hash of the encryption key as identifier.
  \item A \Gls{provider} deletes a key immediately if there is a corresponding key stored in its database.
\end{enumerate}

\section{Further Considerations}
Numerous decisions were made throughout the development of Frosix.
Some of them are mentioned and explained below.

\subsubsection{Data Parsing}
In Frosix, the use of JSON data is employed for input, output and transmission.
The parsing of JSON data is facilitated by the use of Libjannson \cite{libjansson}.
This approach significantly mitigates the risks associated with manual parsing,
as Libjansson is widely adopted and reviewed.

\subsubsection{POST vs GET}
The majority of the REST endpoints accept only POST requests, although there are some exceptions (see appendix~\ref{appendix_a}).
This design choice is driven by the objective of transmitting all data encrypted (over TLS).
All data must therefore be in the body of the request.
\custind An identifier in every request allows to correlate the request between the rounds of Frosix key generation and Frosix sign.
This identifier is a hash over all relevant data in the body of the request,
therefore it doesn't matter if the identifier gets logged from various systems while being transmitted between the Frosix client and a \Gls{provider}.

\subsubsection{Denial-of-service}
Since in Frosix's adversary model, an adversary has full control over the user's device and can therefore suppress and manipulate all network communication,
Frosix cannot protect against such denial-of-service (DoS) attacks!
\custind Additionally, Frosix lacks robustness against misbehaving providers, resulting in abortion of the protocol if anything fails.

\subsubsection{Availability Considerations}
Protection against denial-of-service (DoS) attacks is an important aspect in the design of the \Glspl{provider}.
A good protection against a large number of requests, with the objective of exhausting the \Gls{provider} 's resources,
is to always require a small payment in advance. However, this is currently not implemented (see chapter~\ref{chap:payment}).

Frosix incorporates an expiration date for all stored data in the database.
The commitments stored during Frosix key generation, which may consume much memory,
are stored for a few minutes only.
Moreover, the key data, which have a long storage time (some years), require only minimal storage space.
This makes it challenging for an adversary to exhaust a provider's storage capacity.

Furthermore, Frosix employs rate limiting within the database to mitigate exhaustive repeated requests in Frosix Sign.
However, it is important to note that Frosix does not offer protection against other network-based DoS attacks beyond its own scope of operation.

\section{Performance}
This section lists some performance measurements of Frosix, running single threaded on an Intel Core i7-7600U CPU.
All data points represent a single run of the application as performance is quite predictable and thus multiple runs were not deemed necessary.
Furthermore, the measurements are not completely accurate due to heat-based throttling, but they show the general tendency.

\newpage
\subsubsection{Frosix key generation}
Figure~\ref{fig:perf_dkg_threshold} shows the linear increase if the \textit{threshold-value} is raised.
\newline This is done with the number of parties fixed to the value 240.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_perf_dkg_threshold}
    \caption{Performance of Frosix key generation of a \Gls{provider} depending on the threshold value}
    \label{fig:perf_dkg_threshold}
\end{figure}

Figure~\ref{fig:perf_dkg_parties} shows a second linear increase if the \textit{number of parties} is raised.
In this example the threshold-value is fixed to a value of only 10, resulting in much smaller numbers.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_perf_dkg_parties}
    \caption{Performance of Frosix key generation of a \Gls{provider} depending on number of involved \Glspl{provider}}
    \label{fig:perf_dkg_parties}
\end{figure}

Figure~\ref{fig:perf_dkg_combined} shows the behavior if the \textit{threshold-value} is always \textit{number of parties - 1},
therefore we have a situation of nearly $n = k$.
As the incrementation of both values on its own results in a linear increase,
if both values are incremented at the same time the complexity is squared.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_perf_dkg_combined}
    \caption{Performance of Frosix key generation of a \Gls{provider} if k = n}
    \label{fig:perf_dkg_combined}
\end{figure}

\subsubsection{Frosix sign}

Figure~\ref{fig:perf_sign_provider} shows the time required for a \Gls{provider} to create a partial signature.
Since the sole dependency of signing a message is the \textit{threshold-value},
the increase is linear. In contrast to Frosix sign, the maximum number needed to sign a message is smaller by a factor of about 20.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_perf_sign_provider}
    \caption{Performance of Frosix sign of a \Gls{provider} depending on the threshold value}
    \label{fig:perf_sign_provider}
\end{figure}

Figure~\ref{fig:perf_sign_client} shows the time needed for the Frosix client to verify the partial signatures.
This is quite an inefficient computation in the current implementation, with a complexity of $O(n^2)$.
There is in principle a simple way to reduce the computation cost of this step to almost zero.
However, this was left for future work (see chapter \ref{future_work}).

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_perf_sign_client}
    \caption{Performance of Frosix sign of Frosix client depending on the threshold value}
    \label{fig:perf_sign_client}
\end{figure}

\subsubsection{Frosix verify}

The verification of threshold signatures is very fast and because its always one signature to verify,
has a complexity of \(O(1)\), as Figure~\ref{fig:perf_verify_client} shows.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_perf_verify_client}
    \caption{Performance of Frosix verify of Frosix client depending on the threshold value}
    \label{fig:perf_verify_client}
\end{figure}

\newpage
\section{Testing}
At the moment, there are only two integration tests.

\subsubsection{Libfrost}
The first integration test is a compiled binary which runs every high level function of libfrost once.

\begin{itemize}
  \item performs a distributed key generation
  \item signs a message
  \item checks if the resulting signature can be verified
\end{itemize}

\subsubsection{Frosix}
The integration test of Frosix is a shell script which does the following.

\begin{itemize}
  \item creates and resets (in case they already exist) five instances of PostgreSQL databases
  \item starts five \Glspl{provider} with predefined configuration files
  \item runs a Frosix key generation among all five \Gls{provider} with a predefined providerlist
  and stores the result Frosix in a Frosix signing document
  \item runs a Frosix sign with three \Glspl{provider}, using the previously created Frosix signing document
  and stores the result in a signature file
  \item runs Frosix verify with the signature file and the message which was signed and checks if the verification succeeds.
\end{itemize}
