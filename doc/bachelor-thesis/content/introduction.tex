In today's digital landscape, signatures play a crucial role in nearly every
aspect of modern informatics. Through the use of signatures, we are able to
validate the authenticity, integrity and non-repudiation of any digital information.

The private signing key is the most crucial component for creating a signature
and must be kept secret at all costs.
But keeping a private key confidential is a challenging task, often underestimated in the past
\cite{heise:msi-hack} \cite{schneier:leaked-keys} \cite{github:stolen-keys} \cite{malwarebytes:nvidia-keys}.

A popular security solution is the use of a hardware security module (HSM) \cite{IR8320}.
Even though these modules are considered secure, they are quite expensive
and require trust in a single manufacturer.

Qualified signatures represent the digital signature in a legally valid form \cite{admin:qes}.
In Switzerland, there are currently three providers \cite{bit:qes} which provide qualified signatures
for the general public. Only one of these providers allows to use an HSM for signing,
with the other two providers always creating the signatures under their sovereignty.

For signing cryptocurrency transactions, multi-signatures are the preferred way to protect a wallet \cite{bitcoin:multisig}.
With multi-signatures, there are several entities, each possessing its own private and public key.
In order to obtain a valid signature, it is required that a certain number of these entities
must provide a signature on its own.
This is inefficient because the size of the resulting signature increases linearly with
the number of signers and the verification becomes more costly,
because the signature of each signer must be verified individually.

The field of multiparty threshold signature schemes aims to address these three main problems, namely:
\begin{itemize}
  \item keeping a private key confidential,
  \item avoiding the need to trust in a single provider or a single device, and
  \item being efficient in terms of size and verification cost.
\end{itemize}

The contribution of this work is the implementation of such a threshold signature scheme,
combined with strong authenticated signing.

Frosix is mainly based on FROST \cite{cryptoeprint:2020/852},
a promising threshold signature scheme.
Even though FROST proposes an advanced protocol for the distributed creation of signatures,
it lacks any authentication of the central signature aggregator against the signing parties.
Therefore, Frosix extends FROST with an authentication component, inspired by GNU Anastasis
\cite{anastasis}.\footnote{GNU Anastasis is a free software key recovery service and resulted from an earlier bachelor thesis.}

\section{Principles}
The following design objectives were important during the design and development of Frosix.
Those principles were freely adapted from the Anastasis bachelor thesis. \cite{anastasis-thesis}

\begin{itemize}
  \item Frosix must be Free Software \cite{fsf}. Everyone must
  have the right to run the program, study the source code, make modifications
  and share their modifications with others.
  \item Frosix must not rely on the trustworthiness of individual \Glspl{provider} or devices.
  It must be possible to use Frosix safely, even if a subset (\(k - 1\)) of \Glspl{provider}
  and devices, respectively their used authentication methods, are malicious.
  \newline Furthermore, Frosix must minimize the amount of information exposed to
  \Glspl{provider} and the network.
  \item Frosix must put the user in control: They get to decide which
  \Glspl{provider} and which authentication methods to use.
  \item Frosix must be economically viable to operate. This implies usability
  and efficiency of the system.
  \item Signatures generated with Frosix must support a diverse range of use
  cases.
\end{itemize}

\section{Basics}
\textit{Frosix} is the combination of the word \textit{FROST} and the
abbreviation \textit{six} originating from the word \textit{signature}.

\subsubsection{Threshold Cryptosystem}
Frosix is based on a threshold cryptosystem with the following basic properties:

\begin{itemize}
  \item \textbf{Distribution of the secret}: In a threshold cryptosystem, the threshold value $k$, or sometimes $t$, states,
  how many entities an adversary has to compromise in order to reconstruct a distributed secret.
  \item \textbf{Robustness in reconstruction of the secret}: This also implies at the same time that only \(k\) entities have to collaborate in order to reconstruct the secret.
\end{itemize}

A threshold signature scheme involves the following components, which differ from a single-signer signature or multi-signature scheme.

\textbf{Distributed Key Generation}
\newline For the generation of a private key, a distributed key generation protocol ensures
that no entity knows more than a fraction of the private key.
\custind Frosix ensures that even the device of the user who initiated the key generation
learns nothing about the private key, except for the public key that is eventually created.

\textbf{Distributed Signing}
\newline In order to never constitute the private key on a single device,
each entity involved in the signing process generates a partial signature with its fraction of the private key.
\custind A central signature aggregator, the Frosix client, combines those parts to create the final signature.

\subsubsection{Authentication}
In current threshold signature schemes, the trustworthiness of the issuer of a request is not taken into account for the creation of a signature.
Frosix closes this gap with the offering and enforcing of different authentication methods for each involved \Gls{provider} and each signing request.
This results, depending on the combinations of chosen authentication methods,
in the highest possible security level threshold signing can have.

\subsubsection{Privacy by Design}
Frosix is designed to minimize the information exposed to the user, the network and to \Glspl{provider}.
Thus, the data persisted at a \Gls{provider} is either encrypted or just a salted hash.
The encryption key and the salt value are only submitted during the key generation or a signing process.

\section{Use cases}
One of the main advantages of Frosix in comparison to current implementations is the
inclusion of authentication before issuing a signature share.

\subsubsection{Software Signing}
Modern operating systems rely on signed software as root of trust to
protect users from malicious software.
However, many software development companies do not adequately secure
their software signing keys.
Frosix can significantly enhance security in this context,
and it can also establish trust in software updates and releases from independent
developers to reduce the risk of malicious code being committed to public
registries like GitHub.

\subsubsection{Document Signing}
Frosix is designed to sign anything that can be processed by a hash function.
This includes documents such as contracts, testaments,
and various media files like pictures, music, and videos.
For contracts and other legally binding documents, Frosix could take on the role of
a qualified signature, provided the legal conditions are met.

\subsubsection{Cryptocurrency Transactions}
Frosix is particularly well-suited for use in the signing of 
cryptocurrency transactions, as the underlying FROST scheme was originally
designed for this purpose.
With the introduction of the Taproot upgrade in Bitcoin \cite{taproot},
which enables support for Schnorr signatures, Frosix could be customized
to issue valid Bitcoin transaction signatures.

\subsubsection{E-mail / Communication}
Frosix can also be used for signing written digital communication, such as emails.
However, the requirement for multiple authentications makes Frosix less suitable for everyday
and trivial communications.
Nonetheless, this feature becomes increasingly valuable when considering the
security properties of authenticity, integrity, and non-repudiation 
associated with sensitive communications.

\subsubsection{Financial Industry}
Another field of application for Frosix lays in the financial industry.
Banks and other financial institutions manage immense assets,
and are therefore obliged to comply with the highest IT security standards.
Threshold signatures could serve as a supporting element to distribute
and thus reduce financial and regulatory risks.