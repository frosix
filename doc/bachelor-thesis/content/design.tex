Frosix enables secure and trustworthy signing without relying on the security of a specific device.
This is accomplished through the use of a threshold signature scheme,
where the private key is split among several entities, known as \Glspl{provider}.
\custind Additionally, a distributed key generation ensures that the private key is never instantiated in a single place,
preventing the violation of confidentiality of the private key through the compromise of a single device.

To further enhance security, Frosix incorporates multifactor authentication
for each signing operation, allowing users to choose from various combinations of authentication factors.
\custind Considering the untrusted nature of the \Glspl{provider}, they are required to demonstrate
the integrity of the received \gls{authentication data} during the key generation.

To reduce the attack surface of a \Gls{provider}, the long-term stored data is either salted or encrypted,
and the specific nonce and secret key used remain unknown.

\section{Adversary Model}
Frosix is developed under the assumption of a strong and capable adversary with the ability to compromise
any user device and \Gls{provider}, but not more than $k-1$.
The adversary possesses full control over the network infrastructure,
including mobile networks, and can force companies, such as internet service providers, to cooperate.
Furthermore, the adversary is considered omnipresent throughout key generation and signing process.

Despite the adversary's outstanding capabilities, the adversary is not able to \gls{brute-force} a salted hash 
or break state-of-the-art public key cryptography.
\custind Moreover, the assumption made at the beginning is further tightened.
It is assumed, that the adversary is not able to control the devices and \Glspl{provider} involved in more than $k-1$ authentication procedures.

Frosix relies on the presence of multiple authentication and tamper control devices,
and assumes that at least one device is not compromised.

\section{Adaptions of FROST}
Frosix introduces modifications to the key generation and signing scheme FROST.
The changes to the original are marked in red.

\subsubsection{Frosix Key Generation} \label{chap:frosix_key_generation}
The changes from Frosix key generation are:
\begin{itemize}
  \item \textbf{Deterministic Commitments}: A \Gls{provider} uses the submitted \textit{nonce},
  the \textit{secret provider salt} and some more values (see chapter \ref{chap:rnd}) to derive the secret polynomial.
  \item \textbf{Unicast Communication}: In contrast to the original FROST key generation,
  all communication of a key generation process in Frosix goes through the Frosix client.
  \newline This requires further adjustments:
  \begin{itemize}
    \item The Frosix key generation is a three round protocol.
    \item The secret shares are provider-to-provider encrypted, which means that only the legitimate provider can perform the decryption.
    \item The Elliptic curve Diffie-Hellmann Ephemeral (ECDHE) scheme is used for the key exchange.
    \item To prevent a man-in-the-middle attack, the public key used in the ECDHE key exchange is included in the challenge of the zero knowledge proof.
  \end{itemize}
  \item \textbf{Authentication Data}: In order to be able to authenticate a signing request, the Frosix client computes an \gls{authentication hash} $auth\_hash$,
  e.g. a salted hash over a phone number or an email address. A \Gls{provider} includes this hash to derive the commitment.
  \item \textbf{Signed Group Public Key}: At the end, every \Gls{provider} stores the key data, together with the authentication hash in the database,
  signs the resulting group public key and the received authentication data with its long-term public key.
\end{itemize}

\begin{center}
  \textbf{Round 1}
\end{center}

{\large $Frosix\ Client$} \hfill {\large $\displaystyle P_{i,\ 0\ <\ i\ \leq \ n}$}
\par \textcolor{red}{\textbf{Step 1}: The \(Frosix\ client\) samples and sends a \textit{context\_string},
together with the salted hash of the \textit{authentication data} $auth\_hash_i$ to each \(P_i\):}

{\small $\displaystyle \textcolor{red}{(context\_string_1, ..., context\_string_1) \xleftarrow{\$}}$}

\begin{center}
  {\small $\displaystyle \textcolor{red}{(context\_string_i, auth\_hash_i) \longrightarrow} $}
\end{center}

\textbf{Step 2}: Every $P_i$ derives a \textcolor{red}{$seed$ from the $context\_string_i$ and $auth\_hash_i$,
and uses this $seed$ to} derive $t$ values $(a_{i0}, ..., a_{i(t-1)})$.
These values are then used as coefficients to define a degree $t-1$ polynomial ($f_i(x)$):

\begin{flushright}
  {\small $\displaystyle (a_{i0}, ..., a_{i(t-1)}) \leftarrow \textcolor{red}{KDF(seed, secret\_salt_i)}$} \\
  {\small $\displaystyle f_i(x) \ =\ \sum _{j=0} ^{t-1} a_{ij}x^j$}
\end{flushright}

\textbf{Step 3}: Every \(P_i\) \textcolor{red}{samples an \textit{ephemeral key pair} $(esk_i, epk_i)$ and} computes a proof of knowledge to the secret \(a_{i0}\):

\begin{flushright}
  {\small \textcolor{red}{$\displaystyle (esk_i, epk_i) \xleftarrow{\$}$}} \\
  {\small $\displaystyle k \xleftarrow{\$} \mathbb{Z}_q$} \\
  {\small $\displaystyle R_i \ =\ g^k$} \\
  {\small $\displaystyle c_i \ =\ H(i, "DKG-Challenge", g^{a_{i0}}, R_i, \textcolor{red}{epk_i})$} \\
  {\small $\displaystyle z_i \ =\ k + a_{i0} \cdot c_i$} \\
  {\small $\displaystyle \sigma_i \ =\ (R_i, z_i)$}
\end{flushright}

\textbf{Step 4}: Every participant \(P_i\) computes a public commitment \(\vec{C}\):

\begin{flushright}
  {\small $\displaystyle \phi_{ij} \ =\ g^{ij}, 0 \leq j \leq t-1$} \\
  {\small $\displaystyle \vec{C_i} \ =\ \langle \phi_{i0}, ..., \phi_{i(t-1)} \rangle$}
\end{flushright}

\textbf{Step 5}: Every \(P_i\) sends \((\vec{C_i}, \sigma_i, \textcolor{red}{epk_i})\) to \textcolor{red}{\(Frosix\ client\)}:

\begin{center}
  {\small $\displaystyle \longleftarrow (\vec{C_i}, \sigma_i, \textcolor{red}{epk_i})$}
\end{center}

\begin{center}
  \textbf{Round 2}
\end{center}

{\large $Frosix\ Client$} \hfill {\large $\displaystyle P_{i,\ 0\ <\ i\ \leq \ n}$}
\par \textcolor{red}{\textbf{Step 1}: \(Frosix\ client\) sends \((\vec{C_l}, \sigma_l, \textcolor{red}{epk_i})\) to each \(P_i\):}

\begin{center}
  {\small \textcolor{red}{$\displaystyle (\vec{C_l}, \sigma_l, \textcolor{red}{epk_i}) \longrightarrow $}}
\end{center}

\textbf{Step 2}: Upon receiving (\(\vec{C_l}, \sigma_l, \textcolor{red}{epk_i}\)) from \textcolor{red}{\(Frosix\ client\)}, each participant \(P_i\) verifies \(\sigma_l\):

\begin{flushright}
  {\small $\displaystyle c_l \ =\ H(l, "DKG-Challenge", \phi_{l0}, R_l, \textcolor{red}{epk_l})$} \\
  {\small $\displaystyle R_l \overset{?}{=} g^{z_l} \ \cdot \phi_{l0}^{-c_l}$}
\end{flushright}

\par \textbf{Step 3}: Each $P_i$ \textcolor{red}{encrypts the secret shares ($l, f_i(l)$) for every other participant,
using the key $ek_{il}$ which is derived with the help of ECDHE, using another ephemeral key pair $(esk_{il}, epk_{il})$ for each other $P_i$}.
Each $P_i$ sends to \textcolor{red}{$Frosix\ client$ the encrypted secret shares $(es_{il})$ together with the ephemeral public keys $epk_{il}$}.

\begin{flushright}
  {\small \textcolor{red}{$(esk_{il}, epk_{il}) \xleftarrow{\$}$ for $l \in \{1 \ldots n\} \setminus \{i\}$}} \\
  {\small \textcolor{red}{$ek_{il} := ECDHE_{esk_{il}}(epk_i)$ for $l \in \{1 \ldots n\} \setminus \{i\}$}} \\
  {\small \textcolor{red}{$es_{il} := E_{ek_{il}}(l,f_i(l))$ for $l \in \{1 \ldots n\} \setminus \{i\}$}}
\end{flushright}

\begin{center}
  {\small $\displaystyle \longleftarrow (\textcolor{red}{es_{il}, epk_{il}})$}
\end{center}

\begin{center}
  \textbf{Round 3}
\end{center}

{\large $Frosix\ Client$} \hfill {\large $\displaystyle P_{i,\ 0\ <\ i\ \leq \ n}$}
\par \textcolor{red}{\textbf{Step 1}: The \(Frosix\ client\) sends $(es_{li}, epk_{li})$ to each \(P_i\):}

\begin{center}
  {\small \textcolor{red}{$\displaystyle (es_{li}, epk_{li}) \longrightarrow$}}
\end{center}

\textbf{Step 2}: Each \(P_i\) \textcolor{red}{decrypts the secret share, using their $esk_i$ in ECDHE to derive the key $ek_{il}$, and} verifies its received shares:
  
\begin{flushright}
  {\small \textcolor{red}{$ek_{il} := ECDHE_{esk_i}(epk_{li})$ for $l \in \{1 \ldots n\} \setminus \{i\}$}} \\
  {\small \textcolor{red}{$(l, f_i(l)) := D_{ek_{il}}(es_{il})$ for $l \in \{1 \ldots n\} \setminus \{i\}$}} \\
  {\small $\displaystyle g^{f_l(i)} \ \overset{?}{=} \ \prod _{k=0} ^{t-1} \phi_{lk}^{i^k mod \ q}$}
\end{flushright}

\textbf{Step 3}: Each \(P_i\) calculates their long-lived private signing share \(s_i\):

\begin{flushright}
  {\small $\displaystyle s_i \ =\ \sum _{l=1} ^n f_l(i)$}
\end{flushright}

\textbf{Step 4}: Each \(P_i\) calculates their public verification share \(Y_i\) and the group's public key \(Y\):

\begin{flushright}
  {\small $\displaystyle Y_i \ =\ g^{s_i}$} \\
  {\small $\displaystyle Y \ =\ \prod _{j=1} ^n \phi_{j0}$}
\end{flushright}

\textbf{Step 5}: Each \(P_i\) returns the group's public key and a signature ($\phi_i$) over $(Y, auth\_hash_i)$, proving \(P_i\) was part of this key generation process
and the correct authentication data were received:

\begin{center}
  {\small \textcolor{red}{$\displaystyle \longleftarrow (Y, \phi_i)$}}
\end{center}

\textcolor{red}{\textbf{Step 6}: \(Frosix\ client\) compares all received public keys \(Y\) and verifies the signature,
aborting if one of the public key differs from the others or if the signature verification fails.}

Note: The values $context\_string_i$ and $auth\_hash_i$ are also submitted in round 2 and 3 to each $P_i$,
to let them compute the values $(a_{i0}, ..., a_{i(t-1)})$, without the need to store the $seed$ in the database between the rounds.

\subsubsection{Frosix Sign}
In Frosix Sign, a challenge must be requested for every \Gls{provider},
for which the user defined a \gls{challenge-code} based authentication.
There is therefore an additional round in Frosix Sign.
\newline Besides this, the only difference from the original FROST scheme occurs in round 2 (round 1 from FROST).
\custind The modification includes the hash of the message into the commitments.
By making this change, the message becomes linked to the \gls{commitment},
preventing the commitment from being used for any other message.

\begin{center}
  \textbf{Round 1}
\end{center}

{\large \(Frosix\ Client\)} \hfill {\large $\displaystyle P_{i\ \in \ S}$}
\par \textbf{Step 1}: The user chooses the set $S$, \(k\ of\ n\) parties and the $Frosix\ client$ hashes the input message:

{\small $\displaystyle S\ =\ \{P_1, ..., P_k\},\ 0 < P_i \leq  n$} \\
{\small \textcolor{red}{$\displaystyle h \ =\ H(\$message)$}}

\textbf{Step 2}: \textcolor{red}{$Frosix\ client$ sends to each $P_i$ a \textit{challenge request},
containing the hash of the message ($h$), the hash of the encryption key ($enc\_key\_h_i$),
the \gls{authentication method} ($auth\_method_i$), the authentication data ($auth\_data_i$) and the authentication nonce ($auth\_nonce_i$).
But only if a challenge-code based authentication method was chosen for $P_i$ during the key generation.}

\begin{center}
  {\small \textcolor{red}{$\displaystyle (h, enc\_key\_h_i, auth\_method_i, auth\_data_i, auth\_nonce_i) \longrightarrow$}}
\end{center}

\textbf{Step 3}: \textcolor{red}{Each $P_i$ verifies if the hash of over $auth\_data_i$ and $auth\_nonce_i$ match the stored $auth\_hash_i$
and responds with the $challenge\_code_i$ over a different channel, depending on the $auth\_method_i$, to the submitted address in $auth\_data_i$.}

\begin{flushright}
  {\small \textcolor{red}{$\displaystyle H(auth\_data_i, auth\_nonce_i, "FROSIX") \overset{?}{=} auth\_hash$}}
\end{flushright}

\begin{center}
  {\small $\displaystyle challenge\_code_i \longrightarrow $}
\end{center}

\begin{center}
  \textbf{Round 2}
\end{center}

{\large \(Frosix\ Client\)} \hfill {\large $\displaystyle P_{i\ \in \ S}$}
\par \textbf{Step 1}: \textcolor{red}{The $Frosix\ client$ sends to each $P_i$ the hash of the message ($h$) and the
challenge data ($challenge\_data_i$), which is either a hash of the received $challenge\_code_i$ or the signature over $h$ (using the secret answer to derive the signing key, see chapter~\ref{chap:rnd}), in case of a \textit{security question}.}

\begin{center}
  {\small \textcolor{red}{$\displaystyle (h, challenge\_data_i) \longrightarrow$}}
\end{center}

\textbf{Step 2}: Each $P_i$ first verifies the $challenge\_data_i$ and then samples \textcolor{red}{a random $seed_i$.
Together with the hash of the message, each \(P_i\) derives} two random commitment values and send those back to the \(Frosix\ client\).

\begin{flushright}
  {\small \textcolor{red}{$\displaystyle seed_i \xleftarrow{\$}$}} \\
  {\small $\displaystyle (d_i,e_i) \textcolor{red}{\leftarrow KDF(m, seed_i)}$} \\
  {\small $\displaystyle (D_i,E_i) \ = \ (g^{d_i},g^{e_i})$}
\end{flushright}

\begin{center}
  {\small $\displaystyle \longleftarrow (D_i,E_i)$}
\end{center}

\begin{center}
  \textbf{Round 3}
\end{center}

{\large \(Frosix\ Client\)} \hfill {\large $\displaystyle P_{i\ \in \ S}$}

\textbf{Step 1}: The $Frosix\ client$ fetches all commitments, builds the set $B \ = \ \langle i,D_i,E_i \rangle, i \in S$ and sends $B$ together with the hash of the message to all $P_i$ in $S$.

{\small $\displaystyle B \ =\ \langle ( i,D_{i} ,E_{i})\rangle _{i}{}_{\in }{}_{S}$}

\begin{center}
  {\small $\displaystyle ( h,\ B) \longrightarrow $}
\end{center}

\textbf{Step 2}: After receiving $(h,\ B)$, each $P_i$ validates the message $h$, and then checks $D_l,E_l \in \mathbb{G}^*$ for each commitment in $B$, aborting if either check fails.

\textbf{Step 3}: Each $P_i$ then computes the set of binding values.

\begin{flushright}
  {\small $\displaystyle p_l \ = \ H_1(l,h,B), l \in S$}
\end{flushright}

\textbf{Step 4}: Each $P_i$ then derives the group commitment $R$ and the challenge $c$.

\begin{flushright}
  {\small $\displaystyle R \ =\ \prod _{l \in S} D_l \cdot (E_l)^{\rho_l}$} \\
  {\small $\displaystyle c \ =\ H_2(R,Y,h)$}
\end{flushright}

\textbf{Step 5}: Each $P_i$ computes their response $z_i$ using their long-lived secret share $s_i$
\textcolor{red}{and the secret values ($d_i, e_i$) which each $P_i$ computes with the help of the temporary stored $seed_i$ from round 2}.

\begin{flushright}
  {\small $\displaystyle z_i \ =\ d_i + (e_i \cdot \rho_i) + \lambda_i \cdot s_i \cdot c$}
\end{flushright}

\textbf{Step 6}: Each $P_i$ securely deletes the \textcolor{red}{$seed_i$} from their local storage, and then returns $z_i$ to $Frosix\ client$.

\begin{center}
  {\small $\displaystyle \longleftarrow z_i$}
\end{center}

\textbf{Step 7}: The $Frosix\ client$ calculates the set of binding values \(p_l\),
the set of individual commitments \(R_l\), the group commitment \(R\) and the challenge \(c\).
With these values, the $Frosix\ client$ verifies the validity of each response \(z_i\).

{\small $\displaystyle p_l \ =\ H_1(l,h,B), l \in S$} \\
{\small $\displaystyle R_l \ =\ D_l \cdot (E_l)^{\rho_l}, l \in S$}

{\small $\displaystyle R \ =\ \prod _{l \in S} R_l$} \\
{\small $\displaystyle c \ =\ H_2(R,Y,h)$}

{\small $\displaystyle g^{z_i} \overset{?}{=} R_i \cdot Y_i^{c \cdot \lambda_i}$}

\textbf{Step 8}: The $Frosix\ client$ computes the resulting signature.

{\small $\displaystyle z = \sum z_i$} \\
{\small $\displaystyle \sigma \ =\ (R, z)$}

\section{Authentication}
Frosix uses multifactor authentication to enhance the security and trustworthiness of its signing process.
Users are required to provide evidence of their identity, such as answering a security question or submitting a challenge-code.
\custind With this approach, Frosix pays tribute to pseudonymous communication on the Internet,
as a \Gls{provider} cannot distinguish a legitimate request from a malicious one.
For both requests, the sender is simply an IP address.

\subsubsection{Challenge-Code}
The majority of authentication methods employed in Frosix involve the requesting of challenge-codes over various communication channels
such as SMS, email or postal mail.
Those authentication methods are based on the codebase of GNU Anastasis \cite{anastasis}.

\subsubsection{Security Question}\label{chap:sec_question}
One of the objectives of Frosix is to establish trust in the public key,
generated at the end of a Frosix key generation process.
A \Gls{provider} is therefore required to proof the ability to recognize our secret answer.
However, secret answers are typically low in entropy and vulnerable to brute-force attacks.
To address this issue, Frosix introduces a small tweak, compared to the implementation of GNU Anastasis \cite{anastasis}.
\custind It is crucial to consider the potential points an adversary may have to gain knowledge of the secret answer.
If the adversary can observe the secret answer being entered, cryptographic protection becomes ineffective.
\custind Furthermore, it is important to ensure that knowledge of the hash alone cannot authenticate a signing request,
and transmitting the non-hashed value is not a viable option.
\custind Lastly, the authentication should be highly coupled with the hash of the message.
It should not be possible for a man-in-the-middle to change the message during transmission without being noticed.

To fulfill the first requirement, during the key generation process,
a \Gls{provider} receives a salted hash, which then is stored in the database.
The use of a high-entropy nonce used as \gls{salt} makes it difficult to brute-force.
\custind Because this salted hash is signed by the \Gls{provider} at the end of the key generation process,
the same value has to be stored in the Frosix signing document, together with the salt.
To make brute-forcing more challenging, Frosix utilizes a password-based key derivation function (PBKDF).
Users have the option to specify a difficulty level or allow the system to adapt the difficulty
based on the hardware the Frosix client is running on, during key generation.
\custind To address the other concerns, Frosix incorporates simple public key cryptography.
The Frosix client derives a private key from the secret answer, using a PBKDF,
computes the corresponding public key, and uses it to sign the message, along with the nonce.
When a \Gls{provider} receives the triple consisting of the message, the signature and the public key,
it verifies the signature against the message, hashes the public key, and compares it to the stored hash.
If the hashes match, the challenge is considered to be solved, and that request is authorized.
\custind Consequently, only the hash of the derived public key is submitted, signed,
and stored in the Frosix signing document during a key generation process.

\section{Derivation from Randomness} \label{chap:rnd}
Frosix relies on randomness in different places, but otherwise tries to derive as much as possible deterministically.
Many cryptographic systems rely on good randomness, however, using bad, or predictable, randomness can lead to disastrous results.
The Frosix approach therefore reduces potential attack surfaces.
\custind In the figures below, the development of the different derived and random values is shown.

Figure~\ref{fig:figure_legend} shows the legend of the following figures from this chapter.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_entropy_figures_legend}
    \caption{Legend for Figure~\ref{fig:client_entropy} to Figure~\ref{fig:provider_sig_entropy}}
    \label{fig:figure_legend}
\end{figure}


\subsection{Frosix Key Generation}
From a \Gls{provider} 's perspective, all three rounds of Frosix key generation are implemented with full determinism,
ensuring that the outcomes of these rounds remain consistent.
As a result, the corresponding REST endpoints associated with each round exhibit idempotent behavior,
meaning that repeated requests to these endpoints will have the same effect as a single request,
without causing any unintended side effects.

\subsubsection{Frosix Client}
Figure~\ref{fig:client_entropy} shows the different derivations of the Frosix client during a key generation process.
\par First, the Frosix client gets high-entropy seeds from each \Gls{provider}.
Afterwards, it puts the \textit{provider entropy}, together with \textit{local entropy},
in a key derivation function (KDF) and derives the \textit{client masterkey},
as well as all \textit{context strings}.
Finally, with the masterkey, the Frosix client derives the \textit{authentication nonces} and the \textit{pre encryption keys}.
A \textit{pre encryption key} will later serve as nonce to derive an \textit{encryption key}.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_client_entropy}
    \caption{Frosix client: derivations during a key generation process}
    \label{fig:client_entropy}
\end{figure}

The hashing of the authentication data depends on the selected authentication method.
\par If the selected authentication method involves the requesting and submitting of a challenge-code (Figure~\ref{fig:client_auth_hash}),
the \textit{authentication data}, such as a phone number or an email address,
is hashed together with an \textit{\gls{authentication nonce}}, resulting in the \textit{authentication hash}.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_client_authentication_hash}
    \caption{Hashing of authentication data of challenge-code based authentication methods}
    \label{fig:client_auth_hash}
\end{figure}

In case of a security question (Figure~\ref{fig:client_auth_hash_question}),
the \textit{secret answer} and the \textit{authentication nonce} are thrown in a password-based key derivation function (PBKDF).
The \textit{amplifier value} adjusts the number of iterations the PBKDF will perform to derive a \textit{seed}.
A higher value means more iteration and therefore higher costs for a brute-force attack.
\custind From this seed, first a private signing key is created and afterwards the corresponding public key computed.
This public key is then hashed, which results in the \textit{authentication hash}.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_client_authentication_hash_question}
    \caption{Hashing of authentication data of a security question}
    \label{fig:client_auth_hash_question}
\end{figure}

\subsubsection{Frosix Service Provider}
The derivation of entropy is exactly the same for a \textit{POST /dkg-commitment} and a \textit{POST /dkg-shares} request (Figure~\ref{fig:provider_dkg-commitment_entropy}).
Since the key generation process at a \Gls{provider} does not involve further randomness,
virtually every bit of a request is included to prevent different requests resulting in the same key.

First, the submitted \textit{public keys} of all participating \Glspl{provider},
the \textit{threshold value} and the \Gls{provider} 's index are hashed.
A change in the main parameters during a key generation process,
such as changing the \textit{threshold value} or replacing a \Gls{provider},
will result in a different hash and consequently in a different \textit{secret value} and different \textit{coefficients}
and the key generation will fail.
\custind This hash, the \textit{context string}, the \textit{authentication hash} and the \textit{secret provider salt}
(a value which is only known to one \Gls{provider} and which can be reused for many key generations),
serves as input in a key derivation function which outputs the \textit{provider masterkey}.
From this masterkey, the \Gls{provider} derives the \textit{secret value},
the \textit{coefficients} of the degree t-1 polynomial and computes the \textit{commitment values}.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_provider_dkg-commitment_entropy}
    \caption{Derivations of a \textit{POST /dkg-commitment} or \textit{POST /dkg-shares} request}
    \label{fig:provider_dkg-commitment_entropy}
\end{figure}

In the last round of a key generation process, the resulting key data,
consisting of a private and a public key, is encrypted and stored in the \Gls{provider} 's database.
Figure~\ref{fig:provider_dkg-key_entropy} shows how the \textit{encryption key} is derived from the
\textit{pre encryption key}, the \Gls{provider} \textit{index}, the \textit{public provider salt}
(a value which can be publicly received with a \textit{GET /config} request),
and the resulting public key.
The hash of the encryption key serves as identifier for the entry in the database.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_provider_dkg-key_entropy}
    \caption{Derivations of a \textit{POST /dkg-key} request}
    \label{fig:provider_dkg-key_entropy}
\end{figure}

Not mentioned are the proof of knowledge of the \textit{secret value} and the ephemeral keys,
used in the ECDHE for securely transmitting the secret shares.
Both procedures use random values and are described in the Frosix key generation protocol (Chapter~\ref{chap:frosix_key_generation}).

\subsection{Frosix Sign}
Originally, it was planned to derive the nonce, used in the generation of signatures, deterministically from the message and the private key share, analogous to EdDSA.
However, research has shown that this procedure cannot be applied to threshold signatures and that it can result in a key disclosure \cite{10.5555/648120.747057},
due to the possibility of nonce reuse if an adversary has control over at least one \Gls{provider} and the Frosix client.

Therefore, a \Gls{provider} uses the hash over a random \textit{commitment seed}, combined with the \textit{hash of the message} to derive the \textit{secret values}
and consequently the \textit{commitment}.
\custind Both \textit{commitment values} and the \textit{encryption key hash} are hashed together,
and the resulting value serves as identifier to store the \textit{commitment seed} in the database (Figure~\ref{fig:provider_sig_entropy}).

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{frosix_provider_sign_entropy}
    \caption{Derivations of a \textit{POST /sig-commitment} request}
    \label{fig:provider_sig_entropy}
\end{figure}

In the second round of Frosix sign, all \textit{commitments} are sent to each \Gls{provider},
together with the \textit{message hash} and the individual \textit{encryption key}.

A \Gls{provider} now does the following to restore the \textit{secret values}:
\begin{itemize}
  \item hashes the \textit{encryption key} and finds the key data in the database, including its index number
  \item takes the matching commitment from the commitment list and together with \textit{encryption key hash} computes the database identifier of the \textit{commitment seed}
  \item loads and immediately deletes the \textit{commitment seed} from the database (no reuse!)
  \item restores the secret values with the help of the \textit{message hash} and the \textit{commitment seed}
\end{itemize}

\newpage
\section{Long-term Stored Data}
The storage of long-term data introduces potential attack vectors.
One of the key objectives of Frosix was to reduce and prevent such attack vectors.

\subsubsection{Frosix Signing Document}
With access to the Frosix signing document, an adversary learns how many and which \Glspl{provider} are involved,
the corresponding encryption keys, which authentication methods are used and the associated authentication data,
such as phone number, email address, security question (not the answer).
The Frosix signing document should therefore be stored carefully - like a signing key!
\custind In contrast to a signing key from a single-signer signature scheme, this knowledge is not enough to issue arbitrary signatures,
because of the enforced authentication of each signing request.

\subsubsection{Frosix Service Provider Database}
If the database of a \Gls{provider} is leaked, an adversary gains little knowledge:
\begin{itemize}
  \item \textbf{Authentication Data}: The authentication data stored at a \Gls{provider} is a salted hash.
  Without knowledge of the corresponding nonce, it is nearly infeasible for an adversary to brute-force it,
  even if the set of all possible values is small (e.g. a phone number).
  \item \textbf{Key Data}: The key data, consisting of the private key and public key, is stored encrypted.
  Without knowledge of the encryption key used, it is infeasible for an adversary with limited computational power
  to break the encryption.
  \newline Even though the public key does not actually need to be protected, storing it in plaintext
  would allow an adversary to correlate different encrypted key data from the same or from different \Glspl{provider}.
  \item \textbf{Index}: With access to the \Glspl{provider} index in a signing group,
  an adversary can conclude which entries do not belong to each other (if two entries have the same index number).
\end{itemize}

\section{Trusted Public Key}
Since the Frosix adversary model assumes that any device could be compromised,
an adversary could substitute the authentication data during key generation.
Therefore, after a key generation is complete, each \Gls{provider} signs
the resulting public key and the received authentication data with its own long-term private key.
This signature can then be verified by the user (on a non-compromised device).
\custind In addition, it can be proven to third persons which \Glspl{provider} are part of a signing group
represented by the public key.
This can establish further trust in the signatures, if the involved \Glspl{provider} are well-known.

\section{Delete Key Data}
In the design of Frosix, the deletion of data stored at a \Gls{provider} can be accomplished by providing only the hash of the encryption key.
As a result, it may be beneficial to back up URLs of the selected \Glspl{provider}, together with hashes of the encryption key,
in case the Frosix signing document is lost.
\custind Since access to a particular device, cell phone number or email address is not given forever,
the deletion does not need an authentication, which a user then might not be able to fulfill anymore.

Despite the inconvenience of the potential unavailability to create signatures,
this is far less significant compared to the risk of losing control over the private key shares.
And generating a new key is not a big effort with Frosix (perhaps the distribution of the public key is).
