..
  This file is part of Frosix.
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Joel Urech

=======
Sources
=======

  [1] C. Komlo and I. Goldberg, "FROST: Flexible Round-Optimized Schnorr Threshold Signatures", https://eprint.iacr.org/2020/852.pdf 

  [2] C. Komlo, I. Goldberg and C.A. Wood, "Two-Round Threshold Schnorr Signatures with FROST", https://www.ietf.org/archive/id/draft-irtf-cfrg-frost-12.html

  [3] C. Grothoff, D. Meister and D. Neufeld "GNU Anastasis REST API", https://git.taler.net/anastasis.git/tree/doc/sphinx/rest.rst

  [4] libsodium, https://doc.libsodium.org/

  [5] B. Schneier "Leaked Signing Keys Are Being Used to Sign Malware", https://www.schneier.com/blog/archives/2022/12/leaked-signing-keys-are-being-used-to-sign-malware.html

  [6] C. Komlo and I. Goldberg, "frost: An implementation in Rust", https://git.uwaterloo.ca/ckomlo/frost/