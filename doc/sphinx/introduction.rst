..
  This file is part of Frosix.
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Joel Urech

============
Introduction
============

The documentation is not up-to-date and will be updated soon.


The Problem
^^^^^^^^^^^

Virtually our entire modern world is nowadays secured by digital signatures.

Simply stated, a digital signature is a cryptographic proof of the originator of some digital data.
If anyone manipulates signed data or tries to impersonate someone else, this will be detected thanks to signatures.

The trust model of signatures is only discussed here to the extent that a private key is required to generate a valid signature, and we assume that we are able to keep such a private key safe.
But this assumption is not correct, as we have to realize again and again!

In Dezember 2022, Bruce Schneier, a well known cryptographer, stated the following after another incident became public:
"This is a huge problem. The whole system of authentication rests on the assumption that signing keys are kept secret by the legitimate signers. Once that assumption is broken, all bets are off:" [5]


Possible Solution
^^^^^^^^^^^^^^^^^

A possible solution to this problem could lie in the cryptographic field of ``Distributed Key Generation and Threshold Signatures``, which the author has chosen as topic for his Project2 and Bachelor Thesis.

The basic idea behind this is simple: With a Threshold Signature Scheme, we can distribute the responsibility among many persons, services or systems.
And only if a fixed number (threshold) of those work together they can generate a valid signature.
This also means, that an attacker has to compromisse at least this threshold number of persons, services or systems to be able to generate a valid signature on his own.

If we now use a Distributed Key Generation process to generate our key pair, the private key will never be materialized at any time!
In a Distributed Key Generation process, all participants contribute equally to the resulting key pair and no one knows more than exactly his own private key share.

During the semester and while working on this topic, the working title Frosix (FROST Signatures) has evolved.


Frosix
^^^^^^

The goal of Frosix is to provide a safe and secure signature service, based on the FROST protocol by Chelsea Komlo and Ian Goldberg [1].

In the end, Frosix should include an implementation to run small web servers - the providers - which a software, running on a client, can access via a simple REST API.
The software on the client side will be a simple CLI as a first step.

The providers are designed to only store the data necessary for its service, either encrypted without knowing the key, or just in form of a salted hash.
The drawback of this complete zero knowledge driven strategy is that the information which is needed to let a provider participate in a signing process, has again to be stored securly by the user.
Obviously this means that the attacking vector of such a provider is really small. But what happens if the secret data is stolen?

For this reason there will be a further security mechanism in Frosix.
Just as with GNU Anastasis [3], Frosix providers are required to challenge, respectively authenticate their users.
This ensures that, depending on the challenge selected, only the legitimate user can have a valid signature generated.

Additionally there are plans to integrate the payment system GNU Taler, so that providers do not have to offer this service for free.
