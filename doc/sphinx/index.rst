..
  This file is part of Frosix.
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Joel Urech

Frosix Documentation
=======================

At the moment, Frosix is an implementation in a very early state of the FROST threshold signature scheme published at the end of 2020 by Chelsea Komlo and Ian Goldberg [1].


Documentation Overview
----------------------

.. toctree::
  :numbered:
  :maxdepth: 2

  introduction
  libfrost
  rest
  sources
  appendix

.. toctree::
  :hidden:

  fdl-1.3
