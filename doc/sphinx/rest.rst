..
  This file is part of Frosix
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Christian Grothoff
  @author Joel Urech

========
REST API
========

.. _config:

-----------------------
Receiving Configuration
-----------------------

.. http:get:: /config

  Obtain the configuration details of the provider.
  
**Response:**

Returns a `ProviderConfigurationResponse`_.


.. _ProviderConfigurationResponse:
.. ts:def:: ProviderConfigurationResponse

  interface ProviderConfigurationResponse {

    // Protocol identifier, clarifies that this is a Frosix provider.
    name: "frosix";

    // libtool-style representation of the Exchange protocol version, see
    // https://www.gnu.org/software/libtool/manual/html_node/Versioning.html#Versioning
    // The format is "current:revision:age".
    version: string;

    // What is the name of this provider?
    business_name: string;

    // Supported authorization methods.
    methods: AuthorizationMethodConfig[];

    // Payment required to create a key.
    // Not yet implemented!
    // key_creation_fee: Amount;

    // Salt value with 128 bits of entropy.
    // Different providers will use different high-entropy salt values.
    // The resulting **provider salt** is then used in various operations to ensure
    // cryptographic operations differ by provider. 
    // A provider must never change its salt value.
    provider_salt: string;

    // Public key of this provider.
    public_key: string;

  }

.. _AuthorizationMethodConfig:
.. ts:def:: AuthorizationMethodConfig

  interface AuthorizationMethodConfig {
    // Name of the authorization method.
    type: string;

    // Fee for issuing a signature share using this method.
    // Not yet implemented!
    // cost: Amount;

  }


.. _terms:

--------------------------
Receiving Terms of Service
--------------------------

.. http:get:: /terms

  Obtain the terms of service of the provider.
  // Not yet implemented!


.. _seed:

----
Seed
----

.. http:get:: /seed

  Returns an high-entropy seed. Binary data in the HTTP body.
  This API should be used every time the clients need high entropy.
  The entropy returned MUST be mixed with locally generated entropy.


.. _key:

--------------
Key Generation
--------------


Distributed Key Generation
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:post:: /dkg-commitment/$ID/

  Let the provider compute a commitment for a polynomial with degree threshold-1.
  ``$ID`` is the SHA-512 hash over the following data in Crockford base32 endoding: `ContextString`, `AuthHash`, `Identifier`, `NumOfParticipants`, `Threshold`, `PublicProviderSalt`, "FROSIX-DKG-ID".

  **Request:**

  Upload a `DkgCommitmentRequest`.

  **Response:**

  :http:statuscode:`201 Created`:
    The provider responds with a `DkgCommitmentResponse`.

  **Details:**

  .. _DkgCommitmentRequest:
  .. ts:def:: DkgCommitmentRequest

    interface DkgCommitmentRequest {

      // Unique identifier within the group.
      provider_index: Identifier;

      // Minimum number of participants needed to sign a message.
      threshold: Threshold;

      // ContextString is the main source of entropy to sample the commitments and should draw its entropy from several sources (e.g. with GET /seed from each participating provider).
      context_string: ContextString;

      // Salted hash of authentication data, i.e. phone number, e-mail address, ...
      // Or hash of the public key used to verify the secret answer of a security question.
      auth_hash: AuthHash;

      // Public keys of all participating providers.
      // Number of public key equals the total number of participants (n).
      providers_public_keys: ProviderPublicKey[];

    }

  .. _DkgCommitmentResponse:
  .. ts:def:: DkgCommitmentResponse
  
    interface DkgCommitmentResponse {

      // Unique identifier within the group
      provider_index: Identifier;

      // List of commitments, corresponding to the computed polynomial.
      dkg_commitment: DkgCommitment[threshold];

      // Zero Knowledge Proof of the corresponding secret of the computed polynomial.
      zkp: DkgZkp;

      // Public Key to derive an symmetric key to encrypt the secret share intended for this provider (ECDHE).
      provider_public_key: EphemeralPublicKey;
    }

  .. _DkgZkp:
  .. ts:def:: DkgZkp

    interface DkgZkp {
      r: R;
      z: z;
    }


.. http:post:: /dkg-shares/$ID

  Upload the DKG commitments of all other participants and receive encrypted shares for all other participants.
  ``$ID`` is the SHA-512 hash over the following data in Crockford base32 endoding: `ContextString`, `AuthHash`, `Identifier`, `NumOfParticipants`, `Threshold`, `PublicProviderSalt`, "FROSIX-DKG-ID".

  **Request:**

  Upload a `DkgSecretSharesRequest`_.

  **Response:**

  :http:statuscode:`201 Created`:
    The provider responds with a `DkgSecretSharesResponse`_.

  **Details:**

  .. _DkgSecretSharesRequest:
  .. ts:def:: DkgSecretSharesRequest:

    interface DkgSecretSharesRequest {

      // Identifier within the group.
      provider_index: Identifier;

      // Minimum number of participants needed to sign a message.
      threshold: Threshold;

      // Number of participants participating in the distributed key generation.
      num_of_participants: NumOfParticipants;

      // ContextString is the source of entropy to recover commitments.
      context_string: ContextString;

      // Salted hash of some challenge data, i.e. phone number, e-mail address, ...
      // Or hash of the public key used to verify the secret answer of a security question.
      auth_hash: AuthHash;

      // Public keys of all participating providers.
      // Number of public key equals total number of participants (n).
      providers_public_keys: ProviderPublicKey[];

      // The commitments of all participants
      dkg_commitments: DkgCommitmentResponse[num_of_participants - 1];

    }

  .. _DkgSecretSharesResponse:
  .. ts:def:: DkgSecretSharesResponse:

    interface DkgSecretSharesResponse {

      // The secret shares
      secret_shares: DkgSecretShare[num_of_participants - 1];

    }

  .. _DkgSecretShare:
  .. ts:def:: DkgSecretShare:

    interface DkgSecretShare {

      // Identifier of the target participant
      target: Identifier;

      // identifier of the issuer
      issuer: Identifier;

      // Ephemeral key
      ephemeral_key: EphemeralKey;

      enc_secret_share_data: [[

        // The secret share - symmetric encrypted
        secret_share: SecretShare; ]]

    }


.. http:post:: /dkg-key/$ID

  Upload all encrypted secret shares intended for this participant.
  ``$ID`` is the SHA-512 hash over the following data in Crockford base32 endoding: `ContextString`, `AuthHash`, `Identifier`, `NumOfParticipants`, `Threshold`, `PublicProviderSalt`, "FROSIX-DKG-ID".

  **Request:**

  Upload a `KeyGenerateRequest`_.

  **Response:**

  :http:statuscode:`204 No Content`:
    Key pair was successfully created and stored.
  :http:statuscode:`404 Not Found`:
	  The provider does not know the provided ``$ID``.
  :http:statuscode:`409 Conflict`:
    The service already has a key pair stored under the resulting ``$ID`` (hash of resulting public key).
  :http:statuscode:`402 Payment required`:
	  If the desired storage duration is longer than the default value in the terms of service of the provider, the provider must be paid additionally.

  **Details:**

  .. _KeyGenerateRequest:
  .. ts:def:: KeyGenerateRequest:

    interface KeyGenerateRequest {

      // Identifier within the group.
      provider_index: Identifier;

      // Minimum number of participants needed to sign a message. (needed to calculate secret polynomail)
      threshold: Threshold;

      // ContextString is the source of entropy to recover the secret value behind the providers commitment.
      context_string: ContextString;

      // Salted hash of some challenge data, i.e. phone number, e-mail address, ...
      // Or hash of the public key used to verify the secret answer of a security question.
      auth_hash: AuthHash;

      // Public keys of all participating providers.
      // Number of public key equals total number of participants (n).
      providers_public_keys: ProviderPublicKey[];

      // For how many years from now would the client like us to store the truth?
      expiration: Years;

      // All secret shares targeting this participant
      secret_shares: DkgSecretShare[num_of_participants - 1];

    }

  .. _KeyGenerateResponse:
  .. ts:def:: KeyGenerateResponse:

    interface KeyGenerateResponse {

      // Public Key of created Distributed Secret Key
      public_key: PublicKey

      // The signature is over the public key and the received authentication hash.
      // This signature allows to verify the correctness of the authentication hash and to proof to a third person that this provider is involved in this signing group.
      provider_signature: ProviderSignature

    }


.. _challenge:

-----------------
Challenge Request
-----------------

.. http:post:: /auth-request/$ID

  Let the provider create and send a challenge-code.
  ``$ID`` is the SHA-512 hash over the following data in Crockford base32 endoding: `EncryptionKey`, `MessageHash`, "FROSIX-AUTH".

  **Request:**

  The request must be a `ChallengeCodeRequest`_.

  **Response:**

  :http:statuscode:`200 OK`:
    The provider responds with an empty body.
  :http:statuscode:`402 Payment Required`:
    The provider requires payment to issue a challenge code.

  **Details:**

  .. _ChallengeCodeRequest:
  .. ts:def:: ChallengeCodeRequest:

    interface ChallengeCodeRequest {

      // Encryption key
      encryption_key: EncryptionKey;

      // Authentication method
      auth_method: AuthMethod;

      // Authentication data
      auth_data: AuthData;

      // Authentication nonce
      auth_nonce: AuthNonce;

      // Hash of the message.
      // The message is bound to the challenge code. A challenge code can therefore only be used for a specific message.
      message_hash: MessageHash;
    }


.. _sign:

-------
Signing
-------

.. http:post:: /sig-commitment/$ID
  
  A provider first authenticates an incoming request and then issues a commitment, which is bound to the submitted message.
  ``$ID`` is the SHA-512 hash over the following data in Crockford base32 endoding: `EncryptionKeyHash`, `MessageHash`, "FROSIX-SIG".

  **Request:**

  The request must be a `SignatureCommitmentRequest`_.
   
  **Response:**

  :http:statuscode:`200 OK`:
	  The provider responds with a `SignatureCommitmentResponse`_.
  :http:statuscode:`403 Forbidden`:
	  The client failed the authentication challenge.
  :http:statuscode:`404 Not Found`:
	  The provider does not know the provided ``$ID``.
  :http:statuscode:`404 Not found`:
	  The provider does not know the given ``$ID``.
  :http:statuscode:`410 Gone`:
	  The challenge has expired.
  :http:statuscode:`429 Too Many Requests`:
	  Too many attempts.

  **Details:**

  .. _SignatureCommitmentRequest:
  .. ts:def:: SignatureCommitmentRequest:

    interface SigantureCommitmentRequest {

      // Hash of the encryption key, used to identify the corresponding entry in the database.
      encryption_key_hash: EncryptionKeyHash;

      // Authentication method in order to authorize the incoming request.
      auth_method: AuthMethod;

      // (Optional) If its a challenge code based authentication, authentication data contains the hash of the challenge code.
      auth_data: AuthData;

      // (Optional) If its a security question, this authentication signature over the `MessageHash` is created with a private key which is derived from the secret answer.
      auth_sig: AuthSig;

      // (Optional) Public key to verify the `AuthSig` signature. Gets hashed and compared to the hash stored in the provider's database.
      auth_pub: AuthPub;

      // Hash of the message, serves as input for the key derivation function to sample a commitment.
      message_hash: MessageHash;

    }

  .. _SignatureCommitmentResponse:
  .. ts:def:: SignatureCommitmentResponse:

    interface SignatureCommitmentResponse {

      // First part of the commitment.
      // Commitments are used to ensure, that every provider behaves well.
      hiding_commitment: SigCommitment;

      // Second part of the commitment.
      binding_commitment: SigCommitment;

    }


.. http:post:: /sig-share/$ID

  Ask the provider to compute a signature share.
  ``$ID`` is the SHA-512 hash over the following data in Crockford base32 endoding: `EncryptionKeyHash`, `MessageHash`, "FROSIX-SIG".

  **Request:**

  Upload a `SignatureShareRequest`.

  **Response:**

  :http:statuscode:`200 OK`:
	  The provider responds with a `SignatureShareResponse`.
  :http:statuscode:`404 Not found`:
	  The provider does not know the given ``$ID``.

  **Details:**

  .. _SignatureShareRequest:
  .. ts:def:: SignatureShareRequest
   
    interface SignatureShareRequest {

      // Symmetric key to decrypt the key material
      encryption_key: EncryptionKey;

      // Hash of the message
      message_hash: MessageHash;   

      // Sorted list of commitments of all participating providers.
      // The own commitment has to be included.
      commitments: SignatureCommitmentResponse[];

    }

  .. _SignatureCommmitment:
  .. ts:def:: SignatureCommitment

    interface SignatureCommitment {

      // Identifier
      identifier: Identifier;

      // First part of the commitment.
      // Commitments are used to ensure, that every provider behaves well.
      hiding_commitment: SigCommitment;

      // Second part of the commitment.
      binding_commitment: SigCommitment;

    }

  .. _SignatureShareResponse:
  .. ts:def:: SignatureShareResponse

    interface SignatureShareResponse {
      
      // The resulting signature share
      signature_share: SigShare;
      
      // The partial public key for convenience only - could be ommited
      public_key_share: PublicKeyShare;

    }


.. _delete-key:

----------
Delete Key
----------

.. http:delete:: /dkg-key/$ID
  
  Deletes the key data if an entry is found with the ``$ID``.
  ``$ID`` is the `EncryptionKeyHash`.
   
  **Response:**

  :http:statuscode:`200 OK`:
	  The provider responds with a `SignatureCommitmentResponse`_.


.. _encodings-ref:

----------------
Common encodings
----------------

This section describes how certain types of values are represented throughout the API.

.. _base32:

Binary Data
^^^^^^^^^^^

.. ts:def:: foobase

  type Base32 = string;

Binary data is generally encoded using Crockford's variant of Base32
(http://www.crockford.com/wrmg/base32.html), except that "U" is not excluded
but also decodes to "V" to make OCR easy.  We will still simply use the JSON
type "base32" and the term "Crockford Base32" in the text to refer to the
resulting encoding.


Hash codes
^^^^^^^^^^
Hash codes are strings representing base32 encoding of the respective
hashed data. See `base32`_.

.. ts:def:: HashCode

  // 64-byte hash code.
  type HashCode = string;

.. ts:def:: SHA512HashCode

   type SHA512HashCode = HashCode;

.. ts:def:: PublicProviderSalt

   type PublicProviderSalt: HashCode;

.. ts:def:: ContextString

   // To generate a "good" ContextString,
   // the client has to get fresh entropy from at least
   // all participating providers (GET seed) and from itself.
   type ContextString = SHA512HashCode;

.. ts:def:: MessageHash

   // Hash of the message, which shall be signed. 
   type MessageHash = SHA512HashCode;

.. ts:def:: EncryptionKey

   // The encryption key is used to encrypt and decrypt the key data stored in the provider's database.
   type EncryptionKey = SHA512HashCode;

.. ts:def:: EncryptionKeyHash

  // Hash of the encryption key. Used to identify the corresponding entry in the provider's database.
  type EncryptionKeyHash = SHA512HashCode;

.. ts:def:: AuthHash

  // The authentication hash is a salted hash of the authentication data which gets stored in the provider's database.
  type AuthHash = SHA512HashCode;

.. ts:def:: AuthNonce

  // Nonce used as salt to hash `AuthData` to receive `AuthHash`.
  tpye AuthNonce = SHA512HashCode;


Curve25519
^^^^^^^^^^
32-byte values representig base32 encoding of either a point on Curve25519 or a scalar for operations on points on Curve 25519.

.. ts:def:: Cs25519Point

   type Cs25519Point = string;

.. ts:def:: Cs25519Scalar

   type Cs25519Scalar = string;

.. ts:def:: SigCommitment

   type SigCommitment = Cs25519Point;

.. ts:def:: DkgCommitment

   type DkgCommitment = Cs25519Point;

.. ts:def:: SecretShare

   type SecretShare = Cs25519Scalar;

.. ts:def:: EddsaPublicKey

   // EdDSA and ECDHE public keys always point on Curve25519
   // and represented  using the standard 256 bits Ed25519 compact format,
   // converted to Crockford `Base32`.
   type EddsaPublicKey = string;

.. ts:def:: EddsaPrivateKey

   // EdDSA and ECDHE public keys always point on Curve25519
   // and represented  using the standard 256 bits Ed25519 compact format,
   // converted to Crockford `Base32`.
   type EddsaPrivateKey = string;


Signatures
^^^^^^^^^^

.. ts:def:: EddsaSignature

  // EdDSA signatures are transmitted as 64-bytes `base32`
  // binary-encoded objects with just the R and S values (base32_ binary-only).
  type EddsaSignature = string;

.. ts:def:: R

   // The signature challenge.
   type R = Cs25519Point;

.. ts:def:: z

   // A single share of the resulting signature.
   type z = Cs25519Scalar;

.. ts:def:: ProviderSignature

  // Signature of a provider over the public key and the authentication data.
  type ProviderSignature = EddsaSignature;

.. ts:def:: AuthSig

  // Signature over the message with a private key derived from the secret answer.
  type AuthSig = EddsaSignature;


Keys
^^^^

.. ts:def:: SecretKeyShare

   type SecretKeyShare = Cs25519Scalar;

.. ts:def:: PublicKeyShare

   type PublicKeyShare = Cs25519Point;

.. ts:def:: PublicKey

   type PublicKey = Cs25519Point;

.. ts:def:: SigShare

   type SigShare = Cs25519Scalar;

.. ts:def:: ProviderPublicKey

   type ProviderPublicKey = EddsaPublicKey;

.. ts:def:: EphemeralPublicKey

   type EphemeralPublicKey = EddsaPublicKey;

.. ts:def:: EphemeralKey
   
   type EphemeralKey = EddsaPublicKey;

.. ts:def:: AuthPub
  
   type AuthPub = EddsaPublicKey;

Integers
^^^^^^^^

.. ts:def:: UInt8

   // JavaScript numbers restricted to the range of 0-255
   type UInt8 = number;

.. ts:def:: Identifier

   // Identifier in the group
   type Identifier = UInt8;

.. ts:def:: Threshold

   type Threshold = UInt8;

.. ts:def:: NumOfParticipants
   
   type NumOfParticipants = UInt8;

.. ts:def:: Years

   type Years = number;


Strings
^^^^^^^

.. ts:def:: AuthMethod

   type AuthMethod = string;

.. ts:def:: AuthData

   type AuthData = string;
