..
  This file is part of Frosix.
  Copyright (C) 2022, 2023 Joel Urech

  Frosix is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  Frosix is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Frosix; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>

  @author Joel Urech

============================
How to use Frosix's Libfrost
============================

The library libfrost is an implementation of the FROST protocol and does not reflect the latest design decisions of Frosix at the moment.
Additionally, the distributed key generation is generating correct key material, but needs a revision and a stricter typification.


Key Generation
==============

Libfrost provides two possibilities to generate key pairs.


Trusted Dealer
^^^^^^^^^^^^^^

Trusted dealer should only be used for testing purposes!
With trusted dealer everything can be generated by itself and the need for several participants to generate keys is eliminated.
But also the main security property of the whole system is eliminated and can not be guaranteed.
The reason for this is the fact, that with trusted dealer the secret key exists for a short time. But that could be enough for a strong attacker to learn and therefore misuse the secret key.

To let Libfrost create key pairs with trusted dealer, just call the function below and provide the number of key shares (num_of_participants) and the threshold value.

``FROST_trusted_dealer_keygen (key_pairs[], num_of_participants, threshold)``


Distributed Key Generation
^^^^^^^^^^^^^^^^^^^^^^^^^^

Round 1
-------

First every participant initializes an object of the struct FROST_DkgCommitment.
This will allocate some memory on the heap. The threshold value must be already known.

  ``FROST_initialize_dkg_commitment (dkg_commitment, my_index, threshold)``

Every participant does now makes a kind of a trusted dealer on his own and commits to each of the t-1 values of the created polynomial.
Additionally, every participant has to compute a zero knowledge proof of the underlying secret (y-value of the point x=0 on the polynomial).

This function below does all of this and returns a commitment and a share for each participant.
The commitment should now be send to all other participants. But do not send the dkg shares!

  ``FROST_keygen_begin (*dkg_commitment, dkg_shares[], my_index, num_of_participants, threshold)``


Round 2
-------

Before receiving the commitments, one should initialize further objects of type FROST_DkgCommitment.

After receiving all commitments from all other participants, they have to be validated.
This means, that every value will be checked if it is a point on the curve25519 and correctly encoded.
Additionally the included zero knowledge proof will be validated as well.

If any of the participants commitment or zero knowledge proof fails the validation, exclude this participant.

  ``FROST_keygen_receive_commitments_and_validate_peers (dkg_commitments[], num_of_participants)``


Round 3
-------

Finally send to each participant his share and also receive from each participant a share.
This shares will be verified against the previously received commitments.
If this check failes, the distributed key generation process must be restarted without the failing participant.
But if all received shares are correct, this function computes the individual key pair and also derives the group public key from all commitments.

  ``FROST_keygen_finalize (*key_pair, my_index, shares[], commitments[], num_of_participants)``

In the end, every initialized struct should be properly freed.

  ``FROST_free_dkg_commitment (*dkg_commitment)``

Every participant now has a key share and can participate in a signing process.


Sign a message
==============
  
Round 1
^^^^^^^

The Signature Aggregator 'SA' chooses 't' (t = threshold) random participants out of all available participants and asks them to send him a commitment.  
  
Each chosen participant generates a nonce and a commitment and sends the commitment back to the 'SA'.
The nonce should be stored securely, we will use it in round 2.
 
  ``FROST_generate_nonce_and_commitment (nonce, commitment, my_key_pair)``
  
  
Round 2
^^^^^^^

To issue a signature over an arbitrary long message (msg with length msg_len), the message must first be converted to a point on the curve by the 'SA'.
  
  ``FROST_message_to_hash (message_hash, msg, msg_len)``
  
The 'SA' constructs then a sorted list out of all received commitments and send this list of commitments together with the message hash to each chosen participant.  
Each chosen participant validates the data from the 'SA' and aborts if the validation failes.
  
  ``FROST_validate_message_hash_and_commitments (message_hash, commitments[], commitments_len))``
  
  
If all data from the 'SA' are valid, every chosen participant computes a signature share and sends this signature share back to the 'SA'.
  
  ``FROST_sign_message_hash (signature_share, mesage_hash, commitments[], commitments_len, my_key_pair, my_nonce)``
  
The 'SA' receives and verifies each partial signature and aborts if the verification fails.
  
  ``FROST_verify_signature_share (commitment_i, signature_share_i, commitments[], commitments_len, public_key, message_hash)``
  
If all partial signatures are succesfully verified, the 'SA' finally creates the signature.
  
  ``FROST_compose_signature (signature, commitments[], signature_shares[], commitments_and_sig_shares_len, message_hash)``
  

Verify
======

The verification of a given signature over a specific message is simple. Just call the function below with the public key, the signature and the hash of the message.
If the return value is 1, the signature is correct.

  ``FROST_verify_signature (public_key, signature, message_hash)``
